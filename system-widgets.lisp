(in-package :cl-sfx)


(defclass script-repl (widget)
  (
   (repl-result :initarg :repl-result
		 :accessor repl-result
		 :initform nil
		 )))

(defmethod render ((widget script-repl) &key)
  
  (with-html
      (:form :method "post"
	     :action ""
	     (:input :type :hidden :id "pageid"
		     :name "pageid"
		     :value (or (context-id *sfx-context*)
				(parameter "pageid")))
	     (:div
	      (:label "Script REPL - Test your scripts here.."))
	     (:div
	      (:textarea :id (name widget) :name (name widget) 
			 :cols 65 :rows 10
			 (or (str (parameter (name widget))) "")))
	     (:input :name "repl-eval"
		     
		     :type "submit" :value (tlp "EVAL"))
	     (:div
	      (:br)
	      (:span :style "color:blue;"
		     :id (frmt "~A-result" (name widget)) 
		     :name (frmt "~A-result" (name widget)) 
		     :cols 75 :rows 30  (str (frmt "~A" (repl-result widget))))))
      
    
      (defer-js "var myCodeMirror_~a = CodeMirror.fromTextArea(~a);myCodeMirror_~a.getDoc().setValue(\"~a\");
"
	  (substitute #\_ #\- (name widget))
	(name widget)
	(substitute #\_ #\- (name widget))
	(frmt "~A" 
	      (replace-all
		 (replace-all
		  (replace-all (or (parameter (name widget)) "")
			       "\""
			       "\\\"" 
			       )
		  (frmt "~A" #\Tab)
		  "\\\t")
		 (frmt "~A~A" #\return  #\linefeed)
		 "\\\n")))))

(defmethod action-handler ((widget script-repl))
  (multiple-value-bind (result error)
	(ignore-errors (sfx-eval (parameter (name widget))))    
      (setf (repl-result widget)
	    (or error result ))))


;;TODO: Check why deffer-include-js is not working???
(defclass login (container)
  ((email :initarg :email
          :initform nil
          :accessor email)
   (password :initarg :password
             :initform nil
             :accessor password)
   (message :initarg :message
            :initform nil
            :accessor message)
   (image :initarg :image
            :initform nil
            :accessor image))
  (:metaclass widget-class)
  (:defer-include-js "$('#email').focus()")
  (:base-theme (:login.outer-div.class nil)
	       (:login.inner-div.class "eish")
	       (:login.header nil)
	       (:login.form-div.class nil)
	       (:login.login-form.class)
	       (:login.user-group-div.class nil)
	       (:login.user-div.class nil)
	       (:login.user-label.class nil)
	       
	       (:login.user-div.class)
	       (:login.user-label.class nil)
	       (:login.user.class nil)
	       (:login.password-form-group.class nil)
	       (:login.password-div.class)
	       (:login.password-label.class nil)
	       (:login.password.class nil)
	       (:login.login-submit.class nil)))

(defmethod initialize-widget ((container login))
  (let ((outer-div (make-widget 'div :name "outer-div" :class "test"))
	(image (or (image container) (make-instance 'img :name "image")))
	(inner-div (make-widget 'div :name "inner-div"))
	(form-div (make-widget 'div :name "form-div"))
	(header (make-widget 'generic-container-html-element 
			     :html-element :h4 
			     :name "header"
			     :content (tlp "Login to get started")))
	(form (make-widget 'form :name "login-form"))
	(user-group-div (make-widget 'div :name "user-group-div"))
	(user-label (make-widget 'generic-container-html-element 
				 :html-element :label
				 :name "user-label" 
			         ;;	   :for "email"
				 :content (tlp "Email")))
	(user-div (make-widget 'div :name "user-div"))
	(email (make-widget 'input 
			    :type "text"
			    :name "email"			      
			    :place-holder (tlp "Email")
			    :value (email container)))	 
	(password-group-div (make-widget 'div :name "password-group-div"))
	(password-label (make-widget 'generic-container-html-element 
				     :html-element :label
				     :name "password-label" 
			             ;;	   :for "password"
				     :content (tlp "Password")))
	(password-div (make-widget 'div :name "password-div"))
	(password (make-widget 'input :name "password"
				      :type "password"
				      :place-holder (tlp "Password")
				      :value (password container)))
	(submit (make-widget 'input
			     :type "submit" 
			     :name "submit-login" 
			     :value (tlp "Login")))
	(error-div (make-widget 'div :name "error-div")))
    
    (setf (content container) outer-div)
    (setf (content outer-div) (list image inner-div))
    (setf (content inner-div) form-div)
    (setf (content form-div) (list header form))
    (setf (content form) (list user-group-div password-group-div submit error-div))
    (setf (content user-group-div) (list user-label user-div))
    (setf (content user-div) email)
    (setf (content password-group-div) (list password-label password-div))
    (setf (content password-div) password)
    (setf (content error-div) (message container))))

(defgeneric validate-user (login))

(defmethod validate-user ((widget login))
  (let ((user (get-user (email widget))))
    (unless (and user (check-password user (password widget)))      
      (setf user nil)
      (setf (message widget) "Email or password incorrect"))
    user))



(defgeneric init-user-session (user))

(defmethod init-user-session ((user user))
  
  (dolist (doc (coerce (get-collection-docs 'sfx-script) 'list))
    (sfx-eval (script doc))))

(defgeneric on-success (login &key))
(defgeneric on-failure (login &key))

;;TODO: Remove session?
;;TODO: Put log back
(defmethod on-success ((login login) &key user)
  
  (when (cl-sfx::current-user)
;;    (break "Shit user")
    ;;(remhash  (sfx-session-id *sfx-session*) (sessions *sfx-system*))
    ;;(hunchentoot:remove-session *session*)
    )
  
  (let ((active-user (find-docx 'sfx-active-user
				:test (lambda (doc)
					(equal user (user doc))))))
    (unless active-user
      (setf active-user (persist (make-instance 'sfx-active-user
						:user user))))

    (setf (user *sfx-session*) active-user))
  
  (init-user-session user)

;;  (log-login "Login" (email login) "Passed" "Login passed.")

  (redirect (or (session-value 'redirect-after-login)
                (default-context *sfx-system*))))

(defmethod on-failure ((login login) &key)
 ;; (log-login "Login" (get-val login 'email) "Failed" "User name or password incorrect.")
  
  )

(defmethod action-handler ((widget login))
  (when (and (email widget) (password widget))
    (let ((user (validate-user widget)))
        (if user
          (on-success widget :user user)
          (on-failure widget)))))

#|
(defmethod render ((widget login) &key image-alt)
  (with-html
    (:div
     :class (attv :outer-div-class)
     (:img :src "/images/insite-logo.png" :class (attv :image-class)
           :alt (tlp image-alt))
     (:div :class (attv :inner-div-class)
           (:div :class (attv :form-div-class)
                 (:h4 :class (attv :header-class) 
		      :style "margin-bottom: 25px;" (tlp "Login to get started"))
                 (:form :method "post"
                        :action ""
                        :class (attv :form-class)
                        (:input :type "hidden" :name "pageid" 
				:value (context-id *sfx-context*))
                        (:div :class (attv :user-form-group) 
                              (:label :for "email" :class (attv :label-class)
                                      :style "text-align: left;" (str (tlp "Email")))
                              (:div :class (attv :user-div-class)
                                    (:input :type "text" 
                                            :class (attv :user-class)
                                            :name (widgy-name widget "email")
                                            :id "email"
                                            :placeholder (tlp "Email")
                                            :value (email widget))))
                        (:div :class (attv :password-form-group)
                              (:label :for "password" :class (attv :label-class)
                                      :style "text-align: left;" (str (tlp "Password")))
                              (:div :class (attv :password-div-class)
                                    (:input :type "password" 
                                            :class (attv :password-class)
                                            :name (widgy-name widget "password")
                                            :id "password"
                                            :placeholder (tlp "Password"))))
                        (:input :name (widgy-name widget "submit-login")
                                :class (attv :login-button-class)
                                :type "submit" :value (tlp "Login")))
                 (:br)
                 (:div :id "thawteseal"
                       :style "text-align:center;" 
                       :title "Click to Verify - This site chose Thawte SSL for secure e-commerce and confidential communications."
                       (:script :type "text/javascript" 
                                :src "https://seal.thawte.com/getthawteseal?host_name=app.gsi-insite.com&amp;size=S&amp;lang=en"))
                 (:div :style "text-align:center;"
                       (:a :href "http://www.thawte.com/ssl-certificates/" 
                           :target "_blank" 
                           :style "color:#000000; text-decoration:none; font:bold 10px arial,sans-serif; margin:0px; padding:0px;"
                           "ABOUT SSL CERTIFICATES"))
                 
                 
                 (when (message widget)
                   (htm
                    (:div :class "error" (esc (tlp (message widget))))))
                 (defer-js "$('#email').focus()"))))))

|#



(defclass user-entity-editor (widget)
  ((value :initform nil
          :accessor value)
   (user :initarg :user
	 :initform nil
         :accessor user)
   (cb-lists :initform nil
             :accessor cb-lists)))


(defun entity-children (rel)
  (labels ((recurse (rels)
	     (map 'list
		  (lambda (rel)
		    (let* ((entity (parent rel))
			   (id (xdb2:id entity)))
		      (list* id
			     (entity-name entity)
			     t
			     (recurse (cl-sfx::children rel)))))
		  rels)))
    (when (and rel (xdb2:id rel))

      (recurse (list rel)))))

(defun entity-tree (user)
  (let ((user-entities (and user
                            (accessible-entities user))))
    (labels ((accessible-p (id)
               (loop for (root entities) on user-entities by #'cddr
                     thereis (find id entities)))
             (recurse (rels)
               (map 'list
                    (lambda (rel)
                      (let* ((entity (entity rel))
                             (id (id entity)))
                        (list* id
                               (entity-name entity)
                               (accessible-p id)
                               (recurse (cl-sfx::children rel)))))
                    rels)))
      (recurse (docs (xdb2:get-collection (system-db) "entities"))))))

(defmethod render ((widget user-entity-editor) &key  user)

  (let* ((user (if user
                   (setf (user widget) user)
                   (user widget)))
         (entity-tree (if (typep user 'cl-sfx::entity)
			  (entity-children user)
			  (entity-tree user))))
    (with-html
      (setf (cb-lists widget)
            (loop for root in entity-tree
                  for (id name) = root
                  for cb-list = (make-widget 'checkbox-list :name
                                             (frmt "entity-tree-~a" id))
                  do
                  (setf (items cb-list) (list root))
                  (htm (:h4 (esc name)))
                  (render cb-list)
                  collect (cons id cb-list))))))

(defmethod action-handler ((widget user-entity-editor))
  (setf (value widget)
        (loop for (id . cb-list) in (cb-lists widget)
              for value = (alexandria:flatten (value cb-list))
              when value
              collect id
              and
              collect value)))


