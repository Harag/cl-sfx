
(defpackage :test-hunch-xdb
  (:use :c2cl :cl-who :cl-sfx)
  (:export))

(in-package :test-hunch-xdb)
(defvar *hunch-sys* nil)


;; Loading tests should not run them.
;; TODO: use a makefile to load and run the tests.

(defun test-hunch-xdb ()
  (let  ((*hunch-sys* (make-instance 'sfx-system 
				     :system-name "Test Hunch"
				     :port 5555
				     :system-folder "/test-hunch-xdb/"
				     :site-url "/hunch-test/"
				     :ajax-url "/hunch-test/ajax/"
				     :login-url "/hunch-test/login" 
				     :debug-errors-p t
				     :message-log-destination *standard-output*
				     :default-context "/hunch-test/users")))


    #|

    (cl-sfx::define-context-def *hunch-sys*  
    :name 'test-page 
    :url "login3" 
    :for-everyone t
    :permissions nil
    :body
    `(let ((login (make-widget 'cl-sfx::login :name "login")))
    (with-html-string
    (:div (render login)))))

    |#


    (start-sys *hunch-sys*)

    (let ((*sfx-system* *hunch-sys*))
      (unless (get-user "admin@cl-sfx.com")
        (xdb2:persist (make-user "admin@cl-sfx.com" "admin"
                                 :super-user-p t))))

    (values)))




