(in-package :cl-sfx)

(defclass sfx-script (license-doc)
  ((name :initarg :name
	 :accessor name
	 :initform nil
	 :db-type string
	 :key t)
   (script :initarg :script
           :accessor script
           :initform nil
	   :db-type script))
  (:metaclass data-versioned-class)
  (:after-persist #'(lambda (doc)			      
		      (sfx-eval 
		       (script doc))))
  (:collection-name "sfx-scripts")
  (:collection-type :merge)
  (:default-initargs :top-level t))


(defun parse-script (string)
  (with-input-from-string (stream string)
    (labels ((read-script ()
               (peek-char t stream) ;; skip whitespaces
               (let ((char (read-char stream)))
                 (case char
                   (#\" (read-string))
                   (#\( (read-list))
                   (#\) (error "Unmatched close parenthesis."))
                   (t (read-token char)))))
             (read-list ()
               (loop for next-char = (peek-char t stream)
                     when (char= next-char #\))
                     do (read-char stream)
                        (loop-finish)
                     collect (read-script)))
             (read-string ()
               (with-output-to-string (new-string)
                 (loop for char = (read-char stream)
                       do
                       (case char
                         (#\\ (write-char (read-char stream) new-string))
                         (#\" (return))
                         (t (write-char char new-string))))))
             (read-token (first-char)
               (let ((string
                       (with-output-to-string (token)
                         (loop for char = first-char then (read-char stream nil nil)
                               while char
                               do
                               (case char
                                 (#\\
                                  (write-char (read-char stream) token))
                                 ((#\" #\Space #\Newline #\Tab #\( #\))
                                  (unread-char char stream)
                                  (return))
                                 (t
                                  (write-char char token)))))))
                 (if (every #'digit-char-p string)
                     (parse-integer string)
                     (make-symbol string)))))
      (read-script))))

(defparameter *allowed-format-directives*
  "acsdxbp(%~")

(defun check-format (control-string)
  (flet ((end-prematurely ()
           (error "error in FORMAT: ~
                   string ended before directive was found: ~a"
                  control-string)))
    (with-input-from-string (stream control-string)
      (loop for char = (read-char stream nil nil)
            while char
            when (char= char #\~)
            do (loop for char = (read-char stream nil nil)
                     do
                     (cond ((null char)
                            (end-prematurely))
                           ((or (digit-char-p char)
                                (find char ":@,v#")))
                           ((char= char #\')
                            (or (read-char stream nil nil)
                                (end-prematurely)))
                           (t
                            (if (find (char-downcase char)
                                      *allowed-format-directives*)
                                (return)
                                (error "Format directive ~~~c is not allowed"
                                       char)))))))
    t))

(defun script-format (control &rest args)
  (and (check-format control)
       (apply #'format nil control args)))

(defun script-search (sequence1 sequence2)
  (search sequence1 sequence2 :test #'equalp))

(defun script-find (item sequence)
  (find item sequence :test #'equalp))

(defparameter *script-operators*
  `(("progn" progn)
    ("if" if 2 3)
    ("not" not 1 1)
    ("and" and)
    ("or" or)
    ("equal" equal 2 2)
    ("equalp" equalp 2 2)
    ("=" = 1)
    ("/=" /= 1)
    ("+" +)
    ("-" -)
    ("*" *)
    ("/" / 1)
    (">" > 1)
    ("<" < 1)
    (">=" >= 1)
    ("<=" <= 1)
    ("floor" floor 1 2)
    ("ceiling" ceiling 1 2)
    ("mod" mod 2 2)
    ("rem" rem 2 2)
    ("subseq" subseq 2 3)
    ("nth" nth 2 2)
    ("replace-all" replace-all 3 4)
    ("split-string" split-string 2 2)
    ("format" script-format 1)
    ("parse-number" parse-number 1 1)
    ("parse-date" parse-date 1 1)
    ("search" script-search 2 2)
    ("list" list)
    ("find" script-find 2 2)
    ("date=" date= 2 2))
  "(name cl-operator min-args* max-args*)")

(defvar *defined-functions* nil)
(defvar *defun-enabled* nil)

(defun variable-value (variables name)
  (cdr (assoc name variables :test #'equalp)))

(defun compile-script (string variables &key slots slot-package)
  (let ((script (parse-script string)))
    (labels ((check-variable (token)
               (cond ((or (null token)
                          (not (symbolp token)))
                      token)
                     ((string-equal token nil) nil)
                     ((string-equal token t) t)
                     ((find token variables :test #'string-equal)
                      `(variable-value variables ,(string-downcase token)))
                     (t
                      (error "Undefined variable ~a" token))))
             (compile-it (s-exp)
               (cond ((atom s-exp)
                      (check-variable s-exp))
                     ((not (symbolp (car s-exp)))
                      (error "Illegel function call ~a"
                             s-exp))
                     ((and *defun-enabled*
                           (string-equal (car s-exp) "defun"))
                      (destructuring-bind (name &rest definition) (cdr s-exp)
                        (setf (alexandria:assoc-value *defined-functions* name :test #'string-equal)
                              `(lambda ,@definition))
                        nil))
                     ((let* ((function (assoc (car s-exp)
                                              *script-operators*
                                              :test #'string-equal))
                             (args (cdr s-exp))
                             (arg-length (length args)))
                        (and function
                             (destructuring-bind (function &optional min max)
                                 (cdr function)
                               (cond ((and min
                                           (< arg-length min))
                                      (error "Invalid number of arguments for ~a, ~
                                         at least ~a is required:~% ~a"
                                             function min s-exp))
                                     ((and max
                                           (> arg-length max))
                                      (error "Invalid number of arguments for ~a, ~
                                         at most ~a is allowed:~% ~a"
                                             function max s-exp)))
                               (cons function
                                     (loop for arg in args
                                           collect (compile-it arg)))))))
                     ((let ((function (cdr (assoc (car s-exp) *defined-functions*
                                                  :test #'string-equal)))
                            (args (cdr s-exp)))
                        (and function
                             (list* 'funcall function
                                    (loop for arg in args
                                          collect (compile-it arg))))))
                     ((let ((slot (or (find (car s-exp) slots :test #'string-equal)
                                      (and slot-package
                                           (find-symbol (string-upcase (car s-exp))
                                                        slot-package)))))
                        (and slot
                             (= (length s-exp) 2)
                             `(let ((object ,(compile-it (cadr s-exp))))
                                (and object
                                     (slot-value object ',slot))))))
                     (t
                      (error "Undefined function: ~a" (car s-exp))))))
      (compile-it script))))
