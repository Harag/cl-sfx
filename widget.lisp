(in-package :cl-sfx)



(defun js-inclusion-string (path)
  (format nil "<script type='text/javascript' src='~a'></script>" path))

(defun js-defer-inclusion-string (path)
  (format nil "<script defer type='text/javascript' src='~a'></script>" path))

(defun css-inclusion-string (path)
  (format nil "<link  rel='stylesheet' href='~a' />" path)) ;; type='text/css'

(defun print-included (accessor &key key)

  (let ((printed (make-hash-table :test #'equal)))
    (map-cache 
     (lambda (value)
       (when (subtypep (class-of (class-of value)) 'widget-class)
         (loop for css in (funcall accessor (class-of value))
               unless (gethash css printed)
               do
               (princ (if key
                          (funcall key css)
                          css))
	      (setf (gethash css printed) t))))
     *sfx-context*
  
     ))

  )

(defun page-include-css ()
  (print-included #'include-css :key #'css-inclusion-string))

(defun page-include-js ()
  (print-included #'include-js :key #'js-inclusion-string))

(defun page-defer-include-js ()
  (print-included #'defer-include-js :key #'js-defer-inclusion-string))

(defun widget-include-bits (widget-class-instance)
  "Returns the include statements for the widget's include files."
  (when widget-class-instance
    (map nil #'princ (include-bits widget-class-instance))))

(defun page-include-bits ()
  "Takes stuff that needs to be written to the html that is independant of the rendering of the actual widget. The method page-include-bits goes through all the widgets for a page on in the dom and adds any include-bits found and this method needs to be explicitly called in html page."
  (print-included #'include-bits))



;;Used for custom slot attributes
(defclass widget-mixin (standard-class)
  ())

(defmethod validate-superclass ((class widget-mixin)
    (superclass standard-class))
   t)

(defmethod validate-superclass ((superclass standard-class)
                                (class widget-mixin))
  t)

(defclass widget-slot-class (widget-mixin)
  ())

(defmethod validate-superclass ((class widget-slot-class)
                                (superclass standard-class))
  t)

(defmethod validate-superclass ((superclass standard-class)
                                (class widget-slot-class))
  t)


(defclass widget-slot ()
  ((html-attribute-p :initarg :html-attribute-p
		    :accessor html-attribute-p
		    :initform nil)))

(defclass widget-direct-slot-definition (widget-slot
					standard-direct-slot-definition)
  ())

(defclass widget-effective-slot-definition (widget-slot 
					   standard-effective-slot-definition)
  ())

(defmethod direct-slot-definition-class ((class widget-mixin) &key)
  (find-class 'widget-direct-slot-definition))

(defmethod effective-slot-definition-class ((class widget-mixin) &key)
  (find-class 'widget-effective-slot-definition))



;;TODO: Why then consp check?
(defun compute-slot-optionx (effective-definition
                            slot direct-definitions)
  (let ((value
          (loop for dd in direct-definitions
                for value = (slot-value dd slot)
             ;;   unless
	      ;; (and (consp value)
		;;    (eq (car value) *slot-dummy*))
	     return value
	     finally
	       (return (cadr (slot-value (car direct-definitions)
					 slot))))))
    (setf (slot-value effective-definition slot)
          value)))

(defmethod compute-effective-slot-definition
    ((class widget-mixin) slot-name direct-definitions)
  (declare (ignore slot-name))
  (let ((effective-definition (call-next-method))
        (direct-definition (car direct-definitions)))
    (when (typep direct-definition 'widget-slot)
      (compute-slot-optionx effective-definition
                            '(html-attribute-p)
                            direct-definitions))
    effective-definition))




(defclass widget-class (widget-mixin)
  ((events :initarg :events
	   :accessor events
	   :initform nil
	   :documentation "(group (event event event)) ...")
   (base-theme :initarg :base-theme
	       :accessor base-theme
	       :initform nil
	       :documentation "List of theme attributes of (attribute value)... or hashtable. These will be superceded by any other theme settings.")
   ;;TODO: Should this not rather be set on system level?
   (cached-p :initarg :cached-p
	     :accessor cached-p
	     :initform nil
	     :documentation "Set to t if the widget needs to maintain state accross actions.")
   (include-bits
    :initarg :include-bits
    :initform nil
    :accessor include-bits
    :documentation "Stuff that needs to be written to the html header to make a widget work. Like a style block or javascript block. See page-include-bits method.")
   (include-js :initarg :include-js
               :initform nil
               :accessor include-js)
   (defer-include-js :initarg :defer-include-js
     :initform nil
     :accessor defer-include-js)
   (include-css :initarg :include-css
                :initform nil
                :accessor include-css)
   )
  (:documentation "Meta class for widgets."))

(defun initialize-meta-class (next-method class default-superclass
                                  &rest args
                                  &key name direct-superclasses &allow-other-keys)
  (if (equal (or name (class-name class))
             default-superclass)
      (funcall next-method)
      (let ((default-superclass (find-class default-superclass)))
        (if (some (lambda (x) (subtypep x default-superclass))
                  direct-superclasses)
            (funcall next-method)
            (apply next-method class
                   :direct-superclasses (list* default-superclass direct-superclasses)
                   args)))))

(defmethod initialize-instance :around ((class widget-class)
                                        &rest args
                                        &key (default-superclass 'widget))
  (apply #'initialize-meta-class #'call-next-method
         class default-superclass args))

(defmethod reinitialize-instance :around ((class widget-class)
                                          &rest args
                                          &key (default-superclass 'widget))
  (apply #'initialize-meta-class #'call-next-method
         class default-superclass args)
  ;;(initialize-class-slots class)
  )

(defmethod validate-superclass ((class widget-class)
                                (superclass standard-class))
  t)

(defmethod validate-superclass ((superclass standard-class)
                                (class widget-class))
  t)




(defclass widget (standard-object)
  ((name :initarg :name
          :accessor name
	  :initform nil
	  :documentation "Name of the widget instance used by the widget caching mechanism and render code to differentiate between widgets *(widget class is used in combination with widget name).")
   (%theme-attributes :initarg :theme-attributes
		      :accessor theme-attributes
		      :initform nil
		      :documentation "List of theme attributes of (attribute value)... or hashtable. These will only be superceded by theme passed directly to render.")
   (%context :initarg :context
	     :accessor widget-context
	     :initform nil)
   (parent :initarg :parent
	   :accessor parent
	   :initform nil))
  (:documentation "Widgets are the building blocks of the cl-sfx ui."))


(defgeneric initialize-widget (widget)
  (:documentation "Implement this method to do widget initialization. Called by make-widget.
This method should be used to keep render code clean and simple."))

(defmethod initialize-widget ((widget widget))
  ;;Default method.
  )

;;TODO: Is this still needed for widgy-name
(defun nto-html (string)
  (declare ((simple-array character (*)) string))
  (loop for i below (length string)
        for char = (char string i)
        when (char= char #\-)
        do (setf (char string i) #\_))
  string)

(defun widgy-name (instance slot-name)
  (nto-html
   (concatenate 'string
                (name instance)
                "."
                slot-name)))

(defgeneric render-event-handler (widget name)
  (:documentation "Conceptually splitting render-events and other events to make code clear. Render events should be used to make a widget reusable, giving the user a chance to customize any rendering that is customizable. This also affords the chance to clearly split logging of normal events and render events."))


#|

;;Possible replacement for make-widget????????????

(defmethod initialize-instance :around ((widget widget) &key context)
  (declare (ignore widget))
  (let* ((*sfx-context* (or context *sfx-context*))
	 (*sfx-session* (session *sfx-context*))
	 (*sfx-system* (system *sfx-session*)))
    (call-next-method)))


(defmethod initialize-instance :after ((new-widget widget) &key context)
  (declare (ignore context))
  (let* ((class (class-of new-widget))
	 (name (or (name new-widget)
		   (string-downcase (if (classp class)
					    (class-name class)
					    class))))
	     (widget (gethash name (cache *sfx-context*))))
	
	(unless widget
	  (setf widget new-widget))
	
	(if (and widget
		 (not (eq (class-name (class-of widget)) 
			  class)))
	    (restart-case
		(error "Widget \"~a\" of class ~a already exist. Requested ~a class."
		       (name widget)
		       (class-name (class-of widget))
		       class)
	      (replace-widget ()
		:report "Replace old widget"	
		(setf (gethash name (cache *sfx-context*)) 
		      nil)
		new-widget))
	    widget)))

|#


(defgeneric make-widget (class &rest args &key name context)
  (:documentation ""))

(defmethod make-widget :around (class &rest args &key name context)
  (declare (ignore name class args))
  (let* ((*sfx-context* (or context *sfx-context*))
	 (*sfx-session* (if *sfx-context* 
			    (session *sfx-context*)))
	 (*sfx-system* (if *sfx-session*
			   (system *sfx-session*)
			   )))
	(call-next-method)))

(defun widget-name-split (name)
  (split-sequence:split-sequence #\. name))

(defun widget-name (class name)
  (let ((name% (or
		name
		(string-downcase (if (classp class)
					 (class-name class)
					 class)))))
    (if (not *parent-container*)
	name%
	(nto-html
	 (concatenate 'string
		      (name *parent-container*)
		      "."
		      name%)))))

(defmethod make-widget (class &rest args &key name context &allow-other-keys)
  (declare (ignore context))
  
  (let* ((name (widget-name class name))
	 (widget (gethash name (cache *sfx-context*))))
    
	(unless widget
	  (setf widget (apply #'make-instance class :name name args))
	  ;;TODO: what about cache-p
	  (setf (gethash name (cache *sfx-context*)) widget))

	;;todo move to below if
	(initialize-widget widget)
	(if (and widget
		 (not (eq (class-name (class-of widget)) (etypecase class
							   (symbol class)
							   (class (class-name class))))))
	    (restart-case
		(error "Widget \"~a\" of class ~a already exist. Requested ~a class."
		       (name widget)
		       (class-name (class-of widget))
		       class)
	      (replace-widget ()
		:report "Replace old widget"	
		(setf (gethash name (cache *sfx-context*)) 
		      nil)
		(apply #'make-instance class :name name args)))
	    
	    widget)))

#|

(defgeneric register-widget-events (widget &key &allow-other-keys)
	    (:documentation "Registers widget events with *events*."))


;;TODO: Ponder event inheritance????
(defmethod register-widget-events ((widget widget) &key)
  (let ((events (events (class-of widget))))
    (dolist (group events)
      (let ((event-group (gethash (list (intern (package-name *package*) :keyword)
				  (class-name (class-of widget))
				  group)
		       
			    *events*)))
	
	(setf (events event-group) nil )
	(unless event-group
	  (setf event-group (make-instance 'sfx-event-group :key group)))
	
	(dolist (event group)
	  (pushnew event (events event-group)))
	(setf (events event-group) (reverse (events event-group)) )
	(setf (gethash (list (intern (package-name *package*) :keyword)
				  (class-name (class-of widget))
				  group)
		       
		       *events*)
	      event-group)))))

|#


(defgeneric render (widget &key &allow-other-keys)
  (:documentation "Method that needs to be implemented to render a widget. *Try to limit themeing code to render or functions called by render because sender sets up convenience scoping to and functions."))

(defun all-widget-superclasses (class)
  (labels ((all-superclasses (class)
             (cons class
                   (loop for super in (class-direct-superclasses class)
			 when (typep super 'widget-class)
			 append (all-superclasses super)))))
    (all-superclasses class)))

(defun get-all-class-themes (widget)
  (let ((themes nil))
    (dolist (class (all-widget-superclasses (class-of widget)))
      (when (and (slot-exists-p class 'base-theme) (base-theme class))
	(pushnew (base-theme class) themes)))
    themes))

;;TODO: get themes all the way back in the inheritance????
(defmethod render :around ((widget widget) &key theme context)
  "Exposing *theme* and *widget* within a let binding to be used inside of the widget render method for convenience."
;;  (break "render ~A" *sfx-context*)
  (let* ((*sfx-context* (or context *sfx-context*))
	 (*sfx-session* (session *sfx-context*))
	 (*sfx-system* (if *sfx-session* (system *sfx-session*) *sfx-system*))
	 (*widget* widget)
	 (*theme* (apply #'merge-themes *sfx-system* 
			 (append 
			  (list theme 
				(theme-attributes widget)
				*current-theme*)
			   (get-all-class-themes widget)
			   (get-all-class-themes (parent widget))))))
    (pushnew widget (widget-sequence *sfx-context*) )
    (call-next-method)))

;;TODO: What to do about events?
(defmethod render :before ((widget widget) &key)
  "Does event maintenance."
  
  ;;(register-widget-events widget)
  )

;;TODO: Should a container keep track of its widgets?
;;TODO: Implement widget theme-attribute inheritance from parent widget?? Do we need to??
(defclass container (widget)
  ((content :initarg :content
	    :accessor content
	    :initform nil)
   (events :initarg :events
	   :accessor events
	   :initarg nil)
   (data :initarg :data
	 :accessor data
	 :initform nil))
  (:metaclass widget-class)
  (:documentation "A widget that can contain other widgets. Takes some of the pain of implementing container behaviour away."))

#|
(defgeneric initialize-container-widgets (container)
  (:documentation "Do child widget initialization here."))
|#

(defvar *init-container-p* nil)


;;TODO: Cant figure out how to make *parent-container* work.
#|
(defmethod initialize-widget ((widget container))
  (setf *init-container-p* nil)
  )


(defmethod initialize-widget :around ((widget container))
  (let ((*parent-container* widget)
	(*init-container-p* t))    
    (call-next-method)))


(defmethod initialize-widget :after ((widget container) &key)
  (when (and *init-container-p* (not (equal widget *parent-container*)))
    (setf (parent widget) *parent-container*)))
|#

(defmethod render ((widget container) &key body)
  (cond (body
           (render body))
          (t
	   (when (content widget)
	     (render (content widget))))))

;;TODO: Tree and entity tree is the only things that still use (data widget) are
;;those trees still in use?
(defgeneric synq-data (widget &optional data))

;;TODO: Generic implementation of sync data
(defgeneric update-slot (object slot value))

(defmethod update-slot (object slot value)
  (setf (slot-value-using-class (class-of object) object slot)
                 value))

(defun find-slot-definition (slot-name object)
  (find slot-name (class-slots (class-of object))
        :key #'slot-definition-name
        :test #'string-equal))

(defun find-slot (slot-name object)
  (let ((slot (find-slot-definition slot-name object)))
    (when slot
      (slot-definition-name slot))))

(defmethod synq-data ((widget widget) &optional data)
  (if (listp (data widget))
        (dolist (object (data widget))
          (when (typep object 'standard-object)
            (loop for (key . value) in data
               for slot = (find-slot key object)
               when slot
               do (update-slot object slot value))))
        (when (typep (data widget) 'standard-object)
          (loop for (key . value) in data
                for slot = (find-slot key (data widget))
                when slot
                do (update-slot (data widget) slot value)))))

(defun find-slot-definition* (slot-name object start)
  (declare ((simple-array character (*)) slot-name)
           (fixnum start)
           (optimize speed))
  (let ((length1 (- (length slot-name) start)))
   (loop for slot in (class-slots (class-of object))
         for name = (the (or
                          (simple-array character (*))
                          (simple-array base-char (*)))
                         (string (the symbol (slot-definition-name slot))))
         for length2 = (length name)
         when (and (= length1 length2)
                   (loop for i below length2
                         for j from start
                         for char1 of-type base-char = (char slot-name j)
                         for char2 of-type base-char = (char name i)
                         always (or (char-equal char1 char2)
                                    (and (char= char1 #\_)
                                         (char= char2 #\-)))))
         return slot)))

(defun un-widgy-name (instance name)
  (declare ((simple-array character (*)) name)
           (optimize speed))
  (let* ((instance-name (name instance))
         (length1 (length name))
         (length2 (length instance-name)))
    (declare ((or (simple-array character (*))
                  (simple-array base-char (*))) instance-name))
    (if (and (> length1 length2)
               (char= (char name length2) #\.))
      (let ((dot
              (loop for i below length2
                    for char1 = (char name i)
                    for char2 = (char instance-name i)
                    always (or (char= char1 char2)
                               (and (char= char1 #\_)
                                    (char= char2 #\-)))
                    finally (return i))))
        (if dot
	    (find-slot-definition* name instance (1+ dot))
	    (find-slot-definition name instance)
	    ))
      (find-slot-definition name instance)
      )))

(defgeneric synq-cache (widget &optional data)
  (:documentation "Updates widget slots values. Slots that have names that match parameter names are updated with the parameter values."))

(defmethod synq-cache ((widget widget) &optional data)
  (when (name widget)
      (loop for (key . value) in data
	 for slot = (un-widgy-name widget key)
            when slot
	 do (update-slot widget slot value))))

;;TODO: Ponder renaming this or using standard event handler???? Cache events?
(defgeneric action-handler (widget)
  (:documentation "This method is called after the dom has been updated from parameters.")
  (:method ((widget widget))))


(defmethod action-handler ((widget widget))
  
  )

(defgeneric map-cache (function context &key args))

(defmethod map-cache (function context &key args)
 ;; (break "func = ~A" function)
  (maphash   (lambda (key widget)
		  (declare (ignore key))
		  (apply function widget args))
	     (cache context)))

;;TODO: Ponder renaming to after cash events?
(defgeneric map-cache-events (context))

(defgeneric handle-action (widget action))

(defun handle-messages ()
  (let* ((widget (get-widget (post-parameter "message-dest")))
         (type (post-parameter "message-type"))
         (action (and type 
                      (find-symbol (string-upcase type) :keyword))))
    (when (and widget action)
      (handle-action widget action))))

(defmethod map-cache-events ((context sfx-context))
 
  (dolist (widget (widget-sequence context))
    (apply #'action-handler (list widget)))
;;  (map-cache #'action-handler context)
  (handle-messages))
