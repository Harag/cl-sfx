(in-package :common-lisp-user)

(defpackage :cl-sfx
  (:use :c2cl :alexandria :hunchentoot :cl-who :xdb2)
  (:export
  
   ;;Common
   :strip-name
   :frmt
   :with-html-string
   :make-name
   :plural-name
   
   ;;MOP
   
   :system-db
   
   :data-mixin
   :collection
  
   
   
   :all-slots
   :url
 
   :data-class
   :data-versioned-class
   :data-object
   :data-versioned-object
   
   :data-slot
   :header
   :required
   :virtual
   :validate
   
   :data-versioned-slot
   
   :slot-val
  
   :doc
   :date-doc
   
   :user
   :log-action
   
   :doc-collection
   :find-docx
   :get-collection-docs
   :find-docsx
   
   
  
   ;;DB-Types
   :string
   :multi-line-string
   :yes-no
   :date
   :number
   :integer
   :list
   :db-object
   :percentage
   :member
   :data-member
   :values
   :data-group
   :data-tree
   :data-tree-selector
   
   
   ;;Events
   :sfx-event
   :name
   :event-class
   
   :sfx-event-group
   :event-key
   :events
   

   :asfx-event-handlers
   :event-name
   ;;:event-class
   
   ;;:*event-handlers
   ;;:*events ()
   
   :event-handler
   :define-event-handler

   ;;System
   :sfx-data

   :init-sys-data
   :load-sys-data
   :unload-sys-data
   
   :sfx-user-preference
   ;;:name
   ;;:preference
   
   :sfx-permission
   :permission-name
   :parameters
   
   ;;:name
   :permission-list
   
   :sfx-user
   :email
   :password
   :salt
   :permissions
   :preferences
   :super-user-p
   :status
   :make-password
   :check-password
   :make-user
   :change-user
   :get-user
   :find-users
   
   ;;:active-user
   :*sfx-session*  
   :sfx-session
   :sfx-session-id
   :system
   :user
   :contexts
   :cache
   
   :curent-user
    
   :entity
   :entity-type
   :entity-name
   
   :entity-doc
   
   :context-parameter
   :context-parameter-name
   :parameter-value
   
   :menu-item
   :item-name
   :children
   :context-definition
   :context-parameters
   
   :menu
   :menu-name
   :menu-items
   :make-menu-item
   
   :module
   :module-name
   :contexts
   ;;:menu
   
   :payment-details
   :company-name
   
   
   :license-module
   ;;:module
   :entities
   
   :sfx-license
   :license-code
   :license-holder
   :payment-detail
   :license-modules
   :license-entities
   :license-date
   :license-status
   :super-user-access-p
   
   :find-license
   :assin-license-code
   
   :license-doc
   :license
   
    ;;:session
   :body
   :setup-sfx-session
   :start-sfx-session
   
   
   :doc-slot
   
   :doc-spec
   
   :create-or-find-class
   
   :sfx-context-definition
   :context-name
   :from-class
   :doc-spec
   :permissions
   :url
   :for-everyone
   :args
   :body
   :grid-p
   
   :get-context-def
   :define-context-def
   :create-context-definition
   :load-default-pages
   
   :sfx-context
   :context-id
   :context-definition
   
   :setup-sfx-context
   :start-sfx-context
   
   :*sfx-context*
   :with-session-context
   :find-context
   
   
   :sfx-locale
   :language
   :currency
   :date-format
   :locale-language
   :translation
   ;;:language
   :phrase
   :tlp
   
   ;;:system-comms
   
   ;;:system-lock
   
   :sfx-system
   :system-name
   :port
   :message-log-destination
   :system-status
   :system-folder
   :system-data
   :context-definitions
   :with-system
   
   :init-system
   :start-sys
   :stop-sys
   
   
   
   :sessions

   :*sfx-system*
   
   :system-package
   
   :getx
   
   :add-event
     
   :widget-class
   :base-theme
   :widget
   ;;:name
   :attributes
   :theme-attributes
   :parent
   :widget-name-split
   ;;context
 
   :widgy-name
   :unwidgy-name
   
   :make-widget
   ;;:name
   ;;session
   :class
   
   :%render
   :render
   
   :update-slot
   :synq-data
   :synq-cache
   :action-handler
   :map-cache-events
   :map-cache
   
   ;;*theme*
   ;;*widget*
   :*current-theme*
   
   :theme
   ;;:name
   ;;:attributes
   
   :with-theme
   :with-widget-theme
   :theme-attribute
   :theme-value
   :att
   :attv
   
   ;;apply-theme-limiting
   
   
   
   :container   
   :content
   :events
   :initialize-widget
   
   :ui-canvas
   ;;:header
   :body
   :footer
   :data
   
   :repl-input

   :with-html
   :with-html-no-indent
   :with-html-string-no-indent
   :copy-object
   :escape
   :add
   
   :sfx-script
   :scriptc
   
   
   :report
   :report-element
   :name
   :label
   :element-type
   :sequence-no
   :elements
   :script
   :theme
   :cache
   :body
   
   ;;Report stuff
   :*cache*
   :*data-row*
   :*data-summary-row*   
   :get-slot-definition
   :get-slot-value
   :get-data-row-val
   :group-by
   :with-report
   
   :do-doc-search-string
   
   :parse-number
   :parse-date
   :format-date
   :decode-date
   :short-month-name
   
   :*tmp-directory*
   ))

(defpackage :cl-sfx-doc
  (:use :cl :alexandria :cl-who)
  (:nicknames :doc)
  (:import-from :cl-sfx #:with-html #:escape)
  (:export #:row
           #:document-element
           #:document
           #:orientation
           #:paragraph
           #:render-element
           #:escape
           #:add
           #:table
           #:with-table
           #:with-document
           #:with-element
           #:header
           #:cell
           #:render-pdf))








