(in-package :cl-sfx)

(defvar *public-dir* #p"/home/phil/Development/umage/web/pdf/")

(defclass report-element ()
  ((name :initarg :name
	  :accessor name
	  :initform nil
	  :db-type string
	  :key t)
   (label :initarg :label
	  :accessor label
	  :initform nil
	  :db-type string)
   (element-type :initarg :element-type
		 :accessor element-type
		 :initform nil
		 :db-type (member :report :table :row :cell))
   (sequence-no :initarg :sequence-no
	     :accessor sequence-no
	     :initform 1
	     :db-type number)
   (elements :initarg :elements
	  :accessor elements
	  :initform nil
	  :db-type (list report-element))
   (script :initarg :script
	  :accessor script
	  :initform nil
	  :db-type script)
   (theme :initarg :theme
	 :accessor theme
	 :initform nil
	 :db-type (data-member theme :key-accessor name))
   (cache :initarg :cache
	  :accessor cache
	  :initform (make-hash-table :test 'equalp))
   (body :initarg :body
	 :accessor body
	 :initform nil))  
  (:metaclass data-class))

(defclass report ()
  ((name :initarg :name
	  :accessor name
	  :initform nil
	  :db-type string
	  :key t)
   (label :initarg :label
	  :accessor label
	  :initform nil
	  :db-type string)
  (report :initarg :report
	  :accessor report
	  :initform nil
	  :db-type script))
  (:metaclass data-class)
  (:collection-name "reports")
  (:collection-type :license)
  (:default-initargs :top-level t))

(defclass report-view (widget)
  ((report :initarg :report
	    :accessor report
	    :initform nil)))

(defvar *cache* nil)
(defvar *data-row* nil)
(defvar *data-summary-row* nil)

(defun parse-element (element)
  (destructuring-bind (type . attributes-body) element
    (values type
            (loop while (keywordp (car attributes-body))
                  collect (pop attributes-body)
                  collect (pop attributes-body))
            attributes-body)))

(defun get-fx (list key)
  (getf list key))

(defun data-row (row row-element)
  (let ((*data-row* row))
    (declare (special *data-row*))
    (with-html
     (mapc #'render-element row-element))))

(defun render-element (element)
  (let ((eish (if (not (listp (first element)))
		  (list element)
	     element)))
    (dolist (e eish)
      (when e
	(multiple-value-bind (element-key attributes body) 
	    (parse-element e)	  

	  (case element-key
	    (:report
	     (with-html
	       (:div :class "row no-gutters"
		     (:div :class "col align-self-start"
			   (:h4 (str (get-fx attributes :label))))
		     
		     (:div :class "col align-self-end col-md-auto hidden-print"
			   (:form :method "post"
				  (:div :class "row no-gutters"
					(:div :class "col align-self-end col-md-auto"
					      (:input :type :hidden :id "pageid"
						      :name "pageid"
						      :value (or (context-id *sfx-context*)
								 (parameter "pageid"))))
					(:div :class "col align-self-end col-md-auto"
					      (:input :class "form-control"
						      :type "text" 
						      :name "report-search"
						      :value 
						      (str (parameter "report-search"))))
					(:div :class "col align-self-end col-md-auto"
					      (:input :class "btn btn-primary"
						      :type "submit"
						      :name "do-report-search"
						      :value "Search")))))
		     (:div :class "col align-self-end col-md-auto"
			   (:form :target "_blank"
				  ;;:style "display:inline-block"
				  :method "post"
				  :action (frmt "~a?pdf=t~@[&~a~]" (script-name*)
						(query-string*))
				  (:div :class "hidden-print"
					(:button :class "btn btn-secondary"
						 :type "submit"
						 :name "pageid"
						 :value (context-id *sfx-context*)
						 "PDF")))))
	       (:div :class "row"
		     (:div :class "col"
			   (render-element body)))))
	    (:table
	     (with-html
	       (:table :class (get-fx attributes :class)
		       (render-element body))))
	    (:thead
	     (with-html
	       (:thead :class "thead-default"
		       (render-element body))))
	    (:slot
	     (return-from render-element (list 
				       (get-fx attributes :name)
				       (get-fx attributes :accessor)
				       (get-fx attributes :script))))
	    (:sum-slot
	     (return-from render-element (list 
					  (get-fx attributes :name)
					  (get-fx attributes :accessor)
					  (get-fx attributes :sum-func))))
	    (:slots
	     (let ((slots))	       
	       (dolist (slot body)		
		 (setf slots (append slots (list (render-element slot)))))
	       (return-from render-element slots)))
	    (:group-by	    
	     (group-by (get-fx attributes :collection)
		       (render-element (get-fx attributes :group-slots))
		       (render-element (get-fx attributes :sum-slots))))
	    (:data
	     (when (get-fx attributes :group-by)	      
	       (render-element (get-fx attributes :group-by)))
	     (when (get-fx attributes :script)	      
	       (sfx-eval (get-fx attributes :script))))
	    (:table-data-row
	     (when (get-fx attributes :data)	      
	       (render-element (get-fx attributes :data)))
	     (when (get-fx attributes :script)	      
	       (sfx-eval (get-fx attributes :script)))
	     
	     (let ((data (if *cache* (gethash :data *cache*))))
	       (with-html
		 (if (listp data)
		     (loop for row in data
			do
			  (data-row row body))
		     (loop for row across data
			do
			  (data-row row body))))))
	    (:table-data-cell    
	     (with-html
	       (:td :class (get-fx attributes :class)
		    :colspan (get-fx attributes :colspan)
		
		    (if (get-fx attributes :script)
			(str (sfx-eval 
			      (get-fx attributes :script)))
			(str (get-fx attributes :label))))))
	    (:table-data-footer
	     (with-html
	       (:tfoot :class "table-active"
		       (render-element body))))
	    (:tr
	     (with-html
	       (:tr :class (get-fx attributes :class)
		(render-element body))))
	    (:td
	     (with-html
	       (:td :class (get-fx attributes :class)
		    (render-element body))))
	    
	    (t
	     (if element-key
		 (eval
		  `(with-html  
		     (,element-key
		       ,@attributes				    
		       ,(if (or (> (length body) 1) (listp (car body)))
			    (render-element body)
			    (car body)))))
		 (if (listp body)
		     (render-element body)
		     (with-html (str body)))))))))))

(defun render-report-html (report)
  (render-element (if (stringp (report report))
		      (sfx-read-string (report report))
		      (report report))))

(defun create-report (code)
  (unless (getf code :report)
    (break "No report tag WTF"))
  
  (let* ((report-body (cdr code))
	 (name (getf report-body :name))
	 (label (getf report-body :label))
	 (report (find-docx 'report :test (lambda (doc)
					    (equalp (name doc) name)))))
    (when report
      (setf (report report) code)
      (setf (label report) label))
      
    (unless report
      (setf report (make-instance 'report :name name :label label
				  :report code)))
    (xdb2::persist report)))

(defmacro with-report (&body body)  
  `(create-report ',@body))

(defun get-slot-definition (class name)
  (loop for slot in (class-slots class)
     when (string-equal (slot-definition-name slot) name)
       do (return-from get-slot-definition slot)))

(defun get-slot-value (doc slot)
  (if (and (slot-boundp doc slot)
	   (slot-value doc slot))
      (if (typep (class-of (slot-value doc slot))
		 'data-versioned-class)
	  (destructuring-bind 
		(data-member &key key-accessor)	  
	         
	    (cdr (db-type (get-slot-definition
				   (class-of doc)
				   slot)))
	    (declare (ignore data-member))
	   
	    (slot-value (slot-value doc slot) key-accessor))
	  (slot-value doc slot))))

(defun get-data-row-val (name &optional slot-name)
  (when (and *data-row* 
	     (getf *data-row* name))
    
    (if (typep (class-of (getf *data-row* name))
	       'data-versioned-class)
	(get-slot-value 
	 (getf *data-row* name) slot-name)
	(getf *data-row* name))))

(defun do-doc-search-string (doc slots &optional search-term)
  (let ((search-term (or search-term
			 (if (hunchentoot:parameter "report-search")
			     (if (not (equalp (hunchentoot:parameter
					       "report-search") ""))
				 (hunchentoot:parameter "report-search")))))
	(*doc* doc))
    (if search-term
	(dolist (slot slots)  
	  (if (search search-term
		      (if (listp slot)
			  (if (slot-exists-p doc (first slot))
			      (if (slot-value doc (first slot))
				  (frmt "~A" 
					(slot-value (slot-value doc (first slot))
						    (second slot))))
			      (if (third slot)
				  (frmt "~A" (sfx-eval (third slot)))))
			  (frmt "~A" (slot-value doc slot)))
		      :test #'string-equal)
	      (return-from do-doc-search-string doc)))
	doc)))

(defun group-by* (docs slots grouping-function totals-function)
  (let ((groups (make-hash-table :test 'equalp))
	(group-data)
	(summary-data))
    
    (setf (gethash :total-hours *cache*) 0)

    (loop for doc across docs
	 do
	 (let ((key))
	     (loop for slot in slots 
		do
		  (setf 
		   key 
		   (concatenate 
		    'string 
		    key 
		    "|"
		    (frmt "~A" (get-slot-value doc slot)))))
	    
	     (setf (gethash key groups) (pushnew doc (gethash key groups)))))
 
    (dolist (key (sort (alexandria:hash-table-keys groups) #'string-greaterp ))
      (pushnew (funcall grouping-function key (gethash key groups)) group-data))
    
    (setf summary-data (funcall totals-function docs group-data))
    
    (list group-data summary-data)))

(defparameter *doc* nil)

(defun group-by (collection group-slots summary-slots)
  (let ((data (find-docsx
		(lambda (doc)
		  (do-doc-search-string 
		      doc 
		    group-slots))
		collection))
	(groups (make-hash-table :test 'equalp)))
    
    (loop for doc across data
       do
	 (let ((key)
	       (*doc* doc))
	   (loop for slot in group-slots 
	      do
		(setf 
		 key 
		 (concatenate 
		  'string 
		  key 
		  "|"
		  (frmt "~A" 
			(if (third slot)
			    (sfx-eval (third slot))
			    (if (second slot)
				(slot-value 
				 (slot-value doc 
					     (first slot))
				 (second slot))
				(slot-value doc 
					    (first slot))))))))
	   
	   (setf (gethash key groups) (pushnew doc (gethash key groups)))))
    
    (dolist (key (sort (alexandria:hash-table-keys groups) #'string-greaterp))
      (let* ((values (gethash key groups))
	     (grouping)
	     (summary)
	     (first-doc (first values))
	     (*doc* first-doc))
	
	(dolist (slot group-slots)
	  (setf grouping (append grouping 
				 (list (key-intern (first slot))
				       (if (third slot)
					   (sfx-eval (third slot))
					   (slot-value first-doc 
						       (first slot)))))))
	(let ((computed-vals (make-hash-table :test 'equalp)))
	  (dolist (doc values)
	    (let ((*doc* doc))
	      (dolist (slot summary-slots)
		(let* ((slot-name (first slot))
		       (slot-object-name (second slot))
		       (slot-sum-function (third slot))
		       (total-name (key-intern (frmt "total-~A" slot-name)))
		       (val (if (second slot)
				(slot-value 
				 (slot-value doc 
					     slot-name)
				 slot-object-name)
				(slot-value doc 
					    slot-name)))
		       (computed-val (or (gethash slot-name computed-vals) 0))
		       (total-val (or (gethash total-name *cache*) 0)))
		  
		  (setf (gethash slot-name computed-vals)
			(if slot-sum-function
			    (apply slot-sum-function
				   val 
				   computed-val)
			    (incf computed-val 
				  val)))
		  
		  (setf (gethash total-name *cache*)
			(incf 
			 total-val
			 val))))))
	  (maphash
	   (lambda (key val)
	     (setf summary (append summary (list (key-intern key)  val))))
	   computed-vals)
	  
	  (setf (gethash :data *cache*) 
		(append (gethash :data *cache*) 
			(list (append grouping summary))))
	  (setf (gethash :data-summary *cache*) 
		(append (gethash :data-summary *cache*) summary)))))))


(defun row-cells (report-element)
  (dolist (cell (elements report-element))
	(with-html
	 (:td (str (sfx-eval 
		    (script cell)))))))

(defun render-report-pdf (report-element)
  (sfx-eval (script report-element))
  (case (element-type report-element)
    (:report
     (doc:add (doc:paragraph  (label report-element)))
     (mapc #'render-report-pdf (elements report-element)))
    (:table
     (doc:with-table ()
       (mapc #'render-report-pdf (elements report-element))))
    (:row
     (let ((data (if *cache* (gethash :data *cache*))))
       (doc:add (doc:row))
       (dolist (cell (elements report-element))
         (doc:add (doc:header (label cell))))
       
       (loop for row across data
	    do
         (let ((*data-row* row))
           (declare (special *data-row*))
           (doc:add (doc:row))
           (dolist (cell (elements report-element))
             (doc:add (doc:cell (sfx-eval 
                                 (script cell)))))))))))

(defun render-pdf (report)
  (doc:render-pdf (doc:with-document ()
                    (render-report-pdf report))))

(defun serve-pdf (document)
  (let ((path (make-pathname
               :name (format nil "~a-~a"
                             (string-left-trim
                              "-" (ppcre:regex-replace-all "/+" (script-name*) "-"))
                             (get-universal-time))
               :type "pdf"
               :directory
               (append (pathname-directory *public-dir*) '("pdf")))))
    (ensure-directories-exist path)    
    (dx-pdf::with-pdf (path)
      (render-pdf document))
    (redirect (conc "/pdf/" (file-namestring path)))))

(defmethod render ((widget report-view) &key)
  (let* ((report (find-docx 'report
                            :test (lambda (doc)
                                    (string-equal (parameter "report-name") 
                                                  (name doc)))))
	 (*cache* (make-hash-table :test 'equalp)))   
    (declare (special *cache*))
    
    (if (equal (parameter "search") "t")
      (setf (gethash :search  *cache*) (parameter "report-search")))
    (when report
      (setf (report widget) report)
     
     
      (if (equal (parameter "pdf") "t")
          (serve-pdf report)
	  (render-report-html report)
          ))))

