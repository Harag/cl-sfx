(in-package :cl-sfx)

(defun current-license-code ()
  (if (and *sfx-session* (user *sfx-session*))
      (license-code (license (user (user *sfx-session*))))
      "000000"))

(defun system-db ()
  (let ((db (get-db (system-data *sfx-system*)
	   (list (frmt "~A" (strip-name (system-name *sfx-system*)))
		 "000000"))))
    (when (not db)     
      (setf db (add-db (system-data *sfx-system*) 
		       (list (frmt "~A" (strip-name (system-name *sfx-system*)))
			     "000000")))
      (load-collections db)
      (xdb2:clear-db-cache db))
    db))

(defun license-db ()
 ;; (break "~A~A" (current-license-code) (system-data *sfx-system*))
  (let ((db (get-db (system-data *sfx-system*)
		    (list (frmt "~A" (strip-name (system-name *sfx-system*)))
			  (current-license-code)))))
    (when (not db)     
      (setf db (add-db (system-data *sfx-system*) 
	   (list (frmt "~A" (strip-name (system-name *sfx-system*)))
			  (current-license-code))))
      (load-collections db)
      (xdb2:clear-db-cache db))
    db))

(defun get-col-sys (collection-name)
  (or
   (get-collection (system-db) collection-name)
   (add-collection (system-db) collection-name :force-load t)))

(defun get-col-lic (collection-name)
  (or
   (get-collection (license-db) collection-name)
   (add-collection (license-db) collection-name :force-load t)))

(defun make-key-val (doc)
  (let ((keys))
    (dolist (slot (all-slots (class-of doc)))
      (when (key slot)
	(pushnew (slot-value doc (slot-val slot 'SB-PCL::NAME)) keys)))))

(defun get-col-docs-merge (collection-name
			   &key (key-func #'make-key-val) 
			     (result-type 'vector))
  
  (let* ((sys-col (get-col-sys collection-name))
	 (sys-docs (if sys-col (docs sys-col)))
	 (lic-col (get-col-lic collection-name))
	 (lic-docs (if lic-col (docs lic-col))))
    (concatenate result-type 
		 (remove-if (lambda (doc)
			      (find doc lic-docs 
				    :test (lambda (doc tdoc)
					    (equalp (funcall key-func doc)
						    (funcall key-func tdoc)))))
			    sys-docs) lic-docs)))


(defun select-collection-class (doc-class)
  (let* ((col-type (car (collection-type doc-class)))
	 (collection-name (car (collection-name doc-class))))
      
      (cond ((equalp col-type :system)
	     (get-col-sys collection-name))
	    ((equalp col-type :license)
	     (get-col-lic collection-name))
	    ((equalp col-type :merge)
	   
	     (if (current-user)
		 (if (and (license (current-user)) 
			  (string-equal (license-code (license (current-user)))
					"000000"))
		     (get-col-sys collection-name)
		     (get-col-lic collection-name))
		 (get-col-sys collection-name))))))

(defun select-collection (doc)
  (select-collection-class (class-of doc)))

(defun get-collection-docs (class-name)
  (let* ((class (find-class class-name))	
	 (collection-name (car (collection-name class)))
	 (collection-type (car (collection-type class)))) 
    
    
    (cond ((equalp collection-type :merge)
	   (get-col-docs-merge collection-name))
	  ((equalp collection-type :system)	
	   (let ((collection 
		  (get-col-sys collection-name)))
	     (docs collection)))
	  ((equalp collection-type :license)
	   (let ((collection 
		  (get-col-lic collection-name)))
	     (docs collection)))
	  (t
	   (break "Collection type not set. --- get-collection-docs~%~A~A~A"
		  class collection-name collection-type)
	   (error "Collection type not set. --- get-collection-docs")))))

(defgeneric find-doc-from-docs (container &key predicate))

(defmethod find-doc-from-docs ((docs sequence) &key predicate)
  (when predicate
    (find-if predicate docs)))

(defmethod find-doc-from-docs ((docs hash-table) &key predicate)
  (when predicate
    (maphash (lambda (key value)
               (declare (ignore key))
               (when (funcall predicate value)
                 (return-from find-doc-from-docs value)))
             docs)))

(defun find-docx (class-name &key test)
  (let ((docs (get-collection-docs class-name)))
 
    (if test
	(map
	 nil
	 (lambda (doc)
	   (when (funcall test doc)
	     (return-from find-docx doc)))
	 docs))))

(defun find-docs-merge (function collection-name 
			&key (key-func #'make-key-val) 
			  (result-type 'vector))
  
  (let* ((system (get-col-sys collection-name))
	 (license (get-col-lic collection-name))
	 (sys-docs (if system 
		       (remove-if #'not (map result-type function (docs system)))))
	 (lic-docs (if license 
		       (remove-if #'not (map result-type function (docs license))))))
    
    (concatenate result-type 
		 (remove-if (lambda (doc)
			      (find doc lic-docs 
				    :test (lambda (doc tdoc)
					    (equalp (funcall key-func doc)
						    (funcall key-func tdoc)))))
			    sys-docs) lic-docs)))

;;TODO: find all the dolist coerce and replace with find-docsx and loop?
(defun find-docsx (function class-name &key (result-type 'vector)
					 (key-func #'make-key-val))
  
  (let* ((class (find-class class-name))
	 (collection-name (car (collection-name class)))
	 (collection-type (car (collection-type class))))
    
    (cond ((equalp collection-type :merge)
	   (find-docs-merge function collection-name
			    :key-func key-func
			    :result-type result-type))
	  ((equalp collection-type :system)
	  
	   (let* ((collection (get-col-sys collection-name))
		  (docs (if collection 
			    (docs collection)) ))
	     (when docs
	       (remove-if #'not (map result-type function docs)))))
	  ((equalp collection-type :license)
	   
	   
	   (let* ((collection (get-col-lic collection-name))
		  (docs (if collection 
			    (docs collection)) ))
	     
	     (when docs
	       (remove-if #'not (map result-type function docs)) )))
	  (t
	   (break "Collection type not set. --- find-docsx~%~A~A~A"
		  class collection-name collection-type)
	   (error "Col-type not set.")))))


(defclass data-mixin (storable-mixin)
  (
#|   (collection :initarg :collection
               :initform nil
               :accessor collection)
  |# 
   (collection-type :initarg :collection-type
               :initform nil
               :accessor collection-type
	       :documentation "System,merge,license. Nil = system default")
   (collection-name :initarg :collection-name
               :initform nil
               :accessor collection-name)

   (report :initarg :report
           :initform nil
           :accessor report)
   (all-slots :initarg :all-slots
              :initform nil
              :accessor all-slots
              :documentation "Includes virtual slots")
   (before-persist :initarg :before-persist
		  :accessor before-persist-event
		  :initform nil)
   (after-persist :initarg :after-persist
		  :accessor after-persist-event
		  :initform nil)
  #| (url :initarg :url
        :initform nil
        :accessor url)
   |#
   ))

(defclass data-class (data-mixin storable-class)
  ())

(defclass data-versioned-class (data-mixin storable-versioned-class)
  ())

(defclass data-object (storable-object)
  ()
  (:metaclass data-class))

(defclass data-versioned-object (storable-versioned-object data-object)
  ()
  (:metaclass data-versioned-class))

(defclass data-slot (storable-slot)
  ((header :initarg :header
           :initform `(,*slot-dummy* nil)
           :accessor header)
   (required :initarg :required
             :initform `(,*slot-dummy* nil)
             :accessor required)
   (virtual :initarg :virtual
            :initform `(,*slot-dummy* nil)
            :accessor virtual)
   (validate :initarg :validate
             :initform `(,*slot-dummy* nil)
             :accessor validate)))

(defclass data-versioned-slot (data-slot storable-versioned-slot)
  ())

(defclass data-direct-slot-definition
    (data-slot standard-direct-slot-definition)
  ())

(defclass data-effective-slot-definition
    (data-slot standard-effective-slot-definition)
  ())

(defclass data-versioned-direct-slot-definition
    (data-versioned-slot standard-direct-slot-definition)
  ())

(defclass data-versioned-effective-slot-definition
    (data-versioned-slot standard-effective-slot-definition)
  ())

(defmethod direct-slot-definition-class ((class data-mixin)
                                         &rest initargs)
  (declare (ignore initargs))
  (find-class 'data-direct-slot-definition))

(defmethod effective-slot-definition-class ((class data-mixin) &key)
  (find-class 'data-effective-slot-definition))

(defmethod direct-slot-definition-class ((class data-versioned-class) &key)
  (find-class 'data-versioned-direct-slot-definition))

(defmethod effective-slot-definition-class ((class data-versioned-class)
                                            &key)
  (find-class 'data-versioned-effective-slot-definition))


(declaim (inline slot-val))
(defun slot-val (instance slot-name)
  (if (and instance
           (slot-boundp instance slot-name))
      (slot-value instance slot-name)))


(defmethod compute-effective-slot-definition
    ((class data-mixin) slot-name direct-definitions)
  (declare (ignore slot-name))

  (call-next-method)

  ;; (break "~A ~A" class slot-name)

  (let ((effective-definition (call-next-method))
        (direct-definition (car direct-definitions))
        (slots '(header required virtual validate)))
    (when (typep direct-definition 'data-slot)
      (compute-slot-option effective-definition
			   slots
			   direct-definitions))
    (loop for slot in slots
          for value = (slot-val effective-definition slot)
          when (and (consp value)
		    (eq (car value) *slot-dummy*))
          do (setf (slot-value effective-definition slot) (cadr value)))
      
    (setf (virtual effective-definition)
	  (parse-function (virtual effective-definition))
	  (validate effective-definition)
	  (parse-function (validate effective-definition)))
    effective-definition))

(defmethod compute-slots ((class data-mixin))
  (let* ((slots (call-next-method))
         (sorted (stable-sort
                  (loop for slot in slots
                        collect (cons (cond ((numberp (header slot))
                                             (header slot))
                                            ((numberp (key slot))
                                             (key slot))
                                            (t
                                             0))
                                      slot))
                  #'< :key #'car)))
    (setf (all-slots class)
          (map-into sorted #'cdr sorted))
    (remove-if #'virtual slots)))



(defmethod initialize-instance :around ((class data-class)
                                        &rest args)
  (let ((instance (apply #'initialize-storable-class #'call-next-method 
                         class 'data-object args)))
   ;; (break "a ~A ~A ~A" class instance args)
   ;; (apply #'initialize-data-class (list class instance))
    ))

(defmethod reinitialize-instance :around ((class data-class)
                                          &rest args)
  (let ((instance (apply #'initialize-storable-class #'call-next-method
                         class 'data-object args)))
    ;;(break "b ~A ~A ~A" class instance args)
   ;; (apply #'initialize-data-class (list class instance))
    ))


(defmethod initialize-instance :around ((class data-versioned-class)
                                        &rest args)
  
  (let ((instance (apply #'call-next-method class 
			 :default-superclass 'data-versioned-object
                         args)))
    (when (equalp 'sfx-license (class-name class))
    ;;  (break "c ~A ~A" class instance)
      )
   ;; (apply #'initialize-data-class (list class instance))
    ))

(defmethod reinitialize-instance :around ((class data-versioned-class)
                                          &rest args)
  
  (let ((instance (apply #'call-next-method  class 
			 :default-superclass 'data-versioned-object
                         args)))
    (when (equalp 'sfx-license (class-name class))
     ;; (break "d ~A ~A" class instance)
      )
   ;; (apply #'initialize-data-class (list class instance) )
    ))


(defclass doc ()
  ((user :initarg :user
         :initform nil
         :accessor user)
   (log-action :initarg :log-action
               :initform nil
               :accessor log-action
               :documentation "Inserted, updated, deleted, rolledback."))
  (:metaclass data-mixin))

(defmethod doc-collection ((doc data-object))
  (when (top-level doc)
    (select-collection doc)))


(defclass date-doc ()
  ((start-date :initarg :start-date
               :initform nil
               :accessor start-date
               :db-type date
               :parser #'parse-date)
   (end-date :initarg :end-date
             :initform nil
             :accessor end-date
             :db-type date
             :parser #'parse-date
             :validate #'validate-end-date))
  (:metaclass data-mixin))
