(in-package :cl-sfx)

;;TODO: Does this work right?
(defmacro with-box ((&rest args) &body body)
  `(render (make-instance 'box ,@args
                          :content (with-html-string
                                     ,@body))))


(defclass layout-column (container)
  ((class :initarg :class
          :accessor layout-row-class)
   (width :initarg :width
           :accessor :width)))

(defgeneric width-to-class (column &key))

(defmethod width-to-class ((column layout-column) &key)
  (format nil "~A ~A" (width column) (layout-row-class column)))

(defmethod render ((column layout-column) &key width-to-class-p body)
  (with-html
    (:div :class (if width-to-class-p
                     (width-to-class column)
                     (layout-row-class column))
          (render column :body body))))

(defclass layout-row (container)
  ((class :initarg :class
          :accessor layout-row-class)
   (columns :initarg :columns
              :accessor columns)))

(defmethod render ((row layout-row) &key body)
  (with-html
    (:div :class (layout-row-class row)
          (cond ((columns row)
                 (dolist (column (columns row))
                    (render column)))
                (t
                  (render (content row) :body body))))))
;;;

(defclass box (container)
  ((container :initarg :container
            :initform nil
            :accessor container)
   (header :initarg :header
           :initform nil
           :accessor header)
   (header-content :initarg :header-content
                   :initform nil
                   :accessor header-content)
   (icon :initarg :icon
         :initform nil
         :accessor icon)
   (icon-size :initarg :icon-size
              :initform 16
              :accessor icon-size)
   (collapsible :initarg :collapsible
                :initform t
                :accessor collapsible)
   (collapsed :initarg :collapsed
              :initform nil
              :accessor collapsed)
   ))

(defmethod render ((box box) &key)
  ;;(render (content box))
  (with-html
    (:div :class "card"
          :id (name box)
	  
          (:div :class "card-header "
             
                :id (frmt "~a-heading" (name box))
                (when (icon box)
                  (htm (:i :class (format nil "fa ~A" (icon box)))))
                (:h4 :id (frmt "~a-heading-desc" (name box)) 
                     :class "card-title" 
                     (esc (header box)))
                (when (header-content box)
                        (str (header-content box))))
          (:div :class "card-block"
               
                (str (content box)))))
  )

(defclass tab-box (box)
  ((tabs :initarg :tabs
         :initform nil
         :accessor tabs)
   (title :initarg :title
          :initform nil
          :accessor title)
   (body-content :initarg :body-content
                 :initform nil
                 :accessor body-content)))

(defmethod render ((widget tab-box) &key)
   (setf (header-content widget)
        (with-html-string
          (:ul :class "nav nav-tabs"
               (loop for (title) in (tabs widget)
                     for active = "active" then nil
                     for i from 0
                     when title
                     do
                     (htm
                      (:li :class "nav-item" active
                           (:a :class (frmt "nav-link ~A" active) 
			       :href (format nil "#~a-tab-~a" (name widget) i)
			       :role "tab"
                               :data-toggle "tab"
                               (esc title))))))))
   (setf (content widget)
        (with-html-string
          (:div :class "tab-content"
                (loop for (nil content) in (tabs widget)
                      for class = "tab-pane active" then "tab-pane"
                      for i from 0
                      when content
                      do
                      (htm
                       (:div :class class
                             :id (format nil "~a-tab-~a" (name widget) i)
                             (str content)))))
          (str (body-content widget))))
  (call-next-method)
   )

(defclass button (widget)
  ((event :initarg :event
          :initform nil
          :accessor event)
   (url :initarg :url
        :initform nil
        :accessor url)
   (text :initarg :text
         :initform nil
         :accessor text)
   (icon :initarg :icon
         :initform nil
         :accessor icon) 
   (hint :initarg :hint
         :initform nil
         :accessor hint)
   (confirm :initarg :confirm
            :initform nil
            :accessor confirm)
   (permission :initarg :permission
               :initform t
               :accessor permission)
   (permission-check
    :initarg :permission-check
    :initform nil
    :accessor permission-check)))

(defmethod render ((button button) &key parent-widget form-id)
  (with-slots (hint icon text event url confirm permission permission-check) button
    (when (or (null permission) 
              (and permission 
                   (if permission-check
                       (funcall permission-check permission)
                       t)))
      (with-html
        (:button :class (css-class button)
             :onclick
             (format nil "event.preventDefault(); ~a"
                     (if form-id
                         (js-render-form-values parent-widget
                            form-id                            
                            (js-pair "event" (event button)))
                         (js-render parent-widget 
                                    (js-pair "event" (event button)))))
             (str (text button)))))))

(defclass simple-form (container)
  ((width :initarg :width
              :initform nil
              :accessor width)
   (parent-widget :initarg :parent-widget
                  :initform nil
                  :accessor parent-widget)
   (form-id :initarg :form-id
            :initform nil
            :accessor form-id)
   (ajax-render-widget :initarg :ajax-render-widget
                       :initform nil
                       :accessor ajax-render-widget)))

(defmethod event-handler ((form simple-form) (event (eql :init-simple-form-box))
                          &key)
  (make-widget 'box :name 
               (sub-name form "box")))

(defmethod event-handler ((form simple-form) (event (eql :init-simple-form-buttons))
                          &key)
  (list
    (make-widget 'button
                :name "close-button"
                :event "close" 
                :text "Close"
                ;;:css-class "button grey fl-space"
                )))

(defmethod render ((widget simple-form) &key body)
  (let* ((name (sub-name widget "form"))
         (box (event-handler widget :init-simple-form-box))
         (form (make-widget 'form 
                            :name name
                            :id (or (form-id widget)
                                    name)
                            :method "post"
                            :on-submit "return false;")))
    (setf (container box) (event-handler box :init-box-container))
    (setf (content box) form)

    (setf (content form) 
            (with-html-string
                     (if body
                         (render body)
                         (render (content widget)))
                     (let ((buttons (event-handler widget :init-simple-form-buttons)))
                       (dolist (button buttons)
                         (htm
                          (render button (parent-widget widget)
                                         :form-id (form-id widget)))))))

    (render box)))


(defclass form-section (widget)
  ((section-size :initarg :section-size
                 :initform 100
                 :accessor section-size)))

(defmethod render ((widget form-section) &key label input label-for)
  (with-html
    (:div :class "control-group"
          (:label :id (strip-name label) :class "control-label docs" :for label-for
                  (str label))
          (:div :class "controls"
                (str input)))))

(defclass modal (widget)
  ((header :initarg :header
           :initform nil
           :accessor header)
   (content :initarg :content
            :initform nil
            :accessor content)
   (save-button :initarg :save-button
                :initform t
                :accessor save-button)
   (buttons :initarg :buttons
            :initform nil
            :accessor buttons)
   (grid :initarg :grid
         :initform nil
         :accessor grid)
   (size :initarg :size
         :initform :large
         :accessor size)
   (form-id :initarg :form-id
            :initform nil
            :accessor form-id)
   (upload :initarg :upload
           :initform nil
           :accessor upload)   )
  (:metaclass widget-class))

(defmethod render ((widget modal) &key)
  (let ((grid (grid widget))
        (upload (upload widget)))
    (with-html
      (:div :class
            (frmt "modal ~a" (ecase (size widget)
                               ;(:full "modal-large modal-full")
                               ((:full :large) "modal-large")
                               (:medium "modal-medium")
                               (:small "modal-small")))
            :id (name widget)
            :role "dialog"
            :aria-labelledby "vmap-label"
            (:div :class (frmt "modal-dialog~a"
                               (ecase (size widget)
                                 (:full " modal-lg modal-full")
                                 (:large " modal-lg")
                                 (:medium "")
                                 (:small " modal-sm")))
                  (with-html
                    (:div :class "modal-content"
                          (when (header widget)
                            (htm
                             (:div :class "modal-header"
                                   (:h4 :class "modal-title"
                                        :id "data-fix-label"
                                        (esc (header widget))))))
                          (if (form-id widget)
                              (htm
                               (:div :class "modal-body"
                                     (str (content widget))))
                              (htm
                               (:div :class "modal-body"
                                     ;; What? bootstrap or something else removes the form element for some uknown reason.
                                     :id (and (not upload)
                                              (frmt "~a-form" (name widget)))
                                     (:div :class "row"
                                           (:form :action ""
                                                  :method (if upload "post")
                                                  :enctype (if upload "multipart/form-data")
                                             
                                                  :name (frmt "~a-form" (name widget))
                                                  :id (and upload
                                                           (frmt "~a-form" (name widget)))
                                                  :class "form-horizontal"
                                                  :method "post"
                                                  :onsubmit (and (not upload)
                                                                 "return false;")
                                                  :enctype (and upload
                                                                "multipart/form-data")
                                                  (when (typep grid 'grid)
                                                    (htm (:input :type "hidden" :name "grid-name"
                                                                 :value (name grid))))
                                                  (str (content widget)))))))
                          (:div :class "modal-footer"
                                (if (buttons widget)
                                    (str (buttons widget))
                                    (htm
                                     (when (save-button widget)
                                       (htm
                                        (:button :class "btn-primary btn"
                                                 :onclick
                                                 (if upload
                                                     (frmt "$(\"#~a\").submit()"
                                                           (frmt "~a-form" (name widget)))
                                                     (frmt
                                                     ;; "{$(\"#~a\").modal(\"hide\");tinyMCE.triggerSave();~a;}"
						      "{$(\"#~a\").modal(\"hide\");~a;}"
                                                      (name widget)
                                                      (js-render-form-values (if (typep grid 'grid)
                                                                                 (editor grid)
                                                                                 grid)
                                                                             (or (form-id widget)
                                                                                 (frmt "~a-form" (name widget)))
                                                                             (js-pair "action" "save")
                                                                             (js-pair "grid-name" (and grid
                                                                                                       (name grid))))))
                                                 "Save")))
                                     (:button :class "btn-default btn"
                                              :onclick
                                              (frmt
                                               "{$(\"#~a\").modal(\"hide\");~@[~a;~]}"
                                               (name widget)
                                               (and (typep grid 'grid)
                                                    (js-render (editor grid)
                                                               (js-pair "grid-name" (and grid (name grid)))
                                                               (js-pair "action" "cancel"))))
                                              "Cancel")))))))))
    (defer-js "$('#~a').modal({backdrop: 'static'})" (name widget))))
