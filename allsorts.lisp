(in-package :cl-sfx)

(defclass allsort-code ()
  ((sort-code :initarg :sort-code
	      :accessor sort-code
	      :initform nil
	      :db-type string
	      :key t
	      )
   (sort-order :initarg :sort-order
               :initform nil
               :accessor sort-order
	       :db-type string)
   (description :initarg :description
                :initform nil
                :accessor description
		:db-type string))
  (:metaclass data-versioned-class))

(defclass allsort (doc)
  ((sort :initarg :sort
         :initform nil
         :accessor allsort-sort
	 :db-type string
         :key t)
   (sort-value :initarg :sort-value
               :initform nil
               :accessor sort-value
	       :db-type string
               :key t)
   (sort-codes :initarg :sort-codes
	       :initform nil
	       :accessor sort-codes
	       :db-type (list allsort-code)))
  (:metaclass data-versioned-class)
  (:collection-name "allsorts")
  (:collection-type :system)
  (:default-initargs :top-level t))

(defun get-allsort (sort &key sort-code)
  (find-doc  (xdb2:get-collection (system-db) 'all-sort)
	     :test
	     (lambda (doc)
	       (and (equalp sort (allsort-sort doc))
		    (if sort-code				  
			(dolist (code (sort-codes doc))
			  (when (equalp sort-code (sort-code code))
			    (return (values doc code))))
			(values doc nil))))))

(defun add-allsort (sort sort-value &key sort-code sort-order description)
  (let* ((allsort (get-allsort sort :sort-code sort-code))
	 (sortx (car allsort))
	 (codex (cdr allsort)))

    (unless sortx
      (setf sortx (make-instance 'allsort :sort sort :sort-value sort-value)))
    
    (when sort-code
      (when sortx
	(setf (sort-order codex) sort-order)
	(setf (description codex) description))
      
      (unless sortx
	(setf codex (make-instance 'allsort-code
				   :sort-code sort-code
				   :sort-order sort-order
				   :description description)))
      (pushnew codex (sort-codes sortx)))
    (persist sortx)))

(defun find-allsorts-for-select (sort &key (sort-key #'sort-order))
  (map 'list (lambda (doc)
               (list (sort-value doc)
                     (description doc)))
       (find-allsorts sort :sort-key sort-key)))

(defun find-allsorts-for-validation (sort)
  (loop for allsort across (get-collection-docs 'all-sort)
        when (equalp (allsort-sort allsort) sort)
        collect (sort-value allsort)))

(defun find-allsorts (sort &key (sort-key #'sort-order))
  (let ((docs (find-docsx (lambda (doc)
			    (string-equal (allsort-sort doc) sort))
			  'all-sort)))
    (if sort-key
        (stable-sort docs (lambda (x y)
                            (cond
                              ((not (integerp y))
                               (integerp x))
                              ((not (integerp x))
                               nil)
                              (t (< x y))))
                     :key sort-key)
        docs)))
