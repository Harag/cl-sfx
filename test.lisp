(defun map-report* (report function)
   (labels ((tail-report (report-element function)
	      (let ((child-html ""))
		(dolist (child-element (elements report-element))
		  (setf child-html (concatenate 'string
						child-html
						(tail-report child-element function))))
		(funcall function report-element child-html))))
     (tail-report report function)))


(defun render-report-html (report-element children-html)
  (cond ((string-equal (element-type report-element) :report)
		  (sfx-eval (script report-element))
		  (with-html-string
		    (:div
		     (:h4 (str (label report-element)))
		     (:div
		      (str children-html)))))
		 ((string-equal (element-type report-element) :table)
		  (sfx-eval (script report-element))
		  (with-html-string
		    (:table :class "sfx-report-table"
		     (str children-html))))
		 ((string-equal (element-type report-element) :row)
		  (sfx-eval (script report-element))
		  (let ((data (if *cache* (gethash :data *cache*)))
			(html ""))
		    
		    
		    (setf html
			  (with-html-string
			    (:thead
			     (dolist (cell (elements report-element))
			       (setf html
				     (concatenate 
				      'string
				      html
				      (htm
				       (:td (str (label cell))))))))))
		    
		    (dolist (row data)
		      (let ((*data-row* row))
			(declare (special *data-row*))
			(setf html 
			      (concatenate 
			       'string
			       html
			       (with-html-string
				 (:tr
				  (let ((cell-html ""))
				    (dolist (cell (elements report-element))
				      (setf cell-html
					    (concatenate 
					     'string
					     cell-html
					     (htm
					      (:td (str (sfx-eval 
							 (script cell))))))))
				    cell-html)))))))
		    html))
		 ((string-equal (element-type report-element) :cell)
		  nil)))

(defun map-report (report)
  (let ((*cache* (make-hash-table :test 'equalp)))
    (declare (special *cache*))
   ;; (break "?? ~A" *cache*)
    (let ((*cache* (cache report)))    
      (map-report*
       report
       #'render-report-html))))


(defmacro shit (&body body)
  (break "~A" body)
  )
