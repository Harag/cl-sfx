(defsystem cl-sfx
  :name "cl-sfx"
  :version "0.1"
  :depends-on (:closer-mop :alexandria :ironclad :split-sequence :xdb2 
			   :hunchentoot :ht-simple-ajax :cl-who :cl-json 
			   :parse-number
			   :csv-parser
			   :cl-ftp
			   :dx-pdf
                           :vecto-graphs
			   :lparallel
			   :local-time)
  :serial t
  :components ((:file "package")
	       (:file "common")
	       (:file "mop")
	       (:file "sfx-script")
	       (:file "event-handler")
	       (:file "scheduler")
	       (:file "system")
	       (:file "allsorts")
	       (:file "entity")
	       (:file "module")
	       (:file "doc-specs")
	       
	       (:file "user")
	       (:file "session")
	       (:file "requests")
	       (:file "context")
	       (:file "event")  	       
	       (:file "theme")
	       (:file "widget")
               (:file "document")
               (:file "document-html")
               (:file "document-pdf")
	       (:file "report")
	       (:file "ajax")
	       (:file "js")
	       (:file "base-widgets")
	       (:file "html-events")
	       (:file "html-elements")
	       (:file "framework-widgets")
	       (:file "grid")
	       (:file "object-editor")
	       (:file "generic-grid")
	       (:file "system-widgets")
	       (:module "importer"
			:serial t
			:components 
			((:module "db"
				  :serial t
				  :components
				  ((:file "importer")))
			 (:file "common")
			 (:file "csv")
			 
			 (:file "importer-meta")
			 (:file "ftp")
			 (:file "class-wrappers")
			 (:file "importer-substitution")
			 (:file "importer-script")
			 (:file "importer-templates")
			 (:file "importer-replacements")
			 (:file "importer")
			 
			 (:file "new-importer")
			 
			 (:module "pages"
				  :serial t
				  :components
				  ((:file "importer"))))
			)
	       ))




