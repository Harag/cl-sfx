(in-package :cl-sfx)

(defclass doc-slot ()
  ((name :initarg :name 
         :initform nil
         :accessor name
	 :db-type string
	 :key t)
   (label :initarg :label
		 :initform nil
		 :accessor label
		 :db-type string
		 :header t)
   (db-type :initarg :slot-db-type
	      :initform nil
	      :accessor slot-db-type
	      :db-type string
	      :header t)
   (key :initarg :slot-key
        :initform nil
        :accessor slot-key
	:db-type boolean)
   (parser :initarg :slot-parser
           :initform nil
           :accessor slot-parser
	   :db-type script)
   (printer :initarg :slot-printer
            :initform nil
            :accessor slot-printer
	    ::db-type script)
   (valid :initarg :slot-valid
            :initform nil
            :accessor slot-valid
	    ::db-type script)
   (header :initarg :slot-header
           :initform nil
           :accessor slot-header
	   :db-type string)
   (required :initarg :required
             :initform nil
             :accessor required
	     :db-type boolean)
   (virtual :initarg :slot-virtual
            :initform nil
            :accessor slot-virtual
	    :db-type script)
   (validate :initarg :slot-validate
             :initform nil
             :accessor slot-validate
	     :db-type script))
  (:metaclass data-versioned-class))


(defun name-of-doc (doc)
  (typecase doc 
    (sfx-context-definition
     (context-name doc))
    (doc-spec
     (name doc))))

(defun create-class-name (doc)
  (proper-intern (name-of-doc doc) 
		 (or (find-package (doc-package doc)) *package*)))

(defun compile-class (class-name superclasses slots &rest class-options)
  "Define a class with all the bells and whistles on the fly." 
  (eval
     `(progn 
	(in-package ,(package-name (symbol-package class-name)))
	,(if superclasses
	     `(progn
		(defclass ,class-name ,superclasses 
		  (,@slots)
		  ,@class-options))
	     
	     `(defclass ,class-name ()
		(,@slots)
		,@class-options)))))

;;TODO: Find a better way to check for name classhes!!
(defun slot-accessor-name (name package)
  (if (find name (list 'entities 'modules 'sort))
      (proper-intern (frmt "~A-x" name)
		     package)
      (proper-intern (frmt "~A" name)
		     package)))

(defun create-slot (doc-slot package)
  (let ((slot-name (proper-intern (name doc-slot)
				  package)))
    `( ,slot-name
       :initarg ,(proper-intern (frmt "~A" (name doc-slot))	  
			  "KEYWORD")
	:initform nil
	:accessor ,(slot-accessor-name slot-name package)
	:printer ,(read-from-string-no-eval (cl-sfx::slot-printer doc-slot))
	:header ,(slot-header doc-slot)
	:valid ,(read-from-string-no-eval (slot-valid doc-slot))
	:virtual ,(slot-virtual doc-slot)
	:validate ,(read-from-string-no-eval (slot-virtual doc-slot))
	:parser ,(read-from-string-no-eval (cl-sfx::slot-parser doc-slot))
;;	:label 
	:db-type ,(read-from-string-no-eval (slot-db-type doc-slot))
	:key ,(slot-key doc-slot))))

(defun un-list (list)
  (if (or (listp list) (consp list))
      (car list)
      list))

(defun compile-import-wrapper-class (class &optional slots)
  (sfx-eval
   `(defclass ,(alexandria:symbolicate class '-import) (import-object ,class)
	,(append
	  slots
	  (loop for (slot . options) in (cdr (assoc class *import-slot-descriptions*))
	     when (getf options :key)
	     collect (list (intern (frmt "~a-~a" 'import-old slot))
			   :initform nil)))
       (:metaclass data-versioned-class)))
  (setf *import-classes* 
	(pushnew `(,(frmt "~A" class) ,class) *import-classes*))
 ;; (break "~A" *import-classes*)
  )



(defun create-class (spec)
  (if (equalp (name spec) 'doc-spec)
      (find-class (name spec))
      (let ((superclasses (spec-superclasses spec))
	    (class)
	    (slots)
	    (class-name (create-class-name spec))) 

	(dolist (slot (doc-slots spec))
	  (pushnew (create-slot slot (find-package (doc-package spec))) slots))   
	
	(setf class (compile-class class-name 
				   superclasses 
				   slots 
				   `(:metaclass ,(proper-intern (doc-class spec) 'cl-sfx))
				   `(:collection-name ,(un-list (collection-name-x spec)))
				   `(:collection-type ,(un-list (collection-type-x spec)))
				   `(:default-initargs :top-level ,(top-level-p spec))
				   `(:before-persist 
				     ,(read-from-string-no-eval 
				       (before-persist-event-x spec)))
				   `(:after-persist 
				     ,(read-from-string-no-eval 
				       (after-persist-event-x spec)))))
	

      
	(ensure-finalized class)
	
	;;TODO: Add check for importer-p
;;	(compile-import-wrapper-class class-name)
	class)))

(defun check-top (from-class)
  (top-level (make-instance from-class)))

(defun set-spec-slot-values (spec-slot class-slot)
  (dolist (slot-slot (class-slots (class-of class-slot)))
    (let ((slot-slot-name (slot-definition-name slot-slot)))
      (dolist (spec-slot-slot (class-slots (class-of spec-slot)))
	(let ((spec-slot-name (slot-definition-name  spec-slot-slot)))
	  (when (equalp (frmt "~A" spec-slot-name) (frmt "~A" slot-slot-name))
	    (setf (slot-value spec-slot spec-slot-name) 
		  ;; (frmt "~A" (slot-value class-slot slot-slot-name))
		   (slot-value class-slot slot-slot-name))))))))

(defun make-doc-slots-from (class)
  (c2cl:ensure-finalized class)
  (c2cl:ensure-finalized (find-class 'doc-slot))
  
  (let ((slots))
    (dolist (slot (class-slots class))
      (let ((slot-name (slot-definition-name slot))
	    (doc-slot))
	;;TODO: find a better way to exclude super class slots
	(when (not (find  slot-name '(collection-name collection collection-type deleted 
				      effective-date stamp-date id log-action old-versions
				      top-level user version written top-version)
			  :test 'equalp))
	  (setf doc-slot (make-instance 'doc-slot
					:name slot-name
					:label (if (stringp (header slot))
						   (header slot)
						   (make-name slot-name))
					:slot-db-type (db-type slot)
					:slot-key (xdb2::key slot)
					:slot-printer (frmt "~A" 
							    (xdb2::slot-printer slot))))
	  (set-spec-slot-values doc-slot slot)
	  
	  (pushnew doc-slot slots))))
   
    slots))

(defun get-doc-spec (name)
  (find-docx 'doc-spec
		  :test (lambda (doc)
			  (string-equal name 
					(name doc)))))

(defun create-or-find-class (doc-spec class-name)
  (let ((class)
	(class-name (proper-intern class-name))
	(doc-spec (or doc-spec (get-doc-spec class-name))))
    
    (when doc-spec 
      
      (setf class-name (create-class-name doc-spec))
      (setf class (create-class doc-spec))
   ;;   (break "found doc ~A ~A" class *package*)
      )
    
    (unless doc-spec
      (setf class (and class-name
		       (find-class class-name) 
		       (ensure-finalized (find-class class-name))))
      
  ;;    (break "not found doc ~A ~A" class *package*)

      (setf doc-spec (make-instance 'doc-spec :name class-name
				    :doc-package (package-name (symbol-package class-name))
				    :license (sys-license) 
				    :classes (class-name 
					      (first 
					       (last 
						(class-direct-superclasses 
						 class))))
				    :top-level-p (check-top 
						  class-name)
				    :collection-name-x (collection-name class)
				    :collection-type-x (collection-type class)
				    :doc-class (type-of class)
				    :doc-slots nil))
      (setf (doc-slots doc-spec) (make-doc-slots-from (find-class class-name)))
      
      (setf (after-persist-event-x doc-spec) (after-persist-event class))

      (persist doc-spec))
    
    doc-spec))

(defun create-context-definition (doc-spec context-name 
				  &key (for-everyone t) permissions
				    body)
  (let ((def (or
	      (get-context-def (frmt "~A" context-name))
	      (persist (make-instance 
			'sfx-context-definition 
			:license (sys-license)
			:context-name (frmt "~A" context-name)
			:url (plural-name-string
			      (create-class-name doc-spec))
			:for-everyone for-everyone
			:permissions (parse-permissions permissions)
			:from-class (create-class-name doc-spec)
			:doc-spec doc-spec
			:body body)))))
      
  
    ;;TODO: Add code to handle class name changes!!!!
    ;;  (break "~A" (from-class def))
    ;;TODO: Create a function that does this from def object or from-class
      
    (when def
      (eval (load-context-def def *sfx-system*)))))

(defclass doc-spec (license-doc)
  ((name :initarg :name 
         :initform nil
         :accessor name
         :key t
	 :db-type string)
   (doc-package :initarg :doc-package 
         :initform "CL-SFX"
         :accessor doc-package
         :key t
	 :db-type string)
   (top-level-p :initarg :top-level-p
	       :initform nil
	       :accessor top-level-p
	       :db-type boolean)
   (collection-name-x :initarg :collection-name-x
		    :initform nil
		    :accessor collection-name-x
		    :db-type string)
   (collection-type-x :initarg :collection-type-x
		    :initform nil
		    :accessor collection-type-x
		    :db-type string)
   
   (doc-class :initarg :doc-class
	    :accessor doc-class
	    :initform 'data-versioned-class
	    :db-type string)
   
   (classes :initarg :classes
	    :accessor classes
	    :initform nil
	    :db-type (member doc entity-doc license-doc date-doc))
   (import-doc-p :initarg :import-doc-p
		 :initform nil
		 :accessor import-doc-p
		 :db-type boolean)
   (doc-slots :initarg :doc-slots
	      :initform nil
	      :accessor doc-slots
	      :db-type (list doc-slot))
   (before-persist-event :initarg :before-persist-event
		  :accessor before-persist-event-x
		  :initform nil
		  :db-type script)
   (after-persist-event :initarg :after-persist-event
		  :accessor after-persist-event-x
		  :initform nil
		  :db-type script))
  (:metaclass data-versioned-class)  
  (:after-persist #'(lambda (doc)	
		      (when doc
			;; (break "~a" doc)
			(create-or-find-class doc nil)		
			(create-context-definition doc (name doc)))))
  (:collection-name "doc-specs")
  (:collection-type :merge)
  (:default-initargs :top-level t))

(defun yes-p (val)
  (equalp val "YES"))

(defun proper-concat (list)
  (let ((strip-list (mapcar #'strip-name list)))
    (format nil "~{~a~^-~}" strip-list)))

(defun spec-superclasses (spec)
  ;;TODO: what happens when we want to use existing classes as the base?
  (if (yes-p (top-level-p spec)) 
			  (list 'entity-doc 'doc)
			  '()))


