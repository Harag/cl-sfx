(in-package :cl-sfx)

;;NOTE to SELF: If anything in the code/design looks over complicated ask yourself it makes running multiple systems and system instances possible!!!!

;;NOTE to SELF: Be clear in code about what is a method that directly affects an object and 
;;an event that has to be handled by the object, events could use direct methods though but that just makes the code less clear about its purpose.!!!



;;TODO: Consider system logging of user actions and non user actions. User actions can go around widget event handlers. How to get current user?
;;TODOO Consider how logging of db/data related actions can be used for rollbacks!!!Add function to see user actions.


;;TODO: System messages to communicate with non active users. Emails? Messages logged while ofline?
;;TODO: Users to be able to comm with system regarding system messages?
;;TODO: Users comms with each other?
;;TODO: Users to comms with system? In system and out of system? Mails, sms and restless api.
;;TODO: Restless api integrated into core? When creating event-handlers create api calls to accomplish same job?
;;TODO: Add script-class to log allowed functions. Add allowed functions to script class as well to dynamically add or remove access



(defclass sfx-data (dbs)
  ((data-folder :initarg :data-folder
		   :accessor data-folder
		   :initform "~~/xdb2/")) 
  (:documentation "Class representing data object. 
Used: load-sys-data, unload-sys-data."))

(defgeneric load-sys-data (sfx-data &key &allow-other-keys)
  (:documentation "Method to be used to initialize any data objects on system startup.
Used : start-sys :after."))

(defgeneric unload-sys-data (sfx-data &key &allow-other-keys)
  (:documentation "Method to be used to clean up any data objects on system shutdown. 
Used: stop-sys :before"))

(defmethod unload-sys-data ((data sfx-data) &key &allow-other-keys))

;;Could apply per session
(defclass sfx-comms ()
  ((message-id)
   (can-reply)))

;;Could apply per session
(defclass sfx-lock ()
  ((id)
   (start)
   (end)
   (comms)
   (actions
    :documentation "script and/or list of events to fire?")))

(defclass sfx-system (easy-acceptor)
  ((system-name :initarg :system-name
		:accessor system-name
		:initform nil)  
   (system-status :initarg :system-status
		  :accessor system-status
		  :initform nil
		  :documentation ":initialized :stopped :started :locked")
   (system-folder :initarg :system-folder
		  :accessor system-folder
		  :initform "/sfx-system/")
   (sfx-web-folder :initarg :sfx-web-folder
		  :accessor sfx-web-folder
		  :initform "/Development/cl-sfx/")
   (system-web-folder :initarg :system-web-folder
		  :accessor system-web-folder
		  :initform "")
   (licenses :initarg :licenses
	     :accessor licenses
	     :initform nil)
   (system-data :initarg system-data
	     :accessor system-data
	     :initform nil)
   (system-package :initarg :system-package
		   :accessor system-package
		   :initform nil)
   
   (context-definitions :initarg :context-definitions
			:accessor context-definitions
			:initform (make-hash-table :test #'equalp))
   (modules :initarg :modules
	    :accessor system-modules
	    :initform nil)
   (sessions :initarg :sessions
	     :accessor sessions
	     :initform (make-hash-table :test #'equalp))
   (site-url :initarg :site-url
             :initform (error "site-url should be provided")
             :accessor site-url
	     :documentation "Not the same as hunchentoot acceptor address! Must be of the form  /my-system/, 
it is used to differentiate systems in hunchentoot parlance because hunchentoot 
does not have vhosts by default.")
   (ajax-processor :initarg :ajax-processor
		   :accessor ajax-processor
		   :initform nil)
   (ajax-url :initarg :ajax-url           
             :accessor ajax-url
	     :initform nil
	     :documentation "Must be of the form /my-system/ajax/")
   (image-url :initarg :image-url
	      :accessor :image-url
	      :initform "/images")
   (tmp-directory :initarg :tmp-directory
		  :accessor :tmp-directory
		  :initform #p"~/hunchentoot-upload/")
   (image-processor :initarg :image-processor
		    :accessor image-processor
		    :initform nil)
   (login-url :initarg :login-url
	      :accessor login-url
	      :initform nil)
   (debug-errors-p :initarg :debug-errors-p
		   :accessor debug-errors-p
		   :initform nil)
   (page-permissions :initarg :page-permissions		     
		     :accessor page-permissions
		     :initform nil)
   (default-context :initarg :default-context
     :accessor default-context
     :initform nil)
   (logging-p :initarg :logging-p
              :initform nil
              :accessor logging-p)
   ;;??????
   (locks))
  (:documentation "This is an conceptual framework to help package the functionality 
of an application. Most systems with a ui have the same basic structure/needs."))

(defmacro with-system (system &body body)
  :documentation "Makes *sfx-system* available."
  `(let ((*sfx-system* ,system))
     ,@body))

(defmacro define-ajax (system name lambda-list &body body)
   `(ht-simple-ajax:defun-ajax ,name ,lambda-list ((ajax-processor ,system))
       ,@body))


;;TODO: How to replace global with tmp-directory from system in image-dispathcer
(defparameter *tmp-directory* #p"~/hunchentoot-upload/")

(defun %image-processor ()
  (let ((uri (url-decode (request-uri*))))
    (cond ((and
            (alexandria:starts-with-subseq "/umage/images" uri)
            (handle-static-file (merge-pathnames (subseq uri (length "/umage/")) *tmp-directory*))))
          (t
           (setf (return-code*) +http-not-found+)
           (abort-request-handler)))))

#|



;;TODO: Cant find any place where *image-dispatcher* is used, it is not a hunchentoot thingy either????

(defvar *image-dispatcher*
  (car (push
        (create-prefix-dispatcher "/umage/images" #'image-processor)
        *dispatch-table*)))
|#

;;TODO: Does this have to be a function because it gets passed to the prefix-dispatcher
;;or can we make it a method and specialize on sfx-system??
(defun ajax-call-lisp-function (system processor)
  "This is called from hunchentoot on each ajax request. It parses the
   parameters from the http request, calls the lisp function and returns
   the response."
  
  (let* ((*sfx-system* system)
	 (fn-name (string-trim "/" (subseq (script-name* *request*)
                                           (length (ht-simple-ajax::server-uri processor)))))
         (fn (gethash fn-name (ht-simple-ajax::lisp-fns processor)))
         (args (mapcar #'cdr (get-parameters* *request*))))
    (unless fn
      (error "Error in call-lisp-function: no such function: ~A" fn-name))

    (setf (reply-external-format*) (reply-external-format processor))
    (setf (content-type*) (content-type processor))
    (no-cache)
    
    ;;TODO: Theming ... (theme site)..(theme system)
    
    
    (let ((*current-theme* nil))
      (declare (special *current-theme*))
      (apply fn args))
    
    
    ))

;;TODO: The only reason why system is passed to ajax-call-lisp-function is for the theme???
(defun create-ajax-dispatcher (system processor)
  "Creates a hunchentoot dispatcher for an ajax processor"
  (create-prefix-dispatcher (ht-simple-ajax::server-uri processor)
                            (lambda () (ajax-call-lisp-function system processor))))

(defun assert-ends-with-/ (string)
  (assert (char= (alexandria:last-elt string) #\/)))



  
(defgeneric init-sys-data (sfx-system &key &allow-other-keys)
  (:documentation "Initialize data storage for system.
Notes:
This is called on initialize-instance :after for sfx-system."))

(defmethod init-sys-data ((system sfx-system) &key &allow-other-keys)
  (let* ((data-folder (format nil "~~/~A/xdb2/" 
			      (system-folder system)))
	 (data (make-instance 'sfx-data
			      :base-path data-folder
			      :data-folder data-folder)))
    
    (ensure-directories-exist (data-folder data))
    (add-db data (list (frmt "~A" (strip-name (system-name *sfx-system*)))
		       (current-license-code)))
    (setf (system-data *sfx-system*) 
	  data)
    (load-collections (system-db))
    ))


(defgeneric init-system (sfx-system &key &allow-other-keys)
  (:documentation "Method to be used to implement system initialization tasks.
Notes:
This is called on initialize-instance :after for sfx-system."))

(defmethod  init-system ((system sfx-system) &key &allow-other-keys)
  (unless (system-folder system)
    (setf (system-folder system) (format nil "~~/~A" (strip-name (system-name system))))
    (ensure-directories-exist (system-folder system))))

(defmethod initialize-instance :after ((system sfx-system) &rest args)  
  (declare (ignore args))
  :documentation "Dont mess with this just use init-system and init-sys-data effectively."
  (let ((*sfx-system* system))
    (init-system system)   
    (init-sys-data system))
  (setf (system-status system) :initialized))

(defgeneric start-sys (sfx-system &key &allow-other-keys)
  (:documentation "Method to be used to implement system startup tasks.
Notes:
Must be called by the framework implementer to get a system going.
Calls in order:
 load-sys-data
 load-context-definitions
 load-modules"))

(defgeneric load-modules (sfx-system &key &allow-other-keys))

(defmethod load-modules ((system sfx-system) &key &allow-other-keys))

(defun get-context-def (context-name)
  (find-docx 'sfx-context-definition
	     :test (lambda (doc)
		     (unless (typep doc 'sfx-context-definition)
		       (break "You broke something to do with collection of doc!!!"))
		     (when (typep doc 'sfx-context-definition)
		       (string-equal context-name (context-name doc))))))

(defun make-menu-item (name context-name)
  (make-instance 
   'menu-item
   :item-name name
   :context-definition 
   (get-context-def context-name)))

(defmethod load-modules :around ((system sfx-system) &key &allow-other-keys)
  (let ((sys-mod (find-docx 'module
			    :test (lambda (doc)
				    (string-equal "System Admin" (module-name doc))))))
    (unless sys-mod
      (setf sys-mod
	    (make-instance 
	     'module :module-name "System Admin"
	     :menu (list (make-instance 
			  'menu :menu-name "System")))))
    (setf (contexts sys-mod)
	  (list
	   (get-context-def "theme")
	   (get-context-def "allsort")
	   (get-context-def "sfx-script")
	   (get-context-def "repl")
	   (get-context-def "doc-spec")
	   (get-context-def "sfx-context-definition")
	   (get-context-def "report")
	   (get-context-def "report-view")
	   (get-context-def "module")
	   (get-context-def "sfx-license")
	   (get-context-def "user")
	   (get-context-def "import-data")
	   ;;(get-context-def "----------" "-")	
	   ))
    
    
    (setf (menu-items (first (menu sys-mod)))
	  (loop for def in (contexts sys-mod)
	       when def
	     collect (make-menu-item (make-name (plural-name (context-name def))) 
				     (context-name def))))

    ;;TODO: Persist only if nothing has changed!!!!!This is racking up to many 
    ;; versions!!!!!
    (persist sys-mod)))



(defgeneric load-context-definitions (sfx-system &key &allow-other-keys))

(defmethod start-sys :after ((system sfx-system) &key &allow-other-keys)
  (let ((*sfx-system* system))
    (declare (special *sfx-system*))
    
    (sys-admin-license "000000")
    
    (load-context-definitions system)
    
    (load-modules system)
    
    (setf (system-status system) :started)))

;;TODO: Maybe move check for system already started to start-sys :before
(defmethod start-sys ((system sfx-system) &key &allow-other-keys)
  (when (or (not (system-status system)) 
	    (equalp (system-status system) :initialized) 
	    (equalp (system-status system) :stopped) )
    
    (assert-ends-with-/ (site-url system))
    
    (hunchentoot:start system)
    
    (let* ((ajax-processor
	    (make-instance 'ht-simple-ajax:ajax-processor
			   :server-uri (frmt "~Aajax" (site-url system))))
	   (ajax-prefix-dispatcher
	    (create-ajax-dispatcher system ajax-processor))
	   (image-processor (create-prefix-dispatcher 
			     (frmt "~Aimages" (site-url system)) #'%image-processor))
	   (sfx-file-dispatcher (create-folder-dispatcher-and-handler
			     (frmt "~Aweb/" (site-url system)) 
			     (sfx-web-folder system)))
	   (file-dispatcher (create-folder-dispatcher-and-handler
			     (frmt "~Asys-web/" (site-url system)) 
			     (system-web-folder system)))
	   )
      
      (pushnew ajax-prefix-dispatcher *dispatch-table*)
      (pushnew image-processor *dispatch-table*)
      (pushnew sfx-file-dispatcher *dispatch-table*)
      (pushnew file-dispatcher *dispatch-table*)
      
      (setf (ajax-processor system) ajax-processor)
      (setf (image-processor system) image-processor)))
  system)


(defgeneric stop-sys (sfx-system &key &allow-other-keys)
  (:documentation "Method to be used to implement system cleanup tasks.
Notes:
Calls unload-sys-data to clean up data."))

(defmethod stop-sys ((system sfx-system) &key &allow-other-keys))

(defmethod stop-sys :before ((system sfx-system) &key &allow-other-keys)
  (unload-sys-data (system-data system)))

(defmethod stop-sys :after ((system sfx-system) &key &allow-other-keys)
  (setf (system-status system) :started))


;;TODO: How to force lock, how to stop users or more by system lock so that only admin can perform tasks on the system?"
(defgeneric sfx-lock (sfx-system &key &allow-other-keys)
  (:documentation "Implement system lock tasks."))

(defmethod sfx-lock ((system sfx-system) &key &allow-other-keys))

(defmethod sfx-lock :after ((system sfx-system) &key &allow-other-keys)
  (setf (system-status system) :locked))

(defgeneric sfx-unlock (sfx-system &key &allow-other-keys)
  (:documentation "Implement system unlock tasks."))

(defmethod sfx-unlock ((system sfx-system) &key &allow-other-keys))

(defmethod sfx-unlock :after ((system sfx-system) &key &allow-other-keys)
  (setf (system-status system) :started))

