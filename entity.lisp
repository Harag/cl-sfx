(in-package :cl-sfx)

(defclass entity (license-doc)
  ((entity-name :initarg :entity-name 
                :initform nil
                :accessor entity-name
		:db-type string
                :key t)
   (entity-type :initarg :entity-type
		:accessor entity-type
		:initform nil
		:db-type string)
   (children :initarg :children
	     :accessor children
	     :initform nil
	     :db-type (list entity)))
  
  (:metaclass data-versioned-class)
  (:collection-name "entities")
  (:collection-type :license)
  (:default-initargs :top-level t))


(defun print-entity-name (doc)
  (typecase doc
    (entity
     (entity-name doc))
    (entity-doc
     (and (entity doc)
          (entity-name (entity doc))))))

(defclass entity-doc ()
  ((entity :initarg :entity
           :initform nil
           :accessor entity
           :db-type (data-member entity)
           :key t
           :header t
	   :required t
           :printer print-entity-name))
  (:metaclass data-mixin))


(defgeneric match-context-entities (doc))

(defmethod match-context-entities ((doc t))
  t)



(defun get-license-entity (license entity-name)
   (labels ((tail-entity (entities entity-name)
	      (dolist (entity entities)
		(if (string-equal entity-name (entity-name entity))
		  (return-from get-license-entity entity)
		  (tail-entity (children entity) entity-name)))))
     (tail-entity (license-entities license) entity-name)))

(defun get-entity (entity-name)
  (get-license-entity  (license (user (current-user)))
		      entity-name))

(defun match-entity (object)
  #|(and (typep object 'entity-doc)
       (assoc (entity object) (request-page-permissions *current-page*))
       t)
  |#
  object
  )

(defmethod match-context-entities ((doc entity-doc))
  (match-entity doc))
