(in-package :cl-sfx-doc)

(defvar *header-color* "4f4c4c")
(defvar *header-text-color* "ffffff")
(defvar *current-elements* nil)

(defclass document-element ()
  ())

(defclass document (document-element)
  ((elements :initarg :elements
             :initform nil
             :accessor elements)))

(defclass orientation (document-element)
  ((orientation :initarg :orientation
                :initform :previous
                :accessor orientation
                :type (member :portrait :landscape
                              :previous :next))))

(defclass paragraph (document-element)
  ((elements :initarg :elements
             :initform nil
             :accessor elements)
   (h-align :initarg :h-align
            :initform :center
            :accessor h-align)
   (top-margin :initarg :top-margin
               :initform 0
               :accessor top-margin)
   (bottom-margin :initarg :bottom-margin
                  :initform 0
                  :accessor bottom-margin)))

(defclass text (document-element)
  ((text :initarg :text
         :initform nil
         :accessor text)
   (style :initarg :style
          :initform nil
          :accessor style
          :type '(member nil :italic))
   (weight :initarg :weight
           :initform nil
           :accessor weight
           :type '(member nil :bold))
   (size :initarg :size
         :initform nil
         :accessor size)))

(defclass link (text)
  ((url :initarg :url
        :initform nil
        :accessor url)
   (target :initarg :target
           :initform nil
           :accessor target)))

(defclass popover (link)
  ((content :initarg :content
            :initform nil
            :accessor content)))

(defclass heading (text)
  ((size :initarg :size
         :initform 1
         :accessor size
         :documentation "From 1-6, like h1-h6 in HTML")))

(defvar *table-title* nil)

(defclass table (document-element)
  ((title :initarg :title
          :initform *table-title*
          :accessor title)
   (rows :initarg :rows
         :initform nil
         :accessor rows)
   (class :initarg :class
          :initform "quickview_table"
          :accessor html-class)
   (widths :initarg :widths
           :initform nil
           :accessor widths)))

(defclass row (document-element)
  ((cells :initarg :cells
          :initform nil
          :accessor cells)))

(defclass cell (document-element)
  ((text :initarg :text
         :initform nil
         :accessor text)
   (row-span :initarg :row-span
             :initform 1
             :accessor row-span)
   (col-span :initarg :col-span
             :initform 1
             :accessor col-span)
   (bg-color :initarg :bg-color
             :initform nil
             :accessor bg-color)
   (text-color :initarg :text-color
               :initform nil
               :accessor text-color)
   (v-align :initarg :v-align
            :initform :center
            :accessor v-align)
   (h-align :initarg :h-align
            :initform :center
            :accessor h-align)
   (css-class :initarg :css-class
              :initform nil
              :accessor css-class)
   (id :initarg :id
       :initform nil
       :accessor id) 
   (vertical-text :initarg :vertical-text
                  :initform nil
                  :accessor vertical-text)
   (title :initarg :title
          :initform nil
          :accessor title)))

(defun print-text (object stream text)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~@[~a~]" text)))

(defmethod print-object ((cell cell) stream)
  (print-text cell stream (text cell)))

(defmethod print-object ((text text) stream)
  (print-text text stream (text text)))

(defmethod print-object ((paragraph paragraph) stream)
  (print-unreadable-object (paragraph stream :type t :identity t)
   (format stream "~{~a~^ ~}" (elements paragraph))))

(defclass header (cell)
  ()
  (:default-initargs :bg-color *header-color*
                     :text-color *header-text-color*))

(defclass html (document-element)
  ((html-only :initarg :html-only
              :initform t
              :accessor html-only)
   (string :initarg :string
           :initform nil
           :accessor rendered-string)))

(defclass pdf (document-element)
  ((code :initarg :code
         :initform nil
         :accessor pdf-code)
   (elements :initarg :elements
             :initform nil
             :accessor elements)))

(defclass new-page (document-element)
  ())

(defclass new-line (document-element)
  ())

(defmethod text ((new-line new-line))
  "
")

(defclass image (document-element)
  ((path :initarg :path
         :initform nil
         :accessor path)
   (width :initarg :width
          :initform nil
          :accessor width)
   (height :initarg :height
           :initform nil
           :accessor height)))
;;;

(defclass graph (document-element)
  ((width :initarg :width
          :initform nil
          :accessor width)
   (height :initarg :height
           :initform 400
           :accessor height)
   (max-y :initarg :max-y
          :initform nil
          :accessor max-y)
   (title :initarg :title
          :initform nil
          :accessor title)
   (data :initarg :data
         :initform nil
         :accessor data)
   (id :initarg :id
       :initform nil
       :accessor id)
   (grid-width :initarg :grid-width
               :initform 12
               :accessor grid-width)
   (remove-zero :initarg :remove-zero
                :initform nil
                :accessor remove-zero)
   (legend :initarg :legend
           :initform t
           :accessor legend)))

(defclass line-graph (graph)
  ((x-label :initarg :x-label
            :initform nil
            :accessor x-label)
   (y-label :initarg :y-label
            :initform nil
            :accessor y-label))
  (:default-initargs :id "line-graph"))

(defclass bar-graph (graph)
  ((labels :initarg :labels
                 :initform nil
                 :accessor graph-labels)
   (x-label :initarg :x-label
            :initform nil
            :accessor x-label)
   (y-label :initarg :y-label
            :initform nil
            :accessor y-label)
   (percentage :initarg :percentage
               :initform nil
               :accessor percentage)
   (stacked :initarg :stacked
            :initform nil
            :accessor stacked)
   (orientation :initarg :orientation
                :initform :horizontal
                :accessor orientation))
  (:default-initargs :id "bar-graph"))

(defclass bar-line-graph (bar-graph)
  ((line-data :initarg :line-data
              :initform nil
              :accessor line-data)
   (line-label :initarg :line-label
               :initform nil
               :accessor line-label))
  (:default-initargs :id "bar-line-graph"))

(defclass pie-chart (graph)
  ((breakdown :initarg :breakdown
              :initform nil
              :accessor breakdown))
  (:default-initargs :id "pie-chart"))

(defun line-graph (title data &rest args
                   &key . #1=(labels width height max-y id grid-width remove-zero
                              x-label y-label legend))
  (declare (ignore . #1#))
  (add (apply #'make-instance 'line-graph :title title :data data args)))

(defun bar-graph (title data &rest args
                  &key . #1=(labels width height max-y id grid-width remove-zero
                              x-label y-label percentage legend stacked orientation))
  (declare (ignore . #1#))
  (add (apply #'make-instance 'bar-graph :title title :data data args)))

(defun bar-line-graph (title data line-data &rest args
                  &key . #1=(labels line-label width height max-y id grid-width remove-zero
                              x-label y-label percentage legend stacked orientation))
  (declare (ignore . #1#))
  (add (apply #'make-instance 'bar-line-graph
              :title title :data data :line-data line-data args)))

(defun pie-chart (title data &rest args
                  &key . #1=(width height id grid-width breakdown))
  (declare (ignore . #1#))
  (add (apply #'make-instance 'pie-chart :title title :data data args)))

;;;

(defun cell (text &rest args &key row-span col-span bg-color text-color vertical-text
                                  h-align v-align
                                  css-class id title)
  (declare (ignore row-span col-span bg-color vertical-text h-align v-align
                   css-class id text-color title))
  (apply #'make-instance 'cell :text text args))

(defun header (text &rest args &key url row-span col-span bg-color text-color vertical-text
                                    h-align v-align
                                    css-class id)
  (declare (ignore url row-span col-span bg-color text-color vertical-text h-align v-align
                   css-class id))
  (apply #'make-instance 'header :text text args))

(defun table (&rest rows)
  (make-instance 'table :rows rows))

(defun row (&rest cells)
  (make-instance 'row :cells cells))

(defun document (&rest elements)
  (make-instance 'document :elements elements))

(defun paragraph (text &rest args &key elements h-align
                                       top-margin bottom-margin)
  (declare (ignore h-align top-margin bottom-margin))
  (apply #'make-instance 'paragraph
         :elements (if text
                       (cons (make-instance 'text :text text) elements)
                       elements)
         args))

(defun heading (text &rest args &key size)
  (declare (ignore size))
  (apply #'make-instance 'heading :text text args))

(defun make-text (string &rest args &key size style weight)
  (declare (ignore size style weight))
  (apply #'make-instance 'text :text string args))

(defun make-link (text &optional url)
  (make-instance 'link :text text
                       :url url))

(defun image (path width)
  (make-instance 'image :path path :width width))

(defmacro with-add-html ((&key (html-only t)) &body body)
  `(add (make-instance 'html
                       :html-only ,html-only
                       :string
                       (with-html-string
                         ,@body))))

(defmacro with-add-pdf (&body body)
  `(add (make-instance 'pdf
                       :code
                       (lambda () ,@body))))

(defun filter-result (value)
  ;; (if (bogus-result-p value)
  ;;     "N/A"
  ;;     (html-to-document value))
  value)

(defun rows-from-alist (alist)
  (loop for (a b) in alist
        do (add (row (cell a)
                  (cell (filter-result b))))))

(defun new-page ()
  (make-instance 'new-page))

(defun new-line ()
  (make-instance 'new-line))

(defun html (string &rest args &key html-only)
  (declare (ignore html-only))
  (apply #'make-instance 'html :string string
         args))

(defun multiline-text (&rest args)
  (loop for (arg . rest) on args
        when arg
        collect arg
        when rest
        collect (new-line)))

;;;

(defun spanned-row (row)
  (loop for cell in (cells row)
        append (make-list (col-span cell)
                          :initial-element cell)))

(defun spanned-rows (table)
  (mapcar #'spanned-row (rows table)))

(defun table-columns (table)
  (let* ((rows (spanned-rows table))
         (columns-number (reduce #'max rows :key #'length
                                            :initial-value 0))
         (rows-length (length rows)))
    (loop repeat columns-number
          collect
          (loop for i = 0 then (+ i (if cell
                                        (row-span cell)
                                        1))
                while (< i rows-length)
                for cell = (pop (nth i rows))
                when cell
                collect cell))))

(defgeneric render-element (object type))
(defgeneric element-type (object))

(defgeneric make-color (spec type))

(defgeneric h-alignment (spec type))
(defgeneric v-alignment (spec type))

(defgeneric process-text-style (text type))

(defun render-objects (objects type)
  (loop for object in (alexandria:ensure-list objects)
        do (typecase object
             (cons
              (render-objects object type))
             (t
              (render-element object type)))))

(defmethod render-element ((symbol symbol) type)
  (render-element (string symbol) type))

(defmethod render-element ((string string) type)
  (render-element (make-instance 'text :text string) type))

(defmethod render-element ((number number) type)
  (render-element (make-instance 'text :text (princ-to-string number))
                  type))

(defmethod render-element ((none null) type))

(defmethod render-element :around ((element document-element) type)
  (let* ((*current-elements* (or *current-elements*
                                 (make-hash-table :test #'eq)))
         (type (element-type element)))
    (push-element type element)
    (call-next-method)
    (pop-element type)))

(defmethod element-type (object)
  (type-of object))

(defmethod element-type ((object document))
  'document)

;;;

(defun current-element (type)
  (gethash type *current-elements*))

(defun add-element (type element)
  (setf (gethash type *current-elements*) element))

;;;

(defun push-element (type element)
  (push element (gethash type *current-elements*)))

(defun pop-element (type)
  (pop (gethash type *current-elements*)))

(defun top-element (type)
  (car (gethash type *current-elements*)))

(defmacro with-element ((type element) &body body)
  (let ((type-symbol (gensym "TYPE"))
        (element-symbol (gensym "ELEMENT")))
    `(let ((,type-symbol ,type)
           (,element-symbol ,element))
       (unwind-protect
            (progn
              (push-element ,type-symbol ,element-symbol)
              ,@body
              ,element-symbol)
         (pop-element ,type-symbol)))))

(defvar *orientations*)

(defun add-orientation (orientation)
  (let* ((current-orientation (car *orientations*))
         (orientation (cond ((not current-orientation)
                             (make-instance 'orientation :orientation :portrait))
                            ((eq (orientation current-orientation)
                                 orientation)
                             current-orientation)
                            ((and (eq (orientation current-orientation) :next)
                                  (eq orientation :previous))
                             current-orientation)
                            ((eq (orientation current-orientation) :next)
                             (setf (orientation current-orientation) orientation)
                             current-orientation)
                            (t
                             (make-instance 'orientation :orientation (or orientation :portrait))))))
    (unless (eq current-orientation orientation)
      (add orientation)
      (push orientation *orientations*))
    orientation))

(defmacro with-document ((&key (orientation :previous)) &body body)
  `(cond (*current-elements*
          (add-orientation ,orientation)
          ,@body)
         (t
          (let ((*current-elements*
                  (or *current-elements* (make-hash-table :test #'eq)))
                (*orientations* (and (boundp '*orientations*)
                                     *orientations*)))
            (with-element ('document (make-instance 'document))
              (add-orientation ,orientation)
              ,@body)))))

(defmacro with-table ((&whole args &key class title) &body body)
  (declare (ignore class title))
  `(progn
     (add (make-instance 'table ,@args))
     ,@body))

(defgeneric add (object))

(defmethod add ((element document-element))
  (add-element (type-of element) element)
  (alexandria:appendf (elements (top-element 'document))
   (list element))
  element)

(defmethod add ((table table))
  (add-element 'table table)
  (alexandria:appendf (elements (top-element 'document))
                      (list table))
  table)

(defmethod add ((row row))
  (add-element 'row row)
  (alexandria:appendf (rows (current-element 'table))
                      (list row))
  row)

(defmethod add ((cell cell))
  (alexandria:appendf (cells (current-element 'row))
   (list cell))
  cell)
