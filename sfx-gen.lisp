(in-package :cl-sfx)

(defun instance-class-name (object)
  (class-name (class-of object)))

(defun instance-super-class (object)
  (first (class-direct-superclasses (class-of object))))

(defun instance-super-class-name (object)
  (class-name (instance-super-class object)))

(defun type-arg (type argname)
  (if type 
      `(,argname ,type)
      `,argname))

(defun read-with-package (system code)
  (let ((*package* (system-package system)))
    (if (stringp code)
	(read-from-string code)
	code
	)))

(defun % (symbol)
  (break "~A" *package*)
  (intern (if (stringp symbol)
	      (string-upcase symbol)
	      (symbol-name symbol)
	      )
	  *package*))


(defgeneric code-gen-event (system group-name event &key type index))

(defmethod code-gen-event ((system sfx-system) group-name event &key type index)
  (let ((code (code-gen-event system event :type type)))
    (add-event system group-name event index)
    (eval-code system code)))

#|
(defmethod code-gen-event (system event &key type)
 
  (let ((*package* (system-package system)))
    (eval
     `(progn
	(in-package ,(package-name (system-package system)))
	(defmethod event-handler 
	    (,(type-arg type 'object) 
	     (event (eql ,(symbol-value (make-keyword (name event))))))
	  ,(read-from-string (action event))))))
  )
|#

(defun %code-gen-event (system event &key type)
  (format nil "(progn
		(in-package ~A)
		(defmethod event-handler 
		    (~A 
		     (event (eql :~A)))
		  ~A))"
	  (package-name (system-package system))
	  (type-arg type 'object)
	  (symbol-value (make-keyword (name event)))
	  (action event)))

(defun eval-code (system code)
  ;;Add any sandbox protection by parsing the code first for malisious stuff??
  (eval (read-from-string code)))


(defun init-system-events (system)
  (let ((init-event (make-instance 'sfx-event 
				 :name :init				
				 :action `(format nil "Not implemented")))
	(load-event (make-instance 'sfx-event 
				 :name :load
				 :action `(format nil "Not implemented")))
	(suspend-event (make-instance 'sfx-event 
				 :name :suspend
				 :action `(format nil "Not implemented")))
	(shutdown-event (make-instance 'sfx-event 				
				 :name :shutdown
				 :action `(format nil "Not implemented"))))

    (register-event system 'load init-event :type (instance-class-name system))
    (register-event system 'load load-event :type (instance-class-name system))
    (register-event system 'load suspend-event :type (instance-class-name system))
    (register-event system 'load shutdown-event :type (instance-class-name system))))
