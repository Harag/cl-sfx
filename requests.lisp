(in-package :cl-sfx)


(setf *catch-errors-p* nil)
(setf *show-lisp-errors-p* t)

(defun bad-request ()
  (setf (return-code *reply*) +http-bad-request+)
  (abort-request-handler))

(defun find-request-page-context (&optional (id (parameter "pageid")) (error t))
  (or (gethash id (contexts *sfx-session*))
      (and error
	   (bad-request))))

(defun ajax-request-p (request)
  (alexandria:starts-with-subseq (ajax-url (request-acceptor request))
                                 (script-name request)))

(defmethod handle-request :before ((acceptor sfx-system) request)
  (unless (equal (script-name*) (login-url acceptor))
    (unless (equal (session-value 'current-uri) (request-uri*))
      (setf (session-value 'previous-uri) (session-value 'current-uri)
            (session-value 'previous-page) (session-value 'current-page)))
    (setf (session-value 'current-uri) (request-uri*)
          (session-value 'current-page) (script-name*))))

(defgeneric login-not-required (acceptor script-name))

(defmethod login-not-required ((acceptor sfx-system) script-name)
  (if (login-url acceptor)
      (equal script-name (login-url acceptor))
      t))

(defmacro with-debugging (&body body)
  ;; Using this as debugging tool because hunchentoot
  ;; swallows all errors here if set to swallow errors.
  `(handler-bind ((error #'invoke-debugger))
     ,@body))

#|
(defmethod update-slot ((object xdb2:storable-slot) slot value)
  (let* ((slot (if (typep slot 'slot-definition)
                   slot
                   (find-slot-definition slot instance)))
         (parser (and (typep slot 'xdb2:storable-slot)
                      (xdb2:slot-parser slot)))
         (value (if parser
                    (funcall parser value)
                    value)))
    (cond ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :list))
           (push value (slot-value-using-class (class-of instance) instance slot)))
          ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :number))
           (setf (slot-value-using-class (class-of instance) instance slot)
                 (if (stringp value)
                     (parse-number value)
                     value)))
          ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :integer))
           (setf (slot-value-using-class (class-of instance) instance slot)
                 (if (stringp value)
                     (parse-integer value)
                     value)))
          (t
           (setf (slot-value-using-class (class-of instance) instance slot)
                 value)))))
|#

(defun acceptor* ()
  (hunchentoot:request-acceptor *request*))

;;TODO: Where to log errors? Why to log errors?
(defgeneric render-error-page (acceptor &key))

(defmethod render-error-page ((acceptor sfx-system) &key condition)
  (with-html-string
    (:html (:head (:title (esc (frmt "Error - ~a" (script-name*)))))
           (:body
            (:div :class "error-description"
                  (:strong :style "color: red;"
                           "Error: ")
                  (esc (princ-to-string condition)))))))

(defun call-with-error-handling (function)
  (if (debug-errors-p (acceptor*))
      (funcall function)
      (let ((condition
              (catch 'error
                (funcall function))))
        (if (typep condition 'error)
            (render-error-page (acceptor*) :condition condition)
            condition))))

(defmacro with-error-handling (&body body)
  `(call-with-error-handling
    (lambda () ,@body)))


;;TODO: MUST log permission denied 
(defgeneric render-permission-denied-page (acceptor &key))

(defmethod render-permission-denied-page ((acceptor sfx-system) &key)
  (let ((title (frmt "Access denied - ~a" (script-name*))))
    (with-html-string
      (:html (:head (:title (esc title)))
             (:body
              (:div :class "error-description"
                    (:strong :style "color: red;"
                             "Error: ")
                    (esc title)))))))

(define-condition permission-denied ()
  ())

(defun call-with-permissions (function)
  (handler-case (funcall function)
    (permission-denied ()
      (render-permission-denied-page (acceptor*)))))

(defmacro with-permissions (&body body)
  `(call-with-permissions
    (lambda () ,@body)))

(defun redirect-ajax-to-login ()
  (frmt "[null,\"window.location.replace('~a')\"]"
        (login-url (acceptor*))))




;;TODO: consider prefixing with hunch-
;;TODO: see where *current-permissions* *sfx-context* is used and why?
(defvar *widget-parameters* nil) ;;used in ajax code ?
(defvar *context-permissions* nil)
;;(defvar *sfx-context* nil)
;;(defvar *session* nil)
;;TODO: Sort out theming



(defmethod handle-request :around ((acceptor sfx-system) request)
  (hunchentoot:start-session)
  
  
  (setf  (debug-errors-p (acceptor*)) t)
  ;;TODO: clear sfx sessions when hunchentoot session expires.
  
  (let* ((*sfx-system* acceptor)
	(*sfx-session* (start-sfx-session acceptor))
	(*tmp-directory*  (system-web-folder *sfx-system*)))
    (declare (special *sfx-system* *sfx-session* *tmp-directory*))
    ;;(break "~A" *tmp-directory*)
    (with-error-handling
      (with-debugging
	
	(let ((script-name (script-name request))
	      (ajax (ajax-request-p request)))

	  (cond ((or (cl-sfx::current-user)		   
		     (login-not-required acceptor script-name))
		 
		 (let* (
			;;*print-pretty* cant find this used anywhere
			
			;;TODO: Why do this check js.lisp????
			(*widget-parameters* nil)
			
			;;TODO: sort this out
			;; (*current-theme* (theme acceptor))
			
			
			;;TODO: creating context on each GET need to either find a way to 
			;;do pageid better or a method to clear old contexts.
			(*sfx-context* 
			 (and (not ajax)
			      
			      (start-sfx-context *sfx-session* script-name
						 :id (or (parameter "i") (parameter "pageid"))
						 :request request)))
			;;TODO: use to be *current-permissions* -- where should context permissions be set?
			;;TODO: check permission no longer needs this!!!!????
			#|(*context-permissions* 
			(if (context-definition *sfx-context*)
			(permissions (context-definition *sfx-context*))))
			|#
			)
		   (declare (special *sfx-context*))
		   ;;	  (break "~A" (contexts *sfx-session*))    
		   (unless ajax
		     (with-debugging
		     ;;  (break "~A" (post-parameters*))
		       (map-cache #'synq-cache *sfx-context* 
				  :args (list (append (get-parameters *request*)
						      (post-parameters *request*))))
		     ;;  (break "?huh1")
		       #| (map-cache #'synq-data *sfx-context* 
		       :args (list (append (get-parameters *request*)
		       (post-parameters *request*))))
		       |#
		      ;; (break "? ~A ~A" *sfx-context* *sfx-session*)
		     ;;  (break "~A~%~A" script-name (post-parameters*))
		       (map-cache-events *sfx-context*)
+		     ;;  (break "?huh2")
		       ))
		   
		   ;;TODO: what does with permissions do? does it use *context-permissions*?
		   (with-error-handling
		     (with-permissions
		       (call-next-method)))))
		(ajax
		 
		 (redirect-ajax-to-login))
		(t
	;;	 (break "what the fuck 2 ~A" script-name)
		 (unless (search "$(" script-name)
		   (setf (session-value 'redirect-after-login) script-name)
		   (redirect (or (login-url acceptor) "/~login"))))))))
 
    ))

;;TODO: Bring in permissions from wfx, need to align it with sfx system user and permissions???

(defun split-string (string char)
    "Returns a list of substrings of string
divided by char."
    (loop for i = 0 then (1+ j)
          as j = (position char string :start i)
          collect (subseq string i j)
          while j))

(defun parse-query-string (query-string)
  (let ((pairs (split-string query-string #\&)))
    (loop for pair in pairs
          for equal = (position #\= pair)
          collect (if equal
                      (list (subseq pair 0 equal)
                            (url-decode (subseq pair (1+ equal))))
                      (list pair)))))

(defun uri-parameters (uri)
  (let ((? (position #\? uri)))
    (if ?
        (values (subseq uri 0 ?)
                (parse-query-string (subseq uri (1+ ?))))
        (values uri nil))))


1(defun current-context-permission (context-name)
  (let ((contexts (context-definitions (system *sfx-session*))))
    (when contexts
      (permissions (gethash context-name contexts)))))

;;TODO: Figure out ho to do any and some if still needed
(defun check-parameter-permission (user-permission parameters)
  (break "wtf permission ~A" user-permission)
  (let ((page-permissions (permissions (context-definition user-permission))))

    
    (if page-permissions
	(dolist (sub-permission page-permissions)
	  (break "wtf ~A ~%~A" (parameters sub-permission)(parameters sub-permission)
		 parameters
		 )
	  (if (parameters sub-permission)
	      (dolist (user-permission (permissions user-permission))
		(when (equalp (permission-name sub-permission) 
			      (permission-name user-permission))
		  (let ((parameter-value (assoc-value 
					  (permission-name sub-permission) parameters)))
		    
		    (when (find parameter-value (parameters sub-permission) 
				:test 'string-equal)
		      (return-from check-parameter-permission t)))))
	      (return-from check-parameter-permission t)))
	t)
    #|
    (loop for sub-permission in (cdr page-permissions)
          never (and (consp sub-permission)
                     (not (assoc (format nil "&~a = :ANY" (car sub-permission)) user-permission
                                 :test #'equal))
                     (let ((parameter (assoc-value (car sub-permission) parameters)))
                       (if parameter
                           (not (assoc (format nil "&~a = ~a" (car sub-permission) parameter)
                                       user-permission :test #'equal))
                           (not (assoc (format nil "&~a = :SOME" (car sub-permission)) user-permission
                                       :test #'equal))))))
    |#
    
    ))

(defun current-user-context-permission (context-name)
  (let ((user (cl-sfx::current-user)))
    (when user
      
      (dolist (permission (permissions user))
	(when (string-equal context-name 
			    (url (context-definition permission)))
	  (return-from current-user-context-permission permission))))))



(defun check-sub-permission (user-permission sub-permission)
  (let ((page-permissions (permissions (context-definition user-permission))))
    (if page-permissions
	(dolist (user-permission (permissions user-permission))
		(when (equalp (permission-name sub-permission) 
			      (permission-name user-permission))
		  (return-from check-sub-permission t)))
	t)))

;;TODO: I cant see that sub-permision is ever used in insite????? Consider dumping it.
(defun check-permission (uri &optional sub-permission
                                       get-parameters)
  (multiple-value-bind (uri parameters)
      (typecase get-parameters
        ((eql t) (values uri (get-parameters*)))
        (cons
         (values uri get-parameters))
        (t
         (uri-parameters uri)))
    
    (let ((user (cl-sfx::current-user)))
      (cond ((not user)
             nil)
            ((super-user-p user)
             t)
            (t
             (let ((permission (current-user-context-permission 
				(replace-all uri (site-url *sfx-system*) ""))))
	  
	       (cond ((and (not parameters)
			   (not sub-permission)
			   permission)
		      ;;Has access to context-def and no sub to check
		      t)
		     ((and (not parameters) 
			   sub-permission 
			   permission)
		      ;;Check sub
		      (check-sub-permission permission sub-permission))
		     ((and parameters 
			   (not sub-permission)
			   permission)
		      ;;TODO: 
		      (break "wtf sort out permissions with parameters~%~A" parameters)
		       ;;(check-parameter-permission permission parameters )
		      t
		      )
		     ((and parameters 
			   sub-permission 
			   permission)
		     ;; (break "sort out permissions with sub with parameters~%~A~%~A"  parameters sub-permission)
		      ;;(check-parameter-permission permission parameters )
		      t)
		     (t
		      ;;(break "wtf check-permissions ~A ~A ~A ~A" uri permission sub-permission parameters)
		      nil))))))))

(defun check-permission-or-error (uri &optional sub-permission get-parameters)
 
  (or (check-permission uri sub-permission get-parameters)
      (signal 'permission-denied)))

(defun add-context-permission (system name permissions)
  (check-type name string)
  (setf (alexandria:assoc-value (context-definitions system) name :test #'equal)
        permissions))


(defun check-page-permission-or-error (&optional sub-permission)
  (check-permission-or-error (script-name*) sub-permission t))


(hunchentoot:define-easy-handler (login-does-not-exist :uri "/~login") ()
  (setf (hunchentoot:content-type*) "text/plain")
 "Login page was specified for the system but no handler for it exists")



(defun nid-ajax (system)
  (ht-simple-ajax:defun-ajax nid
      (id) ((ajax-processor system))
    (setf (content-type*) "text/plain")
    (let ((page (find-context id)))
      (cond ((not page)
	     (bad-request))
	    ((sb-ext:compare-and-swap (slot-value page 'new-p)
				      t
				      nil)
	     "")
	    (t
	     "false")))))

(defun rid-ajax (system)
  (ht-simple-ajax:defun-ajax rid
      (id) ((ajax-processor system))
    (setf (content-type*) "text/plain")
    (let ((page (find-context id)))
      (cond ((not page)
	     (bad-request))
	    ((sb-ext:compare-and-swap (slot-value page 'reuse-p)
				      t
				      nil)
	     "")
	    (t
	     "false")))))

(defun table-ajax (system)
  (ht-simple-ajax:defun-ajax table
      (script-name widget-id &rest args) ((ajax-processor system))
    (declare (ignore args))
    (with-debugging
      (setf (hunchentoot:content-type*) "text/json")
      (let* ((*sfx-context* (find-request-page-context))
	     (widget (get-widget widget-id)))
	
	(when widget
	  (setf (slot-value *request* 'script-name) script-name)
	  (process-data-table widget)
	  )
	))))

#|
(defun grid-test-ajax (system)
  (ht-simple-ajax:defun-ajax grid-test
      (script-name grid-name action row-id) ((ajax-processor system))
  
    (setf (content-type*) "text/json")
    (let* ((*sfx-context* (find-request-page-context))
	   
	   (grid (get-widget grid-name)))
      (declare (special *sfx-context*))
      (with-debugging
	(cond (grid
	       (setf (slot-value *request* 'script-name) script-name)
	       (let ((result (test-grid-action grid row-id action)))
		 (if result
		     (prin1-to-string result)
		     "null")))
	      (t
	       (prin1-to-string (frmt "No grid ~s found" grid-name))))))))


(defun grid-depend-ajax (system)
  (ht-simple-ajax:defun-ajax grid-depend
      (script-name grid-name name value values) ((ajax-processor system))
  
    (setf (content-type*) "text/plain")
    (let* ((*sfx-context* (find-request-page-context))
	   (grid (get-widget grid-name)))
      (declare (special *sfx-context*))
      (with-debugging
	(cond (grid
	       (setf (slot-value *request* 'script-name) script-name)
	       (update-dependent-slots grid name value values))
	      (t
	       (prin1-to-string (frmt "No grid ~s found" grid-name))))))))

|#

(defun cl-ajax-render-ajax (system)
  (ht-simple-ajax:defun-ajax cl-ajax-render
      (script-name widget-id) ((ajax-processor system))
  
    (setf (content-type*) "text/json")
    (setf (slot-value *request* 'script-name) script-name)
    (let ((*sfx-context* (find-request-page-context)))
      (declare (special *sfx-context*))
      (with-debugging

	(map-cache #'synq-cache *sfx-context*)
	;;  (map-dom #'synq-data)
	(map-cache-events *sfx-context*)
      
	(let ((widget (get-widget widget-id)))
	  (when widget
	    ;;  (enable-notifications)
	    (json:encode-json-to-string
	     (list (render-to-string widget :from-ajax t)
		   (deferred-js)))))))))

(defun load-default-ajax (system)
  (nid-ajax system)
  (rid-ajax system)
  (table-ajax system)
;;  (grid-test-ajax system)
;;  (grid-depend-ajax system)
  (cl-ajax-render-ajax system))



#|


(defmacro define-handler (system description lambda-list &body body)
  (when (atom description)
    (setf description (list description)))
  (destructuring-bind (name &rest args
                       &key uri for-everyone permissions
                       &allow-other-keys) description
    
    `(progn
       ,(when (and uri (not for-everyone))
          `(add-page-permission ,system 
				,uri
				(append ,permissions '("Delete All" "Advanced Search"))))
       (define-easy-handler (,name :uri ,uri 
				   ,@(subseq args 2) 
				   :allow-other-keys t) ,lambda-list
           ,@(when (and uri (not for-everyone))
               `((check-permission-or-error ,uri nil t)))
	   (break "what the fuck??")
         ,@body))))

(defmacro define-handler (system description lambda-list &body body)
  (when (atom description)
    (setf description (list description)))
  (destructuring-bind (name &rest args
                       &key uri for-everyone permissions
                       &allow-other-keys)  description
    
    `(define-easy-handler (,name :uri ,(frmt "~A~A" (site-url system) uri) ,@(subseq args 2) :allow-other-keys t) ,lambda-list
           ,@(when (and uri (not for-everyone))
               `((check-permission-or-error ,uri nil t)))
         ,@body)))
|#



;;Test stuff

