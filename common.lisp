(in-package :cl-sfx)

(defvar *sfx-context* nil
  "Not to be set. Exposes the current context.")

(defvar *sfx-session* nil
  "Not to be set. Used internally Exposes the current session.")

(defvar *sfx-system* nil
  "Global variable designating the current system. 
Notes:
Dont set manually use with-system macro.")

(defvar *theme* nil
  "Not to be set by user. Used by convenience functions.")

(defvar *current-theme* nil
  "Active themearound a widget render,  used by convenience functions. Not to be set by user.")

(defvar *widget* nil
  "Active widget around a widget render, used by convenience functions. Not to be set by user.")

;;TODO: Figure out how to get this to actually work!!!!!!!!!
(defvar *parent-container* nil
  "Active parent container widget around widget initialize, used by convenience functions.")

(defvar *import-slot-descriptions* nil
  "(class (slot-name &key key important parser valid default alternative-key)*)*
KEY boolean.
IMPORTANT means the value needs to be correct.
PARSER a function name.
DEFAULT value to be used when the field is empty.
VALID-VALUES is either a list of valid values or a function name for a
 function of one argument which checks for validity.
ALTERNATIVE-KEY this slot may be used as an alternative for the specified slot.")

(defvar *import-classes* nil)

(defvar *short-months*
  #("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))

(defvar *long-months*
  #("January" "February" "March" "April" "May" "June" "July" "August" "September"
    "October" "November" "December"))

(defvar *short-months-afrikaans*
  #("Jan" "Feb" "Mrt" "Apr" "Mei" "Jun" "Jul" "Aug" "Sep" "Okt" "Nov" "Des"))

(defvar *long-months-afrikaans*
  #("Januarie" "Februarie" "Maart" "April" "Mei" "Junie"
    "Julie" "Augustus" "September" "Oktober" "November" "Desember"))

;;FORMAT
(declaim (inline frmt))
(defun frmt (control-string &rest args)
  (apply #'format nil control-string args))

(declaim (inline scriptc))
(defun scriptc (script)
  (frmt "~S" script))

(defun sfx-read-string (string)
  (let ((*read-eval* nil))
   (read-from-string string)))

(defun sfx-eval (script)
  (let ((*read-eval* nil)
	(*package* (system-package *sfx-system*)))
    ;;(break "~A" *package*)
    (when script
      (if (stringp script)
	  (eval (read-from-string script))
	  (eval script)))))

(defgeneric getx (object key of &key qualifier &allow-other-keys)
  (:documentation "The getx method is to be used to get objects from various collections specialized on the collection type. The collection can be represented by complext types thats why the collection var is called object instead. Don't use this to mirror what accessors or slot-value can do for an object either. This is meant for repetitive complicated tasks where you need to find a single value/object in a complicated structure of objects.
Qualifier is an exstra test that can be applied to the objects retrieved from the collection and needs to be a (lambda (object)..return objects) or a function designator that operates in the same fasion. This was not intended to be specialized on lisp types because lisp allready has get functions for its collection types.
key can really be anything, just document your implementation to help out others that want to use it.
of should be of form form (of (eql :what-am-i-getting)) and is just a descriptor to differentiate between getx's.
"))


(defun getx-qualifier (qualifier object)
  (if object
	(if qualifier
	    (apply qualifier object)
	    object)))

;;TODO: Is strip-name and remove-non-id-chars used for the same?
(defun strip-name (val)
  (unless (stringp val)
    (setf val (frmt "~A" val)))
  (let ((copy (copy-seq val)))
    (nstring-downcase (substitute #\- #\Space copy))))

(defun remove-non-id-chars (id)
  (ppcre:regex-replace-all "[^A-Za-z0-9-_:.]"
                           (substitute #\- #\Space id) ""))


(defun princ-esc (x)
  (princ (escape x)))

(defun trim-whitespace (string)
  (string-trim
   '(#\Space #\Newline #\Tab #\Return) string))

;;VALUE CHECKS
(defun empty-p (value)
  "Checks if value is null or an empty string."
  (or (null value)
      (equal value "")
      (equal (trim-whitespace (princ-to-string value)) "")))

(defun invalid-value-p (value)
  "Checks if value is null, :null, empty-string or nil string."
  (or (not value)
      (eq value :null)
      (and (stringp value)
           (or (string-equal value nil)
               (empty-p value)))))



;;TODO: investigate this
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *indent-code* t))

(defmacro with-html (&body body)
  `(with-html-output (*standard-output* nil :indent *indent-code*)
     ,@body))

(defmacro with-html-string (&body body)
  `(with-html-output-to-string (*standard-output* nil :indent *indent-code*)
     ,@body))

(defmacro with-html-no-indent (&body body)
  `(with-html-output (*standard-output* nil :indent nil)
     ,@body))

(defmacro with-html-string-no-indent (&body body)
  `(with-html-output-to-string (*standard-output* nil :indent nil)
     ,@body))

(defun html-from-list (list)
  (cl-who::string-list-to-string (cl-who::tree-to-template list)))

(defmacro with-parameters (parameters &body body)
  "with-parameters ((variable-name \"parameter-name\") | variable-name) body"
  `(let ,(loop for parameter in parameters
               for (variable parameter-name) =
               (if (consp parameter)
                   parameter
                   (list parameter
                         (string-downcase parameter)))
               collect `(,variable (parameter ,parameter-name)))
     ,@body))

(defun render-to-string (widget &rest args)
  (with-html-string
    (apply #'render widget args)))

(defun escape (x)
  (escape-string
   (typecase x
     (string x)
     (null nil)
     (t (princ-to-string x)))))

(defun escape-js (string)
  (when string
    (with-output-to-string (out)
      (loop for char across (if (stringp string)
                                string
                                (princ-to-string string))
            do
            (case char
              ((#\' #\\ #\")
               (write-char #\\ out)
               (write-char char out))
              (#\Newline
               (write-string "\\n" out))
              (t
               (write-char char out)))))))

(defun sub-name (widget name)
  (concatenate 'string (name widget) "-" name))

(defun ensure-parse-integer (value &key (start 0) end)
  (typecase value
    (string
     (multiple-value-bind (integer position)
         (parse-integer value :junk-allowed t
                        :start start :end end)
       (when (= (length value) position)
         integer)))
    (integer value)))

(defun proper-intern (text &optional package)
  (if (symbolp text)
      text
      (intern (string-upcase (strip-name text)) (or package *package*))))

(defun key-intern (text )
  (intern (string-upcase (strip-name (frmt "~A" text))) "KEYWORD"))

(defun read-from-string-no-eval (string)
  (if (not (stringp string))
      string
      (let ((*read-eval* nil)) 
	(read-from-string string))))

(defun plural-name (name)
  (setf name (frmt "~A" name))
  (let ((last-char (char name (1- (length name)))))
    (cond ((char-equal last-char #\s)
	   name)
	  ((char-equal last-char #\y)
	   (alexandria:symbolicate (subseq name 0
					   (1- (length name)))
				   'ies))
	  (t
	   (alexandria:symbolicate name 's)))))

(defun plural-name-string (name)
  (setf name (string-downcase (frmt "~A" name)))
  (let ((last-char (char name (1- (length name)))))
    (cond ((char-equal last-char #\s)
	   name)
	  ((char-equal last-char #\y)
	   (frmt "~A~A" (subseq name 0
					   (1- (length name)))
				   "ies"))
	  (t
	   (frmt "~A~A" name "s")))))

(defun make-name (val)
  (let* ((val (string val))
         (copy (if (alexandria:ends-with-subseq "-P" val :test #'char-equal)
                   (subseq val 0 (1- (length val)))
                   (copy-seq val))))
    (nstring-capitalize (substitute #\Space #\- copy))))

(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part 
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos)))


(defun copy-slots (from to)
  (let ((class-from (class-of from))
        (class-to (class-of to)))
    (loop for slotd in (class-slots class-from)
          when (slot-boundp-using-class class-from from slotd)
          do (setf (slot-value-using-class class-to to slotd)
                   (slot-value-using-class class-from from slotd)))
    to))

(defun copy-into (from to)
  (assert (eq (class-of from) (class-of to)))
  (copy-slots from to))

(defun copy-object (object &rest initargs)
  (let* ((class (class-of object))
         (new (allocate-instance class)))
    (loop for slotd in (c2mop:class-slots class)
          when (c2mop:slot-boundp-using-class class object slotd)
          do (setf (c2mop:slot-value-using-class class new slotd)
                   (c2mop:slot-value-using-class class object slotd)))
    (when initargs
      (apply #'reinitialize-instance new initargs))
    new))


;;Grid stuff

(defstruct value-info
  value
  row
  read-only
  data
  invalid
  dependants
  no-accept)

(defun build-date (year month day)
  (format nil "~d ~a ~d" day (short-month-name month) year))

(defun build-date-time (year month day hour min sec
                        &optional timezone)
  (declare (ignore timezone))
  (format nil "~d ~a ~d ~@{~2,'0d~^:~}"
          day (short-month-name month) year hour min sec))

(defvar *time-zone* 0)

(defun format-universal-date (universal-date)
  (when universal-date
    (if (stringp universal-date)
        universal-date
        (multiple-value-bind (a b c day month year)
            (decode-universal-time universal-date
                                   *time-zone*)
          (declare (ignore a b c))
          (build-date year month day)))))

(defun format-date (date)
  (if (typep date 'unsigned-byte)
      (format-universal-date date)
      date))


(defun month-number (month)
  (let ((position (or (position month *short-months*
                                :test #'equalp)
                      (position month *long-months*
                                :test #'equalp)
                      (position month *short-months-afrikaans*
                                :test #'equalp)
                      (position month *long-months-afrikaans*
                                :test #'equalp))))
    (when position
      (1+ position))))

(defvar *time-zone* 0)

(defun decode-iso-date (date)
  (ppcre:register-groups-bind ((#'parse-integer year)
                               (#'parse-integer month)
                               (#'parse-integer day)) ("(\\d{4})-(\\d{1,2})-(\\d{1,2})" date)
    (values year month day)))

(defun decode-date-string (date)
  (multiple-value-bind (year month day) (decode-iso-date date)
    (if year
        (values year month day)
        (let ((date-split (ppcre:split "[.\\/ -]+" (trim-whitespace date))))
          (and date-split
               (let* ((month-raw (second date-split))
                      (month (or (month-number month-raw)
                                 (ensure-parse-integer month-raw)))
                      (year (ensure-parse-integer (third date-split)))
                      (day (ensure-parse-integer (first date-split))))
                 (values year month day)))))))

(defun decode-date (date &key time-zone)
  (etypecase date
    (string
     (decode-date-string date))
    (integer
     (multiple-value-bind (a b c day month year)
         (decode-universal-time date (or time-zone *time-zone*))
       (declare (ignore a b c))
       (values year month day)))))

(defvar *month-days* #(31 28 31 30 31 30 31 31 30 31 30 31))

(defun leap-year-p (year)
  (cond
    ((zerop (rem year 400)) t)
    ((zerop (rem year 100)) nil)
    ((zerop (rem year 4)) t)))

(defun check-date (date month year)
  ;; Technically, there can be a 31 dec 1899 date, if the time-zone is
  ;; west of GMT, but it's not particularly important.
  (when (and (typep year '(integer 1900))
             (typep month '(integer 1 12))
             (plusp date))
    (let ((days (svref *month-days* (1- month))))
      (cond ((and (= month 2)
                  (leap-year-p year))
             (<= date 29))
            (t
             (<= date days))))))

(defun parse-date (date &key time-zone)
  (etypecase date
    (integer date)
    (string
     (multiple-value-bind (year month date) (decode-date-string date)
       (when (check-date date month year)
         (encode-universal-time 0 0 0 date month year
                                (or time-zone *time-zone*)))))
    (null nil)))
