(in-package :cl-sfx)

(defclass entity-tree (widget)
  ())

(defun entity-children (rels)
  (labels ((recurse (rels)
	     (map 'list
		  (lambda (rel)
		    (let* ((entity (parent rel))
			   (id (xdb2:id entity)))
		      (list* id
			     (entity-name entity)
			     t
			     (recurse (cl-sfx::children rel)))))
		  rels)))
    (when (and rels (xdb2:id rels))
      (recurse (list rels)))))

(defmethod render ((widget entity-tree) &key)
  (let ((form (make-widget 'form :name "entity-tree-form"))
	(root-select (make-widget 'select :name "root-select"))
	(user (user *sfx-context*)))
    
    (setf (items root-select)
          (if (super-user-p user) 
              (loop for entity-rel across (cl-sfx::entity)
                    for entity = (entity entity-rel)
                    collect (list (xdb2::id entity)
                                  (entity-name entity)))
              (loop for root in (cl-sfx::accessible-entities user)
                    when (not (listp root))
                    collect (list root
                                  (entity-name root)))))
    
    (with-html
      (render form 
	      :body (str (render root-select))
	      
		       ))
    ))
