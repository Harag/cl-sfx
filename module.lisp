(in-package :cl-sfx)

(defclass context-parameter (doc)
  ((parameter-name :initarg :parameter-name
		   :accessor parameter-name
		   :initform nil
		   :db-type string
		   :key t)
   (parameter-value :initarg :parameter-value
		    :accessor parameter-value
		    :initform nil
		    :db-type string))
  (:metaclass data-versioned-class))

(defclass menu-item (doc)
  ((item-name :initarg :item-name
	      :accessor item-name
	      :initform nil
	      :db-type string
	      :key t)
   (children :initarg :children
             :initform ()
             :accessor children
	     :db-type (list menu-item))
   (context-definition :initarg :context-definition
		 :accessor context-definition
		 :initform nil
		 :db-type (data-member sfx-context-definition :key-accessor context-name))
   (context-parameters :initarg :context-parameters
		       :accessor context-parameters
		       :initform nil
		       :db-type (list context-parameter)))
  (:metaclass data-versioned-class))

(defclass menu (doc)
  ((menu-name :initarg :menu-name
	      :accessor :menu-name
	      :initform nil
	      :db-type string
	      :key t)
   (menu-items :initarg :menu-items
	       :accessor menu-items
	       :initform nil
	       :db-type (list menu-item)))
  (:metaclass data-versioned-class))

(defclass module (doc)
  ((module-name :initarg :module-name
		 :accessor module-name
		 :initform nil
		 :db-type string
		 :key t)
   (contexts :initarg :contexts
	     :accessor contexts
	     :initform nil
	     :db-type (data-group sfx-context-definition :key-accessor context-name))
   (menu :initarg :menu
	 :accessor menu
	 :initform nil
	 :db-type (list menu))
   ;;(help)
   )
  (:metaclass data-versioned-class)
  (:collection-name "modules")
  (:collection-type :system)
  (:default-initargs :top-level t))

(defun get-module (module-name)  
  (find-docx 'module
	     :test (lambda (doc)
		     (string-equal module-name (module-name doc)))))

(defclass payment-detail (doc)
  ((company-name)
   (company-no)
   (vat-no)
   (currency)
   (frequency))
  (:metaclass data-versioned-class))

;;TODO: is this still being used?
(defclass license-module (doc)
  ((module :initarg :module
	   :accessor module
	   :initform nil
	   :key t
	   :db-type (data-member module))
   (entities :initarg :entities
	     :accessor entities
	     :initform nil
	     :db-type (data-group entity :key-accessor entity-name)))
  (:metaclass data-versioned-class))

(defclass sfx-license (doc)
  ((license-code :initarg :license-code
		 :accessor license-code
		 :initform nil
		 :db-type string
		 :key t)
   (license-holder :initarg :license-holder
		   :accessor license-holder
		   :initform nil
		   :db-type string
		   :header t)
   (payment-detail :initarg :payment-details
		   :accessor payment-details
		   :initform nil)
   (license-modules :initarg :license-modules
		    :accessor license-modules
		    :initform nil
		    :db-type (data-group module :key-accessor module-name))
   (license-entities :initarg :license-entities
	     :accessor license-entities
	     :db-type (list entity)
	     :initform nil)
   (license-date :initarg :license-date
		 :accessor license-date
		 :initform (get-universal-time)
		 :db-type date
		 :header t)
   (license-status :initarg :license-status
		   :accessor license-status
		   :initform :demo 
		   :documentation "(:demo :suspended :active)"
		   :db-type string
		   :header t)
   (super-user-access-p :initarg :super-user-access-p
			:accessor super-user-access-p
			:initform t
			:db-type boolean
			))
  (:metaclass data-versioned-class)
  (:after-persist #'(lambda (doc)
		      (add-db (system-data *sfx-system*)
			      (list 
			       (frmt "~A" (strip-name (system-name *sfx-system*)))
			       (current-license-code)))))
  (:collection-name "sfx-licenses")
  (:collection-type :system)
  (:default-initargs :top-level t))


(defun print-license-code (doc)
  (license-code doc))

(defclass license-doc (doc)
  ((license :initarg :license
           :initform nil
           :accessor license
           :db-type (data-member sfx-license :key-accessor license-code)
           :key t
           :header t
	   :required t
           :printer #'print-license-code))
  (:metaclass data-mixin))

(defun sys-license ()
  (sys-admin-license "000000"))

(defun find-license (code)
  (find-docx 'sfx-license
	     :test (lambda (doc)
		     (equalp (license-code doc) code))))

(defun sys-admin-license (code)
  (let ((license (find-license code)))
     (unless license
      
      (setf license (persist (make-instance 'sfx-license :license-code code
					    :license-holder "System Admin")))
      (xdb2:persist (make-user "admin@cl-sfx.com" "admin"
			       :super-user-p t
			       :license license)))
    license))

(defvar *license-code-lock* (bordeaux-threads:make-lock))

(defun generate-license-code ()
  (declare (optimize speed))
  (format nil "~:@(~36r~)" (random (expt 2 32))))

(defun generate-new-license-code ()
  (loop for id = (generate-license-code)
	 unless (find-license id)
	 return id))

(defgeneric assign-license-code (license)
  (:documentation "Assigns a code to the license. Uses a thread lock in :around to ensure unique code."))

(defmethod assign-license-code :around ((license sfx-license))
  (bordeaux-threads:with-lock-held (*license-code-lock*)
      (call-next-method)))

(defmethod assign-license-code ((license sfx-license))
  (let ((code (generate-new-license-code)))
    (setf (license-code license) code)))







