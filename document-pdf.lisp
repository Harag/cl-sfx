(in-package :cl-sfx-doc)

(defvar *pdf-image-path*
  (asdf:system-relative-pathname 'insite "public/"))

(defparameter *pdf-heading-sizes*
  #(nil 20 18 16 14 12 10))

(defvar *h-alignment* :left)
(defvar *page-start*) ;; Where the output will go after headers are inserted

(defun time-stamp ()
  (local-time:format-timestring
   nil
   (local-time:universal-to-timestamp (get-universal-time))
   :format
   '((:day 2) " " :short-month " " :year " "
     (:hour 2) ":" (:min 2) ":" (:sec 2))))

(defun add-page-footers ()
  ;; (let* ((pages (reverse (dx-pdf::pages-kids (dx-pdf::catalog-pages dx-pdf::*catalog*))))
  ;;        (total (length pages))
  ;;        (time (time-stamp)))
  ;;   (loop for i from 1
  ;;         for page in pages
  ;;         for media-box = (dx-pdf::page-media-box page)
  ;;         do
  ;;         (dx-pdf::set-page page)
  ;;         (dx-pdf::new-line)
  ;;         (setf dx-pdf::*y* dx-pdf::*page-margin*)
  ;;         (let ((dx-pdf::*y* dx-pdf::*y*))
  ;;           (dx-pdf::output-text (format nil "Produced by Insite. ~a" time)
  ;;                                :size 10
  ;;                                :add-new-line nil))
  ;;         (dx-pdf::output-text (format nil "~a of ~a" i total)
  ;;                              :size 10
  ;;                              :h-align :right
  ;;                              :add-new-line nil)
  ;;         (when (and
  ;;                *quickview-page*
  ;;                (logo *quickview-page*))
  ;;           (dx-pdf::write-object (dx-pdf::make-image
  ;;                                  :file
  ;;                                  (image-path "/images/insite-logo.png")
  ;;                                  :x (/ (svref (dx-pdf::page-media-box dx-pdf::*page*) 2) 2)
  ;;                                  :y (+ 25 dx-pdf::*y*)
  ;;                                  :width 40
  ;;                                  :height 40
  ;;                                  :h-centered t)))))
  )

(defun remove-empty-elements (document)
  (cl-sfx:copy-object document
                      :elements
                      (loop with orientation
                            for element in (elements document)
                            append (cond ((typep element 'orientation)
                                          (setf orientation element)
                                          nil)
                                         ((and (typep element 'html)
                                               (html-only element))
                                          nil)
                                         ((and (typep element 'pdf)
                                               (not (elements element)))
                                          (list element))
                                         ((not orientation)
                                          (list element))
                                         (t
                                          (list (shiftf orientation nil) element))))))


(defun render-pdf (document)
  (let ((*page-start*))
   (render-element (remove-empty-elements document)
                   :pdf)))

(defmethod render-element ((document document) (type (eql :pdf)))
  (render-objects (elements document) type))

(defmethod render-element ((orientation orientation) (type (eql :pdf)))
  (let ((orientation (orientation orientation))
        (current-orientation (and dx-pdf::*page*
                                  (dx-pdf::page-orientation dx-pdf::*page*))))
    (case orientation
      ((:next :previous)
       (setf orientation current-orientation)))
    (when (or (null dx-pdf::*page*)
              (not (eq orientation current-orientation)))
      (dx-pdf::new-page :orientation orientation))))

(defun remove-nil-properties (plist)
  (loop for (property value) on plist by #'cddr
        when value
        collect property
        and
        collect value))

(defmethod render-element ((string string) (type (eql :pdf)))
  (dx-pdf::output-text string
                       :h-align *h-alignment*))

(defmethod process-text-style ((text text) (type (eql :pdf))))

(defmethod render-element ((text text) (type (eql :pdf)))
  (let (;; (style (process-text-style text type)) FIXME
        )
    (render-element (text text) type)))

(defmethod render-element ((paragraph paragraph) (type (eql :pdf)))
  (let ((*h-alignment* (h-align paragraph)))
    (dx-pdf::new-line)
    (render-objects (elements paragraph) type)))

(defmethod make-color (spec (type (eql :pdf)))
  (and spec
       (parse-integer spec :radix 16)))

(defun ensure-string (string)
  (etypecase string
    (null "")
    ((or number string symbol character)
     (princ-to-string string))
    (document-element
     (ensure-string (text string)))
    (list
     (with-output-to-string (str)
       (loop for x in string
             do (write-string (ensure-string x) str))))))

(defun compute-col-widths (table orientation)
  (let ((p-table (convert-table table)))
    (dx-pdf:column-widths p-table orientation)))

(defun convert-table (table)
  (let ((p-table
          (apply #'dx-pdf::make-table
                 (loop for row in (rows table)
                       collect
                       (apply #'dx-pdf::make-row
                              (loop for cell in (cells row)
                                    collect
                                    (funcall (if (typep cell 'header)
                                                 #'dx-pdf::make-header
                                                 #'dx-pdf::make-cell)
                                             (ensure-string (text cell))
                                             :row-span (row-span cell)
                                             :col-span (col-span cell)
                                             :bg-color (dx-pdf::make-color (bg-color cell))
                                             :color (dx-pdf::make-color (text-color cell)))))))))
    (setf (dx-pdf::table-title p-table) (title table)
          (dx-pdf::table-precomputed-widths p-table) (widths table))
    p-table))

(defmethod render-element ((table table) (type (eql :pdf)))
  (when (rows table)
    (dx-pdf::write-object (convert-table table))))


;; (defmethod render-element ((element html) (type (eql :pdf)))
;;   (unless (html-only element)
;;     (render-objects (html-to-document (rendered-string element))
;;                     type)))

(defmethod render-element ((heading heading) (type (eql :pdf)))
  (check-type (size heading) (integer 1 6))
  (let ((dx-pdf:*font-size* (elt *pdf-heading-sizes* (size heading))))
    (render-element (text heading) type)))

(defmethod render-element ((page new-page) (type (eql :pdf)))
  (dx-pdf::new-page))

(defmethod render-element ((line new-line) (type (eql :pdf)))
  (dx-pdf::new-line))

(defmethod render-element ((element pdf) (type (eql :pdf)))
  (render-objects (elements element) :pdf)
  (when (pdf-code element)
    (funcall (pdf-code element))))

(defun image-path (path)
  (merge-pathnames (enough-namestring path "/") *pdf-image-path*))


(defmethod render-element ((image image) (type (eql :pdf)))
  ;; (tt:image :file (path image)
  ;;           :dx (width image)
  ;;           :dy (height image))
  )

(defun make-new-page-hook (&key entities-header
                                title)
  (lambda ()
    ;; (let ((width (svref (dx-pdf::page-media-box dx-pdf::*page*) 2)))
    ;;   (multiple-value-bind (width height)
    ;;       (dx-pdf::write-object (dx-pdf::make-image
    ;;                              :file
    ;;                              (if (and *quickview-page*
    ;;                                       (logo *quickview-page*))
    ;;                                  (merge-pathnames (logo *quickview-page*) *tmp-directory*)
    ;;                                  (image-path "/images/insite-logo.png"))
    ;;                              :x (/ width 2)
    ;;                              :y dx-pdf::*y*
    ;;                              :width 100
    ;;                              :height 100
    ;;                              :h-centered t))
    ;;     (declare (ignore width))
    ;;     (dx-pdf::write-object (dx-pdf::make-text title
    ;;                                              :x dx-pdf::*x*
    ;;                                              :y (- dx-pdf::*y* 12)))
    ;;     (and *reporting-period*
    ;;          (dx-pdf::output-text (format nil "~a - ~a"
    ;;                                       (format-date (start-date *reporting-period*))
    ;;                                       (format-date (end-date *reporting-period*)))
    ;;                               :size 10 :h-align :right))
    ;;     (decf dx-pdf::*y* height)
    ;;     (when entities-header
    ;;       (dx-pdf::output-text (format-entities) :size 10 :h-align :center))
    ;;     (decf dx-pdf::*y* 10)
    ;;     (setf *page-start* dx-pdf::*y*)))
    ))

;;;

(defun graph-title (title height page-width)
  (let ((dx-pdf::*font-size* (svref *pdf-heading-sizes* 5)))
    (when (and (not (= dx-pdf::*y* *page-start*))
               (< (- dx-pdf::*y*
                     (+ height
                        (dx-pdf:string-height title page-width)
                        (* dx-pdf::*font-size* 7/10 2)))
                  dx-pdf::*bottom-margin*))
      (dx-pdf::new-page))
    (decf dx-pdf:*y* (* dx-pdf::*font-size* 7/10))
    (dx-pdf::output-text title :h-align :center)
    (decf dx-pdf:*y* (* dx-pdf::*font-size* 7/10))
    (min height
         (- dx-pdf:*y* dx-pdf::*bottom-margin*))))

(defun empty-bar-graph-p (data)
  (loop for (title . values) in data
        always (every #'zerop values)))

(defmethod render-element ((graph bar-graph) (type (eql :pdf)))
  (let* ((current-orientation (and dx-pdf::*page*
                                   (dx-pdf::page-orientation dx-pdf::*page*)))
         (dimensions (getf dx-pdf::*orientations* current-orientation))
         (page-width (- (svref dimensions 2) (* dx-pdf::*page-margin* 2)))
         (vecto-graphs:*margins* (/ dx-pdf::*page-margin* 2))
         (dx-pdf:*font-size* dx-pdf:*font-size*))
    (with-slots (title height labels data id grid-width
                 orientation x-label y-label legend
                 percentage stacked max-y) graph
      (unless (empty-bar-graph-p data)
        (let ((height (graph-title title (pixels-to-points height)
                                   page-width)))
          (dx-pdf:push-stack)
          (vecto-graphs:with-graph-context (:pdf
                                            :width page-width
                                            :height height)
            (vecto-graphs::translate (vecto-graphs::point 0 (- dx-pdf::*y* height)))
            (vecto-graphs:draw-bar-graph data
                                         :x-label (and legend
                                                       labels)
                                         :y-label y-label
                                         :stacked stacked
                                         :max-y max-y
                                         :percentage percentage))
          (decf dx-pdf:*y* height)
          (dx-pdf:pop-stack))))))

(defun pixels-to-points (x)
  x)

(defun pie-pixels-to-points (x)
  (* x 72/150))

(defun empty-pie-chart-p (data)
  (loop for (title . value) in data
        always (zerop value)))

(defmethod render-element ((graph pie-chart) (type (eql :pdf)))
  (let* ((current-orientation (and dx-pdf::*page*
                                   (dx-pdf::page-orientation dx-pdf::*page*)))
         (dimensions (getf dx-pdf::*orientations* current-orientation))
         (page-width (- (svref dimensions 2) (* dx-pdf::*page-margin* 2)))
         (vecto-graphs:*margins* (/ dx-pdf::*page-margin* 2))
         (dx-pdf:*font-size* dx-pdf:*font-size*))
    (with-slots (title data id grid-width
                 breakdown height percent) graph
      (unless (empty-pie-chart-p data)
        (let ((height (graph-title title (pie-pixels-to-points height)
                                   page-width)))
          (dx-pdf:push-stack)
          (vecto-graphs:with-graph-context (:pdf
                                            :width page-width
                                            :height height)
            (vecto-graphs::translate (vecto-graphs::point 0 (- dx-pdf::*y* height)))
            (vecto-graphs:draw-pie-chart data
                                         (vecto-graphs::point (/ page-width 2)
                                                              (/ height 2))
                                         (- (/ height 2) 40)
                                         :percent t))
          (decf dx-pdf:*y* height)
          (dx-pdf:pop-stack))))))


(defmethod render-element ((graph line-graph) (type (eql :pdf)))
  ;; FIXME
  )
