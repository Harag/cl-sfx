(in-package :cl-sfx)

(defvar *slot-value-info*)
(defvar *parent-slot-value-info*)
(declaim (hash-table *slot-value-info* *parent-slot-value-info*))

(defvar *current-slot*)

(defvar *sub-grids*)



(defparameter *delete-version-button*
  (make-instance 'grid-button
                 :action "delete-version"
                 :icon "box--minus"
                 :hint "Delete Version"
                 :confirm t
                 :permission "Delete Version"
                 :writep t
                 :test (lambda (grid x)
                         (and (eq (grid-filter grid) 'with-audit-data)
                              (typep x 'storable-versioned-object)
                              (or (plusp (version x))
                                  (top-version x))))))

(defparameter *delete-button*
  (make-instance 'grid-button
                 :action "delete"
                 :icon "card--minus"
                 :hint "Delete"
                 :confirm t
                 :permission "Delete"
                 :writep t
                 :test (lambda (grid x)
                         (not (and (eq (grid-filter grid) 'with-audit-data)
                                   (typep x 'storable-versioned-object)
                                   (top-version x))))))

(defvar *test-delete-button*
  (copy-object *delete-button* :before-submit t))



(defclass generic-grid (grid)
  ((read-only-slots :initarg :read-only-slots
                    :initform nil
                    :accessor read-only-slots)
   (object-editor :initarg :object-editor
                  :accessor object-editor)
   (scroll-x :initarg :scroll-x
             :initform nil
             :accessor scroll-x)
   (buttons :initform
            (list *edit-button* *delete-button*
                  *delete-version-button*))
   )
  (:metaclass widget-class)
  (:default-initargs :top-level t
    :icon ""
   ;; :advanced-search-slot-package #.*package*
    ))

(defclass generic-sub-grid (generic-grid)
  ((info :initarg :info
         :initform nil
         :accessor info)
   (slot :initarg :slot
         :initform nil
         :accessor slot)
   (initargs :initarg :initargs
             :initform nil
             :accessor initargs))
  (:metaclass widget-class)
  (:default-initargs :top-level nil
    :modal-edit nil))

(defmethod class-export-csv-header (class)
  (nconc (loop for slot in (class-slots (class-of class))
               when (db-type slot)
               collect (make-name (slot-definition-name slot)))
         '("Effective Date"
           "Stamp Date")))

(defmethod export-csv ((grid generic-grid))
  (let* ((data (grid-filtered-rows grid))
         (class (row-object-class grid))
         (prototype (class-prototype class)))
    (when data
      (with-output-to-string (stream)
	(format stream "~a Export.~@[Filter: ~A,~]~@[ Search: ~s,~] Date: ~A ~%Context: ~A~2%"
		(make-name (class-name class))
		(grid-filter grid) (search-term grid)
		(current-date-time) (print-context))
	(flet ((format-row (list)
		 (format stream "~{~a~^|~}~%" list)))
	  (format-row (class-export-csv-header prototype))
	  (loop for doc being the element of data
	     do
	       (multiple-value-bind (row multiple) (class-export-csv-row doc)
		 (if multiple
		     (loop for row in row
			do (format-row (mapcar #'object-description row)))
		     (format-row (mapcar #'object-description row))))))))))

(defun make-generic-grid-columns (class)
  (c2cl:ensure-finalized class)
  (loop for slot in (cl-sfx::all-slots class)
        for slot-name = (slot-definition-name slot)
        when (or (header slot)                 
		 (cl-sfx::key slot))
        collect (make-instance 'grid-column
                               :name (or (cl-sfx::virtual slot)
                                         slot-name)
                               :header
                               (if (stringp (header slot))
                                   (header slot)
                                   (make-name slot-name))
                               :printer (or (cl-sfx::slot-printer slot)
                                            (make-slot-printer (cl-sfx::db-type slot))))))

(defgeneric class-grid-buttons (class))

(defmethod class-grid-buttons ((class t))
  (list *edit-button* *delete-button* *delete-version-button*))

(defmethod initialize-instance :after ((grid generic-grid)
                                       &key row-object-class
                                            (buttons nil buttons-p))
  (declare (ignore buttons))
  (setf (columns grid)
        (make-generic-grid-columns row-object-class)
        (object-editor grid)
        (make-widget 'object-editor :name (sub-name grid "object-editor")))
  (unless buttons-p
    (setf (buttons grid)
          (class-grid-buttons (class-prototype row-object-class)))))

(defgeneric class-grid-filters (class))
(defmethod class-grid-filters ((class t)))

(defgeneric filter-class-data (class objects filter))

(defmethod list-grid-filters ((grid generic-grid))
  (list* 'with-audit-data
         (class-grid-filters (class-prototype (row-object-class grid)))))

(defmethod get-rows ((grid generic-grid))
  (let* ((filter (grid-filter grid))
	 (collection (select-collection-class (row-object-class grid)))
         (class (row-object-class grid))
         (docs (if collection (xdb2::docs collection))))
    (setf (rows grid)
          (case filter
            (with-audit-data
                (loop for doc across docs
                      when (cl-sfx::match-context-entities doc)
                      collect doc
                      and
                      append (cl-sfx::old-versions doc)))
            ((nil)
             (remove-if-not #'cl-sfx::match-context-entities docs))
            (t
             (filter-class-data (class-prototype class) docs filter))))))


(defmethod get-rows ((grid generic-sub-grid))
  (let ((filter (grid-filter grid))
        (docs (value-info-value (info grid))))
    
    (setf (rows grid)
          (case filter
            (with-audit-data
                (loop for doc being the element of docs
                      collect doc
                      append (cl-sfx::old-versions doc)))
            (t
             docs)))))

(defmethod handle-action ((grid generic-sub-grid) (action (eql :new)))
  (let ((class (row-object-class grid)))
    
    (setf (editing-row grid)
          (apply #'make-instance class (initargs grid)))))

(defmethod render-row-editor :around ((grid generic-sub-grid) row)
  (let ((*parent-slot-value-info* (slot-value-info (object-editor (parent-grid grid)))))
    (call-next-method)))

(defmethod render-row-editor ((grid generic-grid) row)
  (unless (typep grid 'generic-sub-grid)
    (defer-js "$(window).on('beforeunload', function(){ return 'Unsaved changes'; });"))
  (setf (object (object-editor grid)) row)
  (render (object-editor grid)
	  
          :modal-edit (modal-edit grid)
          :read-only-slots (read-only-slots grid)
          :target grid))




(defun check-already-exists (object iterator)
  (unless (cl-sfx::id object)
    (let* ((class (class-of object))
           (accessors (cl-sfx::key-accessors class))
           (comparers (loop for (slot-name . accessor) in accessors
                            collect (cons accessor
                                          (value-info-value
                                           (gethash slot-name *slot-value-info*)))))
           (exists
             (funcall iterator
                      (lambda (doc)
                        (loop for (accessor . value) in comparers
                              always (and (typep doc class)
                                          (equalp (funcall accessor doc)
                                                  value)))))))
      (when exists
        (loop for (slot-name) in accessors
              for info = (gethash slot-name *slot-value-info*)
              do (setf (value-info-invalid info)
                       "Document with this key already exists."))
        t))))

(defun already-exists-in-collection (object)
  (check-already-exists
   object
   (lambda (test)
     (let* ((class (class-of object))
	    (collection (doc-collection class))
	    (docs (if collection
		      (docs collection))))
       (when docs
	 (loop for doc across docs
	    thereis (funcall test doc)))))))

(defun already-exists-in-parent (object parent-info)
  (check-already-exists
   object
   (lambda (test)
     (loop for doc in (value-info-value parent-info)
           thereis (and (not (eq object doc))
                        (funcall test doc))))))

(defgeneric generic-grid-persist (doc))


;;TODO: Put log back
(defmethod generic-grid-persist (doc)
;;  (setf (log-action doc) :edited)
  (xdb2::persist doc))

(defmethod generic-grid-persist :before (doc)
  (when (and doc (before-persist-event (class-of doc)))
    (let ((script (before-persist-event (class-of doc))))
     (call-script script doc))))


;;TODO: see if read-from-string-no-eval does not simplify this shit
(defun call-script (script doc)
  (unless (or (string-equal "nil" (frmt "~A" script))
	      (string-equal "(nil)" (frmt "~A" script)))
    (let ((script script))
      (if (stringp script)
	  (setf script (read-from-string script))
	  (setf script 
		(if (stringp (first script))
		    (read-from-string (first script))
		    (if (not (consp (first script)))
			script
			(if (not (consp (first (first script))))
			    (first script)
			    (first (first script))))))) 
      
      (setf script (eval script))

      (let ((*package* (cond ((typep doc 'value-info)
			      (cond ((slot-exists-p (class-of (value-info-row doc)) 
						    'doc-package)
				     (find-package (or (doc-package 
							(class-of (value-info-row doc)))
						       "CL-SFX")))
				    (t
				     (find-package "CL-SFX"))))
			     ((not doc)
			      (find-package "CL-SFX"))
			     ((not (slot-exists-p (class-of doc) 
						  'doc-package))
			      (find-package "CL-SFX")
			      )
			     (t
			      (find-package (doc-package doc))))))
	(etypecase script
	  (function
	   (funcall script doc))
	  (cons	 
	   (funcall (eval script) doc))
	  (t	 
	   (error (frmt "Script not computing...~%~%~A" script))))))))

(defmethod generic-grid-persist :after (doc)
  (when (and doc (after-persist-event (class-of doc)))
    (let ((script (after-persist-event (class-of doc))))
     (call-script script doc))))


(defmethod handle-action ((grid generic-grid) (action (eql :save)))
  (defer-js "$(window).off('beforeunload');")
  (let* ((row (editing-row grid))
         (*slot-value-info* (slot-value-info (object-editor grid)))
         (success (accept-doc row)))
    
    
    (when (already-exists-in-collection row)
      (setf success nil))
    (when success
      (commit-values row *slot-value-info*)
      (clrhash *slot-value-info*)
      (generic-grid-persist row)
      (finish-editing grid))))

(defmethod handle-action ((grid generic-sub-grid) (action (eql :save)))
  (let* ((row (editing-row grid))
         (*slot-value-info* (slot-value-info (object-editor grid)))
         (success (accept-doc row))
         (parent-info (info grid)))
    
  ;;  (break "~A~%~A~%~A~%~A~%~A" row grid parent-info *slot-value-info* (object-editor grid))
    
    (when (already-exists-in-parent row parent-info)
      (setf success nil))
    
    (when success
      (commit-values row *slot-value-info*)
      
      (clrhash *slot-value-info*)
      (when (top-level row) ;;(getf (initargs grid) :top-level)
        (generic-grid-persist row))
      (pushnew row (value-info-value parent-info))
      (finish-editing grid))))



(defmethod handle-action ((grid generic-sub-grid) (action (eql :delete)))
  (let ((row (editing-row grid))
        (parent-info (info grid)))
    (when (getf (initargs grid) :top-level)
      (xdb2::remove-doc row (email (cl-sfx::current-user))))
    (alexandria:removef (value-info-value parent-info) row)))

(defmethod handle-action :before ((grid generic-grid) (action (eql :edit)))
  (clrhash (slot-value-info (object-editor grid))))

(defmethod handle-action :after ((grid generic-grid) (action (eql :cancel)))
  (unless (typep grid 'generic-sub-grid)
+    (defer-js "$(window).off('beforeunload');"))
  (clrhash (slot-value-info (object-editor grid))))

(defgeneric class-export-csv-header (class))
(defgeneric class-export-csv-row (object))

(defun short-month-name (n)
  (when (array-in-bounds-p *short-months* (1- n))
    (aref *short-months* (1- n))))

(defun format-date-time (year month day hour min sec
                        &optional timezone)
  (declare (ignore timezone))
  (format nil "~d ~a ~d ~@{~2,'0d~^:~}"
          day (short-month-name month) year hour min sec))

(defvar *time-zone* 0)

(defun format-universal-date-time (universal-date)
  (if (stringp universal-date)
        universal-date
        (multiple-value-bind (sec min hour day month year)
            (decode-universal-time
             (or universal-date (get-universal-time))
             *time-zone*)
          (format-date-time year month day hour min sec))))


(defmethod class-export-csv-row (object)
  (let ((class (class-of object)))
    (nconc (loop for slot in (class-slots class)
                 for db-type = (cl-sfx::db-type slot)
                 when db-type
                 collect (funcall (make-slot-printer db-type)
                                  (slot-value-using-class class object slot)))
           (list (format-universal-date-time (cl-sfx::effective-date object))
                 (format-universal-date-time (cl-sfx::stamp-date object))))))

(defmethod class-export-csv-header (class)
  (nconc (loop for slot in (class-slots (class-of class))
               when (cl-sfx::db-type slot)
               collect (make-name (slot-definition-name slot)))
         '("Effective Date"
           "Stamp Date")))







(defgeneric object-description (object)
  (:documentation "Return a string descrbing the object."))

(defmethod object-description (object)
  (princ-to-string object))

(defmethod object-description ((object string))
  object)

(defmethod object-description ((object class))
  (make-name (class-name object)))

(defmethod object-description ((object symbol))
  (make-name object))

(defun context ()
  (current-entities (active-user))
  
  )

(defun print-context ()
 #|
  (with-output-to-string (str)
    (loop for space = "" then ", "
          for id in (context)
          for entity = (get-entity-by-id id)
          when entity
          do
          (princ space str)
          (princ (entity-name entity) str)))
 |#
  )


(defun current-date-time ())




(defgeneric ensure-value-info (slot-name))

(defmethod ensure-value-info (slot-name)
  (alexandria:ensure-gethash slot-name *slot-value-info* (make-value-info)))


(defun present-slot (slot object &key read-only)

  (let* ((*current-slot* slot)
         (class (class-of object))
         (slot-name (slot-definition-name slot))
         (db-type (cl-sfx::db-type slot))
         (name (string slot-name)))
    (multiple-value-bind (info existing-info) (ensure-value-info slot-name)
      (let* ((virtual (cl-sfx::virtual slot))
             (value (if existing-info
                        (value-info-value info)
                        (setf (value-info-read-only info) read-only
			      (value-info-row info) object
			      (value-info-value info)
                              (if virtual
                                  (funcall virtual object)
                                  (if (slot-boundp object slot-name)
				      (slot-value-using-class class object slot))))))
             (invalid (shiftf (value-info-invalid info) nil)))

        (if read-only
            (with-html

              (:div :class "form-group"
                    (:label ;;:class "col-sm-3 control-label docs"
		   
                            (esc (make-name name)))
                    (:div ;;:class "col-sm-6"
                          (esc (funcall (make-slot-printer db-type) value)))))
            (catch 'sub-grid
	   ;;  (break "~A ~A ~A ~A" db-type name value info)
              (let* ((result)
                     (body (with-html-string
                             (setf result (present-value db-type name value info)))))

                (if (eq result :inline)
                    (princ body)
                    (with-html
		      
                      (:div :class (format nil "form-group~:[~; has-error~]" invalid)
                            (:label :id (frmt "label-~a" (remove-non-id-chars name))
                                    ;;:class "col-sm-3 control-label docs"
                                    (esc (make-name name)))
                            (:div ;;:class "col-sm-6"
                                  :id (frmt "group-~a" (remove-non-id-chars name))
                                  (str body)
                                  (when invalid
                                    (htm
                                     (:span :class "help-block"
                                            (esc invalid)))))))))))))))

(defun checkbox-list-values (name)
  (let ((params nil))
    (dolist (param (post-parameters*))
      (when (and (not (equalp (cdr param) "off")) 
		 (search name (car param) :test #'string-equal))
	(pushnew (cdr param) params)))
    (reverse params)))

(defun list-values (name)
  (let ((params nil))
    (dolist (param (post-parameters*))
      (when (equalp name (car param))	
	(pushnew (cdr param) params)))
    (reverse params)))

(defun parse-value-crap (val)
  (cond ((string-equal "nil" (frmt "~A" val))
	 nil)	
	(t val)))

(defun accept-slot (slot)
  (let* ((slot-name (slot-definition-name slot))
         (name (string slot-name))
         (info (ensure-value-info slot-name))
         (posted (parse-value-crap (post-parameter name))))
    
      (cond ((value-info-read-only info)
	 ;;  (break "fuck-me")
	   )
          ((value-info-no-accept info)
           ;; It was already processed by some other means
	 ;;  (break "? esi")
           (if (and (not (value-info-value info))
                    (or (cl-sfx::required slot)
                        (cl-sfx::key slot)))
               (not (setf (value-info-invalid info) "Required field"))
               t))
	  ((and (consp (db-type slot))
		(equalp (frmt "~A" (car (db-type slot))) "data-tree"))
	   (multiple-value-bind (value error) 
	       (accept-list 'data-tree 
			    (cdr (cl-sfx::db-type slot)) 
			    (checkbox-list-values (substitute #\_ #\- name))
			    info)
	  
	     (cond (error
		    
                    (setf (value-info-value info) posted
                          (value-info-invalid info) error)
                    nil)
                   (t
		  
                    (setf (value-info-value info) value)
                    t))))
	  ((and (consp (db-type slot))
		(equalp (frmt "~A" (car (db-type slot))) "data-group"))
	   (multiple-value-bind (value error) 
	       (accept-list 'data-group 
			    (cdr (cl-sfx::db-type slot)) 
			    (checkbox-list-values (substitute #\_ #\- name))
			    info)
	     ;;(break "error 1 ~A ~A" error value)
	     (cond (error
		    
                    (setf (value-info-value info) posted
                          (value-info-invalid info) error)
                    nil)
                   (t
                    (setf (value-info-value info) value)
                    t))))
	  ((and (consp (db-type slot))
		(equalp (frmt "~A" (car (db-type slot))) "data-member"))
	   (multiple-value-bind (value error) 
	       (accept-list 'data-member 
			    (cdr (cl-sfx::db-type slot)) 
			    posted
			    info)
	   ;;  (break "error 1.1 ~A ~A" value (post-parameters*))
	     (cond (error
	;;	    (break "error 1.1.1 ~A ~A" error value)
                    (setf (value-info-value info) posted
                          (value-info-invalid info) error)
                    nil)
                   (t
                    (setf (value-info-value info) value)
                    t))))
	  ((and (consp (db-type slot))
		(equalp (frmt "~A" (car (db-type slot))) "values"))
	   (multiple-value-bind (value error) 
	       (accept-list 'values
			    (cdr (cl-sfx::db-type slot)) 
			    (list-values name)
			    info)
	    ;; (break "error 2 ~A" error)
	     (cond (error
		    
                    (setf (value-info-value info) posted
                          (value-info-invalid info) error)
                    nil)
                   (t
                    (setf (value-info-value info) value)
                    t))))
          ((not (empty-p posted))
           (multiple-value-bind (value error) 
	       (accept-value (cl-sfx::db-type slot) posted info)
	    ;; (break "error 3 ~A" error)
             (cond (error
		    
                    (setf (value-info-value info) posted
                          (value-info-invalid info) error)
                    nil)
                   (t
                    (setf (value-info-value info) value)
                    t))))
          ((or (cl-sfx::required slot)
               (cl-sfx::key slot))
	 ;;  (break "?")
           (setf (value-info-invalid info) "Required field")
           nil)
         
	  (t
	   ;;(break "??????")
	   t
	   ))))

(defun info-slot-value (slot-name)
  (value-info-value (gethash slot-name *slot-value-info*)))

(defun parent-info-slot-value (slot-name)
  (value-info-value (gethash slot-name *parent-slot-value-info*)))

(defun validate-slot (slot)
  (let* ((slot-name (slot-definition-name slot))
         (info (ensure-value-info slot-name)))
    (if (or (value-info-invalid info)
            (not (value-info-value info)))
        t
        (not
         (setf (value-info-invalid info)
               (funcall (cl-sfx::validate slot) (value-info-value info)))))))



(defgeneric make-slot-printer (db-type)
  (:documentation
   "Return a function that can be used to print a value of DB-TYPE"))

(defmethod make-slot-printer (db-type)
  #'object-description)

(defgeneric present-value (type name value info))
(defgeneric accept-value (type value info))

(defmethod present-value ((type (eql 'string)) name value info)
  (with-html
    (:input :type type
            :class "docs form-control"
            :name name
            :value (escape value))))

(defmethod accept-value ((type (eql 'string)) value info)
  (trim-whitespace value))

(defun render-select (name items value &key first-value blank-allowed
                                            allow-deselect
                                            on-change)
  (let ((select (make-widget 'select
                             :name name
                             :class "docs form-control")))
    (setf (blank-allowed select) blank-allowed)
    (setf (first-item select) first-value)
    (setf (items select) items)
    (setf (allow-deselect select) allow-deselect)
    (setf (value select) value)
    (setf (on-change select) on-change)
    (render select)
    select))



(defun render-edit-field (name value
                          &key data-element
                            required
                            data-type
                            (type :text)
                            data (blank-allowed t)
                            min
                            max
                            posted-value
                            on-change
                            first-value
                            (cols 85)
                            (rows 5)
                            (mce t))
  (let ((value (or (and posted-value
                        (parameter name))
                   value)))
    (with-html
      (case type
        (:span
         (htm (:input :class "docs form-control"
                      :type "text"
                      :name name
                      :disabled t
                      :readonly t
                      :value (escape value))))
        (:select
         (render-select (or data-element name) data value
                        :blank-allowed blank-allowed
                        :on-change on-change
                        :first-value first-value))
        (:textarea
         (htm (:textarea :class (frmt "docs form-control~:[ no-mce~;~]~@[ ~a~]" mce (if required "required"))
                         :id name
                         :name name
                         :cols cols :rows rows
                         :onchange on-change
                         (str (escape value)))))
        (:password
         (htm (:input :type "password"
                      :class (frmt "form-control~@[ ~a~]" (if required "required"))
                      :name name
                      :value (escape value))))
        (:date
         (htm (:input :type "text"
                      :class (frmt "docs form-control date~@[ ~a~]"
				   (if required "required"))
		      :data-provide "datepicker"
		      :data-date-format "dd M yyyy"
                      :name name
                      :id name
                      :value (escape value)
                      :onchange on-change)))
        (:checkbox
         (htm (:input :type "checkbox"
                      :class "docs"
                      :id name
                      :name name
                      :checked (if value "true")
                      :onchange on-change)))
        (t
         (htm
          (:input :type type
                  :class (frmt "docs form-control ~:[~;required~]~@[~a~]" required data-type)
                  :name name
                  :min min
                  :max max
                  :value (escape value))
          (:div :style "display:none;"
                :name (format nil "validate-~A" name)
                :id (format nil "validate-~A" name)
                (:img :src "/images/q-icon.png"))))))))

(defmethod present-value ((type (eql 'multi-line-string)) name value info)
  (render-edit-field
   name value
   :type :textarea))

(defmethod accept-value ((type (eql 'multi-line-string)) value info)
  (accept-value 'string value info))

(defmethod present-value ((type (eql 'boolean)) name value info)
  (with-html
    (:input :type "checkbox"
            :name name
            :id name
            :checked value)))

(defmethod accept-value ((type (eql 'boolean)) value info)
  (equal value "true"))


(defmethod present-value ((type (eql 'integer)) name value info)
  (present-value 'string name value info))

(defmethod accept-value ((type (eql 'integer)) value info)
  (let ((string (trim-whitespace value)))
    (or (ensure-parse-integer string)
        (values nil "Invalid integer"))))

(defmethod present-value ((type (eql 'percentage)) name value info)
  (present-value 'string name value info))

(defmethod accept-value ((type (eql 'percentage)) value info)
  (let ((integer (ensure-parse-integer (trim-whitespace value))))
    (cond ((not integer)
           (values nil "Invalid integer"))
          ((<= 0 integer 100)
           integer)
          (t
           (values nil "Percentage must be between 0 and 100.")))))

(defun fmt-money (value &key (include-comma t))
  (typecase value
    (null "")
    (number
     (multiple-value-bind (quot rem) (truncate (round value 1/100) 100)
       (format nil "~@?.~2,'0d"
               (if include-comma "~:d" "~d")
               quot (abs rem))))
    (t
     (princ-to-string value))))

(defmethod present-value ((type (eql 'number)) name value info)
  (present-value 'string name (fmt-money value) info))

(defmethod make-slot-printer ((db-type (eql 'number)))
  #'fmt-money)

(defun whitespace-p (char)
  (case char
    ((#\Space #\Newline #\Tab #\Return)
     t)))

(defun parse-money (string)
  (if (stringp string)
      (let* ((comma  (position #\, string))
             (string (if comma
                         (remove #\, string :start comma)
                         string))
             (dot (position #\. string))
             (start (position-if-not #'whitespace-p string))
             (last-non-whitespace (position-if-not #'whitespace-p string :from-end t))
             (end (and last-non-whitespace
                       (1+ last-non-whitespace))))
        (labels ((bad-number ()
                   (return-from parse-money))
                 (parse-integer-completely (start end)
                   (multiple-value-bind (number pos)
                       (parse-integer string :junk-allowed t :start start :end end)
                     (if (= end pos)
                         number
                         (bad-number))))
                 (parse-decimal (start end multiple)
                   (when (= start end)
                     (bad-number))
                   (let ((result 0))
                     (loop for i from start below end
                           for digit = (digit-char-p (char string i))
                           unless digit do (bad-number)
                           do
                           (setf multiple (* multiple 10)
                                 result (+ (* result 10) digit)))
                     (values result multiple))))
          (cond ((not start)
                 (bad-number))
                (dot
                 (let* ((whole (or (parse-integer-completely start dot)
                                   0)))
                   (multiple-value-bind (decimal multiple)
                       (parse-decimal (1+ dot) end
                                      (if (char= (char string start) #\-)
                                          -1
                                          1))
                     (cond ((zerop decimal)
                            whole)
                           ((zerop whole)
                            (/ decimal multiple))
                           (t
                            (/ (+ (* whole multiple) decimal)
                               multiple))))))
                (t
                 (parse-integer-completely start end)))))
      string))

(defmethod accept-value ((type (eql 'number)) value info)
  (or (parse-money value)
      (values nil "Invalid number")))

(defmethod present-value ((type (eql 'float)) name value info)
  (present-value 'string name (and value
                                   (format nil "~f" value)) info))

(defmethod make-slot-printer ((db-type (eql 'float)))
  (lambda (x)
    (and x
         (format nil "~f" x))))

(defun parse-number (string)
  (etypecase string
    (string
     (values (ignore-errors (parse-number:parse-number string))))
    (number
     string)
    (null)))

(defmethod accept-value ((type (eql 'float)) value info)
  (or (parse-number value)
      (values nil "Invalid number")))

(defmethod present-value ((type (eql 'longitude)) name value info)
  (present-value 'float name value info))

(defmethod present-value ((type (eql 'latitude)) name value info)
  (present-value 'float name value info))

(defmethod make-slot-printer ((db-type (eql 'longitude)))
  (make-slot-printer 'float))

(defmethod make-slot-printer ((db-type (eql 'latitude)))
  (make-slot-printer 'float))

(defmethod accept-value ((type (eql 'longitude)) value info)
  (let ((number (parse-number value) ))
    (or (and (numberp number)
             (< -180 number 180)
             number)
        (values nil "Invalid longitude"))))

(defmethod accept-value ((type (eql 'latitude)) value info)
  (let ((number (parse-number value)))
    (or (and (numberp number)
             (< -90 number 90)
             number)
        (values nil "Invalid latitude"))))

(defun enumerate (sequence &key key)
  (loop for element being the element of sequence
        for i from 0
        collect (list i (if key
                            (funcall key element)
                            element))))

(defun present-select (items name value info &key key)
  (setf (value-info-data info) items)
  (render-select name (enumerate items :key key)
                 (if (and value key) (funcall key value) value) :blank-allowed t))

(defun accept-select (value info)
  (let ((index (ensure-parse-integer value)))
    (if (and (typep index 'unsigned-byte)
             (< index (length (value-info-data info))))
        (elt (value-info-data info) index)
        (values nil "Bad value"))))

;;TODO: Sort out entities set if none has been set??
(defmethod present-value ((type (eql 'cl-sfx::entity)) name value info)
  (present-select (loop for entity in (cl-sfx::relevant-entities)
                      
                        collect entity)
                  name value info :key #'entity-name))

(defmethod accept-value ((type (eql 'cl-sfx::entity)) value info)
  (let* ((index (ensure-parse-integer value))
         (entity (and (typep index 'unsigned-byte)
                      (nth index (value-info-data info)))))
    (or entity
        (values nil "Bad value"))))

(defmethod present-value ((type (eql 'root-entity)) name value info)
  #|
  (let* ((roots (find-root-entities))
         (entities (loop for entity across (docs (get-collection (system-db) "entities"))
                         when (and (find (id entity) (context))
                                   (find entity roots :key #'entity))
                         collect entity))
         (names (loop for i from 0
                      for entity in entities
                      collect (list i (entity-name entity)))))
    (setf (value-info-data info) entities)
    (render-select name names
                   (and (typep value 'entity)
                        (entity-name value))
                   :blank-allowed t))
  
  (loop for allsort across (allsorts)
        when (equalp (allsort-sort allsort) sort)
     collect (sort-value allsort))
  |#
  )

(defmethod accept-value ((type (eql 'root-entity)) value info)
  (accept-value 'entity value info))

(defmethod present-value ((type (eql 'email)) name value info)
  (present-value 'string name value info))

(defmethod accept-value ((type (eql 'email)) value info)
  (let ((value (trim-whitespace value)))
    (if (find #\@ value)
        value
        (values nil "Invalid email address"))))

(defmethod present-value ((type (eql 'unique-email)) name value info)
  (present-value 'email name value info))

(defmethod accept-value ((type (eql 'unique-email)) value info)
  (multiple-value-bind (value invalid) (accept-value 'email value info)
    (cond (invalid
           (values value invalid))
          ((get-user value)
           (values nil "User with this email already exists."))
          (value))))


(defmethod present-value ((type (eql 'url)) name value info)
  (present-value 'string name value info))

(defmethod accept-value ((type (eql 'url)) value info)
  (accept-value 'string value info))

(defmethod make-slot-printer ((type (eql 'url)))
  #'identity
  ;; (lambda (x)
  ;;   (with-html (:a :href (frmt "/insite/~a" x) (esc x))))
  )

(defmethod present-value ((type (eql 'yes-no)) name value info)
  (render-select name '("Yes" "No") value :blank-allowed t))

(defmethod accept-value ((type (eql 'yes-no)) value info)
  (or (car (member value '("Yes" "No") :test #'equalp))
      (values nil "Bad value")))


(defun present-allsort (sort name value info)
  (present-select
   (find-allsorts-for-validation sort)
   name value info))

;;;

(defmethod present-value ((type cons) name value info)
  (present-list (car type) (cdr type) name value info))

(defmethod accept-value ((type cons) value info)
  (accept-list (car type) (cdr type) value info))

(defmethod present-value ((type (eql 'date)) name value info)
  (render-edit-field name (format-universal-date value) :type :date))

(defmethod accept-value ((type (eql 'date)) value info)
  (or (parse-date value)
      (values nil "Invalid date format")))

(defun format-date (date)
  (if (typep date 'unsigned-byte)
      (format-universal-date date)
      date))



(defmethod make-slot-printer ((type (eql 'date)))
  #'format-date)

(defmethod present-value ((type (eql 'boolean)) name value info)
  (with-html
    (:input :type "checkbox"
            :name name
            :id name
            :checked value)))

(defmethod accept-value ((type (eql 'boolean)) value info)
  (equal value "true"))

(defun script-to-js-cleanup (script)
  (let ((js-script script))
    
    ;;Remove bangs
    (setf js-script (replace-all  	
		     (if (stringp js-script)
			 js-script
			 (if (listp js-script)
			     (string-downcase 
			      (write-to-string (first js-script) :escape t))
			     (string-downcase 
			      (write-to-string js-script :escape t))
			     ))
		     "#'" ""))
    
    ;;replace newline
    (setf js-script (replace-all js-script (frmt "~A" #\Newline) "\\\n"))
    
     ;;replace newline
    (setf js-script (replace-all js-script (frmt "~A" #\Linefeed) "\\\n"))
    
    ;;replace tab
    (setf js-script (replace-all js-script (frmt "~A" #\Tab) "\\t"))
    
     ;;replace '
    (setf js-script (replace-all js-script "'" "\\'"))
    
   ;;    #\Page  #\Return 
    
    )
  )

(defmethod present-value ((type (eql 'script)) name value info)
  
  
  (let ((js-name (frmt "~A_~A" (substitute #\_ #\- name) (random 123456))))
  
    (with-html
      (:textarea :id js-name :name name :cols 50 :rows 10 value)

  
      
      (defer-js "var myCodeMirror_~a = CodeMirror.fromTextArea(document.getElementById(\"~a\"), {autoCloseBrackets: true, matchBrackets: true});myCodeMirror_~a.getDoc().setValue('~a');
add_scripts(myCodeMirror_~a);
"
	  js-name
	js-name
	js-name     
	(script-to-js-cleanup value)      
	js-name))))

(defmethod accept-value ((type (eql 'script)) value info)
  ;;TODO: read-from-string check valid functions
  ;;compile to check for syntax errors
(frmt "~A" value))

;;;



(defun find-allsorts-for-validation (sort)
  (declare (ignore sort))
  #|
  (loop for allsort across (allsorts)
        when (equalp (allsort-sort allsort) sort)
     collect (sort-value allsort))
  |#
  )




(defmethod present-value ((type cons) name value info)
  (present-list (car type) (cdr type) name value info))

(defmethod accept-value ((type cons) value info)
(accept-list (car type) (cdr type) value info))


(defmethod present-list ((type (eql 'member)) parameters name value info)
  (present-select parameters name value info :key #'object-description))

(defmethod accept-list ((type (eql 'member)) parameters value info)
  (let* ((index (ensure-parse-integer value))
         (value (nth index parameters)))
    (or value
        (values nil "Bad value"))))


(defun enumerate-member (sequence &key key)
  (loop for element being the element of sequence
     
        collect (list (if key
                            (funcall key element)
                            element)
		      (if key
                            (funcall key element)
                            element))))

(defun present-select-member (items name value info &key key)
  (setf (value-info-data info) items)
  (render-select name (enumerate-member items :key key)
                 (if (and (not (empty-p value)) key) 
		     (funcall key value) 
		     value) 
		 :allow-deselect t
		 :blank-allowed t))


;;TODO: do a generic get col and pass lic or sys or merge in with parameters 

(defmethod present-list ((type (eql 'data-member)) parameters name value info)
  (when (atom parameters)
    (setq parameters (list parameters)))
   (destructuring-bind (data-member &key key-accessor) parameters
     (present-select-member 
      (loop for doc across (get-collection-docs
				  data-member)
	 collect doc)
      name value info :key key-accessor)))

(defmethod accept-list ((type (eql 'data-member)) parameters value info)
  (destructuring-bind (data-member &key key-accessor) parameters
    (let* ((valid-value (find-docx 
			 data-member
			 :test (lambda (doc)
				 (string-equal value (slot-value doc key-accessor))))))
      
      (or (or valid-value "")
	  (values nil "Bad value")))))


(defun render-check-list (name items value &key)
  (let ((cb-list (make-widget 'checkbox-list
                             :name name
                             ;;:class "docs form-control"
			     
			     )))

    (setf (orientation cb-list) :vertical)
    (setf (value cb-list) value)
    (setf (items cb-list) items)
    (render cb-list)
    cb-list))

(defun enumerate-check-items (sequence value &key key)
  (loop for element being the element of sequence
        for i from 0
        collect (list i (if key
                            (funcall key element)
                            element)
		      (find element value)
		      )))


(defun present-checkbox-list (items name value info &key key)
  (setf (value-info-data info) items)
  (render-check-list name (enumerate-check-items items value :key key)
               value))



;;NEXT: sort out sub-group
(defmethod present-list ((type (eql 'data-group)) parameters name value info)
  (when (atom parameters)
    (setq parameters (list parameters)))
  (destructuring-bind (data-member &key key-accessor script)
      parameters    
    (present-checkbox-list
     
     (if script
	 (call-script script info)
	 (loop for doc across (get-collection-docs
			       data-member)
	    collect doc))
     name value info :key key-accessor)))

(defmethod accept-list ((type (eql 'data-group)) parameters value info)
  (let ((values)
	(index 0))          
    (dolist (tick value)    
      (when (equalp tick "true")
	(pushnew (nth index (value-info-data info)) values))
      (incf index))
    (values (reverse values) nil)))

(defun present-checkbox-tree (name tree selected child-func value-func check-func info)
  
  (let ((tree-widget (make-widget 'data-tree-selector 
				  :name name)))
  
    (setf (tree tree-widget) tree)
    (setf (selected tree-widget) selected)
    (setf (child-func tree-widget) child-func)
    (setf (value-func tree-widget) value-func)
    (setf (check-func tree-widget) check-func)
    (render tree-widget)
   (setf (value-info-data info) (enum-data tree-widget))
    ))


(defmethod present-list ((type (eql 'data-tree)) parameters name value info)
  (when (atom parameters)
    (setq parameters (list parameters)))
  (destructuring-bind (data-member &key tree-func
				   selected
				   child-func value-func check-func)
      parameters    
  
    (present-checkbox-tree
     name
     (if tree-func
	 (call-script tree-func info)
	 (loop for doc across (get-collection-docs
			       data-member)
	    collect doc))
     (or value selected)
     child-func value-func check-func
     info)))

(defmethod accept-list ((type (eql 'data-tree)) parameters value info)
  (let ((values)
	(index 0))    
    (dolist (tick value)    
      (when (equalp tick "true")
	(pushnew (nth index (value-info-data info)) values))
      (incf index))
    (values (reverse values) nil)))

(defun select-add (name values)
  (with-html    
    (:div :class "select-add"
          (:input :type "text"
                  :class "form-control"
                  :id name)
          (:a :href (frmt "javascript:select_add_row(\"~a-table\",\"~:*~a\")"
                          name)
              :title "Add"
              ;;(:span :class "grid-button glyphicon glyphicon-plus-sign")
	      "Add")
          (:table :id (frmt "~a-table" name)
            (:thead (:tr (:th "") (:th "")))
            (:tbody
             (loop for value in values
                   do (htm (:tr
                            (:td (:input :type "hidden" :name name :value (escape value))
                                 (esc value))
                            (:td "")))))))
    (defer-js "(function(){~
var table = $('#~a-table').DataTable({info:false,searching:false, ordering: false,~
'language': {'emptyTable': 'Add new entries above'},~
paging:false,scrollY:'200px','scrollCollapse':true,~
'columnDefs': [{~
'targets': 1,'data': null,~
'defaultContent': '<a href=\"javascript:;\" title=\"Remove\" alt=\"Remove\">Remove</a>'}]
});
$('#~:*~a-table tbody').on('click', 'a', function () {table.row($(this).parents('tr')).remove().draw(); })
})()"
      name)))

(defmethod present-list ((type (eql 'values)) parameters name value info)
 (select-add name value))

(defmethod accept-list ((type (eql 'values)) parameters value info)
  value)

(defmethod present-list ((type (eql 'integer)) parameters name value info)
  (present-value type name value info))

(defmethod accept-list ((type (eql 'integer)) parameters value info)
  (multiple-value-bind (value error) (accept-value type value info)
    (if error
        (values nil error)
        (destructuring-bind (min &optional (max '*)) parameters
          (cond ((and (not (eq min '*))
                      (not (eq max '*)))
                 (if (<= min value max)
                     value
                     (values nil (format nil "The value must be in the range of ~a-~a" min max))))
                ((not (eq max '*))
                 (if (<= value max)
                     value
                     (values nil (format nil "The value must be less than or equal to ~a" max))))
                (t
                 (if (<= min value)
                     value
                     (values nil (format nil "The value must be more than or equal to ~a" min)))))))))

(defmethod present-list ((type (eql 'list)) parameters name value info)
  (setf (getf *sub-grids* *current-slot*) info)
  (throw 'sub-grid t))



(defmethod accept-list ((type (eql 'list)) parameters value info)
  (value-info-value info))

(defmethod present-list ((type (eql 'db-object)) parameters name value info)
  (setf (getf *sub-grids* *current-slot*) info)
  (throw 'sub-grid t))



(defmethod accept-list ((type (eql 'db-object)) parameters value info)
  (value-info-value info))




(defmethod class-grid-buttons ((class t))
  (list *edit-button* *delete-button* *delete-version-button*))

;;(register-widget *insite-theme* 'grid 'generic-grid)

(defmethod handle-action ((grid generic-grid) (action (eql :delete)))
  (when (editable grid)
    (remove-doc (editing-row grid) (email (current-user)))))

(defmethod handle-action ((grid generic-grid) (action (eql :delete-all)))
  (when (editable grid)
    (loop for doc being the elements of (rows grid)
          do (remove-doc doc (email (current-user))))
    (finish-editing grid)
    (update-table grid)))

(defmethod handle-action ((grid generic-grid) (action (eql :delete-version)))
  (when (editable grid)
    (let ((doc (editing-row grid)))
      (delete-version (or (top-version doc)
                          doc)
                      doc
                      (email (current-user))))
    (finish-editing grid)
    (update-table grid)))

(defmethod handle-action ((grid generic-grid) (action (eql :delete-all-versions)))
  (when (editable grid)
    (loop for doc being the elements of
          ;; delete older versions first so that the 0 version doesn't
          ;; become the top-level
          (sort (copy-seq (rows grid)) #'< :key #'version)
          do (delete-version (or (top-version doc)
                                 doc)
                             doc
                             (email (current-user))))
    (finish-editing grid)
    (update-table grid)))

;;;


(defun grid-value (grid name selector)
  (frmt "[~s, $(\"#~a ~a\").val()]"
        name (name grid) selector))



(defmethod init-data-table ((grid generic-grid))
  (defer-js "
\(function(){~
var init = true;~
$('#~a').DataTable({~
'deferRender': true,~
'processing': false,~
'serverSide': true,~
'ajax': function(data, callback, settings) {~
    var url = '~Aajax/TABLE?script-name=~a&id=~a~@[&~a~]';~
    var select = $('#~a')[0];~
    if (select) {~
      data.filter = select.value
    }~

    var script = $('#~a .search-script').val();
    
    if (script) {~
      data.script = script
    }

    if (init) {~
      data.init = 't';
    }~

    if (settings._row_to_expand != undefined) {~
      data.expand = settings._row_to_expand;
      settings._row_to_expand = undefined;
    }~

    if (settings._row_to_collapse != undefined) {~
      data.collapse = settings._row_to_collapse;
      settings._row_to_collapse = undefined;
    }~
    data.pageid = pageid;
    $.post(url, data, function(json) {~
        if($.isArray(json)) eval(json[1]);~
        else {
          $('~:*#~a .script-error').html(json['script-error'] || '');
          $('~:*#~a .filter-error').html(json['filter-error'] || '');
          callback(json);
        }
    });~
    init = false;
},~
~:['searching': false,~;~]~
~:['ordering': false,~;~]~
~:[~;'scrollX': true,~]~
'lengthMenu': [5, 10, 25, 50, 100],
'pageLength': 10,
'scrollCollapse': true,
~@['columnDefs': [{'orderable': false, 'targets': [~{~a~^,~}]}],~]~
~@['drawCallback': ~a,~]
~:[~2*~;'order': [[~a,~s]],~]
'dom':~:['lf<\"grid-inline-buttons\">rtip'~;'l<\"asearch-toggle\">f<\"grid-inline-buttons\"><\"col-sm-offset-1 col-sm-4 filter-error\"><\"asearch\">rtip'~],~:*
});

})();~
~:[~4*~;~
$('.dataTables_filter input').addClass('form-control').attr('placeholder','Search...');~
$('.dataTables_length select').addClass('form-control');
$('#~a .asearch-toggle').click(function(){$('~:*#~a .asearch').toggle()}).html(\"~a\");
$('#~a .asearch').html(\"~a\");
~]
$('#~a .grid-inline-buttons').html(\"~a\");"
      (sub-name grid "table") (site-url *sfx-system*) (script-name*) (name grid)
(query-string*)
    (sub-name grid "filter-selector-select")
    (name grid)
    (searchable grid)
    (sortable grid)
    (scroll-x grid)
    (and (editable grid)
         (list* (+ (length (columns grid))
                   (if (prefix-buttons grid)
                       1
                       0))
                (if (prefix-buttons grid)
                    (list 0))))
    (js-callback grid)
    (or
     (prefix-buttons grid)
     (not (equal (initial-sort-column grid)
                 '(0 :ascending))))
    (+ (car (initial-sort-column grid))
       (if (prefix-buttons grid)
           1
           0))
    (if (eq (cadr (initial-sort-column grid))
            :ascending)
        "asc"
        "desc")
    (and
     (check-page-permission "Advanced Search")
     (searchable grid)
     (advanced-search grid))
    (name grid)
    (replace-newlines (with-html-string-no-indent
                        (:span :class "glyphicon glyphicon-cog")))
    (name grid)
    nil;;(make-script-editor grid)

    (name grid)
    (replace-newlines
     (escape-js
      (with-html-string-no-indent
        (render-inline-buttons grid))))))

(defclass peach-grid-filter-selector (grid-filter-selector)
  ()
  (:default-initargs :clear-filter-title "Clear Filter"
                     :css-class "grid-filter"))

;(register-widget *insite-theme* 'grid-filter-selector 'peach-grid-filter-selector)


(defmethod render-export-button ((grid generic-grid))
  (with-html
    (:a 
     :href "#"
     :onclick (format nil "window.open(\"~Aexport-csv?grid=~a&pageid=~a~@[&~a~]\")"
		      (site-url *sfx-system*)
                      (name grid)
                      (context-id *sfx-context*)
                      (query-string*))
     :title "Export CSV"
     (:i :id "export-csv" :class "fa fa-table docs"))))

(defmethod render-header-buttons ((grid generic-grid))
  (call-next-method)
  (render-export-button grid))

(defmethod open-dialog (widget (grid generic-grid) &key width height)
  (declare (ignore widget grid width height))
  )

(defmethod close-dialog (widget (grid generic-grid))
  (declare (ignore widget grid))
  )

;;;
;;;
;;;;
;;;;


(defclass grid-script-filter (entity-doc grid-filter)
  ((page :initarg :page
         :initform nil
         :accessor page)
   (grid-name :initarg :grid-name
              :initform nil
              :accessor grid-name) 
   (script :initarg :script
           :initform nil
           :accessor script))
 ;; (:collection "grid-script-filter")
  (:metaclass data-class))

#|
(defmethod handle-action ((grid generic-grid) (action (eql :save-filter)))
  (with-parameters (name script)
    (unless (or (empty-p name)
                (empty-p script))
      (persist (make-instance 'grid-script-filter
                              :entity (let ((entity-id (car (context))))
                                        (and entity-id
                                             (get-entity-by-id entity-id)))
                              :name name
                              :page (page-name)
                              :grid-name (name grid)
                              :script script
                              :top-level t))
      (defer-js "$('#~a').append($('<option>', {value: '~a', text: '~:*~a'}))"
          (sub-name grid "filter-selector-select") name))))

(defmethod list-grid-filters :around ((grid generic-grid))
  (let ((defined (call-next-method)))
    (append defined
            (loop with page = (page-name)
                  with grid-name = (name grid)
                  for doc across (grid-script-filters)
                  when
                  (and (match-context-entities doc)
                       (equal (page doc) page)
                       (equal (grid-name doc) grid-name))
                  collect doc))))

(defmethod apply-grid-filter (grid (filter grid-script-filter))
  (let* ((script (compile-grid-script (script filter) grid "filter-error"))
         (var-cons (cons "x" nil))
         (vars (list var-cons)))
    (declare (dynamic-extent vars var-cons))
    (values
     (and script
          (handler-case
              (loop for doc being the element of (get-rows grid)
                    do (setf (cdr var-cons) doc)
                    when (funcall script vars)
                    collect doc) 
            (error (c)
              (setf *script-error*
                    (list "filter-error"
                          (replace-newlines (with-html-string-no-indent
                                              (:div :class "alert alert-danger"
                                                    (:span :class "glyphicon glyphicon-exclamation-sign")
                                                    "Error executing filter:"
                                                    (:p (esc (princ-to-string c))))))))
              nil)))
     t)))
|#

