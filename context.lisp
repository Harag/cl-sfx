(in-package :cl-sfx)

(defclass sfx-permission ()
  ((permission-name :initarg :permission-name
		 :accessor permission-name
		 :initform nil
		 :db-type string
		 :header t
		 :key t)
   (parameters :initarg :parameters
		    :accessor parameters
		    :initform nil
		    :db-type (values string)))
  (:documentation "")
  (:metaclass data-versioned-class))


(defclass sfx-context-definition (license-doc)
  ((context-name :initarg :context-name
		 :accessor context-name
		 :initform nil
		 :db-type string
		 :key t)
   (from-class :initarg :from-class
	       :accessor from-class
	       :initform nil
	       :db-type string
	       :header t)
   (doc-spec :initarg :doc-spec
	     :accessor doc-spec
	     :initform nil
	     :db-type (data-member doc-spec :key-accessor name))
   (permissions :initarg :permissions
			:accessor permissions
			:initform nil
			:db-type (list sfx-permission))
   (url :initarg :url
	:accessor url
	:initform nil
	:db-type string)
   (for-everyone :initarg :for-everyone
	:accessor for-everyone
	:initform nil
	:db-type boolean)
   (args :initarg :args
	 :accessor args
	 :initform ()
	 ;;:db-type (list string)
	 )
   (body :initarg :body
	 :accessor body
	 :initform nil)
   (grid-p :initarg :grid-p
	   :accessor grid-p
	   :initform nil))
  (:documentation "")
  (:metaclass data-versioned-class)
  (:collection-name "sfx-context-definitions")
  (:collection-type :merge)
  (:default-initargs :top-level t))

(defclass sfx-context ()
  ((context-id :initarg :context-id
	       :accessor context-id
	       :initform (random 10810))
   (context-definition :initarg :context-definition
		       :accessor context-definition
		       :initform nil)
   (session :initarg :session
	    :accessor session
	    :initform nil)
   (url :initarg :url
	:accessor url
	:initform nil)
   (new-p :initarg :new-p
	  :accessor new-p
	  :initform t)
   (reuse-p :initarg :reuse-p
	    :accessor reuse-p
	    :initform nil)
   (widget-sequence :initarg :widget-sequence
		    :accessor widget-sequence
		    :initform nil)
   (cache :initarg :cache
	     :accessor cache
	     :initform (make-hash-table :test #'equalp)))
  (:documentation "An instance of a context within the current user session."))


(defvar *page-id-lock* (bordeaux-threads:make-lock))

(defun generate-context-id ()
  (declare (optimize speed))
  (format nil "~:@(~36r~)" (random (expt 2 32))))

(defun generate-new-sfx-context (session name)
  (let* ((contexts (contexts session))
	 (context-def (find-doc-from-docs (context-definitions (system session))
					  :predicate (lambda (doc)
						       (string-equal name (context-name doc)))))
	 (sfx-context (make-instance 'sfx-context 
			             :context-definition context-def)))
    
    ;;TODO: figure this shit out! NID and RID
    (unless context-def
      ;; (break "~A ~A ~A" context-def name (type-of name))
      
      )
    
    
    (bordeaux-threads:with-lock-held (*page-id-lock*)
      (loop for id = (generate-context-id)
	    unless (gethash id contexts)
	    do (setf (context-id sfx-context) id
		     (gethash id contexts) sfx-context)
	    return id))
    sfx-context))


;;TODO: Consider putting down structure for context calling etc. 
;;At the moment hunchentoot is taking care of it in requests.
(defmacro define-context (context-definition-list &body body)
  "*sfx-session* and *sfx-context* is availible for code in body to use."

  (when (atom context-definition-list)
    (setf context-definition-list (list context-definition-list)))
  
  (destructuring-bind (name 
		       &rest args
                       &key for-everyone permissions
                       &allow-other-keys) context-definition-list
    (declare (ignore args))
    `(progn
       ;;Register context with system.
       (setf (gethash ,name (context-definitions *sfx-system*)) 
	      (make-instance 
	       'sfx-context-definition 
	       :context-name ,name
	       :permissions (make-instance 'sfx-permission
					   :context-name ,name
					   :sub-permissions ,(or for-everyone permissions)
   					   )))
       ;;define-handler or register-handler????
       ,@body)))


(defgeneric start-sfx-context (session context-name &key &allow-other-keys)
  (:documentation "Creates a context instance."))

(defgeneric setup-sfx-context (session context)
  (:documentation "To be used to setup a context instance.."))

;;TODO: called by system (in init.lsip) to init permissions and ????
(defmethod setup-sfx-context ((context sfx-context) session)
    ;;TODO: Go look in old insite code what needs to go here? Page setup stuff?
 ;; (setf  (permissions context) nil)
  
 ;;  (setf (request-page-permissions context)        (setup-page-permissions context))
  )

(defmethod start-sfx-context ((session sfx-session) context-name  &key id request)
  (let* ((id (or id (post-parameter "pageid" request)))
	 (context (if id
		     (or (gethash id (contexts session))
			 (bad-request))
		     (generate-new-sfx-context session context-name))))
    (setf (session context) session)
    (setup-sfx-context context session)
    context))

(defmacro with-session-context (session &body body)
  :documentation "Makes sure that right session for right system instance handles renders and events. Without having to pass them to render and event-hanler."
  `(let* ((*sfx-session* ,session))
     (let ((*sfx-system* (system *sfx-session*)))
       (setf *sfx-context* (start-sfx-context ,session))
       ,@body)))

(defun find-context (id)
  (values (gethash id (contexts *sfx-session*))))

(defun parse-permissions (permissions)
  (let ((permission-objects))
    (dolist (perm permissions)
      (let* ((perm-name (if (consp perm)
			   (car perm)
			   perm)))
	(pushnew (make-instance 'sfx-permission
				:permission-name perm-name
				:parameters (and (consp perm)
						(second perm)))
		 permission-objects)))
    permission-objects))

(defun define-context-def (system		       		   
			   &key name url for-everyone permissions 
			     args body
			     no-page)

  (let ((def (or
	      (find-docx 'sfx-context-definition
			 :test (lambda (doc)
				 (string-equal (frmt "~A" name)
					       (context-name doc))))
	      (persist (make-instance 
			'sfx-context-definition 
			:license (sys-license)
			:context-name (frmt "~A" name)
			:url url
			:for-everyone for-everyone
			:permissions (parse-permissions permissions)
			:args args
			:body body)))))
    (unless def
      (break "context not created or found  ~A ~A ~A" name url *sfx-system*)
      )
    (when def
      (eval (load-context-def def system :no-page no-page))
      )))


(defgeneric load-default-pages (sfx-system))


(defmethod load-default-pages :before ((system sfx-system))

  (define-context-def system   
      
      :name 'export-csv
      :url "export-csv" 
      :for-everyone t
      :permissions nil
      :args '(grid)
      :no-page t
      :body
      `(progn 

	 (setf (content-type*) "text/plain")
	 (export-csv (get-widget grid)))
  
      )
  
  (define-context-def system  
      :name 'login 
      :url "login" 
      :for-everyone t
      :permissions nil
      :body
      `(let ((login (make-widget 'login :name "login")))
	 (with-html-string
	   (:div (render login)))))
  
  (define-context-def system  
      :name 'repl 
      :url "repl" 
      :for-everyone t
      :permissions nil
      :body
      `(let ((repl (make-widget 'script-repl :name "repl")))
	 (with-html-string
	   (:div (render repl)))))
  
  (define-context-def system  
      :name 'report-view
      :url "report-view" 
      :for-everyone t
      :permissions nil
      :body
      `(let ((report-view (make-widget 'report-view :name "report-view")))
	 (with-html-string
	   (:div (str (render report-view))))))
  
  
  (define-context-def system 
      :name 'importer
      :url "importer"
      :for-everyone t
      :permissions
      `("Update" "Delete" "Reject"
		 "Authorize" "Review" "Import"
		 "Effective Date"
		 "Effective Stamp"
		 "Clear Old Versions"
		 "Rename Keys"
		 "drill-down"
		 ,@(mapcar #'car *import-classes*))
      :body
    
      `(render (make-widget 'import-page)))
   
  (define-context-def system
      :name 'import-template
      :url "import-templates"
      :for-everyone t
      :permissions '("Update" "Delete") 
      :body 
      `(with-html-string (:div (render (make-widget 'import-templates-page)))))


  (define-context-def system 
      :name 'ftp-import 
      :url "ftp-import"
      :for-everyone t
      :permissions '("Update" "Delete")
      :body
      `(with-html-string (render-ftp-import)))


  (create-context-definition (create-or-find-class nil 'sfx-script)  
			     'sfx-script 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
  
  (create-context-definition (create-or-find-class nil 'theme)  
			     'theme 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
  
  (create-context-definition (create-or-find-class nil 'allsort)  
			     'allsort 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
  
  (create-context-definition (create-or-find-class nil 'sfx-context-definition)  
			     'sfx-context-definition
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
  
  (create-context-definition (create-or-find-class nil 'report)  
			     'report 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
  
  (create-context-definition (create-or-find-class nil 'module)  
			     'module 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
  
  (create-context-definition (create-or-find-class nil 'sfx-license)  
			     'sfx-license 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
 
  (create-context-definition (create-or-find-class nil 'entity)  
			     'entity 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
   
  (create-context-definition (create-or-find-class nil 'user)  
			     'user 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)
  
  (create-context-definition (create-or-find-class nil 'import-data)  
			     'import-data			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil)

  (create-context-definition (create-or-find-class nil 'doc-spec)  
			     'doc-spec 			     
			     :for-everyone nil
			     :permissions '(update delete search)
			     :body nil))

(defun load-context-def (def system &key no-page)
  `(define-easy-handler (,(alexandria:symbolicate (proper-intern (context-name def)) '-page)  
				     :uri ,(frmt "~A~A" (site-url system) (url def))  
				     :allow-other-keys t) ,(args def)
		,(if no-page
		     `,(body def)
		     `(with-debugging
			(progn
			  ,(when (and (url def) 
				      (not (for-everyone def)))
				 `(check-permission-or-error ,(url def) nil t))
			  ,(if (from-class def)
			       `(let* ((grid (make-widget 'generic-grid
							  :name ,(format nil "~a-grid" 
									 (strip-name
									  (context-name def)))
							  :title ,(context-name def)
							  :row-object-class 
							  (find-class ',(proper-intern (from-class def)))
							  ,@(args def))))
				  
				  (render (make-widget 'page :title ,(context-name def))
					  :body (render-to-string grid)))
			       `(render (make-widget 'page :title ,(context-name def))
					:body ,(body def))))))))


(defmethod load-context-definitions ((system sfx-system) &key &allow-other-keys)
  (with-debugging
    (load-default-ajax system)
    
    (load-default-pages system)

    (setf (context-definitions system)
	  (get-col-docs-merge "sfx-context-definitions"))

    (loop for doc across (get-col-docs-merge "doc-specs")  
       do (eval
	   (create-class doc)))
    
    (loop for doc across (get-col-docs-merge "sfx-context-definitions")  
       do (eval
	   (load-context-def doc system)))))


