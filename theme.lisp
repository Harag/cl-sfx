(in-package :cl-sfx)

(declaim (optimize (debug 3)))

(defclass key-value-pair ()
  ((key :initarg :key
	:accessor key
	:initform nil
	:key t
	:db-type string)
   (value :initarg :value
	  :accessor value
	  :initform nil
	  :db-type string))
  (:metaclass data-class))

(defclass theme (license-doc)
  ((name :initarg :name
	 :accessor name
	 :initform nil
	 :db-type string
	 :key t)
   (attributes :initarg :attributes	      
	       :accessor attributes
	       :initform nil
	       :db-type (list key-value-pair)))
  (:metaclass data-class)
  (:collection-name "themes")
  (:collection-type :license)
  (:default-initargs :top-level t)
  (:documentation "Themes are used to apply styling consistantly to widgets."))


(defmacro with-theme ((&key theme) &body body)
  "This macro is a convenience macro to expose *theme* within a let binding to be used outside of the widget render method. This macro does not honours widget theme settings only the passed or *current-theme*."
  `(let ((*theme* (merge-themes *sfx-system* ,theme *current-theme*)))
      ,@body))

(defmacro with-widget-theme ((&key widget theme) &body body)
  "This macro is a convenience macro to expose *theme* within a let binding to be used outside of the widget render method. This macro honours widget theme settings."
  `(with-theme (merge-theme *sfx-system*
			    ,theme 
			    ,(theme-attributes widget) 
			    *current-theme* 
			    ,(if (slot-exists-p 
				  (class-of (or *widget* widget)) 
				 'base-theme)
				(base-theme (class-of (or *widget* widget)))))
       ,@body))

(defgeneric theme-attribute (theme key &key &allow-other-keys)
  (:documentation "Returns theme attribute from theme by key."))

(defmethod theme-attribute ((theme theme) key &key)
  (if (listp theme)
      (find key theme :test #'equalp :key #'car)
      (if (listp (attributes theme))
	  (find key (attributes theme) :test #'equalp :key #'car)
	  (gethash key (attributes theme)))))

(defmethod theme-attribute ((theme list) key &key)
  (find key theme :test #'equalp :key #'car))

(defmethod theme-attribute ((theme hash-table) key &key)
  (gethash key theme))

(defgeneric theme-value (theme key &key &allow-other-keys)
  (:documentation "Returns theme attribute value from theme by key."))

(defmethod theme-value ((theme theme) key &key)
  (theme-attribute theme key))

(defmethod theme-value ((theme list) key &key)
  (second (theme-attribute theme key)))

(defmethod theme-value ((theme hash-table) key &key)
  (theme-attribute theme key))

(defun att (key)
  "Returns attribute from active theme (ie *theme*)."
  (theme-attribute *theme* key))

(defun attv (key)
  "Returns attribute value from active theme (ie *theme*)."
   (theme-value *theme* key))

(defgeneric merge-themes (system &rest themes)
  (:documentation "Merge themes in order that they are presented. The first theme will be taken as the base and all following themes will override base theme attributes.."))


;;TODO: use ecase
(defmethod merge-themes ((system sfx-system) &rest themes)
  (let ((merge (make-hash-table :test #'equalp)))
 
    (dolist (theme (reverse themes))
      (if (hash-table-p theme)
	  (maphash 
	       (lambda (key value)
 		 (setf (gethash key merge) value))
	       theme)
	  (if (listp theme)
	      (dolist (att theme)
		(setf (gethash (first att) merge) (second att)))
	      (dolist (att (attributes theme))
		(setf (gethash (first att) merge) (second att))))))
    merge))
