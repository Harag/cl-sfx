(in-package :cl-sfx)

(defvar *event-handlers* (make-hash-table :test #'equalp))

(defvar *system-event-log* nil
  "Event-handler events are logged to here.")


;;TODO: Ponder a log reader method!!!
(defclass sfx-log ()
  ((source :initarg :sources
	   :accessor source
	   :initform nil)
   (log-context :initarg :log-context
		:accessor log-context
		:initform nil)
   (stamp :initarg :stamp
	  :accessor stamp
	  :initform nil)))


(defgeneric log-event (source log-context)
  (:documentation "Default logging of events."))

(defvar *logging-p* t
  "Setting *logging-p* to nil will stop all event logging."
  )

(defmethod log-event :around (source log-context)
  "Log events if *logging-p* is t, which is the default."
  (when *logging-p*
    (call-next-method)))

(defmethod log-event (source log-context)
  (pushnew  (make-instance 'sfx-log 
			   :source source 
			   :log-context log-context 
			   :stamp (get-universal-time)) 
	    *system-event-log*))

(defgeneric event-handler (object name &key &allow-other-keys)
  (:documentation "Generic method for event handling. Event handlers must be created using define-event-handler macro."))


;;TODO: Do we even need to register events if we dont use keys and all dynamic events are
;;loaded with define-event-handler? Call of event would just be (apply #'event-handler args)
;;TODO: Implement keys?????
;;How to get implementation of eventhandler instead of code without using eval

;;TODD: Labmda list could be data slots to access? Or should we drop it completely?
(defmacro define-event-handler (description &body body)
  "Creates a defmethod for the event.

description should match the following descrtucturing lambda list
   (name class-name &key args...)

NOTE: Should not be seeing this called in cl-sfx package code!!!????
"
  (when (atom description)
    (setf description (list description)))
  (destructuring-bind (name class-name &rest args
                       &key 
                       &allow-other-keys) description
    (let ((ev-object (intern "OBJECT" (symbol-package name))))   
      
      ;;We can do this but how do we use it? For what do we use it?
      (setf (gethash (list class-name name) *event-handlers*) 
	name)
      
      ;;Unbind any existing methods to not cause issues with sig changes.
      ;;TODO: What happends if two events have the same ligit name?
      ;;At compile time this is fine, at run time this causes only the last
      ;;event defined to be created.
      ;;Maybe do lookup in *event-handlers* to try and sort this out.
      (fmakunbound name)
      
      `(progn
	 
	#| 
	 (defgeneric ,name (object 
			     ,@args)
	 ;;  "Todo add doc string arg???"
	   )
	 |#
	 
	 (defmethod ,name :before ((,ev-object ,class-name) 
				    ,@args)
		    (log-event ,ev-object ,(list name :before)))

	 (defmethod ,name :after ((,ev-object ,class-name) 
				   ,@args)
		    (log-event ,ev-object ,(list name :after)))

	 (defmethod ,name 
	     ((,ev-object ,class-name) 
	       ,@args)
	   
	   ,@body)))))
