(in-package :cl-sfx)

(defvar *form-id*)

(defgeneric present-list (type parameters name value info))
(defgeneric accept-list (type parameters value info))

(defclass find-object (ajax-widget)
  ((info :initarg :info
         :initform nil
         :accessor info)
   (entity-info :initarg :entity-info
                :initform nil
                :accessor entity-info)
   (object-class :initarg :object-class
                 :initform nil
                 :accessor object-class)
   (slot-id :initarg :slot-id
            :initform nil
            :accessor slot-id))
  (:metaclass widget-class))

(defparameter *find-object-select-button*
  (make-instance 'grid-button
                 :action "select"
                 :icon "glyphicon glyphicon-ok"
                 :hint "Select"))

(defclass find-object-grid (generic-grid)
  ((find-object :initarg :find-object
                :initform nil
                :accessor find-object)
   (modal :initarg :modal
          :initform nil
          :accessor modal))
  (:default-initargs :bottom-buttons nil
                     :buttons (list *find-object-select-button*)))

(defmethod render-header-buttons ((grid find-object-grid)))

(defmethod render-save-buttons (grid form-id &key (save-button t)
                                                  (cancel-button t))
  (let ((disable (disable-grid-buttons grid)))
    (with-html
      (:div :class "btn-toolbar panel-footer"
            (when (and save-button
                       (not (and (typep grid 'grid)
                                 (grid-read-only grid)))
                       (check-page-permission "Update"))
              (htm
               (:button :class "btn-primary btn"
                        :onclick
                        (frmt
                         ;;"{tinyMCE.triggerSave();~@[~a;~]~a;}"
			 "{~@[~a;~]~a;}"
                         disable
                         (js-render-form-values (if (typep grid 'grid)
                                                    (editor grid)
                                                    grid)
                                                form-id
                                                (js-pair "grid-name" (name grid))
                                                (js-pair "action" "save")))
                        "Save")))
            (when cancel-button
              (htm
               (:button :class "btn-default btn"
                        :onclick
                        (frmt
                         "{~@[~a;~]~a;}"
                         disable
                         (js-render (if (typep grid 'grid)
                                        (editor grid)
                                        grid)
                                    (js-pair "grid-name" (name grid))
                                    (js-pair "action" "cancel")))
                        "Cancel")))))))

(defmethod get-rows ((grid find-object-grid))
  (let ((child-entities (entity-children (value-info-value (entity-info (find-object grid))))))
    (loop for doc across (docs (collection (row-object-class grid)))
          when (member (entity doc) child-entities)
          collect doc)))

(defmethod handle-action ((grid find-object-grid) (action (eql :select)))
  (let ((object (editing-row grid))
        (find-object (find-object grid)))
    (setf (value-info-value (info find-object))
          object)
    (defer-js (js-render find-object))
    (defer-js "$('#~a').modal(\"hide\");$('~a').change();"
      (name (modal grid))
      (slot-id find-object))
    (finish-editing grid)))

(defmethod render ((widget find-object) &key)
  (cond ((post-parameter "find")
         (let* ((modal (make-widget 'modal :name (sub-name widget "modal")
                                           :save-button nil
                                           :size :full))
                (class (object-class widget))
                (grid (make-widget 'find-object-grid
                                   :name (sub-name widget "grid")
                                   :box nil
                                   :find-object widget 
                                   :row-object-class class
                                   :modal modal)))
           (setf (header modal)
                 (format nil "Select a ~a"
                         (make-name (string (class-name class))))
                 (content modal)
                 (if (value-info-value (entity-info widget))
                     (render-to-string grid)
                     (with-html-string
                       (:div :class "alert alert-info"
                             "Select an entity first."))))
           (render modal))))
  (with-html
    (:div :class "row"
          (when (value-info-value (info widget))
            (htm
             (:label :class "control-label"
                     (esc (object-description (value-info-value (info widget)))))))
          (:button :class "btn"
                   :onclick
                   (js-render widget (js-pair "find" "t"))
                   "Find"))))

(defmethod present-list ((type (eql :object)) parameters name value info)
  (let (;;(entity-info (slot-depend-on 'entity))
        (find-object (make-widget 'find-object :name name
                                               :slot-id (frmt "#~a #~a" *form-id* name))))
    (setf (value-info-no-accept info) t
          (info find-object) info
          ;;(entity-info find-object) entity-info
          (object-class find-object) (find-class (car parameters)))
    (render find-object)))

(defmethod accept-list ((type (eql :object)) parameters value info)
  (value-info-value info))

(defclass object-editor (ajax-widget)
  ((object :initarg :object
           :initform nil
           :accessor object) 
   (slot-value-info :initarg :slot-value-info
                    :initform (make-hash-table :test #'eq)
                    :accessor slot-value-info))
  (:metaclass widget-class))

(defun tab-content (header slot info grid row)
  (list header
	(let* ((class (second (cl-sfx::db-type slot)))
	       (grid
		(make-widget 'generic-sub-grid
			     :parent-grid grid
			     :name (sub-name grid (string class))
			     :title header
			     :row-object-class
			     (find-class class))))

	  (setf (info grid) info
		(initargs grid)
		(subst row :this (getf (cddr (cl-sfx::db-type slot)) :initargs))
		(read-only-slots grid)
		(getf (cddr (cl-sfx::db-type slot)) :read-only))
                   
	;;  (break "grid ~A" (columns grid))
	  (render-to-string grid))))

(defun tab-entity-content (header row)
  (list header
	(render-to-string (make-widget 'user-entity-editor :user row))))

(defun tab-object-content (grid header form main-content)
  (list header
	(render-to-string form
			  :grid grid
			  :content main-content)))

(defun tab-check-list-content (header slot row)
  (declare (ignore slot) (ignore row))
  (list header
	(render-to-string (make-widget 'checkbox-list 
				       :name header))))

(defun tab-select-list-content (header slot row)
  (declare (ignore slot) (ignore row))
  (list header
	(render-to-string (make-widget 'select
				       :name header))))


(defmethod render ((editor object-editor) &key modal-edit
                                               read-only-slots
                                               (target editor))
  (let* ((class (class-of (object editor)))
         (class-name (class-name class))
         (form-name (format nil "~A-~A-form" (name editor) class-name))
         (*form-id* form-name)
         (header (make-name class-name))
         (form (make-widget 'grid-form :name form-name
                                  :grid-size 12
                                  :header header
                                  :form-id form-name))
         (*slot-value-info* (slot-value-info editor))
         (*sub-grids*)
         (main-content
           (with-html-string
	     (loop for slot in (cl-sfx::all-slots class)
		  when (cl-sfx::db-type slot)
		  do (present-slot slot (object editor)
				   :read-only (find (slot-definition-name slot)
						    read-only-slots))))))
        
    (declare (special *form-id*) (special *slot-value-info*) (special *sub-grids*))
     
    (unless modal-edit
      (with-html (:div :class "editing")))
    (let ((content
	   (with-html-string
	     (cond (*sub-grids*
		    (setf (buttons form) nil)
		    (let ((tab-box (make-widget 'tab-box
						:name (format nil "~A-~A-tabs" 
							      (name editor) class-name)
						:header header
						:icon "card--pencil")))
		      (setf (tabs tab-box)
			    (list* (list header
					 (with-html-string
					   (render form
						   :grid target
						   :content main-content)))
				   (reverse
				    (loop for (slot info) on *sub-grids* by #'cddr
				       for header = (make-name (slot-definition-name slot))
				       collect
					 
					 (cond  ((equal (first (cl-sfx::db-type slot)) 
							'xxxxx)
						 (tab-check-list-content header slot 
									 (object editor)))
						(t
						 (tab-content header slot info target
							      (object editor))))
					 )))
			    (body-content tab-box)
			    (unless modal-edit
                               (with-html-string
                                 (render-save-buttons target form-name))))
		      (render tab-box)))
		   (t
		    (when modal-edit
                       (setf (buttons form) nil))
		    (render form
			    :grid target
			    :content main-content))))))
      (if modal-edit
          (let ((modal (make-widget 'modal
                                    :name (frmt "~a-modal" form-name)
                                    :form-id form-name
                                    :grid target)))
            (unless (eq modal-edit t)
              (setf (size modal) modal-edit))
            (setf (content modal) content)
            (render modal))
          (princ content))
    ;;  (process-dependant-slots form-name *slot-value-info* editor)
      ))
  
  )

#|

(defun bind-depend (form-id slot-name editor)
  (format nil
          "$('#~a #~a').change(function(){var value;~
 if($(this).is(':checkbox'))~
 value = $(this).is(':checked');
 else value = $(this).val();
 grid_depend(~s, ~s, '~a',value,[])});"
          form-id slot-name
          (script-name*) (name editor) slot-name))

(defun process-dependant-slots (form-id info editor)
  (loop for value being the hash-value of info
        using (hash-key slot-name)
        when (value-info-dependants value)
        do
        (defer-js (bind-depend form-id slot-name editor))))
|#

(defun commit-values (object infos)
  (let ((class (class-of object)))
    (loop for slot in (class-slots class)
          when (cl-sfx::db-type slot)
          do (let ((info (gethash (slot-definition-name slot) infos)))
               (when info
                 (setf (slot-value-using-class class object slot)
                       (value-info-value info)))))))

(defun accept-doc (doc)
  (let ((success t))
    (loop for slot in (cl-sfx::all-slots (class-of doc))
       when (cl-sfx::db-type slot)
       do (progn
	    (setf success
		       (and (accept-slot slot)
			    success))))

    (loop for slot in (class-slots (class-of doc))
       when (cl-sfx::validate slot)
       do (setf success
		(and (validate-slot slot)
		     success)))
    success))
