(in-package :cl-sfx)


(defclass window-events ()
    ((on-after-print :initarg :on-after-print
                :initform nil
                :accessor on-after-print
		:html-attribute-p t
                :documentation "Script to be run after the document is printed - (IE10 (Buggy) & Firefox) - http://www.w3schools.com/tags/ev_onafterprint.asp")
     (on-before-print :initarg :on-before-print
                :initform nil
                :accessor on-before-print
		:html-attribute-p t
                :documentation "Script to be run before the document is printed - (IE10 & Firefox) - http://www.w3schools.com/tags/ev_onbeforeprint.asp")
     (on-before-unload :initarg :on-before-unload
                :initform nil
                :accessor on-before-unload
		:html-attribute-p t
                :documentation "Script to be run before the document is unloaded - ")
     (on-error :initarg :on-error
                :initform nil
                :accessor on-error
		:html-attribute-p t
                :documentation "Script to be run when an error occur - ")
     (on-has-change :initarg :on-has-change
                :initform nil
                :accessor on-has-change
		:html-attribute-p t
                :documentation "Script to be run when the document has changed - ")
     (on-load :initarg :on-load
                :initform nil
                :accessor on-load
		:html-attribute-p t
                :documentation "Fires after the page is finished loading - http://www.w3schools.com/tags/ev_onload.asp")
     (on-message :initarg :on-message
                :initform nil
                :accessor on-message
		:html-attribute-p t
                :documentation "Script to be run when the message is triggered - ")
     (on-offline :initarg :on-offline
                :initform nil
                :accessor on-offline
		:html-attribute-p t
                :documentation "Script to be run when the document goes offline - ")
     (on-online :initarg :on-online
                :initform nil
                :accessor on-online
                :documentation "Script to be run when the document comes online - ")
     (on-page-hide :initarg :on-page-hide
                :initform nil
                :accessor on-page-hide
		:html-attribute-p t
                :documentation "Script to be run when the window is hidden - ")
     (on-page-show :initarg :on-page-show
                :initform nil
                :accessor on-page-show
		:html-attribute-p t
                :documentation "Script to be run when the window becomes visible  - ")
     (on-pop-state :initarg :on-pop-state
                :initform nil
                :accessor on-pop-state
		:html-attribute-p t
                :documentation "Script to be run when the window's history changes - ")
     (on-redo :initarg :on-redo
                :initform nil
                :accessor on-redo
		:html-attribute-p t
                :documentation "Script to be run when the document performs a redo - ")
     (on-resize :initarg :on-resize
                :initform nil
                :accessor on-resize
                :documentation "Fires when the browser window is resized - http://www.w3schools.com/tags/ev_onresize.asp")
     (on-storage :initarg :on-storage
                :initform nil
                :accessor on-storage
		:html-attribute-p t
                :documentation "Script to be run when a Web Storage area is updated - ")
     (on-undo :initarg :on-undo
                :initform nil
                :accessor on-undo
		:html-attribute-p t
                :documentation "Script to be run when the document performs an undo - ")
     (on-unload :initarg :on-unload
                :initform nil
                :accessor on-unload
		:html-attribute-p t
                :documentation "Fires once a page has unloaded (or the browser window has been closed) - http://www.w3schools.com/tags/ev_onunload.asp")
     )
  (:metaclass widget-slot-class)
  (:documentation "Events triggered for the window object (applies to the <body> tag)"))


(defclass form-events ()
  ((on-blur :initarg :on-blur
            :initform nil
            :accessor on-blur
	    :html-attribute-p t
            :documentation "Fires the moment that the element loses focus - http://www.w3schools.com/tags/ev_onblur.asp")
   (on-change :initarg :on-change
              :initform nil
              :accessor on-change
	      :html-attribute-p t
              :documentation "Fires the moment when the value of the element is changed - http://www.w3schools.com/tags/ev_onchange.asp")
   (on-context-menu :initarg :on-context-menu
                    :initform nil
                    :accessor on-context-menu
		    :html-attribute-p t
                    :documentation "Script to be run when a context menu is triggered - ")
   (on-focus :initarg :on-focus
             :initform nil
             :accessor on-focus
             :documentation "Fires the moment when the element gets focus - http://www.w3schools.com/tags/ev_onfocus.asp")
   (on-form-change :initarg :on-form-change
                   :initform nil
                   :accessor on-form-change
		   :html-attribute-p t
                   :documentation "Script to be run when a form changes - ")
   (on-form-input :initarg :on-form-input
                  :initform nil
                  :accessor on-form-input
		  :html-attribute-p t
                  :documentation "Script to be run when a form gets user input - ")
   (on-input :initarg :on-input
             :initform nil
             :accessor on-input
	     :html-attribute-p t
             :documentation "Script to be run when an element gets user input - ")
   (on-valid :initarg :on-valid
             :initform nil
             :accessor on-valid
	     :html-attribute-p t
             :documentation "Script to be run when an element is invalid - ")
   (on-select :initarg :on-select
              :initform nil
              :accessor on-select
	      :html-attribute-p t
              :documentation "Fires after some text has been selected in an element - http://www.w3schools.com/tags/ev_onselect.asp")
   (on-submit :initarg :on-submit
              :initform nil
              :accessor on-submit
	      :html-attribute-p t
              :documentation "Fires when a form is submitted - ")
   )
  (:metaclass widget-slot-class)
  (:documentation "Events triggered by actions inside a HTML form (applies to almost all HTML elements, but is most used in form elements)")
  )

(defclass keyboard-events ()
  ((on-key-down :initarg :on-key-down
              :initform nil
              :accessor on-key-down
	      :html-attribute-p t
              :documentation "Fires when a user is pressing a key - http://www.w3schools.com/tags/ev_onkeydown.asp")
   (on-key-press :initarg :on-key-press
              :initform nil
              :accessor on-key-press
	      :html-attribute-p t
              :documentation "Fires when a user presses a key - http://www.w3schools.com/tags/ev_onkeypress.asp")
   (on-key-up :initarg :on-key-up
              :initform nil
              :accessor on-key-up
	      :html-attribute-p t
              :documentation "Fires when a user releases a key - http://www.w3schools.com/tags/ev_onkeyup.asp"))
  (:metaclass widget-slot-class)
  (:documentation "")
  )


(defclass mouse-events ()
  ((on-click :initarg :on-click
              :initform nil
              :accessor on-click
	      :html-attribute-p t
              :documentation "Fires on a mouse click on the element - http://www.w3schools.com/tags/ev_onclick.asp")
   (on-dbl-click :initarg :on-dbl-click
              :initform nil
              :accessor on-dbl-click
	      :html-attribute-p t
              :documentation "Fires on a mouse double-click on the element - http://www.w3schools.com/tags/ev_ondblclick.asp")
   (on-drag :initarg :on-drag
              :initform nil
              :accessor on-drag
	      :html-attribute-p t
              :documentation "Script to be run when an element is dragged - ")
   (on-drag-end :initarg :on-drag-end
              :initform nil
              :accessor on-drag-end
	      :html-attribute-p t
              :documentation "Script to be run at the end of a drag operation - ")
   (on-drag-enter :initarg :on-drag-enter
              :initform nil
              :accessor on-drag-enter
	      :html-attribute-p t
              :documentation "Script to be run when an element has been dragged to a valid drop target - ")
   (on-drag-leave :initarg :on-drag-leave
              :initform nil
              :accessor on-drag-leave
	      :html-attribute-p t
              :documentation "Script to be run when an element leaves a valid drop target - ")
   (on-drag-over :initarg :on-drag-over
              :initform nil
              :accessor on-drag-over
	      :html-attribute-p t
              :documentation "Script to be run when an element is being dragged over a valid drop target - ")
   (on-drag-start :initarg :on-drag-start
              :initform nil
              :accessor on-drag-start
	      :html-attribute-p t
              :documentation "Script to be run at the start of a drag operation - ")
   (on-drop :initarg :on-drop
              :initform nil
              :accessor on-drop
	      :html-attribute-p t
              :documentation "Script to be run when dragged element is being dropped - ")
   (on-mouse-down :initarg :on-mouse-down
              :initform nil
              :accessor on-mouse-down
	      :html-attribute-p t
              :documentation "Fires when a mouse button is pressed down on an element - http://www.w3schools.com/tags/ev_onmousedown.asp")
   (on-mouse-move :initarg :on-mouse-move
              :initform nil
              :accessor on-mouse-move
	      :html-attribute-p t
              :documentation "Fires when the mouse pointer moves over an element - http://www.w3schools.com/tags/ev_onmousemove.asp")
   (on-mouse-out :initarg :on-mouse-out
              :initform nil
              :accessor on-mouse-out
	      :html-attribute-p t
              :documentation "Fires when the mouse pointer moves out of an element  - http://www.w3schools.com/tags/ev_onmouseout.asp")
   (on-mouse-over :initarg :on-mouse-over
              :initform nil
              :accessor on-mouse-over
	      :html-attribute-p t
              :documentation "Fires when the mouse pointer moves over an element - http://www.w3schools.com/tags/ev_onmouseover.asp")
   
   (on-mouse-up :initarg :on-mouse-up
              :initform nil
              :accessor on-mouse-up
	      :html-attribute-p t
              :documentation "Fires when a mouse button is released over an element - http://www.w3schools.com/tags/ev_onmouseup.asp")
   (on-mouse-wheel :initarg :on-mouse-wheel
              :initform nil
              :accessor on-mouse-wheel
	      :html-attribute-p t
              :documentation "Script to be run when the mouse wheel is being rotated - ")
   (on-scroll :initarg :on-scroll
              :initform nil
              :accessor on-scroll
              :documentation "Script to be run when an element's scrollbar is being scrolled - "))
  (:metaclass widget-slot-class)
  (:documentation "Events triggered by a mouse, or similar user actions")
  )


(defclass clipboard-events ()
  ((on-copy :initarg :on-copy
              :initform nil
              :accessor on-copy
	      :html-attribute-p t
              :documentation "Fires when the user copies the content of an element - http://www.w3schools.com/tags/ev_oncopy.asp")
   (on-cut :initarg :on-cut
	   :initform nil
	   :accessor on-cut
	   :html-attribute-p t
	   :documentation "Fires when the user cuts the content of an element - http://www.w3schools.com/tags/ev_oncut.asp")
   (on-paste :initarg :on-paste
	     :initform nil
	     :accessor on-paste
	     :html-attribute-p t
	     :documentation "Fires when the user pastes some content in an element - http://www.w3schools.com/tags/ev_onpaste.asp"))
  (:metaclass widget-slot-class)
  (:documentation "")
  )


;;TODO: Add rest of attributes
(defclass media-events ()
  ((on-abort :initarg :on-abort
              :initform nil
              :accessor on-abort
	      :html-attribute-p t
              :documentation "Script to be run on abort - "))
  (:metaclass widget-slot-class)
  (:documentation "Events triggered by medias like videos, images and audio (applies to all HTML elements, but is most common in media elements, like <audio>, <embed>, <img>, <object>, and <video>):")
  )


;;TODO: Add misc events
;;http://www.w3schools.com/tags/ref_eventattributes.asp
