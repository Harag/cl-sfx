(in-package :cl-sfx-doc)

(defmethod make-color (spec (type (eql :html)))
  (and spec
       (format nil "#~a" spec)))

(defvar *html-h-alignment*
  '(:center nil
    :left "left"
    :right "right"
    :justify "justify"))

(defmethod h-alignment (spec (type (eql :html)))
  (and spec
       (getf *html-h-alignment* spec)))

(defvar *html-v-alignment*
  '(:center nil
    :top "top"
    :bottom "bottom"
    :baseline "baseline"))

(defmethod v-alignment (spec (type (eql :html)))
  (and spec
       (getf *html-v-alignment* spec)))

(defmethod render-element ((table table) (type (eql :html)))
  (with-html
    (:table :class (html-class table)
            (when (title table)
              (htm (:caption (esc (title table)))))
            (let* ((body (member-if (lambda (row)
                                      (some (lambda (cell) (not (typep cell 'header)))
                                            (cells row)))
                                    (rows table)))
                   (header (ldiff (rows table)
                                  body)))
              (htm
               (:thead
                (render-objects header type))
               (:tbody
                (render-objects body type)))))))

(defmethod render-element ((row row) (type (eql :html)))
  (with-html
    (:tr
     (render-objects (cells row) type))))

(defun css-properties (&rest args)
  (let ((string
          (with-output-to-string (str)
            (loop for (property value) on args by #'cddr
                  when value
                  do (format str "~(~a~):~a;" property value)))))
    (unless (equal string "")
      string)))

(defun make-vertical-text (string)
  (format nil "~{~c~^<br />~}" (coerce string 'list)))

(defmacro html-cell (cell)
  `(with-html
     (,(ecase cell
         (header :th)
         (cell :td))
      :rowspan (and (/= (row-span ,cell) 1)
                    (row-span ,cell))
      :colspan (and (/= (col-span ,cell) 1)
                    (col-span ,cell))
      :style (css-properties
              :color (make-color (text-color ,cell) :html)
              :background-color (make-color (bg-color ,cell) :html)
              :text-align (h-alignment (h-align ,cell) :html)
              :vertical-align (v-alignment (v-align ,cell) :html))
      :class (css-class ,cell)
      :id (escape (id ,cell))
      :title (escape (title ,cell))
      (render-objects (text ,cell) type))))

(defmethod render-element ((header header) (type (eql :html)))
  (html-cell header))

(defmethod render-element ((cell cell) (type (eql :html)))
  (html-cell cell))

(defmethod render-element ((document document) type)
  (render-objects (elements document) type))

(defmethod process-text-style ((text text) (type (eql :html)))
  (css-properties :font-size (size text)
                  :font-weight (weight text)
                  :font-style (style text)))

(defmethod render-element ((text text) (type (eql :html)))
  (let ((style (process-text-style text type)))
    (if (plusp (length style))
        (with-html
          (:span :style style
                 (esc (text text))))
        (with-html
          (esc (text text))))))

(defmethod render-element ((paragraph paragraph) (type (eql :html)))
  (with-html
    (:p
     :style (css-properties
             :text-align (h-alignment (h-align paragraph) :html))
     (render-objects (elements paragraph) type))))

(defmethod render-element ((element html) (type (eql :html)))
  (princ (rendered-string element)))

(defmethod render-element ((element pdf) (type (eql :html))))

(defmethod render-element ((heading heading) (type (eql :html)))
  (check-type (size heading) (integer 1 6))
  (with-html
    (format t "<h~d>~a</h~@*~d>" (size heading) (escape (text heading)))))

(defmethod render-element ((page new-page) (type (eql :html))))

(defmethod render-element ((orientation orientation) (type (eql :html))))

(defmethod render-element ((line new-line) (type (eql :html)))
  (with-html (:br)))

(defmethod render-element ((link link) (type (eql :html)))
  (with-html
    (:a :href (url link)
        :target
        (ecase (target link)
          (:blank "_blank")
          ((nil)))
        (render-element (text link) type))))

(defmethod render-element ((image image) (type (eql :html)))
  (with-html
    (:img :src (path image))))

(defmacro with-graph-row (&body body)
  `(with-add-html ()
       (:div :class "row qv-graphs"
        ,@body)))

(defmethod render-element ((graph bar-graph) (type (eql :html)))
  ;; (with-slots (title height labels data id grid-width
  ;;              orientation x-label y-label
  ;;              percentage legend stacked max-y remove-zero) graph
  ;;   (with-html
  ;;     (:div :id (or id "bar-graph")
  ;;           :class (format nil "col-md-~a docs" grid-width)
  ;;           (js-bar-graph title labels data
  ;;                         :orientation orientation
  ;;                         :height height
  ;;                         :x-label x-label
  ;;                         :y-label y-label
  ;;                         :percentage percentage
  ;;                         :legend legend
  ;;                         :stacked stacked
  ;;                         :y-max max-y
  ;;                         :remove-zero remove-zero))))
  )

(defmethod render-element ((graph bar-line-graph) (type (eql :html)))
  ;; (with-slots (title height labels data line-data line-label
  ;;              id grid-width
  ;;              orientation x-label y-label
  ;;              percentage legend stacked max-y remove-zero) graph
  ;;   (with-html
  ;;     (:div :id (or id "bar-line-graph")
  ;;           :class (format nil "col-md-~a docs" grid-width)
  ;;           (js-bar-line-graph title labels data
  ;;                              line-label
  ;;                              line-data
  ;;                              :orientation orientation
  ;;                              :height height
  ;;                              :x-label x-label
  ;;                              :y-label y-label
  ;;                              :percentage percentage
  ;;                              :legend legend))))
  )

(defmethod render-element ((graph pie-chart) (type (eql :html)))
  ;; (with-slots (title data id grid-width
  ;;              breakdown) graph
  ;;   (with-html
  ;;     (:div :id (or id "pie-chart")
  ;;           :class (format nil "col-md-~a docs" grid-width)
  ;;           (js-pie-chart title data
  ;;                         :breakdown breakdown))))
  )

(defmethod render-element ((graph line-graph) (type (eql :html)))
  ;; (with-slots (title data id grid-width legend x-label y-label height) graph
  ;;   (with-html
  ;;     (:div :id (or id "line-graph")
  ;;           :class (format nil "col-md-~a docs" grid-width)
  ;;           (js-line-graph title data :legend legend
  ;;                                     :height height
  ;;                                     :x-label x-label
  ;;                                     :y-label y-label))))
  )

(defmethod render-element ((popover popover) (type (eql :html)))
  (with-html
    (:a :class "pop-over"
        :data-toggle "popover"
        :data-trigger "focus"
        :tabindex 0
        :data-placement "bottom"
        :data-html t
        :data-content (content popover)
        (render-element (text popover) type))))
