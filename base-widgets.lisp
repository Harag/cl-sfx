(in-package :cl-sfx)

(defgeneric $render (object &key &allow-other-keys))

(defmethod %render ((integer integer) &key theme (theme-attribute-key :integer-format))
  "Formats string using :integer-format theme attribute. Use the theme-attribute-key to override which theme attribute is used. Does not honour widget theme if not called from within a widget render, because it is not widget."
  (format nil 
	  (or (theme-value 
	       (if *sfx-system*
		   (merge-themes *sfx-system*
				 theme 
				 (if *widget* (theme-attributes *widget*))
				 *current-theme* 
				 (if *widget*
 				     (if (slot-exists-p 
					  (class-of *widget*) 
					  'base-theme)
					 (base-theme (class-of *widget*))))))
	       theme-attribute-key) 
	      "~D")
	  integer))

(defmethod %render ((float float) &key theme (theme-attribute-key :float-format))
  "Formats string using :float-format theme attribute. Use the theme-attribute-key to override which theme attribute is used. Does not honour widget theme if not called from within a widget render, because it is not widget."

  (format nil 
	  (or (theme-value 
	       
	       (if *sfx-system*
		   (merge-themes *sfx-system*
				 theme 
				 (if *widget* (theme-attributes *widget*))
				 *current-theme* 
				 (if *widget*
				     (if (slot-exists-p 
					  (class-of *widget*) 
					  'base-theme)
					 (base-theme (class-of *widget*))))))
	       theme-attribute-key)
	      "~F") 
	  float))

(defmethod %render ((number number) &key theme (theme-attribute-key :number-format))
  "Formats string using :number-format theme attribute. Use the theme-attribute-key to override which theme attribute is used.Does not honour widget theme if not called from within a widget render, because it is not widget."

  (format nil 
	  (or (theme-value 
	       
	       (merge-themes *sfx-system*
			     theme 
			     (if *widget* (theme-attributes *widget*))
			     *current-theme* 
			     (if *widget*
				 (if (slot-exists-p 
				      (class-of *widget*) 
				      'base-theme)
				     (base-theme (class-of *widget*)))))
	       theme-attribute-key)
	      "~D") 
	  number))

(defmethod %render ((string string) &key theme case (theme-attribute-key :string-format))
  "Formats string using :string-format theme attribute, which should be a lisp format function directive. If case is supplied :string-format is ignored and appropriate format strings are applied. Use the theme-attribute-key to override which theme attribute is used. Does not honour widget theme if not called from within a widget render, because it is not widget."

  (let ((case-directive nil))
    (cond ((equalp case :upper)
	   (setf case-directive "~:@(~a~)"))
	  ((equalp case :lower)
	   (setf case-directive "~(~a~)"))
	  ((equalp case :capitilize)
	   (setf case-directive "~:(~a~)"))
	  ((equalp case :capitilize-first)
	   (setf case-directive "~@(~a~)")))
    
    (format nil (or (theme-value
		     (or case-directive 
			 (if *sfx-system* (merge-themes *sfx-system*
							theme 
							(if *widget* (theme-attributes *widget*))
							*current-theme* 
							(if *widget*
							    (if (slot-exists-p 
								 (class-of *widget*) 
								 'base-theme)
								(base-theme (class-of *widget*)))))))
		     theme-attribute-key)
		    "~A")
	     
	    string)))

;;:TODO: Money use attribute key override with :money-format!!


(defmethod render ((integer integer) &key theme (theme-attribute-key :integer-format)) 
  (with-html
      (str (cl-sfx:%render integer :theme theme :theme-attribute-key theme-attribute-key ))))

(defmethod render ((float float) &key theme (theme-attribute-key :integer-format)) 
  (with-html
      (str (cl-sfx:%render float :theme theme :theme-attribute-key theme-attribute-key ))))

(defmethod render ((number number) &key theme (theme-attribute-key :integer-format)) 
  (with-html
      (str (cl-sfx:%render number :theme theme :theme-attribute-key theme-attribute-key ))))

(defmethod render ((string string) &key theme (theme-attribute-key :integer-format)) 
  (with-html
    (str (cl-sfx:%render string :theme theme :theme-attribute-key theme-attribute-key ))))

(defmethod render ((list list) &key theme (theme-attribute-key :integer-format)) 
  (with-html
    (dolist (item list)
      (render item :theme theme :theme-attribute-key theme-attribute-key))))

(defun strip-underscore (val)
  (let ((copy (copy-seq val)))
    (nstring-downcase (nsubstitute #\- #\_ copy))))


;;TODO: walk up the parent tree to find more stuff.
(defun attvw (widget att-name)
  "Checking name variations for attributes widget.element.att, elemen.att, widget.att and parent.element.att."
  (let* ((name (string-upcase (strip-underscore (name widget))))
	     (name% (intern
		     (string-upcase (frmt "~A.~A" name att-name))
		     :KEYWORD))
	     (split-name (widget-name-split name))
	     (part-name (if (and split-name (second split-name)) 
			    (intern  (string-upcase (frmt "~A.~A"
							  (second split-name)
							  att-name))
				     :KEYWORD)))
	     (part-name2 (if (and split-name (first split-name)) 
			     (intern  (string-upcase (frmt "~A.~A.~A"
							   (first split-name)
							   (class-name (class-of widget))
							   att-name))
				      :KEYWORD)))
	     (part-name3 (if (and split-name (first split-name) (slot-exists-p widget 'parent) (parent widget)) 
			     (intern  (string-upcase 
				       (frmt "~A.~A.~A"
					     
					     (class-name 
					      (car (last (class-direct-superclasses 
							  (class-of (parent widget)) ))))
					     (second split-name)
					     att-name))
				      :KEYWORD))))
	(or 
	    (attv name%)
	    (if part-name (attv part-name))
	    (if part-name2 (attv part-name2))
	    (if part-name3 (attv part-name3))
	    (attv att-name))))

(defun get-attributes (widget &optional exclude-list)
  "Loops through the slots of a widget to get the attributes used in rendering the widget."
  (let ((attributes))
    (dolist (slot (class-slots (class-of widget)))
      (let* ((slot-name (slot-definition-name slot))
	     (value (if (slot-boundp widget slot-name)
			(slot-value widget slot-name)
			(parameter slot-name) ;;nil TODO: should caching not have pushed the value onto the slot already?
			)))

        (when (or 
	       (string-equal slot-name "name")
	       (and (slot-exists-p slot 'html-attribute-p) (html-attribute-p slot)))
	  
	  (let ((val (or value (attvw widget 
				      (intern 
				       (string-upcase (frmt "~A" slot-name)) :KEYWORD)))))
	    (when val
	      (unless (find slot-name exclude-list :test #'string-equal)
		(setf attributes (append attributes
					 (list 
					  (intern (frmt "~A" (slot-definition-name slot)) :keyword) 
					  (cond ((equal (slot-definition-name slot)
							"disabled")
						 (if val
						     "disabled"))
						((equal (slot-definition-name slot)
							"checked")
						 (if val
						     "checked"))
						((equal (slot-definition-name slot)
							"readonly")
						 (if val
						     "readonly"))
						(t
						 (escape val))))))))))))
    attributes))

(defclass generic-html-element (html-global-attributes mouse-events form-events)
  ((html-element :initarg :html-element
		:accessor html-element
		:initform :span
		:html-attribute-p nil))
  
  (:metaclass widget-class)
  (:default-initargs 
      :class nil
    :style "margin: 0px;")
  (:documentation "Use this widget when the html element used for implementation should be flexible."))

(defmethod render ((widget generic-html-element) &key)  
  (with-html
    (str (html-from-list
	  `((,(html-element widget)
	      ,@(get-attributes widget nil)))))))

(defclass generic-container-html-element (container html-global-attributes mouse-events form-events)
  ((html-element :initarg :html-element
		:accessor html-element
		:initform nil))  
  (:metaclass widget-class)
  (:default-initargs 
      :class nil
    :style "margin: 0px;")
  (:documentation "Use this widget when the html element used for implementation should be a container but flexible."))

;;TODO: Implement this properly, copy slots etc
(defgeneric change-container-type (generic-container-html-element type))

;;TODO: sort out which attributes are still valid
;;TODO: check if type exists else use generic
(defmethod change-container-type ((container generic-container-html-element) type)
 `(make-widget type ,@(get-attributes container)))

(defmethod render ((widget generic-container-html-element) &key body)
  (with-html    
      (str
       (html-from-list
	`((,(html-element widget)
	    ,@(get-attributes widget nil)
	    ,(or body (with-html-string (render (content widget))))))))))

(defclass hyperlink (generic-container-html-element)
  ((download :initarg :download
	     :accessor link-download
	     :initform nil
	     :html-attribute-p t
	     :documentation "Specifies that the target will be downloaded when a user clicks on the hyperlink")
   (href :initarg :href
	     :accessor href
	     :initform nil
	     :html-attribute-p t
	     :documentation "Specifies the URL of the page the link goes to")
   (hreflang :initarg :hreflang
	     :accessor hreflang
	     :initform nil
	     :html-attribute-p t
	     :documentation "Specifies the language of the linked document")
   (media :initarg :media
	     :accessor media
	     :initform nil
	     :html-attribute-p t
	     :documentation "Specifies what media/device the linked document is optimized for")
   (rel :initarg :rel
	     :accessor rel
	     :initform nil
	     :html-attribute-p t
	     :documentation "Specifies the relationship between the current document and the linked document. Values are alternate,author
bookmark,external,help,license,next,nofollow,noreferrer,noopener,prev,search,tag")
   (target :initarg :type
	     :accessor target
	     :initform nil
	     :html-attribute-p t
	     :documentation "Specifies where to open the linked document")
   (type :initarg :type
	     :accessor link-type
	     :initform nil
	     :html-attribute-p t
	     :documentation "Specifies the media type of the linked document"))
  (:metaclass widget-class)
  (:default-initargs :html-element :a))

(defclass img (generic-html-element)
  ((alt :initarg :alt
         :initform nil
         :accessor alt
	 :html-attribute-p t
         :documentation "Specifies an alternate text for images - http://www.w3schools.com/tags/att_img_alt.asp")
   (cross-origin :initarg :cross-origin
         :initform nil
         :accessor cross-origin
	 :html-attribute-p t
         :documentation "Allow images from third-party sites that allow cross-origin access to be used with canvas - ")
   (height :initarg :height
         :initform nil
         :accessor height
	 :html-attribute-p t
         :documentation "Specifies the height of an image - http://www.w3schools.com/tags/att_img_height.asp")
   (is-map :initarg :is-map
         :initform nil
         :accessor is-map
	 :html-attribute-p t
         :documentation "Specifies an image as a server-side image-map - http://www.w3schools.com/tags/att_img_ismap.asp")
   (src :initarg :src
         :initform nil
         :accessor src
	 :html-attribute-p t
         :documentation "Specifies the URL of an image - http://www.w3schools.com/tags/att_input_src.asp")
   (use-map :initarg :use-map
         :initform nil
         :accessor use-map
	 :html-attribute-p t
         :documentation "Specifies an image as a client-side image-map - http://www.w3schools.com/tags/att_img_usemap.asp")
   (width :initarg :width
	  :initform nil
	  :html-attribute-p t
	  :accessor width
	  :documentation "Specifies the width of an image - http://www.w3schools.com/tags/att_img_width.asp"))
  (:metaclass widget-class)
  (:default-initargs :html-element :img))

(defclass div (generic-container-html-element)
  ()
  (:metaclass widget-class)
  (:default-initargs 
   :html-element :div))

(defclass form (generic-container-html-element)
  ((accept-charset :initarg :accept-charset
         :initform nil
         :accessor accept-charset
	 :html-attribute-p t
         :documentation "Specifies the character encodings that are to be used for the form submission - http://www.w3schools.com/tags/att_form_accept_charset.asp")
   (action :initarg :action
         :initform nil
         :accessor action
	 :html-attribute-p t
         :documentation "Specifies where to send the form-data when a form is submitted - http://www.w3schools.com/tags/att_form_action.asp")
   (auto-complete :initarg :auto-complete
         :initform nil
         :accessor auto-complete
	 :html-attribute-p t
         :documentation "Specifies whether a form should have autocomplete on or off - http://www.w3schools.com/tags/att_form_autocomplete.asp")
   (form-enc-type :initarg :form-enc-type
         :initform nil
         :accessor form-enc-type
	 :html-attribute-p t
         :documentation "Specifies how the form-data should be encoded when submitting it to the server (only for method='post') - http://www.w3schools.com/tags/att_form_enctype.asp")
   (method :initarg :method
         :initform :post
         :accessor form-method
	 :html-attribute-p t
         :documentation "Specifies the HTTP method to use when sending form-data - http://www.w3schools.com/tags/att_form_method.asp")
   (form-no-validate :initarg :form-no-validate
         :initform nil
         :accessor form-no-validate
         :documentation "Specifies that the form should not be validated when submitted - (* IE only from v10) - http://www.w3schools.com/tags/att_form_novalidate.asp")
   (target :initarg :target
         :initform nil
         :accessor target
	 :html-attribute-p t
         :documentation "Specifies where to display the response that is received after submitting the form - "))
  (:metaclass widget-class)
  (:default-initargs  :html-element :form))

(defmethod render ((widget form) &key body)
  (with-html    
      (str
       (html-from-list
	`((,(html-element widget)
	    ,@(get-attributes widget nil)
	    (:input :type "hidden" :name "pageid" :value ,(context-id *sfx-context*))
	    ,(or body (with-html-string (render (content widget))))
	    
	    ))))))

;;(defgeneric render-save-buttons (widget form-id &key &allow-other-keys))
(defgeneric render-save-buttons (widget form-id &key save-button cancel-button))

(defclass grid-form (form)
  ((grid-size :initarg :grid-size
              :initform 6
              :accessor grid-size)
   (header :initarg :header
           :initform nil
           :accessor header)
   (form-id :initarg :form-id
            :initform nil
            :accessor form-id)
   (parent-grid :initarg :parent-grid
                :accessor parent-grid)
   (buttons :initarg :buttons
            :initform t
            :accessor buttons)
   (save-button :initarg :save-button
                :initform t
                :accessor save-button)
   (cancel-button :initarg :cancel-button
                  :initform t
                  :accessor cancel-button)))

(defun disable-grid-buttons (widget)
  (let ((widget (if (typep widget 'grid)
                    (editor widget)
                    widget)))
   (and widget
        (frmt
         "$(\"#~a .btn-toolbar :button\").attr(\"disabled\", \"disabled\")"
         (name widget)))))

(defmethod render ((widget grid-form) &key content grid)
  (with-html
    (:div :class "card"
                (:div :class "card-header"
                      (:h4 :class "card-title" (esc (header widget))))
                (:div :class "card-block"
                      (:div :id (form-id widget)
			    :class "form-horizontal"
			    (:input :type "hidden" :name "form-id"
				    :value (form-id widget))
			    (str content)
			    (when (buttons widget)
			      (render-save-buttons grid (form-id widget)
						   :save-button (save-button widget)
						   :cancel-button (cancel-button widget))))))))


#|
(defmethod render ((widget grid-form) &key content grid)
  (with-html
    (:div :class "card"
                (:div :class "card-header"
                      (:h4 :class "card-title" (esc (header widget))))
                (:div :class "card-block"
                      (:form :id (form-id widget)
                            (:input :type "hidden" :name "pageid" 
				    :value (context-id *sfx-context*))
                            (:input :type "hidden" :name "form-id"
                                    :value (form-id widget))

                            (str content)
                            (let ((disable (disable-grid-buttons grid)))
                              (htm
                               (:div :class "btn-toolbar panel-footer"
                                     (when (and (save-button widget)
                                                (not (and (typep grid 'grid)
                                                          (grid-read-only grid)))
                                                (check-page-permission "Update"))
                                       (htm
                                        (:button :class "btn-primary btn"
                                                 :onclick
                                                 (frmt
                                                 ;; "{tinyMCE.triggerSave();~@[~a;~]~a;}"
						  "save_scripts();{~@[~a;~]~a;}"
                                                  disable
                                                  (js-render-form-values 
						   (if (typep grid 'grid)
						       (editor grid)
						       grid)
						   (form-id widget)
						   (js-pair "grid-name" (name grid))
						   (js-pair "action" "save")))
                                                 "Save")))
                                     (when (cancel-button widget)
                                       (htm
                                        (:button :class "btn-default btn"
                                                 :onclick
                                                 (frmt
                                                  "{~@[~a;~]~a;}"
                                                  disable
                                                  (js-render (if (typep grid 'grid)
                                                                 (editor grid)
                                                                 grid)
                                                             (js-pair "grid-name" (name grid))
                                                             (js-pair "action" "cancel")))
                                                 "Cancel")))))))))))
|#

(defclass input (generic-html-element)
  ((accept :initarg :accept
         :initform nil
         :accessor accept
	 :html-attribute-p t
         :documentation "Specifies the types of files that the server accepts (only for type='file') - http://www.w3schools.com/tags/att_input_accept.asp")
   (alt :initarg :alt
         :initform nil
         :accessor alt
	 :html-attribute-p t
         :documentation "Specifies an alternate text for images (only for type='image') - http://www.w3schools.com/tags/att_input_alt.asp")
   (auto-complete :initarg :auto-complete
         :initform nil
         :accessor auto-complete
	 :html-attribute-p t
         :documentation "Specifies whether an <input> element should have autocomplete enabled - http://www.w3schools.com/tags/att_input_autocomplete.asp")
   (auto-focus :initarg :auto-focus
         :initform nil
         :accessor auto-focus
	 :html-attribute-p t
         :documentation "Specifies that an <input> element should automatically get focus when the page loads - (* IE only from v10)  - http://www.w3schools.com/tags/att_input_autofocus.asp")
   (checked :initarg :checked
         :initform nil
         :accessor checked
	 :html-attribute-p t
         :documentation "Specifies that an <input> element should be pre-selected when the page loads (for type='checkbox' or type='radio') - http://www.w3schools.com/tags/att_input_checked.asp")
   (disabled :initarg :disabled
         :initform nil
         :accessor disabled
	 :html-attribute-p t
         :documentation "Specifies that an <input> element should be disabled - http://www.w3schools.com/tags/att_input_disabled.asp")
   (form :initarg :form
         :initform nil
         :accessor form
	 :html-attribute-p t
         :documentation "Specifies one or more forms the <input> element belongs to - (* Not supported by IE) - http://www.w3schools.com/tags/att_input_form.asp")
   (form-action :initarg :form-action
         :initform nil
         :accessor form-action
	 :html-attribute-p t
         :documentation "Specifies the URL of the file that will process the input control when the form is submitted (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formaction.asp")
   (form-enc-type :initarg :form-enc-type
         :initform nil
         :accessor form-enc-type
	 :html-attribute-p t
         :documentation "Specifies how the form-data should be encoded when submitting it to the server (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formenctype.asp")
   (form-method :initarg :form-method
         :initform nil
         :accessor form-method
	 :html-attribute-p t
         :documentation "Defines the HTTP method for sending data to the action URL (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formmethod.asp")
   (form-no-validate :initarg :form-no-validate
         :initform nil
         :accessor form-no-validate
	 :html-attribute-p t
         :documentation "Defines that form elements should not be validated when submitted - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formnovalidate.asp")
   (form-target :initarg :form-target
         :initform nil
         :accessor form-target
	 :html-attribute-p t
         :documentation "Specifies where to display the response that is received after submitting the form (for type='submit' and type='image') - (* IE only from v10) - http://www.w3schools.com/tags/att_input_formtarget.asp")
   (height :initarg :height
         :initform nil
         :accessor height
	 :html-attribute-p t
         :documentation "Specifies the height of an <input> element (only for type='image') - http://www.w3schools.com/tags/att_input_height.asp")
   (list :initarg :list
         :initform nil
         :accessor data-list
	 :html-attribute-p t
         :documentation "Refers to a <datalist> element that contains pre-defined options for an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_list.asp")
   (max :initarg :max
         :initform nil
         :accessor element-max
	 :html-attribute-p t
         :documentation "Specifies the maximum value for an <input> element - (* IE only from v10 and not for date/time) - http://www.w3schools.com/tags/att_input_max.asp")
   (max-length :initarg :max-length
         :initform nil
         :accessor max-length
	 :html-attribute-p t
         :documentation "Specifies the maximum number of characters allowed in an <input> element - http://www.w3schools.com/tags/att_input_maxlength.asp")
   (min :initarg :min
         :initform nil
         :accessor element-min
	 :html-attribute-p t
         :documentation "Specifies a minimum value for an <input> element - (* IE only from v10 and not for date/time) - http://www.w3schools.com/tags/att_input_min.asp")
   (multiple :initarg :multiple
         :initform nil
         :accessor multiple
	 :html-attribute-p t
         :documentation "Specifies that a user can enter more than one value in an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_multiple.asp")  
   (pattern :initarg :pattern
         :initform nil
         :accessor pattern
	 :html-attribute-p t
         :documentation "Specifies a regular expression that an <input> element's value is checked against - (* IE only from v10) - http://www.w3schools.com/tags/att_input_pattern.asp")
   (place-holder :initarg :place-holder
         :initform nil
         :accessor place-holder
	 :html-attribute-p t
         :documentation "Specifies a short hint that describes the expected value of an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_placeholder.asp")
   (read-only :initarg :read-only
         :initform nil
         :accessor read-only
	 :html-attribute-p t
         :documentation "Specifies that an input field is read-only - http://www.w3schools.com/tags/att_input_readonly.asp")
   (required :initarg :required
         :initform nil
         :accessor required
	 :html-attribute-p t
         :documentation "Specifies that an input field must be filled out before submitting the form - (* IE only from v10) - http://www.w3schools.com/tags/att_input_required.asp")
   (size :initarg :size
         :initform nil
         :accessor size
	 :html-attribute-p t
         :documentation "Specifies the visible width, in characters, of an <input> element - http://www.w3schools.com/tags/att_input_size.asp")
   (src :initarg :src
         :initform nil
         :accessor src
	 :html-attribute-p t
         :documentation "Specifies the URL of the image to use as a submit button (only for type='image') - http://www.w3schools.com/tags/att_input_src.asp")
   (step :initarg :step
         :initform nil
         :accessor element-step
	 :html-attribute-p t
         :documentation "Specifies the legal number intervals for an input field - (* IE only from v10) - http://www.w3schools.com/tags/att_input_step.asp")
   (type :initarg :type
         :initform "text"
         :accessor element-type
	 :html-attribute-p t
         :documentation "Specifies the type <input> element to display - http://www.w3schools.com/tags/att_input_type.asp")
   (value :initarg :value
              :initform nil
              :accessor value
	      :html-attribute-p t
              :documentation "Specifies the value of an <input> element - http://www.w3schools.com/tags/att_input_value.asp")
   (width :initarg :width
	  :initform nil
	  :html-attribute-p t
	  :accessor width
	  :documentation "Specifies the width of an <input> element (only for type='image') - http://www.w3schools.com/tags/att_input_width.asp")
   (submit-on-change :initarg :submit-on-change
                     :initform nil
                     :accessor submit-on-change
		     :html-attribute-p t
                     :documentation
                     "id of the form to submit"))
  (:metaclass widget-class)
  (:default-initargs  :html-element :input))

;;TODO: Should this be moved to generic???
(defmethod synq-data ((widget input) &optional data)
  (declare (ignore data))
  (let ((parameter (parameter (name widget))))
    (when parameter
      (setf (value widget) parameter))))

(defun on-change-function (widget)
  (if (submit-on-change widget)
      (format nil "document.getElementById(\"~a\").submit();"
              (submit-on-change widget))
      (on-change widget)))

(defclass text (input)
  ())

;;TODO: implement slots
(defclass text-area (generic-container-html-element)
  ((auto-focus :initarg :auto-focus
         :initform nil
         :accessor auto-focus
	 :html-attribute-p t
         :documentation "Specifies that an <input> element should automatically get focus when the page loads - (* IE only from v10)  - http://www.w3schools.com/tags/att_input_autofocus.asp")
   (cols :initarg :cols
         :initform nil
         :accessor cols
	 :html-attribute-p t
         :documentation "Specifies the visible width of a text area - http://www.w3schools.com/tags/att_textarea_cols.asp")
   (disabled :initarg :disabled
         :initform nil
         :accessor disabled
	 :html-attribute-p t
         :documentation "Specifies that an <input> element should be disabled - http://www.w3schools.com/tags/att_input_disabled.asp")
   (form :initarg :form
         :initform nil
         :accessor form
	 :html-attribute-p t
         :documentation "Specifies one or more forms the <input> element belongs to - (* Not supported by IE) - http://www.w3schools.com/tags/att_input_form.asp")
   (max-length :initarg :max-length
         :initform nil
         :accessor max-length
	 :html-attribute-p t
         :documentation "Specifies the maximum number of characters allowed in an <input> element - http://www.w3schools.com/tags/att_input_maxlength.asp")
   (place-holder :initarg :place-holder
         :initform nil
         :accessor place-holder
	 :html-attribute-p t
         :documentation "Specifies a short hint that describes the expected value of an <input> element - (* IE only from v10) - http://www.w3schools.com/tags/att_input_placeholder.asp")
   (read-only :initarg :read-only
         :initform nil
         :accessor read-only
         :documentation "Specifies that an input field is read-only - http://www.w3schools.com/tags/att_input_readonly.asp")
   (required :initarg :required
         :initform nil
         :accessor required
	 :html-attribute-p t
         :documentation "Specifies that an input field must be filled out before submitting the form - (* IE only from v10) - http://www.w3schools.com/tags/att_input_required.asp")
   (rows :initarg :rows
         :initform nil
         :accessor rows
	 :html-attribute-p t
         :documentation "Specifies the visible number of lines in a text area - http://www.w3schools.com/tags/att_textarea_rows.asp")
   (wrap :initarg :wrap
         :initform nil
         :accessor wrap
	 :html-attribute-p t
         :documentation "Specifies how the text in a text area is to be wrapped when submitted in a form - http://www.w3schools.com/tags/att_textarea_wrap.asp"))
  (:metaclass widget-class)
  (:default-initargs  :html-element :textarea))

;;TODO: What about body vs content?
(defmethod synq-data ((widget text-area) &optional data)
  (declare (ignore data))
  (let ((parameter (parameter (name widget))))
    (when parameter
      (setf (content widget) parameter))))
;;;

(defclass checkbox (input)
  ((description :initarg :description
                :initform nil
                :accessor description)
   (translate-name :initarg :translate-name
                   :initform t
                   :accessor translate-name)
   (include-hidden :initarg :include-hidden
                   :initform t
                   :accessor include-hidden)))

;;TODO: Why is input in side of label??????
(defmethod render ((widget checkbox) &key no-name)
  (with-slots (on-change on-click) widget
    (let ((name (if (translate-name widget)
                    (widgy-name widget "value")
                    (name widget)))
          (id (or (id widget) (name widget))))
      (with-html
        ;; When checkbox isn't checked, the browser doesn't send anything,
        ;; so we need an additional parameter which takes precedence
        ;; in this case.
        (when (and (include-hidden widget)
                   (not no-name))
          (htm
           (:input :type "hidden"
                   :name name
                   :value "off")))
        (:label :style (style widget)
                (:input :type "checkbox"
                        :name (and (not no-name)
                                   name)
                        :id (and (not no-name)
                                 id)
                        :class (css-class widget)
                        :checked (when (value widget)
                                   "checked"))
                (when (description widget)
                  (esc (frmt "~A" (description widget)))))
        (when (or on-click (on-change-function widget))
          (defer-js
              (frmt "$('#~a').change(function(){~@{~@[~a;~]~}})"
                    id
                    on-click (on-change-function widget))))))))

(defmethod action-handler ((checkbox checkbox))
  (let ((value (value checkbox)))
    (when (stringp value)
      (setf (value checkbox)
            (or (string= value "on") (string= value "true") )))))

;;;

(defclass checkbox-list (input)
  ((items :initarg :items
          :initform ()
          :accessor items)
   (checkboxes :initform ()
               :accessor checkboxes)
   (orientation :initarg :orientation
                :initform :vertical
                :accessor orientation)
   (check-all :initarg :check-all
              :initform t
              :accessor check-all)
   (include-hidden :initarg :include-hidden
                   :initform t
                   :accessor include-hidden))
  (:metaclass widget-class))

(defun %create-checkboxes (checkbox-list
                           items
                           &key change
                                (parent 0)
                                (level 0))
  

;;  (break "~A" items)
  (loop for (nil description selected . sub) in items
        for i from 0
        for checkbox = (make-widget 'checkbox
                                    :name (sub-name checkbox-list
                                                    (frmt "~a-~a-~a"
                                                          parent level i))
                                    :description description
                                    :value selected
                                    :include-hidden (include-hidden checkbox-list))
        when change
        do (setf (value checkbox) selected
                 (description checkbox) description)
        do (setf (style checkbox)
                 (and (plusp level)
                      (frmt "padding-left:~apx" (* 10 level))))
        collect (list* checkbox
                       (%create-checkboxes checkbox-list
                                           sub
                                           :change change
                                           :parent (1+ i)
                                           :level (1+ level)))))

(defun create-checkboxes (checkbox-list
                          &key change)
  (prog1
      (setf
       (checkboxes checkbox-list)
       (%create-checkboxes checkbox-list (items checkbox-list)
                           :change change))
    #|(setf (value checkbox-list)
          (checkbox-list-value checkbox-list))
    |#
    ))

(defun update-checkbox-list-items (checkbox-list)
  (labels ((%update (checkboxes items)
             (loop for (checkbox . check-sub) in checkboxes
                   for (object text nil . items-sub) in items
                   collect (list* object text (value checkbox)
                                  (%update check-sub items-sub)))))
    (setf (items checkbox-list)
          (%update (checkboxes checkbox-list)
                   (items checkbox-list)))))

(defun map-checkboxes-values (function checkbox-list)
  (labels ((recurse (items)
             (loop for (object nil value . items-sub) in items
                   do
                   (funcall function object value)
                   (recurse items-sub))))
    (recurse (items checkbox-list))))

(defun checkbox-list-value (checkbox-list)
  
  (labels ((%value (checkboxes items)
             (loop for (checkbox . check-sub) in checkboxes
                   for (object text value . items-sub) in items
                   when (or value check-sub)
                   collect
                   (cond ((not value)
                          (list (%value check-sub items-sub)))
                         (check-sub
                          (list* object (%value check-sub items-sub)))
                         (t
                          object)))))
    (%value (checkboxes checkbox-list)
            (items checkbox-list))))

(defmethod (setf items) :after (items (object checkbox-list))
  (create-checkboxes object :change t))

(defun all-checkboxes-checked-p (checkbox-list)
  (block nil
    (map-checkboxes-values (lambda (key value)
                             (declare (ignore key))
                             (unless value
                               (return)))
                           checkbox-list)
    t))

(defun make-check-all-button (checkbox-list)
  (when (check-all checkbox-list)
    (let ((box (make-widget 'checkbox
                            :description "Check All"
                            :class "checkall")))
      (setf (value box) (all-checkboxes-checked-p checkbox-list))
      (with-html
        (render box :no-name t)
        (when (eq (orientation checkbox-list) :vertical)
          (htm (:br)))))))


(defun lay-checkbox-list (checkbox-list
                          checkboxes
                          &key (level 0)
                          no-name)
  (with-html
    ;; (cl-naive-who-ext::parse (checkbox . sub))
    ;; ==> type-error: The value  sub is not of type list
    ;; Or perhaps we should modify cl-naive-who-ext::parse to accept such dotted lists?
    ;; In any case, there's no business in parsing non-code parts! What is the code walker doing?
    (loop for clause in checkboxes
          for checkbox := (car clause)
          for sub := (cdr clause)
          do
          (render checkbox :no-name no-name)
          (lay-checkbox-list checkbox-list sub
                             :level (1+ level)
                             :no-name no-name))))

(defmethod render ((checkbox-list checkbox-list) &key no-name)
  (with-html
    (:div
     :class (ecase (orientation checkbox-list)
              (:vertical "cb-list")
              (:horizontal "horizontal-cb-list"))
     :id (name checkbox-list)
     :style (style checkbox-list)
     (make-check-all-button checkbox-list)
     (lay-checkbox-list checkbox-list
                        (create-checkboxes checkbox-list)
                        :no-name no-name)))
  (defer-js "$('#~a :input').change(function(){~@{~@[~a~]~^;~}})"
      (name checkbox-list)
    (on-change checkbox-list)
    (when (check-all checkbox-list)
      "updateCheckAll(this)")))

(defmethod action-handler ((checkbox-list checkbox-list))
  (update-checkbox-list-items checkbox-list)
  (setf (value checkbox-list)
        (checkbox-list-value checkbox-list)))

(defclass select (input)
  ((items :initarg :items
          :initform ()
          :accessor items)
   (first-item :initarg :first-item
               :initform nil
               :accessor first-item)
   (blank-allowed :initarg :blank-allowed
                  :initform t
                  :accessor blank-allowed)
   (allow-deselect :initarg :allow-deselect
                   :initform nil
                   :accessor allow-deselect)
   (auto-focus :initarg :auto-focus
               :initform nil
               :accessor auto-focus
	       :html-attribute-p t
               :documentation "Specifies that an <input> element should automatically get focus when the page loads - (* IE only from v10)  - http://www.w3schools.com/tags/att_input_autofocus.asp")
   (form :initarg :form
         :initform nil
         :accessor form
	 :html-attribute-p t
         :documentation "Specifies one or more forms the <input> element belongs to - (* Not supported by IE) - http://www.w3schools.com/tags/att_input_form.asp")
   (multiple :initarg :multiple
             :initform nil
             :accessor multiple
	     :html-attribute-p t
             :documentation "Specifies that multiple options can be selected at once - http://www.w3schools.com/tags/att_select_multiple.asp")
   (required :initarg :required
             :initform nil
             :accessor required
	     :html-attribute-p t
             :documentation "Specifies that an input field must be filled out before submitting the form - (* IE only from v10) - http://www.w3schools.com/tags/att_input_required.asp")
   (size :initarg :size
         :initform nil
         :accessor size
         :documentation "Defines the number of visible options in a drop-down list - http://www.w3schools.com/tags/att_select_size.asp"))
  (:metaclass widget-class))

(defun item-description (item)
  (if (consp item)
      (second item)
      item))

(defun item-value (item)
  (if (consp item)
      (car item)
      item))

(defun display-select-warning (select)
  (unless (or (invalid-value-p (value select))
              (not (items select)))
    (with-html
      (:img :src "/images/vfr-red-icon.png"
            :alt ""
            :title (format nil "Illegal value \"~A\"! ~
Please select a legal one"
                           (value select))))))

(defun item-equal (item value)
  (or (equalp (item-value item)
                         value)
      (equalp (item-description item)
                         value)))

(defmethod render ((widget select) &key)
  (let ((current-value (value widget))
        (first-item (first-item widget))
        selected-item)
    (with-html
      (:select
       :class (css-class widget)
       :name (name widget)
       :id (or (id widget) (name widget))

       ;;TODO: use normal onchange... move this to appropriate package that needs it.
       :onchange (on-change-function widget)

       ;; (get-attributes
       ;;    widget
       ;;    (list 'type 'name 'id 'onchange 'allow-deselect 'items
       ;;          'first-item 'allow-blank))
       :deselect (allow-deselect widget)
       (when first-item
         (htm (:option :selected
                       (and (item-equal first-item current-value)
                            (setf selected-item t))
                       (esc first-item))))
       (when (and (blank-allowed widget)
                  (invalid-value-p current-value))
         (htm
          (:option
           :style "background-color:pink;"
           :selected t)))
       
       (loop for item being the element of (items widget)
             do
             (htm (:option :selected
                           (and (item-equal item current-value)
                                (setf selected-item t))
                           :value (item-value item)
                           (princ-esc (item-description item)))))
       (when (and (not selected-item)
                  (not (invalid-value-p current-value)))
         (htm
          (:option
           :style "background-color:pink;"
           :selected t
           :value (escape current-value)
           (princ-esc current-value))))))))

(defclass labeled-input (container)
  ((label :initarg :label
          :initform nil
          :accessor label)))

(defmethod render ((widget labeled-input)
                   &key label body)
  (with-html
    (:div
     (:label (str (or label (label widget))))
     (:div
      (render (or body (content widget)))))))

(defclass meta-tag (widget html-element)
  ((meta-type :initarg :meta-type
              :initform "name"
              :accessor meta-type)
   (name :initarg :name
         :initform nil
         :accessor meta-name)
   (content :initarg :content
         :initform nil
         :accessor content)))

(defmethod render ((widget meta-tag) &key)
  (with-html
    (cond ((string-equal (meta-type widget) "charset")
           (htm (:meta :charset (content widget))))
          ((string-equal (meta-type widget) "http-equiv")
           (htm (:meta :http-equiv (name widget) :content (content widget))))
          (t
           (htm (:meta :name (meta-name widget) :content (content widget)))))))

(defclass link (widget html-global-attributes)
  ((link-type :initarg :link-type
              :initform nil
              :accessor link-type)
   (rel :initarg :rel
        :initform nil
        :accessor rel)
   (href :initarg :href
         :initform nil
         :accessor href)
   (href-lang :initarg :href-lang
         :initform nil
         :accessor href-lang)
   (media :initarg :media
         :initform nil
         :accessor media)
   (condition :initarg :condition
              :initform nil
              :accessor link-condition)))

(defmethod render ((widget link) &key)
  (with-html
    (:link
            :type (link-type widget)
            :rel (rel widget)
            :href (href widget)
            :href-lang (href-lang widget)
            :media (media widget))))

(defclass style (html-global-attributes container)
  ((style-type :initarg :style-type
              :initform "text/css"
              :accessor style-type)
   (scope :initarg :scope
        :initform nil
        :accessor scope)
   (media :initarg :media
         :initform nil
         :accessor media)))

(defmethod render ((widget style)
                   &key body)
  (with-html
    (:style
     :type (style-type widget)
     :scope (scope widget)
     :media (media widget)
     (if body
         (render body)
         (render (content widget))))))

(defclass html-script (html-global-attributes container)
  ((script-type :initarg :script-type
              :initform "text/javascript"
              :accessor script-type)
   (async :initarg :async
        :initform nil
        :accessor async)
   (charset :initarg :charset
         :initform nil
         :accessor charset)
   (defer :initarg :defer
         :initform nil
         :accessor defer)
   (src :initarg :src
         :initform nil
         :accessor src)
   (page-bottom-p :initarg :page-bottom-p
                  :initform nil
                  :accessor page-bottom-p)))

(defclass html-condition-group (container)
  ((conditions :initarg :conditions
	       :initform nil
	       :accessor conditions)))

(defmacro html-condition (conditions &body body)
  `(with-html-string
    (str ,(format nil "<!--[if ~{~A~^ ~}]>" conditions))
    (str ,@body)
    (str "<![endif]-->")))

(defmethod render ((widget html-condition-group) &key body)
  (html-condition (conditions widget)
    (if body
	(render body)
	(if (content widget)
	    (render (content widget))))))


(defmethod render ((widget html-script)
                   &key body)
  (with-html
    (htm
     (:script
      :type (script-type widget)
      :async (async widget)
      :charset (charset widget)
      :defer (defer widget)
      :src (src widget)	
      (if body
	  (render body)
	  (if (content widget)
	      (render (content widget))))))))


;;;CONTEXT SELECTION

(defun flatten-data-tree (tree &key child-func check-func)
  (labels ((%en-tree (tree enums)
	       (dolist (item tree)
		 (when (or (not check-func) (funcall check-func item))
		   (pushnew (%en-tree (funcall child-func item)
				      (list item))
			    enums)))
	       enums))
    (reverse (alexandria:flatten (%en-tree tree nil)))))


(defun enumerate-data-tree (tree &key child-func selected value-func check-func)
  (labels ((%en-tree (tree enums)
	       (dolist (item tree)
		 (when (or (not check-func) (funcall check-func item))
		   (pushnew (reverse 
			     (%en-tree (funcall child-func item)
				       (reverse (list item
						      (if value-func
							  (funcall value-func item)
							  item)
						      (find item selected)))))
			    enums)))
	       enums))
    (%en-tree tree nil)))


(defclass data-tree-selector (widget)
  ((tree :initarg :tree
	 :accessor tree
	 :initform nil)
   (enum-data :initarg :enum-data
	      :accessor enum-data
	      :initform nil)
   (selected :initarg :selected
	     :initform nil
	     :accessor selected)
   (child-func :initarg :child-func
	       :initform nil
	       :accessor child-func)
   (value-func :initarg :value-func
	       :initform nil
	       :accessor value-func)
   (check-func :initarg :check-func
	       :initform nil
	       :accessor check-func)
   (cb-lists :initform nil
             :accessor cb-lists)))


(defmethod render ((widget data-tree-selector) &key)
  (let ((enum-data (enumerate-data-tree
		    (tree widget)
		    :selected (selected widget)
		    :child-func (child-func widget)
		    :value-func (value-func widget)
		    :check-func (check-func widget))))
    (setf (enum-data widget) (flatten-data-tree (tree widget)
						 :child-func (child-func widget)
						  :check-func (check-func widget)))
    (with-html
      (setf (cb-lists widget)
            (loop for items in enum-data
                  for i from 0
                  for cb-list = (make-widget 'checkbox-list :name
                                             (frmt "~A-~a" (name widget) i))
                  do
		 (setf (items cb-list) (list items))
		 (htm (:h4 (esc (funcall (value-func widget) 
					 (first items)))))
                  (render cb-list)
                  collect cb-list)))))

(defmethod action-handler ((widget data-tree-selector))
  (when (or (parameter "set") (parameter "save"))
      (setf (selected widget) nil)
    (let ((values (loop for cb-list in (cb-lists widget)
		     append
		       (alexandria:flatten (value cb-list)))))
      (labels ((recurse-check (boxes)
		 (if (listp boxes)
		     (mapcar #'recurse-check boxes)
		     (if (typep boxes 'checkbox)
			 (if (value boxes)
			       (pushnew 
				  (find (description boxes) values 
					:key 'entity-name :test 'string-equal)
				  (selected widget)))
			 (recurse-check boxes)))))
	(dolist (cb (cb-lists widget))
	  (recurse-check (checkboxes cb))))
  
      )))



;;TODO: Check this
(defun check-entity-access (entity)
  (let ((user (current-user)))
    (or (super-user-p user)
	(find entity (accessible-entities user)))))


(defclass context-select (widget)
  ((selector :initarg :selector
	      :accessor selector
	      :initform nil)))

(defmethod render ((widget context-select) &key)
  (let ((box (make-widget 'box :name "context-select-card" :header "Context Entities")))
    
    (setf (selector widget) (make-widget 'data-tree-selector :name "context-selector"))
    (setf (tree (selector widget)) (license-entities (license (current-user))))
   
    (setf (selected (selector widget)) 
	  (or 
	   (current-entities *sfx-session*)
	   (current-entities (active-user))))
    (setf (child-func (selector widget)) #'children)
    (setf (value-func (selector widget)) #'entity-name)
    (setf (check-func (selector widget)) #'check-entity-access)    
    (setf (content box)
          (with-html-string
	    (:form :id "context-select"
                   :action ""
                   :method "post"
                   (render (selector widget))
		   (:input :type :hidden :id "pageid"
			   :name "pageid"
			   :value (or (context-id *sfx-context*)
				      (parameter "pageid")))
                   (:div :class "btn-toolbar"
                         (:button :class "btn-primary btn"
                                  :name "set"
                                  :type "submit"
                                  :value "Set"
                                  "Set")))))
    (render box)))

(defmethod action-handler ((widget context-select))  
  
  (when (parameter "set")

    (call-next-method)
    (setf (current-entities *sfx-session*) (selected (selector widget)))

    (setf (current-entities (active-user)) 
	  (current-entities *sfx-session*))
    (persist (active-user))))


(defclass page (container)
  ((doc-type :initarg :doc-type
             :initform "html"
             :accessor doc-type)
   (manifest :initarg :manifest
             :initform nil
             :accessor manifest
             :documentation "Specifies the address of the document's cache manifest (for offline browsing) - http://www.w3schools.com/tags/att_html_manifest.asp")
   (lang :initarg :lang
	 :accessor lang
	 :initform "en")
   (base :initarg :base
         :initform nil
         :accessor base)
   (title :initarg :title
          :initform (error "An html page must have a title.")
          :accessor title)
   (author :initarg :author
           :initform ""
           :accessor author)
   (description :initarg :description
                :initform ""
                :accessor description)
   (classification :initarg :classification
                   :initform ""
                   :accessor classification)
   (key-words :initarg :key-words
              :initform ""
              :accessor key-words)
   (meta-tags :initarg :meta-tags
              :initform (list
			 "<meta charset=\"utf-8\">"
			 "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">"
			 "<meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">")
              :accessor meta-tags)
   (links :initarg :links
          :initform nil
          :accessor links)
   (styles :initarg :styles
           :initform nil
           :accessor styles)
   (scripts :initarg :scripts
            :initform nil
            :accessor scripts)
   (no-script :initarg :no-script
              :initform nil
              :accessor no-script)
   (body-class :initarg :body-class
               :initform "bodyclass"
               :accessor body-class)))

(defclass nav-item (widget)
  ((description :initarg :description
		:accessor description
		:initform nil)
   (href :initarg :href
	 :accessor href
	 :initform "#")
   (active-p :initarg :active-p
	     :accessor active-p
	     :initform nil))
  (:metaclass widget-class))

#|
(defmethod render ((widget nav-item) &key)
  (with-html-string
    (:li :class "nav-item"
	 (:a :class (frmt "nav-link ~A" (if (active-p widget) "active"))
	     :href (frmt "~A~A" (site-url *sfx-system*) (href widget))
	     (str (description widget))))))
|#

(defmethod render ((widget nav-item) &key)
  (with-html-string
    (:a :class (frmt "nav-link ~A" (if (active-p widget) "active" ))
	     :href (frmt "~A~A" (site-url *sfx-system*) (href widget))
	     (str (description widget)))))

(defclass nav-bar (generic-html-element)
  ((vertical-p :initarg :vertical-p
	       :accessor vertical-p
	       :initform nil)
   (pull-directive :initarg :pull-directive
		   :accessor pull-directive
		   :initform "")
   (menu-items :initarg :menu-items
	       :accessor menu-items
	       :initform nil))
  (:metaclass widget-class))

#|
(defmethod render ((widget nav-bar) &key)
  (with-html-string
    (:nav :class  (frmt "navbar navbar-light bg-faded ~A" (pull-directive widget))
	  (:ul :class (if (vertical-p widget)
			  "nav nav-pills flex-column"
			  "nav nav-pills")
	       (let ((active-p t))
		 (dolist (item (menu-items widget))
		   (str (render item))
		   (setf active-p nil)))))))
|#

(defmethod render ((widget nav-bar) &key)
  (with-html-string
    (:nav :class (if (vertical-p widget)
			  "nav nav-pills flex-column"
			  "nav nav-pills")
	       (let ((active-p t))
		 (dolist (item (menu-items widget))
		   (str (render item))
		   (setf active-p nil))))))

(defun make-nav-item (menu-item)
  (let ((def (context-definition menu-item)))
    (if def
	(make-widget 'nav-item :name (frmt "nav-~A" (context-name def)) 
		     :description (item-name menu-item) 
		     :href
		     (if (context-parameters menu-item)
			 (frmt "~A?~A=~A" 
			       (url def)
			       (parameter-name (first (context-parameters menu-item)))
			       (parameter-value (first (context-parameters menu-item))))
			 (url def)))
	(make-widget 'nav-item :name (frmt "nav-~A" (item-name menu-item)) 
		     :description (item-name menu-item) 
		     :href 
		     (if (context-parameters menu-item)
			 (frmt "~A?~A=~A" 
			       (item-name menu-item)
			       (parameter-name (first (context-parameters menu-item)))
			       (parameter-value (first (context-parameters menu-item))))
			 (item-name menu-item))))))


(defun default-left-menu ()
  (let (navs)
   ;; (break "~A" (get-collection-docs 'module))
    (dolist (doc (coerce (get-collection-docs 'module) 'list))
      (unless (string-equal "System Admin" (module-name doc))
	(setf navs (append navs (loop for item in (menu-items (first (menu doc)))
				   collect (make-nav-item item))))))
    
    navs))


(defun make-drop-item (menu-item)
  (let ((def (context-definition menu-item)))
    (if def
	(make-widget 'hyperlink :class "dropdown-item" :name (frmt "dpi-~A" (context-name def)) 
		     :content (item-name menu-item) 
		     :href (url def))
	(make-widget 'hyperlink :class "dropdown-item" :name (frmt "dpi-~A" (item-name menu-item)) 
		      
		     :href (item-name menu-item)
		     :content (item-name menu-item)
		     ))))

(defun user-menu ()
  (let* ((sys-mod (find-docx 'module
			     :test (lambda (doc)
				     (string-equal "System Admin" (module-name doc)))))
	 (sys-menu (if sys-mod (loop for item in (menu-items (first (menu sys-mod)))
				  collect (make-drop-item item)))))
 
    sys-menu))

(defun default-right-menu ()
  (let* ((sys-mod (find-docx 'module
			     :test (lambda (doc)
				     (string-equal "System Admin" (module-name doc)))))
	 (sys-menu (if sys-mod (loop for item in (menu-items (first (menu sys-mod)))
				  collect (make-nav-item item)))))
 
    sys-menu))

(defmethod render ((widget page) &key body)
  (with-debugging
    (with-html-string
      (fmt "<!doctype ~A>" (doc-type widget))
      (:html
       :lang (lang widget)
       :manifest (manifest widget)
       (:head
	;;TODO: implement as html-element (global)
	(when (base widget)
	  (htm (:base :href (base widget))))
	;;TODO: implement as html-element (global)
	(:title (render (title widget)))
	"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">"	
	(dolist (meta (meta-tags widget))
	  (render meta))
	""
	(dolist (link (links widget))
	  (render link))
	""
	(page-include-css)
	""
	(dolist (style (styles widget))
	  (render style))
	""
	(dolist (script (scripts widget))
	  (unless (page-bottom-p script)
	    (render script)))
	""
	(when (no-script widget)
	  (render (no-script widget)))
	"<style> 

.cb-list > label {
    display: block;
}

.horizontal-cb-list label {
    display: inline-block;
    margin-right: 12px;
}

.horizontal-cb-list .checkbox {
    margin-right: 4px;
}

</style>"
	"<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css\" integrity=\"sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ\" crossorigin=\"anonymous\">"
	


	"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>"
	"<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css\">"
	"<script src=\"https://cdn.datatables.net/1.10.1/js/jquery.dataTables.min.js\"></script>"
	" <script src=\"https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js\" integrity=\"sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB\" crossorigin=\"anonymous\"></script>"
	"<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js\" integrity=\"sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn\" crossorigin=\"anonymous\"></script>"
	
	"<script src=\"web/codemirror/lib/codemirror.js\"></script>
<link rel=\"stylesheet\" href=\"web/codemirror/lib/codemirror.css\">
<script src=\"web/codemirror/mode/commonlisp/commonlisp.js\"></script>
<script src=\"web/codemirror/addon/edit/closebrackets.js\"></script>
<script src=\"web/codemirror/addon/edit/matchbrackets.js\"></script>
<script src=\"web/cl-sfx.js\"></script>
<link rel=\"stylesheet\" href=\"web/cl-sfx.css\">
<link href=https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.standalone.min.css' rel='stylesheet'>
 <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js'></script>
"
	
	
	(page-include-bits)
	"")
       (:body :class (body-class widget)
	(:nav 
	 :class "navbar navbar-fixed-top navbar-light bg-faded justify-content-end hidden-print"
		    
	      (:div 
	       (:a :class "navbar-brand" :href "#" (str (system-name *sfx-system*)))
	       
	       ;;(:span :class "navbar-text float-md-right" (str (email (current-user))))
	       (:ul :class "navbar-nav mr-auto"
		    (:li :class "nav-item dropdown"
			 (:a :class "nav-link dropdown-toggle" :href "" :id "userDropdown" 
			     :data-toggle "dropdown" :aria-haspopup="true" :aria-expanded "false" 
			     (str (if (current-user) (email (current-user)))))
			 (:div :class "dropdown-menu" :aria-labelledby "userDropdown"
			       (dolist (item (user-menu))
				 (render item)))
			 ))))
	(if (current-user)
	    (htm (:button :class "navbar-toggler navbar-toggler-left hidden-print"
			  :type "button"
			  :data-toggle "collapse"
			  :data-target "#exNavbarLeft"
			  :aria-controls "exNavbarLeft"
			  :aria-expanded "true"
			  :aria-label "Toggle menu"
			  (str "&#9776;"))))
	(if (current-user)
	    (htm (:button :class "navbar-toggler navbar-toggler-right hidden-print"
			  :type "button"
			  :data-toggle "collapse"
			  :data-target "#exNavbarRight"
			  :aria-controls "exNavbarRight"
			  :aria-expanded "false"
			  :aria-label "Toggle system menu"
			  (str "&#9776;"))))
	(:div :class "container"
	      (:br)
	      
	      (:div :class "row"
		    (:div :class "collapse col-md-2 col-md-auto show hidden-print"
			  :id "exNavbarLeft"
			  (if (current-user)
			      (str (render 
				    (make-widget 'nav-bar 
						 :name "nav-left"
						 :vertical-p t
						 :menu-items (default-left-menu))))))
		    
		    (:div  :class "col"
			   (if body
			       (render body)
			       (render (content widget))))
		    
		    (:div :class "collapse col-md-2 hidden-print align-self-end" 
			  :id "exNavbarRight" :style "background-color:#FFFFFF"
			  (if (current-user)
			      (str (render 
				    (make-widget 'context-select)))))))
	
	
	(page-include-js)

	(dolist (script (scripts widget))
	  (when (page-bottom-p script)
	    (render script)))
	(page-defer-include-js)
	
	
	
	(:script
	 
	 (str (format nil
		      "
function ajax_call(func, callback, args, widget_args) {
    var uri = '~Aajax/' + encodeURIComponent(func);
    var post_parameters = '&pageid=' + pageid;
    var i;
    if (args.length > 0) {
        uri += '?';
        for (i = 0; i < args.length; ++i) {
            if (i > 0)
                uri += '&';
            uri += 'arg' + i + '=' + encodeURIComponent(args[i]);
        }
    }

    if (widget_args && widget_args.length > 0) {
        for (i = 0; i < widget_args.length; ++i) {
            var arg = widget_args[i]

            post_parameters += '&' + encodeURIComponent(arg[0]) + '='
                + encodeURIComponent(arg[1]);
        }
    }
    fetchURI(uri, callback, post_parameters);

}

" (site-url *sfx-system*))))
	(:script
	 (str (format nil
		      "
var pageid;

function reuse_page(id) {
    if (!pageid) {
        var response = $.ajax({url: '~Aajax/RID?i=' + id,
                             async: false // can't have the user doing anything before we have an id
                            }).responseText;
        if (response == \"false\") {
            window.location.replace(window.location.href)
        }
        pageid = id;
    }
}

function request_page(id) {
    if (!pageid) {
        var response = $.ajax({url: '~Aajax/NID?i=' + id,
                             async: false // can't have the user doing anything before we have an id
                            }).responseText;
        if (response == \"false\") {
            window.location.reload()
        }
        pageid = id;
    }
}" 
		      (site-url *sfx-system*)
		      (site-url *sfx-system*))))
	#|
	(:script "$(document).ready(function () {
  $('[data-toggle=\"offcanvas\"]').click(function () {
    $('.row-offcanvas').toggleClass('active')
  });
});")
	|#
	
	)
       (:script
	(let* ((reuse (parameter "pageid"))
	       (page *sfx-context*)
	       (id (context-id page)))
	  
	  (cond (reuse
		 (when (not (equal id reuse))
		   (bad-request))

		 (if (sb-ext:compare-and-swap (slot-value page 'reuse-p)           
					      nil
					      t)
		     (redirect (request-uri*))
		     (fmt "reuse_page(~s);" reuse)))
		((not (cl-sfx::current-user))
		 (fmt "pageid = ~s;" id))
		(t
		 (fmt "request_page(~s);" id))))
	(str (deferred-js)))
       ))))
;;;

(defclass icon (widget)
  ((icon-name :initarg :icon-name
              :initform nil
              :accessor icon-name)
   (width :initarg :width
          :initform nil
          :accessor width)
   (height :initarg :height
           :initform nil
           :accessor height)
   (alt :initarg :alt
        :initform nil
        :accessor alt)
   (title :initarg :title
          :initform nil
          :accessor title)))


(defmethod render ((icon icon) &key)
  (with-slots (icon-name alt title width height) icon
    (with-html-no-indent
      (if (alexandria:starts-with-subseq "glyphicon" icon-name)
          (htm
           (:span :alt alt
                  :title title
                  :class icon-name))
          (htm
           (:img :src (frmt "~Aweb/images/~a.png"
                            (site-url *sfx-system*) icon-name)
                 :alt alt
                 :title title
                 :width width
                 :height height))))))

(defun make-icon (name &key (size 16)
                            (title ""))
  (render (make-widget 'icon :name (frmt "~@{~a-~}" name size title)
                             :icon-name name
                             :width size
                             :height size
                             :alt title
                             :title title)))

