function updateCheckAll(element) {
    var checkboxList = element.parentNode.parentNode;

    if (element.className == "checkall") {
        var checkboxes = checkboxList.getElementsByTagName("input");

        for (var i = 0; i < checkboxes.length; i++) {
            var checkbox = checkboxes[i];

            if (checkbox.type == "checkbox")
	    {

                checkbox.checked = element.checked;
	    }
        }
    } else {
        var checkboxes = $(":checkbox", checkboxList);
        var checkAll = checkboxes[0];
        var allChecked = true;

        for (var i = 1; i < checkboxes.length; i++) {
            var checkbox = checkboxes[i];
            if (checkbox.type == "checkbox"
                && !checkbox.checked) {
                allChecked = false;
                break;
            }
        }
        checkAll.checked = allChecked;
    }
}

function setCharAt(str, index, chr) {
    return str.substr(0, index) + chr + str.substr(index + 1);
}


var currentCBL;

function restoreState(cbl, setup) {
    var checkboxes = $("#" + cbl + " :checkbox:not(.checkall)");
    var state = $("#" + currentCBL).val();
    if (setup) {
        $("#" + cbl + " .checkall").change(function(){
            $("#" + currentCBL).val($("#" + currentCBL).val()
                                    .replace(/./g,$(this).prop('checked') ? '1' : '0'));
        });
    }
    for (var i = 0; i < checkboxes.length; i++) {
        var cb = checkboxes[i];
        $(cb).prop('checked', state[i] == '1');
        if (setup) {
            (function(index) {
                $(cb).change(function() {
                    var val = $("#" + currentCBL).val();
                    val = setCharAt(val, index, $(this).prop('checked') ? '1' : '0');
                    $("#" + currentCBL).val(val);
                });
            })(i);
        }
    }        
}


function updateTable(table) {
    jQuery('#' + table).dataTable().fnDraw();
}

function scroll_to(id) {

   var element = $('#' + id);


/*
    if ($(element).parents('.modal').length) {
        // figure out how to do that
    } else {
        $('html, body').animate({scrollTop: element.offset().top -
                                         $('#page-content').offset().top - 5});
    }
*/
}

function select_add_row(table, name) {
    var field = $("#" + name);
    if (field.val()) {
        var table = $("#" + table).DataTable();
        table.row.add(['<input type="hidden" name="'+ name + '" value="'+ field.val()+'">'+field.val(),'']).draw();
        field.val("");
    }
}

function before_ajax(context)
{
/*
    $("textarea:not('.no-mce')", context).each(function () {
        tinymce.execCommand('mceRemoveEditor', false, $(this).attr("id"));
    });
*/
}

function applyPeach (context)
{
   /* $('.date', context).datepicker({format: 'dd M yyyy'});

    $("textarea:not('.no-mce')", context).each(function () {

        var id = $(this).attr("id");
        if (!id) {
            id = "textarea" + tmp_counter++;
            $(this).attr("id", id);
        }

        var plugins = [];
        if ($(this).hasClass('image'))
            plugins = ["image","code"];

        tinymce.init({
            selector: "#"+id,
            plugins: plugins,
            toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code",
            file_picker_callback: function(callback, value, meta) {
                $("body").remove("#invisible_div");
                $("body").append('<div id="invisible_div" style="display:none"><input id="mce_upload" type="file" name="image-upload"</div>');
                $('#mce_upload').fileupload({url: '/insite/ajax/IMAGE-UPLOAD',
                                             done:
                                             function(e, data) {
                                                 $("body").remove("#invisible_div");
                                                 callback(data.result);
                                             }});
                $('#mce_upload').click();
            }
        });
    });

*/}
applyPeach();


function fetchURI(uri, callback, parameters) {
    var request;
    if (window.XMLHttpRequest) { request = new XMLHttpRequest(); }
    else {
        try { request = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {
            try { request = new ActiveXObject("Microsoft.XMLHTTP"); } catch (ee) {
                request = null;
            }}}
    if (!request) alert("Browser couldn't make a request object.");

    request.open('POST', uri, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//alert(callback);
    request.onreadystatechange = function() {
        if (request.readyState != 4) return;
        if (request.status >= 200 && request.status < 300
            || request.status == 304) {
            if (callback)
                callback(request.responseText);
        }
        else {
            alert('Error while fetching URI ' + uri);
        }
    };
    request.send(parameters);

    delete request;
}


function cl_ajax_render (script_name, widget_id, args, callback) {
    ajax_call('CL-AJAX-RENDER', callback, [script_name, widget_id], args);
}

function find_widget(id) {
    var widget = document.getElementById(id);
    if (!widget) {
        alert("There's no widget with id " + id);
        return;
    }
    return widget;
}


function ajax_render (script_name, widget_id, args) {
    var widget = find_widget(widget_id);
/*    jQuery.fallr('show', {
        buttons     : {},
	content     : 'Loading data',
	icon        : 'info',
	useOverlay  : false
    });*/


    if (widget) {
        cl_ajax_render(script_name, widget_id, args,
                       function (response) {
//alert("fuck");
                           var json = jQuery.parseJSON(response);
//alert(json);
                           before_ajax(widget);
                           widget.innerHTML = json[0];
                          ;; applyPeach(widget);

                           if (json[1]) {
                               eval(json[1]);
                           }
                           //jQuery.fallr('hide');
                       }
                      );
    }
}

function get_values(widget, tag_name, disabled) {
    var elements = widget.getElementsByTagName(tag_name);
    var result = [];
    for (var i = 0; i < elements.length; i++) {
        element = elements[i];
        if (element.name && (disabled || !element.disabled))
        {
            if (tag_name == 'input')
            {
                if (element.type == 'checkbox')
                {
                    result.push([element.name, element.checked]);
                }
                else if (element.type == 'radio')
                {
                    if (element.checked)
                        result.push([element.name, element.value]);
                }
                else
                {
                    result.push([element.name, element.value]);
                }
            } else if (element.id && tag_name == 'textarea') {
                result.push([element.name, element.value]);
            }
            else
            {
                result.push([element.name, element.value]);
            }
        }
    }
    return result;
}


var scripts = [];

function save_scripts() {

   for (i = 0; i < scripts.length; i++) {
     scripts[i].save();
    }
}

function add_scripts(script) {
   scripts.push(script);
}

function get_form_values(form_id, disabled) {
    var widget = find_widget(form_id);
    var input_types = ['input', 'select', 'textarea'];

    if (widget) {
        //tinyMCE.triggerSave();
        save_scripts();
        var result = [];
        for (var i = 0; i < input_types.length; i++)
            result = result.concat(get_values(widget, input_types[i], disabled));
        return result;
    }
}
