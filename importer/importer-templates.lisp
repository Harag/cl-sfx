(in-package :cl-sfx)

(defclass importer-templates-grid (grid)
  ((rename-keys-checkbox :initarg :rename-keys-checkbox
                         :initform nil
                         :accessor rename-keys-checkbox))
;;  (:collection-name "import-templates-grid")
;;  (:collection-type :license)
  (:default-initargs :top-level t))

(defmethod get-rows ((grid importer-templates-grid))
  (setf (rows grid)
        (import-templates)))

(defun render-import-templates-tab (mapping-tab script-tab subst-tab)
  (let ((tab-box (make-widget 'tab-box
                              :name "import-tab-box"
                              :header "Template"
                              :icon "card--pencil")))
    (setf (tabs tab-box)
          (list
           (list
            "Mapping"
            (with-html-string
              (:div :class "section _100"
                    (str mapping-tab))))
           (list
            "Scripts"
            (with-html-string
              (:div :class "section _100"
                    (str script-tab))))
           (list
            "Substitutions"
            (with-html-string
              (:div :class "section _100"
                    (str subst-tab))))))
    (render tab-box)))

(defmethod render-row-editor ((grid importer-templates-grid) row)
  (render-import-templates-tab
   (with-html-string
     (render-import-mapping-form grid row))
   (with-html-string
     (add-import-script-grid grid row))
   (with-html-string
     (add-import-subst-grid grid row))))

(defmethod handle-action ((grid importer-templates-grid) (action (eql :save)))
  (let ((tempalte (editing-row grid)))
    (when (equal (parameter "form-id") "import-mapping-edit-form")
      (setf (mapping tempalte) nil)
      (dolist (parm (post-parameters*))
        (unless (find (car parm) (list "form-id" "action"
                                       "grid-name" "[object HTMLHtmlElement]")
                      :test 'equalp)
          (push (make-instance 'sfx-import-mapping
                               :import-slot (car parm)
                               :file-column (cdr parm))
                (mapping tempalte)))))
    (finish-editing grid)
    (persist tempalte)))
