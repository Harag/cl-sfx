(in-package :cl-sfx)

(defstruct (source-row
            (:constructor make-source-row (id values)))
  id
  values
  errors)

(defclass import-object ()
  ((import-rows :initarg :import-rows
                :initform nil
                :accessor import-rows)
   (script-results :initarg :script-results
                   :initform nil
                   :accessor script-results)
   (import-duplicate-p :initform nil
		       :accessor import-duplicate-p))
  (:metaclass data-class))

(defmethod doc-collection ((doc import-object))
  (get-col-lic "sfx-imports"))

(defmacro make-import-wrapper-class (class &optional slots)
  `(defclass ,(alexandria:symbolicate class '-import) (import-object ,class)
     ,(append
       slots
       (loop for (slot . options) in (cdr (assoc class *import-slot-descriptions*))
             when (getf options :key)
             collect (list (intern (frmt "~a-~a" 'import-old slot))
                           :initform nil)))
     (:metaclass wfx-versioned-class)))

(defun original-class (class)
  (assert (subtypep class 'import-object))
  (unless (c2mop:class-finalized-p class)
    (c2mop:finalize-inheritance class))
  (third (c2mop:class-precedence-list class)))

(defun allocate-instance-with-nil-slots (class)
  (let ((instance (allocate-instance class)))
    (loop for (loc) across (xdb2::all-slot-locations-and-initforms class)
          do (setf (standard-instance-access instance loc) nil))
    instance))

(defun import-switch-class-recursively (object)
  (typecase object
    (import-object
     (let* ((old-class (class-of object))
            (new-class (original-class old-class))
            (new (allocate-instance new-class)))
       (loop for slotd in (class-slots new-class)
             when (slot-boundp-using-class old-class object slotd)
             do (setf (slot-value-using-class new-class new slotd)
                      (import-switch-class-recursively
                       (slot-value-using-class old-class object slotd))))
       (setf (written new) nil
             (id new) nil)
       new))
    (string object)
    (null object)
    (list
     (mapcar #'import-switch-class-recursively object))
    ((vector t)
     (map 'vector #'import-switch-class-recursively object))
    (t
     object)))
