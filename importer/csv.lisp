(in-package :cl-sfx)
(defun skip-bom (stream)
  (cond ((not (eql (peek-char nil stream nil nil)
                   #\latin_small_letter_thorn))
         nil)
        ((and (read-char stream)
              (eql (peek-char nil stream nil nil)
                   #\latin_small_letter_y_with_diaeresis))
         (read-char stream)
         t)
        (t
         (file-position stream 0)
         nil)))

(defun base-string-p (string)
  (sb-kernel:with-array-data ((string string) (start) (end))
    (declare (ignore start end)
             (simple-string string))
    (loop for char across string
          always (< (char-code char)
                    sb-int:base-char-code-limit))))

(defun parse-csv-header (stream)
  (let* ((csv-parser:*field-separator* #\|)
         (parsed (or (csv-parser:read-csv-line stream)
                     (return-from parse-csv-header)))
         (length (length parsed))
         (result (make-array length))
         (empty-count 0))
    (loop for field in parsed
          for i from 0
          do
          (setf (aref result i)
                (cond ((or (equal field "")
                           (null field))
                       (incf empty-count)
                       nil)
                      ((base-string-p field)
                       (coerce field 'simple-base-string))
                      (t
                       (coerce field 'simple-string)))))
    (values result (- length empty-count))))

(defun parse-csv-line (stream header result-length)
  (let* ((csv-parser:*field-separator* #\|)
         (parsed (or (csv-parser:read-csv-line stream)
                     (return-from parse-csv-line)))
         (length (length parsed))
         (result (make-array result-length))
         (j 0))
    (when (/= length (length header))
      (error "Mismatching number of columns, ~
              the header has ~a, got ~a."
             (length header)
             length))
    (loop for field in parsed
          for i from 0
          when (svref header i)
          do
          (setf (aref result j)
                (cond ((or (equal field "")
                           (null field))
                       nil)
                      ((base-string-p field)
                       (coerce field 'simple-base-string))
                      (t
                       (coerce field 'simple-string))))
          (incf j))
    result))

(defun parse-csv (file)
  (flet ((fail (control-string &rest format-arguments)
           (return-from parse-csv
             (apply #'format nil control-string format-arguments))))
    (let ((line-number 1))
      (with-open-file (stream file
                              :external-format :latin-1)
        (skip-bom stream)
        (handler-case
            (multiple-value-bind (header non-empty-length) (parse-csv-header stream)
              (values (remove nil header)
                      (coerce
                       (cond ((< non-empty-length 2)
                              (fail "0: Malformed CSV header, at least 2 columns required."))
                             (t
                              (loop for line = (parse-csv-line stream header non-empty-length)
                                    unless line do (if (= line-number 1)
                                                       (fail "1: At least one row required.")
                                                       (loop-finish))
                                    when (find nil line :test-not #'eq)
                                    collect line
                                    do (incf line-number))))
                       'vector)))
          (error (c)
            (format nil "~a: ~a" line-number c)))))))

(defun check-csv-stream (stream)
  (let ((line-number 0))
    (handler-case
        (let* ((csv-parser:*field-separator* #\|)
               (header (csv-parser:read-csv-line stream))
               (length (length header)))
          (cond ((< length 2)
                 "0: Malformed CSV header, at least 2 columns required.")
                ((member nil header)
                 "0: The header can't contain empty columns.")
                (t
                 (loop
                  (incf line-number)
                  (multiple-value-bind (line line-length)
                      (csv-parser:read-csv-line stream)
                    (cond ((null line)
                           (return
                             (if (= line-number 1)
                                 "1: At least one row required."
                                 t)))
                          ((/= length line-length)
                           (return
                             (format nil
                                     "~a: Mismatching number of columns, ~
                                              the header has ~a, got ~a."
                                     line-number
                                     length
                                     line-length)))))))))
      (error (c)
        (format nil "~a: ~a" line-number c)))))
