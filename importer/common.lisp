(in-package :cl-sfx)



(defstruct import-slot
  name
  (position -1 :type fixnum)
  (parser #'identity :type function)
  default
  script
  key
  important
  valid-values
  csv-field
  printer
  replaces
  renames
  sum-values)

(defmethod print-object ((object import-slot) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (princ (import-slot-name object) stream)))

(defstruct (import-process
            (:constructor %make-import-process))
  (import nil :type new-sfx-import)
  class
  slots
  virtual-slots
  slots-in-order
  key-slots
  indirect-slots
  indirect-merges
  (cache-insert nil :type function)
  (cache-lookup nil :type function)
  normal-cache-insert
  normal-cache-lookup
  (merge-with-imported t)
  main-key
  scripts
  initform-initializer)




(defun get-slot (instance slot-name)
  (if (slot-boundp instance slot-name)
      (slot-value instance slot-name)))

#|
(defun find-slot-definition (slot-name object)
  (find slot-name (class-slots (class-of object))
        :key #'slot-definition-name
        :test #'string-equal))
|#

(defun find-direct-slot-definition (class slot-name)
  (labels ((find-slot (class)
             (or (find slot-name (class-direct-slots class)
                       :key #'slot-definition-name)
                 (some #'find-slot (class-direct-superclasses class)))))
    (find-slot class)))
#|
(defun find-slot (slot-name object)
  (let ((slot (find-slot-definition slot-name object)))
    (when slot
      (slot-definition-name slot))))


(defun find-slot-definition* (slot-name object start)
  (declare ((simple-array character (*)) slot-name)
           (fixnum start)
           (optimize speed))
  (let ((length1 (- (length slot-name) start)))
   (loop for slot in (class-slots (class-of object))
         for name = (the (or
                          (simple-array character (*))
                          (simple-array base-char (*)))
                         (string (the symbol (slot-definition-name slot))))
         for length2 = (length name)
         when (and (= length1 length2)
                   (loop for i below length2
                         for j from start
                         for char1 of-type base-char = (char slot-name j)
                         for char2 of-type base-char = (char name i)
                         always (or (char-equal char1 char2)
                                    (and (char= char1 #\_)
                                         (char= char2 #\-)))))
         return slot)))
|#

(defun update-slotx (instance slot value)
  (let* ((slot (if (typep slot 'slot-definition)
                   slot
                   (find-slot-definition slot instance)))
         (parser (and (typep slot 'xdb2:storable-slot)
                      (xdb2:slot-parser slot)))
         (value (if parser
                    (funcall parser value)
                    value)))
    (cond ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :list))
           (push value (slot-value-using-class (class-of instance) instance slot)))
          ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :number))
           (setf (slot-value-using-class (class-of instance) instance slot)
                 (if (stringp value)
                     (parse-number value)
                     value)))
          ((and (typep slot 'xdb2:storable-slot)
                (eq (xdb2:db-type slot) :integer))
           (setf (slot-value-using-class (class-of instance) instance slot)
                 (if (stringp value)
                     (parse-integer value)
                     value)))
          (t
           (setf (slot-value-using-class (class-of instance) instance slot)
                 value)))))

(defun prepare-slots (object)
  (when (typep object 'xdb2:storable-object)
    (loop for slot in (class-slots (class-of object))
          when (and (typep slot 'xdb2:storable-slot)
                    (eq (xdb2:db-type slot) :list))
          do (setf (slot-value-using-class (class-of object) object slot) nil))))



(defun need-trim (string char-bag)
  (declare (simple-string string))
  (flet ((whitespace-p (x)
           (and (member x char-bag) t)))
    (declare (inline whitespace-p))
    (let ((length (length string)))
      (and (plusp length)
           (or (whitespace-p (char string 0))
               (whitespace-p (char string (1- length))))))))

(defun need-whitespace-trim (string)
  (declare (simple-string string))
  (flet ((whitespace-p (x)
           (and (member x '(#\Space #\Newline #\Tab #\Return)) t)))
    (declare (inline whitespace-p))
    (let ((length (length string)))
      (and (plusp length)
           (or (whitespace-p (char string 0))
               (whitespace-p (char string (1- length)))
               (loop for previous = #\a then char
                     for char across string
                     thereis (char= char previous #\Space)))))))

(defun strip-white-space (string)
  "Trims white spaces and replaces multiple
spaces between words with a single space."
  (if (and (stringp string)
           (need-whitespace-trim string))
      (ppcre:regex-replace-all
       " +" (trim-whitespace string)
       " ")
      string))

(defun synq-edit-data (object)
  (let ((parameters (append (get-parameters *request*)
                            (post-parameters *request*))))
    (when (typep object 'standard-object)
      (prepare-slots object)
      (loop for (key . value) in parameters
            for slot = (find-slot key object)
            when slot
            do (update-slotx object slot (strip-white-space value))))))

(defun filter-pathname (string)
  (let ((string (nsubstitute-if #\-
                                (lambda (x)
                                  (member x '(#\Space #\/ #\\ #\Newline #\Tab #\Return)))
                                (nstring-downcase (copy-seq string)))))
    (if (need-trim string '(#\-))
        (string-trim '(#\-) string)
        string)))

(defun bogus-result-p (x)
  (and (member x '(nil "false" :null "" "n/a"
                   "NIL")
               :test #'equalp)
       t))

(defun pretty-field-name (name)
  (let ((pretty
          (substitute #\Space #\-
                      (string-capitalize name))))
    (if (alexandria:ends-with-subseq " P" pretty)
        (let ((sub (subseq pretty 0 (- (length pretty) 1))))
          (setf (elt sub (- (length pretty) 2)) #\?)
          sub)
        pretty)))


(defun transform-variable-name (variable)
  (ppcre:regex-replace-all " +" variable "-"))

(defun transform-variable-names (variables)
  (map 'list #'transform-variable-name variables))


(defun current-universal-date ()
  (multiple-value-bind (second minute hour date month year) (get-decoded-time)
    (declare (ignorable second minute hour))
    (encode-universal-time 0 0 0 date month year *time-zone*)))

(defun disable-controls (widget)
  (frmt
   "$(\"#~a :button, ~:*#~a select, ~:*#~a input\").attr(\"disabled\", \"disabled\")"
   (name widget)))






