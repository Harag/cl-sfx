(in-package :cl-sfx)

(defparameter *old-import-data-limit* 20)

(defclass column ()
  ((sfx-import :initarg :sfx-import
                 :initform nil
                 :accessor sfx-import)
   (slot :initarg :slot
         :initform nil
         :accessor slot)
   (old-slot :initarg :old-slot
             :initform nil
             :accessor old-slot) 
   (printer :initarg :printer
            :initform nil
            :storep nil)
   (column-map :initarg :column-map
               :initform nil
               :accessor column-map)
   (substs :initarg :substs
           :initform nil
           :accessor substs)
   (replacements :initarg :replacements
                 :initform nil
                 :accessor replacements))
  (:metaclass data-class))

(defclass real-column (column)
  ((matches :initarg :matches
            :initform nil
            :accessor matches))
  (:metaclass data-class))

(defclass virtual-column (column)
  ()
  (:metaclass data-class))

(defmethod printer ((column real-column))
  (or (slot-value column 'printer)
      (setf (slot-value column 'printer)
            (let ((i (position (column-map column)
                               (csv-fields (sfx-import column))
                               :test #'equal)))
              (assert i)
              (lambda (x)
                (svref x i))))))


(defun compile-virtual-column-map (map fields)
  (compile nil
           `(lambda (x)
              (declare (ignorable x))
              (with-output-to-string (str)
                ,@(loop for (key . value) in map
                        collect
                        (ecase key
                          (:text
                           `(princ ,value str))
                          (:field
                           `(princ (svref x ,(position value fields :test #'equal)) str))))))))

(defmethod printer ((column virtual-column))
  (or (slot-value column 'printer)
      (setf (slot-value column 'printer)
            (compile-virtual-column-map (column-map column)
                                        (csv-fields (sfx-import column))))))

(defclass sfx-import-mapping ()
  ((import-slot :initarg :import-slot
                :initform nil
                :accessor import-slot)
   (file-column :initarg :file-column
                :initform nil
                :accessor file-column)))

(defclass sfx-import-mapping-rename ()
  ((import-slot :initarg :import-slot
                :initform nil
                :accessor import-slot)
   (file-column :initarg :file-column
                :initform nil
                :accessor file-column)))

(defclass sfx-import-alt-key ()
  ((import-slot :initarg :import-slot
                :initform nil
                :accessor import-slot)))

(defmethod print-object ((object sfx-import-mapping) stream)
  (if (slot-boundp object 'import-slot)
      (print-unreadable-object (object stream :type t :identity t)
        (princ (import-slot object) stream))
      (call-next-method)))

(defclass saved-subst (doc)
  ((what :initarg :what
         :initform nil
         :accessor what)
   (with :initarg :with
         :initform nil
         :accessor with))
  (:metaclass data-class))

(defclass saved-replacement (saved-subst)
  ((what :initarg :row
         :initform nil
         :accessor row))
  (:metaclass data-class))

;;; This is only used in old imports
(defclass import-subst (doc)
  ((field :initarg :field
          :initform nil
          :accessor field)
   (what :initarg :what
         :initform nil
         :accessor what)
   (with :initarg :with
         :initform nil
         :accessor with))
  (:metaclass data-versioned-class))

(defclass import-script (doc)
  ((field :initarg :field
          :initform nil
          :accessor field)
   (script :initarg :script
           :initform nil
           :accessor script)
   (raw-script :initarg :raw-script
               :initform nil
               :accessor raw-script))
  (:metaclass data-versioned-class))

(defclass saved-script (import-script)
  ()
  (:metaclass data-versioned-class))

(defclass sfx-import (entity-doc)
  ((import-type :initarg :import-type
                :initform nil
                :accessor import-type
		:db-type string)
   (import-effective-date :initarg :import-effective-date
                          :initform nil
                          :accessor import-effective-date
			  :db-type date)
   (import-stamp-date :initarg :import-stamp-date
                      :initform nil
                      :accessor import-stamp-date
		      :db-type date) 
   (clear-old-versions-p :initarg :clear-old-versions-p
                         :initform nil
                         :accessor clear-old-versions-p
			 :db-type boolean)
   (original-file-name :initarg :original-file-name
                       :initform nil
                       :accessor original-file-name
		       :db-type string)
   (file-name :initarg :file-name
              :initform nil
              :accessor file-name
	      :db-type string)
   (description :initarg :description
                :initform nil
                :accessor description
		:db-type string)
   (status :initarg :status
           :initform nil
           :accessor status
	   :db-type string)
   (mapping :initarg :mapping
            :initform nil
            :accessor mapping
	    :db-type list
	    )
   (imported-docs :initarg :imported-docs
                  :initform nil
                  :accessor imported-docs
                  :last-only t
		  :db-type list
		  )
   (import-errors :initarg :import-errors
                  :initform nil
                  :accessor import-errors
                  :accessor errors
                  :last-only t
		  :db-type list
		  )
   (scripts :initarg :scripts
            :initform nil
            :accessor scripts
	    :db-type list
	    )
   (substs :initarg :substs
           :initform nil
           :accessor substs
	   :db-type list
	   )
   (csv-fields :initarg :csv-fields
               :initform nil
               :accessor csv-fields
	       :db-type list
	       )
   (template :initarg :template
             :initform nil
             :accessor template)
   (rename-key-slots :initarg :rename-key-slots
                     :initform nil
                     :accessor rename-key-slots)
   (import-process :initarg :import-process
                   :initform nil
                   :accessor import-process
                   :storep nil))
  (:metaclass data-versioned-class)
  (:collection-name "sfx-imports")
  (:collection-type :license)
  (:default-initargs :top-level t)
  )

(defclass new-sfx-import (entity-doc)
  ((import-type :initarg :import-type
                :initform nil
                :accessor import-type)
   (import-stamp-date :initarg :import-stamp-date
                      :initform nil
                      :accessor import-stamp-date) 
   (clear-old-versions-p :initarg :clear-old-versions-p
                         :initform nil
                         :accessor clear-old-versions-p)
   (original-file-name :initarg :original-file-name
                       :initform nil
                       :accessor original-file-name)
   (file-name :initarg :file-name
              :initform nil
              :accessor file-name)
   (description :initarg :description
                :initform nil
                :accessor description)
   (status :initarg :status
           :initform nil
           :accessor status)
   (imported-docs :initarg :imported-docs
                  :initform nil
                  :reader %imported-docs
                  :writer (setf imported-docs)
                  :last-only t
                  :storep nil)
   (errors :initarg :errors
           :initform nil
           :accessor errors)
   (scripts :initarg :scripts
            :initform nil
            :accessor scripts)
   (csv-fields :initarg :csv-fields
               :initform nil
               :accessor csv-fields)
   (csv-rows :initarg :csv-rows
             :initform nil
             :reader %csv-rows
             :writer (setf csv-rows)
             :last-only t
             :storep nil)
   (template :initarg :template
             :initform nil
             :accessor template)
   (rename-key-slots :initarg :rename-key-slots
                     :initform nil
                     :accessor rename-key-slots)
   (import-process :initarg :import-process
                   :initform nil
                   :accessor import-process
                   :storep nil)
   (columns :initarg :columns
            :initform nil
            :accessor columns)
   (matches :initarg :matches
            :initform nil
            :accessor matches)
   (alt-keys :initarg :alt-keys
             :initform nil
             :accessor alt-keys)
   (options :initarg :options
            :initform nil
            :accessor options)
   (use-saved-substs :initarg :use-saved-substs
                     :initform t
                     :accessor use-saved-substs)
   (lock :initarg :lock
         :initform nil
         :accessor lock
         :storep nil))
  (:metaclass data-versioned-class))

;;;

(defun clear-old-imports ()
  (let ((imports (sort (copy-seq (get-collection-docs 'sfx-import))
                       #'> :key #'stamp-date)))
    (loop with count = 0
          for import across imports
          if (and
              (eq (status import) :authorized)
              (%csv-rows import)
              (> (incf count) *old-import-data-limit*))
          do
          (setf (imported-docs import) nil
                (csv-rows import) nil))))

(defmethod imported-docs ((import new-sfx-import))
  (cond ((%csv-rows import)
         (%imported-docs import))
        (t
         (when (eq (status import) :authorized)
           (setf (stamp-date import)
                 (get-universal-time))
           (clear-old-imports))
         (do-import import)
         (%imported-docs import))))

(defgeneric csv-rows (import))

(defmethod csv-rows ((import new-sfx-import))
  (or (%csv-rows import)
      (setf (csv-rows import)
            (nth-value 1 (parse-csv (merge-pathnames (file-name import) *tmp-directory*))))))

;;; 
(defmethod print-object ((import sfx-import) stream)
  (print-unreadable-object (import stream :type t :identity t)
    (with-slots (import-type imported-docs status) import
      (format stream "~@[~a~] ~@[~s~] (~a doc~:p)"
              import-type
              status
              (length imported-docs)))))

(defmethod print-object ((import new-sfx-import) stream)
  (print-unreadable-object (import stream :type t :identity t)
    (with-slots (import-type imported-docs status) import
      (format stream "~@[~a~] ~@[~s~] (~a doc~:p)"
              import-type
              status
              (length imported-docs)))))
;;;

(defclass import-template (doc)
  ((name :initarg :name
         :initform nil
         :accessor name)
   (import-type :initarg :import-type
                :initform nil
                :accessor import-type)
   (mapping :initarg :mapping
            :initform nil
            :accessor mapping)
   (scripts :initarg :scripts
            :initform nil
            :accessor scripts)
   (substs :initarg :substs
           :initform nil
           :accessor substs)
   (csv-fields :initarg :csv-fields
               :initform nil
               :accessor csv-fields)
   (rename-key-slots :initarg :rename-key-slots
                     :initform nil
                     :accessor rename-key-slots))
  (:metaclass data-versioned-class))

(defun filter-importer-status-active (doc)
  (or (eq (status doc) :uploaded)
      (eq (status doc) :imported)
      (eq (status doc) :importing)
      (eq (status doc) :review)
      (eq (status doc) :error)))

(defun filter-importer-status-authorized (doc)
  (or (eq (status doc) :authorized)
      (eq (status doc) :archived)))

(defun filter-importer-status-rejected (doc)
  (eq (status doc) :rejected))

;;;

(defun sfx-imports-collection ()
  (get-col-lic "sfx-imports"))

(defun sfx-imports ()
  (get-collection-docs 'sfx-import))

(defmethod doc-collection ((doc new-sfx-import))
  (sfx-imports-collection))

(defmethod doc-collection ((doc sfx-import))
  (get-col-lic "sfx-imports"))

(defun import-templates-collection ()
  (get-collection (system-db) "import-templates"))

(defun import-templates ()
  (docs (import-templates-collection)))

(defmethod doc-collection ((doc import-template))
  (import-templates-collection))

;;;

(defclass import-data ()
  ((import-type :initarg :import-type
                :initform nil
                :accessor import-type
		:db-type string
		)
   (slot-map :initarg :slot-map
             :initform (make-hash-table :test #'equalp)
             :accessor slot-map
	     
	     )
   (substs :initarg :substs
           :initform (make-hash-table :test #'equalp)
           :accessor substs)
   (scripts :initarg :scripts
            :initform nil
            :accessor scripts)
   (options :initarg :options
            :initform nil
            :accessor options))
  (:metaclass data-class)
  (:collection-name "import-data")
  (:collection-type :license)
  (:default-initargs :top-level t))

(defun import-data-collection ()
  (get-col-lic "import-data"))

(defun import-data ()
  (docs (import-data-collection)))

(defmethod doc-collection ((doc import-data))
  (import-data-collection))

(defun find-import-data (type)
  (find type (import-data) :test #'equal :key #'import-type))

(defun ensure-import-data (type)
  (or (find-import-data type)
      (make-instance 'import-data :import-type type :top-level t)))
