(in-package :cl-sfx)

(defvar *import-lock* (sb-thread:make-mutex))
(defvar *current-import-name* nil)
;;; There needs to be two caches when using alternative key slots, one
;;; to look up the object to merge, using the alternative slots, and
;;; one using normal key slots to check that it doesn't collide with
;;; any other objects.
(defvar *merge-cache*)
(defvar *normal-merge-cache*)
(defvar *imported-cache*)
(defvar *deduplicate-cache*)

(defclass upload-file (widget)
  ((grid :initarg :grid
         :initform nil
         :accessor grid)))

(defmethod render ((widget upload-file) &key)
  (let* ((grid (grid widget))
        (box (make-widget 'box))
        (disable (disable-grid-buttons grid)))
    (setf (content box)
          (if (context)
              (with-html-string
                (:form :action ""
                       :method "post"
                       :enctype "multipart/form-data"
                       (:input :type "hidden" :name "form-id" :value "upload-file-form")
                       (:input :type "hidden" :name "pageid" 
			       :value (context-id *sfx-context*))
                       (:div "The file should use <b>|</b> (vertical bar) as the column separator.")
                       (:label :for "file"
                               :style "display: inline-block;"
                               "Select file")
                       (:input :type "file" :name "file" :id "file"
                               :style "display: inline-block;")
                       (:div :class "btn-toolbar panel-footer"
                             (:button :class "btn-primary btn"
                                      :type "submit"
                                      :onclick (when disable
                                                 (frmt "{~a; this.form.submit();}" disable))
                                      "Upload")
                             (:button :class "btn-default btn"
                                      :onclick
                                      (frmt
                                       "{~@[~a;~]~a;}"
                                       disable
                                       (js-render-return-false (editor grid)
                                                               (js-pair "grid-name" (name grid))
                                                               (js-pair "action" "cancel")))
                                      "Cancel"))))
              (with-html-string
                "No context is selected"
                (:div :class "btn-toolbar panel-footer"
                      (:button :class "btn-default btn"
                               :onclick
                               (frmt
                                "{~@[~a;~]~a;}"
                                disable
                                (js-render-return-false (editor grid)
                                                        (js-pair "grid-name" (name grid))
                                                        (js-pair "action" "cancel")))
                               "Cancel")))))
    (render box)))

(defun remove-duplicate-matches (matches)
  (loop for (header match) in matches
      
        for ((first-match . distance)) = match
        for duplicate =
        (and 
	 match
	 (plusp distance)
             (loop for (sub-header sub-match) in matches
                   for ((sub-first-match . sub-distance)) = sub-match
                   thereis (and (not (equalp header sub-header))
                                (equal first-match sub-first-match)
                                (< sub-distance distance))))
        collect (list header
                      (if duplicate
                          (cons nil match)
                          match))))

(defun levenshtein-distance (string1 string2)
  (let ((length1 (length string1))
	(length2 (length string2)))
    (cond ((equalp string1 string2) 0)
          ((zerop length1) length2)
	  ((zerop length2) length1)
          (t
           (let* ((v-length (1+ length2))
                  (v0 (make-array v-length :element-type 'fixnum))
                  (v1 (make-array v-length :element-type 'fixnum)))
             (loop for i below v-length
                   do (setf (aref v0 i) i))
             (loop for i below length1
                   for c1 = (char string1 i)
                   do
                   (setf (aref v1 0) (1+ i))
                   (loop for j below length2
                         for cost = (if (char-equal c1 (char string2 j))
                                        0 1)
                         do (setf (aref v1 (1+ j))
                                  (min (1+ (aref v1 j))
                                       (1+ (aref v0 (1+ j)))
                                       (+ (aref v0 j) cost))))
                   (replace v0 v1))
             (aref v1 length2))))))

(defun match-import (header fields)
  (let ((sum 0))
    (list
     (remove-duplicate-matches
      (loop for column across header
            collect
            (list column
                  (sort (loop for field in fields
                              for distance =
                              (levenshtein-distance column (substitute #\Space #\-
                                                                       (string field)))
                              minimize distance into min
                              collect (cons field distance)
                              finally
                              (incf sum min))
                        #'< :key #'cdr))))
     sum)))

(defun determine-import-type (header)
  (sort (loop for (import-type import-class) in *import-classes*
              for fields = (import-slot-names import-type import-class)
              collect (list* import-type (match-import header fields)))
        #'< :key #'third))

(defun remove-nil-string (x)
  (and (not (equalp x "NIL"))
       x))

(defun get-entity-by-id (id)
  (find-docx 'entity
	     :test (lambda (doc)
		     (equal id
			    (id doc)))))


(defun make-sfx-import (original-file-name file-name
                          header rows &key (class 'new-sfx-import)
                                           entity
                                           type)
 
  (let* ((all-matches (determine-import-type header))
         (allowed-matches (typecase type
                            ((eql :all)
                             all-matches)
                            (string
                             (list (assoc type all-matches :test #'equal)))
                            (t
                             (or (remove-if-not #'check-page-permission
                                                all-matches :key #'car)
                                 (return-from make-sfx-import
                                   "No import types in permissions.")))))
         (match (cadar allowed-matches))
         (type (caar allowed-matches))
         (import (make-instance class
                                :top-level t
                                :matches all-matches
                                :import-type type
                                :original-file-name original-file-name
                                :file-name file-name
                                :status :uploaded
                                :csv-fields header
                                :csv-rows rows
                                :entity
                                (or entity
                                    (let ((entity-id (car (context))))
                                      (and entity-id
                                           (get-entity-by-id entity-id))))
                                :import-stamp-date (get-universal-time)))
         (data (find-import-data type)))
  
    (setf (columns import)
          (loop for field across header
                for matches = (break "~A ~A" field match)
                for i from 0
                collect (make-instance 'real-column
                                       :column-map field
                                       :matches matches
                                       :sfx-import import
                                       :slot (remove-nil-string
                                              (and field
                                                   (caar (or (and data
                                                                  (gethash field (slot-map data)))
                                                             matches)))))))
    (when data
      (find-all-saved-options data import))
    import))

(defun make-import-filename (entity)
  
  (make-pathname :name (format nil "unprocessed-~A-~A"
                               (random 99999) (get-universal-time))
                 :directory `(:relative ,(filter-pathname
                                          (entity-name entity))) 
                 :type "csv"))

;;TODO: Implement
(defun get-root-entity (id)
  (declare (ignore id))
  )

(defun move-upload (file new-name)
  
  (let ((new-path (merge-pathnames new-name *tmp-directory*)))
   
    (ensure-directories-exist new-path)
    ;;(break "~A~%~A~%~A" file new-name new-path)
    
    (nth-value 2 (rename-file file new-name))
    ))

(defun handle-import-upload (parameters)
  (destructuring-bind (path name application-type) parameters
    (declare (ignore application-type))
    (multiple-value-bind (header rows) (parse-csv path)
      (if (stringp header)
          header
          (let* (
                 (entity (car (context)));;TODO: use root instead
                 (new-name (make-import-filename entity))
                 (new-path (move-upload path new-name)))
	    
            (when (probe-file new-path)
	      
              (let ((x (make-sfx-import name new-name
                                          header rows)))
		
                (if (stringp x)
                    x
                    (persist x)))))))))



;;TODO: CSV dispatcher needs to be called when *sfx-system* is available
(defun csv-dispatcher ()
  #|
  (check-permission-or-error `(frmt "~Aimporter" (site-url *sfx-system*)) "drill-down")
  (let ((uri (request-uri*)))
    (cond ((and
            (alexandria:starts-with-subseq `(frmt "~Aimports/" (site-url *sfx-system*)) uri)
            (let* ((position (length `(frmt "~Aimports/" (site-url *sfx-system*))))
                   (import (loop for import across (docs (get-collection (system-db) "sfx-imports"))
                                 when (and (string= (namestring (file-name import))
                                                    uri
                                                    :start2 position)
                                           (check-entity-access (entity import)))
                                 return import)))
              (and import
                   (handle-static-file (merge-pathnames (file-name import) *tmp-directory*))))))
          (t
           (setf (return-code*) +http-not-found+)
           (abort-request-handler))))
  |#
  )

;;TODO: messes up *dispatch-table*
(defvar *csv-dispatcher*
  #|
  (car (push
        (create-prefix-dispatcher `(frmt "~Aimports" (site-url *sfx-system*)) #'csv-dispatcher)
        *dispatch-table*))
  |#
  )


(defmethod action-handler ((widget upload-file))
  (when (and (equal (parameter "form-id") "upload-file-form")
             (parameter "file"))
    (let ((grid (grid widget))
          (result (handle-import-upload (parameter "file"))))
      (if (stringp result)
          (setf (error-message grid) result)
          (setf (editing-row grid) result)))
    ;; Redirect to prevent repeat upload on refresh.
    ;; (redirect (request-uri*))
    ))

(defparameter *import-edit-button*
  (copy-object *edit-button*
               :test (lambda (grid row)
                         (declare (ignore grid))
                       (not (typep row 'bad-ftp-import)))))

(defparameter *view-error-button*
  (make-instance 'grid-button
                 :action "edit"
                 :icon "exclamation-red"
                 :hint "View error"
                 :test (lambda (grid x)
                         (declare (ignore grid))
                         (typep x 'bad-ftp-import))))

(defparameter *review-button*
  (make-instance 'grid-button
                 :icon "binocular"
                 :hint "Review"
                 :permission "Review"
                 :test (lambda (grid row)
                         (declare (ignore grid))
                         (not (typep row 'bad-ftp-import)))
                 :url (lambda (grid row row-id)
                        (let ((url (assoc-value (import-type row) *import-review-urls*)))
                          (when url
                            (frmt "~a?filter=import-review&p=~a&w=~a&i=~a"
                                  url
                                  (context-id *sfx-context*)
                                  (name grid)
                                  row-id))))))

(defparameter *authorize-button*
  (make-instance 'grid-button
                 :action "authorize"
                 :icon "tick"
                 :hint "Authorize"
                 :permission "Authorize"
                 :test (lambda (grid row)
                         (declare (ignore grid))
                         (eq (status row) :imported))))

(defparameter *errors-button*
  (make-instance 'grid-button
                 :action "errors"
                 :icon "exclamation"
                 :hint "View errors"
                 :test (lambda (grid x)
                         (declare (ignore grid))
                         (errors x))))

(defparameter *reject-button*
  (make-instance 'grid-button
                 :action "reject"
                 :icon "cross"
                 :hint "Reject import"
                 :permission "Reject"
                 :test (lambda (grid row)
                         (declare (ignore grid))
                         (not (or (typep row 'bad-ftp-import)
                                  (eq (status row) :authorized)
                                  (eq (status row) :rejected)
                                  (eq (status row) :archived))))))

(defclass importer-grid (grid)
  ((selected-row :initarg :selected-row
                 :initform nil
                 :accessor selected-row)
   (rename-keys-checkbox :initarg :rename-keys-checkbox
                         :initform nil
                         :accessor rename-keys-checkbox))
  (:default-initargs :buttons (list *import-edit-button* *view-error-button* *delete-button*)
                     :additional-buttons
                     (list *review-button* *authorize-button*
                           *reject-button* *errors-button*)
                     :row-object-class 'new-sfx-import))

(defmethod handle-action ((grid importer-grid) (action (eql :authorize)))
  (check-page-permission-or-error "Authorize")
  (setf (selected-row grid) (editing-row grid)))

(defmethod handle-action ((grid importer-grid) (action (eql :errors)))
  (setf (selected-row grid) (editing-row grid)))

(defmethod handle-action ((grid importer-grid) (action (eql :reject)))
  (check-page-permission-or-error "Reject")
  (setf (selected-row grid) (editing-row grid)))

(defmethod handle-action ((grid importer-grid) (action (eql :template)))
  (check-page-permission-or-error "Update")
  (setf (selected-row grid) (editing-row grid)))

(defmethod list-grid-filters ((grid importer-grid))
  '(review
    all-imports
    authorized-imports
    rejected-imports
    with-audit-data))

(defmethod get-rows ((grid importer-grid))
  (let ((filter (grid-filter grid))
        (permissions (loop for (import-type) in *import-classes*
                           when (check-page-permission import-type)
                           collect import-type)))
    (setf (rows grid)
          (case filter
            (with-audit-data
                (loop for doc across (get-collection-docs 'sfx-import)
                      when (match-entity doc)
                      collect doc
                      and
                      append (old-versions doc)))
            (t
	    ;; (break "~A" (system-db))
             (find-docx 
	      'sfx-import
	      :test (lambda (doc)
		      (and 
		       (match-entity doc)
		       (or (super-user-p (current-user))
			   (not (import-type doc))
			   (member (import-type doc) permissions :test #'equal))
		       (case filter
			 (authorized-imports
			  (filter-importer-status-authorized doc))
			 (rejected-imports
			  (filter-importer-status-rejected doc))
			 (all-imports
			  t)
			 (review
			  (or (eq (status doc) :review)
			      (eq (status doc) :error)))
			 (t
			  (filter-importer-status-active doc)))))))))))

(defun get-status-date (import status)
  (let ((found (or (find status (old-versions import) :key #'status
                         :from-end t)
                   (and (eql (status import) status)
                        import))))
    (and found
         (format-universal-date-time (stamp-date found)))))


(defun import-in-progess-p ()
  (not (sb-thread:with-mutex (*import-lock* :wait-p nil)
         t)))

(defun render-import-form (grid row)
  (let* ((box (make-widget 'box))
         (in-progress-p (import-in-progess-p)))
    (setf (content box)
          (with-html-string
            (:form :action ""
                   :id "permissions-form"
                   :method "post"
                   :onsubmit "return false;"
                   (cond (in-progress-p
                          (fmt "~a is in progress"
                               *current-import-name*))
                         ((mapping row)
                          (htm (:div "Import data?")))
                         (t
                          (htm (:div :style "background-color: #ffbfbf;"
                                     "No mapping, cannot import."))))
                   (let ((disable (disable-grid-buttons grid)))
                     (htm
                      (:div :class "btn-toolbar panel-footer"
                            (:button :class "btn-primary btn"
                                     :type "submit"
                                     :onclick
                                     (frmt
                                      "{~@[~a;~]~a;}"
                                      disable
                                      (js-render (editor grid)
                                                 (js-pair "grid-name" (name grid))
                                                 (js-pair "action" "do-import")))
                                     "Import")
                            (:button :class "btn-default btn"
                                     :onclick
                                     (frmt
                                      "{~@[~a;~]~a;}"
                                      disable
                                      (js-render-return-false (editor grid)
                                                              (js-pair "grid-name" (name grid))
                                                              (js-pair "action" "cancel")))
                                     "Cancel")))))))
    (render box)))

(defun render-authorize-form (grid import)
  (let* ((box (make-widget 'box))
         (in-progress-p (import-in-progess-p))
         (changed (and (not in-progress-p)
                       (let* ((import-process (ensure-import-process import))
                              (*merge-cache* (make-merge-cache import-process))
                              (*normal-merge-cache* (make-normal-merge-cache import-process)))
                         (import-check-changes import)))))
    (setf (content box)
          (with-html-string
            (:form
             :action ""
             :id "permissions-form"
             :method "post"
             :onsubmit "return false;"
             (:div
              (cond (in-progress-p
                     (fmt "~a is in progress"
                          *current-import-name*))
                    (changed
                     (fmt "The data was changed after import.~@
                                 Can't authorize")
                     (setf (status import) :uploaded)
                     (persist import :set-time nil)
                     (update-table grid))
                    (t
                     (str "Authorize imported data?"))))
             (let ((disable (disable-grid-buttons grid)))
               (htm
                (:div :class "btn-toolbar panel-footer"
                      (unless (or changed
                                  in-progress-p)
                        (htm
                         (:button :class "btn-primary btn"
                                  :type "submit"
                                  :onclick
                                  (frmt
                                   "{~@[~a;~]~a;}"
                                   disable
                                   (js-render (editor grid)
                                              (js-pair "grid-name" (name grid))
                                              (js-pair "action" "do-aquthorize")))
                                  "Authorize")))
                      (:button :class "btn-default btn"
                               :onclick
                               (frmt
                                "{~@[~a;~]~a;}"
                                disable
                                (js-render-return-false (editor grid)
                                                        (js-pair "grid-name" (name grid))
                                                        (js-pair "action" "cancel")))
                               "Cancel")))))))
    (render box)))

(defun render-reject-form (grid)
  (let* ((box (make-widget 'box)))
    (setf (content box)
          (with-html-string
            (:form :action ""
                   :id "permissions-form"
                   :method "post"
                   :onsubmit "return false;"
                   (:div
                    (fmt "Reject the import? No further actions can be~@
                          taken on the import after rejection."))
                   (let ((disable (disable-grid-buttons grid)))
                     (htm
                      (:div :class "btn-toolbar panel-footer"
                            (:button :class "btn-primary btn"
                                     :type "submit"
                                     :onclick
                                     (frmt
                                      "{~@[~a;~]~a;}"
                                      disable
                                      (js-render (editor grid)
                                                 (js-pair "grid-name" (name grid))
                                                 (js-pair "action" "do-reject")))
                                     "Reject")
                            (:button :class "btn-default btn"
                                     :onclick
                                     (frmt
                                      "{~@[~a;~]~a;}"
                                      disable
                                      (js-render-return-false (editor grid)
                                                              (js-pair "grid-name" (name grid))
                                                              (js-pair "action" "cancel")))
                                     "Cancel")))))))
    (render box)))

(defun render-error-message (import grid)
  (let* ((box (make-widget 'box)))
    (setf (content box)
          (with-html-string
            (:form :action ""
                   :id "permissions-form"
                   :method "post"
                   :onsubmit "return false;"
                   (:div :class "alert alert-danger"
                         (esc (error-message import)))
                   (let ((disable (disable-grid-buttons grid)))
                     (htm
                      (:div :class "btn-toolbar panel-footer"
                            (:button :class "btn-default btn"
                                     :onclick
                                     (frmt
                                      "{~@[~a;~]~a;}"
                                      disable
                                      (js-render-return-false (editor grid)
                                                              (js-pair "grid-name" (name grid))
                                                              (js-pair "action" "cancel")))
                                     "Ok")))))))
    (render box)))

(defun render-template-edit-form (grid row)
  (let ((form (make-widget 'form :name "importer-template-form"
                                       :grid-size 12
                                       :header "Templates"
                                       :form-id "import-template-form"
                                       :save-button nil))
        (form-section (make-widget 'form-section
                                   :name "form-section"))
        (disable (disable-grid-buttons grid)))
    (render form
            :grid grid
            :content
            (with-html-string
              (render form-section
                      :label "Save as a template"
                      :input
                      (with-html-string
                        (render-edit-field "name"
                                           "")
                        (:button :class "btn-primary btn"
                                 :onclick
                                 (frmt
                                  "{~@[~a;~]~a;}"
                                  disable
                                  (js-render-form-values (editor grid)
                                                         (form-id form)
                                                         (js-pair "grid-name" (name grid))
                                                         (js-pair "action" "save-template")))
                                 "Save")))
              (render form-section
                      :label "Apply template"
                      :input
                      (with-html-string
                        (render-edit-field "template-name"
                                           nil
                                           :type :select
                                           :data (loop for template across (import-templates)
                                                       when (equalp (import-type template)
                                                                    (import-type row))
                                                       collect (name template)))
                        (:button :class "btn-primary btn"
                                 :onclick
                                 (frmt
                                  "{~@[~a;~]~a;}"
                                  disable
                                  (js-render-form-values (editor grid)
                                                         (form-id form)
                                                         (js-pair "grid-name" (name grid))
                                                         (js-pair "action" "apply-template")))
                                 "Apply")))))))

(defun render-import-tab (parent import-tab mapping-tab
                          script-tab subst-tab)
  (let ((tab-box (make-widget 'tab-box
                              :name "import-tab-box"
                              :header "Import"
                              :icon "card--pencil")))
    (when (typep parent 'grid)
     (setf (body-content tab-box)
           (with-html-string
             (let* ((grid parent)
                    (disable (disable-grid-buttons grid)))
               (htm
                (:div :class "btn-toolbar panel-footer"
                      (htm
                       (:button :class "btn-submit btn"
                                :type "submit"
                                :onclick
                                (frmt
                                 "{~@[~a;~]~a;}"
                                 disable
                                 (js-render (editor grid)
                                            (js-pair "grid-name" (name grid))
                                            (js-pair "action" "do-authorize")))
                                "Authorize"))
                      (:button :class "btn-default btn"
                               :onclick
                               (frmt
                                "{~@[~a;~]~a;}"
                                disable
                                (js-render-return-false (editor grid)
                                                        (js-pair "grid-name" (name grid))
                                                        (js-pair "action" "cancel")))
                               "Cancel")))))))
    (setf (tabs tab-box)
          (list
           (list
            "Import"
            (with-html-string
              (:div :class "section _100"
                    (str import-tab))))
           (list
            "Mapping"
            (with-html-string
              (:div :class "section _100"
                    (str mapping-tab))))
           (list
            "Scripts"
            (with-html-string
              (:div :class "section _100"
                    (str script-tab))))
           (list
            "Substitutions"
            (with-html-string
              (:div :class "section _100"
                    (str subst-tab))))))
    (render tab-box)))

(defun find-mapping (slot import
                     &key (type 'real-column))
  (loop for column in (columns import)
        when (and (typep column type)
                  (string= (slot column) slot))
        return (column-map column)))

(defun find-probable-match (field-name description headers)
  ;; Could do a match based on the Levenshtein distance as well
  (let ((symbol-name (and (symbolp field-name)
                          (substitute #\Space #\-  (symbol-name field-name)))))
    (loop for header in headers
          when (or (string-equal field-name header)
                   (string-equal symbol-name header)
                   (string-equal description header))
          return header)))

(defclass import-mapping-form (ajax-widget)
  ((parent :initarg :parent
           :initform nil
           :accessor parent)
   (current-import :initarg :current-import
                   :initform nil
                   :accessor current-import)))




(defmethod render ((widget import-mapping-form) &key)
  (let* ((parent (parent widget))
         (import (current-import widget))
         (headers (csv-fields import))
         (form-section (make-widget 'form-section
                                    :name "form-section"))
         (checkbox (make-widget 'checkbox
                                :name "alt-mapping"
                                :description "Rename key fields"
                                :on-click "$(\".rename-slot\").toggle(this.checked)"))
         ;; (slot-description (cdr (assoc (import-class import)
         ;;                               *import-slot-descriptions*)))
         (allsorts (find-allsorts (frmt "Import Type - ~A" (import-type import)))))
    (setf (rename-keys-checkbox parent) checkbox)
    (setf (value checkbox) (rename-key-slots import))
    (with-html
      (:div :class "form-horizontal"
            (render form-section :label (render-to-string checkbox))
            (loop for allsort across allsorts
                  for slot = (sort-value allsort)
                  for description = (description allsort)
                  do
                  (render form-section
                          :label (if (symbolp description)
                                     (pretty-field-name description)
                                     description)
                          :input
                          (with-html-string
                            (render-select (frmt "map-~a" slot)
                                           headers
                                           (find-mapping slot
                                                         import)
                                           :first-value ""
                                           :blank-allowed t
                                           :allow-deselect t)
                            ;; (cond ((not (check-page-permission "Rename Keys")))
                            ;;       ((key-slot-p slot)
                            ;;        (htm
                            ;;         (:div :class "rename-slot"
                            ;;               (:label "Old Value")
                            ;;               (render-select (frmt "old-~a" slot)
                            ;;                              headers
                            ;;                              (match-mapping import
                            ;;                                             slot
                            ;;                                             description
                            ;;                                             :type 'sfx-import-mapping-rename)
                            ;;                              :first-value ""
                            ;;                              :blank-allowed t
                            ;;                              :allow-deselect t))))
                            ;;       ((let ((key (alternative-key-p slot)))
                            ;;          (when key
                            ;;            (let ((checked (%find-mapping import slot
                            ;;                                          'sfx-import-alt-key)))
                            ;;              (htm
                            ;;               (:div :class "rename-slot"
                            ;;                     (render
                            ;;                      (make-instance 'checkbox
                            ;;                                     :name
                            ;;                                     (frmt "alt-key-~a" slot)
                            ;;                                     :translate-name nil
                            ;;                                     :description
                            ;;                                     (frmt "Use instead of \"~a\""
                            ;;                                           (description (find key allsorts :key #'sort-value)))
                            ;;                                     :on-click
                            ;;                                     (frmt "$(\"#old-~a\").attr(\"disabled\", this.checked)~
                            ;;                                           .trigger(\"liszt:updated\")"
                            ;;                                           key)
                            ;;                                     :value checked))))
                            ;;              (when checked
                            ;;                (defer-js
                            ;;                    (frmt "$(\"#old-~a\").attr(\"disabled\", true)~
                            ;;                           .trigger(\"liszt:updated\")"
                            ;;                          key))))))))
                            )))))
    ;; Initially hidden select get the wrong width when made visible.
    ;; Hide them after displaying instead
    (unless (rename-key-slots import)
      (defer-js "$(\".rename-slot\").hide()"))))

(defun render-import-mapping-form (parent import)
  (let ((widget (make-widget 'import-mapping-form)))
    (setf (parent  widget) parent
          (current-import widget) import)
    (render widget)))



(defun render-import-with-tabs (parent import)
  (render-import-tab
   parent
   (with-html-string
     (render-import-edit-form import parent parent))
   (with-html-string
     (render-import-mapping-form parent import))
   (with-html-string
     (add-import-script-grid nil import :box nil))
   (with-html-string
     (add-import-subst-grid nil import :box nil)))
  (defer-js
      (frmt
       "$('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) {
var anchor = e.target.href.substring(e.target.href.indexOf('#') + 1);
if (anchor == 'import-tab-box-tab-0') {
~a
}
if (anchor == 'import-tab-box-tab-1') {
~a
} else if (anchor == 'import-tab-box-tab-2') {
 updateTable('import-script-grid-table');
} else if (anchor == 'import-tab-box-tab-3') {
 updateTable('import-subst-grid-table');
}
})"
       (js-render (if (typep parent 'grid)
                      (editor parent)
                      parent)
                  (js-pair "no-scroll" "t"))
       (js-render "import-mapping-form"))))

(defmethod render-row-editor ((grid importer-grid) import)
  (assert (typep import 'new-sfx-import))
  (let ((action (action grid)))
    (cond ((null (id import))
           (render (make-widget 'upload-file :grid grid)))
          ((equal action :authorize)
           (render-authorize-form grid import))
          ((equal action :reject)
           (render-reject-form grid)
           (finish-editing grid))
          ((equal action :errors)
           (render-errors-form grid))
          ((equal action :template)
           (render-template-edit-form grid import))
          ((typep import 'bad-ftp-import)
           (render-error-message import grid))
          (t
           (render-import-with-tabs grid import)))))

(defun check-duplicate-import (row)
  (let* ((import-type (parameter "import-type"))
         (entity (get-entity-by-id (ensure-parse-integer (parameter "entity"))))
         (duplicate (and import-type
                         (find-if (lambda (x)
                                    (and (typep x 'sfx-import)
                                         (eq entity (entity x))
                                         (equalp import-type
                                                 (import-type x))))
                                  (get-collection-docs 'sfx-import)))))
    (when (and duplicate
               (not (eq row duplicate))
               (not (or (eq (status duplicate) :authorized)
                        (eq (status duplicate) :rejected))))
      (grid-error "Not yet authorized import of type \"~a\" for \"~a\" already exist."
                  import-type
                  (and entity
                       (entity-name entity))))))

(defun check-duplicate-template (name)
  (cond ((zerop (length name))
         (grid-error "Name can't be empty."))
        ((find name (import-templates)
               :key #'name
               :test #'equalp)
         (grid-error "Template with the name \"~a\" already exists." name))))

(defmethod handle-action ((grid importer-grid) (action (eql :save-template)))
  (let ((import (editing-row grid))
        (name (parameter "name")))
    (when (eq (status import) :rejected)
      (grid-error "Can't edit rejected import."))
    (check-duplicate-template name)
    (let ((new-template (make-instance 'import-template
                                       :name name
                                       :import-type (import-type import)
                                       :mapping (mapping import)
                                       :substs (substs import)
                                       :scripts (scripts import)
                                       :csv-fields (csv-fields import))))
      (setf (template import) name)
      (persist new-template)
      (persist import)
      (finish-editing grid))))

(defmethod handle-action ((grid importer-grid) (action (eql :apply-template)))
  (let* ((name (parameter "template-name"))
         (template (or (find name (import-templates)
                             :key #'name
                             :test #'equalp)
                       (grid-error "Template with the name \"~a\" not found" name)))
         (import (editing-row grid)))
    (when (eq (status import) :rejected)
      (grid-error "Can't edit rejected import."))
    (setf (mapping import) (mapping template)
          (substs import) (substs template)
          (scripts import) (scripts template)
          (template import) name)
    (persist import)
    (finish-editing grid)))

(defun apply-all-substs (slots-and-values row import)
  (loop for column in (columns import)
        for (slot . value) in slots-and-values
        for replacement = (find-replacement row column)
        collect (cons slot
                      (or (subst-value replacement)
                          (and value
                               (or (subst-value (find-subst column value))
                                   (strip-white-space value)))))))

(defun apply-import-scripts (variables scripts)
  (loop for script in scripts
        do
        (handler-case
            (return (funcall script variables))
          (error (c)
            (import-error "See scripts." (princ-to-string c))))))

(defun import-script-variables (slots-and-values)
  (loop for (slot . value) in slots-and-values
        when (and value
                  (not (listp (import-slot-csv-field slot))))
        collect (cons (transform-variable-name (import-slot-csv-field slot))
                      value)))

;;; Destructively modifies slots-and-values
(defun apply-all-import-scripts (slots-and-values)
  (let ((variables (import-script-variables slots-and-values)))
    (loop for cons in slots-and-values
          for (slot . original-value) = cons
          for script = (import-slot-script slot)
          when script
          collect
          (let* ((*slot* (import-slot-name slot))
                 (result (apply-import-scripts variables script)))
            (setf (cdr cons) result)
            (cons (import-slot-position slot) result)))))

(defun check-imported-object (object import-process)
  (let ((keys-alright t))
    (loop for slot in (import-process-slots import-process)
          for *slot* = (import-slot-name slot)
          for valid-values = (import-slot-valid-values slot)
          for slot-value = (and (symbolp *slot*)
                                (slot-exists-p object *slot*)
                                (slot-value object *slot*))
          do
          (cond ((null slot-value)
                 ;; (when (import-slot-key slot)
                 ;;   (setf keys-alright nil)
                 ;;   (import-error nil "Missing a key field."))
                 )
                ((null valid-values))
                ((functionp valid-values)
                 (multiple-value-bind (valid replace) (funcall valid-values slot-value)
                   (when replace
                     (setf (slot-value object *slot*) valid))))
                (t
                 (let ((valid (member slot-value valid-values
                                      :test #'equalp)))
                   (if valid
                       (setf (slot-value object *slot*) (car valid))
                       (import-error slot-value
                                     "Invalid value, must be one of ~{~s~^, ~}"
                                     valid-values))))))
    keys-alright))

(defun match-slots-and-values (import-process csv-values)
  (loop for slot in (import-process-slots import-process)
        collect (cons slot (funcall (import-slot-printer slot) csv-values))))

(defgeneric validate-imported-object (object)
  (:documentation "Is run after all substitions and mergings are completed."))

(defmethod validate-imported-object (object))

(defun make-object-from-csv (csv-values import-process row-index)
  (let* ((slots-and-values (match-slots-and-values import-process csv-values))
         (slots-and-values (apply-all-substs slots-and-values row-index
                                             (import-process-import import-process)))
         (instance (allocate-instance-with-nil-slots
                    (import-process-class import-process)))
         (*current-doc* instance)
         missing-slots
         (source-row (make-source-row row-index csv-values)))
    (handler-bind ((import-error
                     (lambda (c)
                       (setf (errors *current-import*) t)
                       (push (list (slot c) (value c) (princ-to-string c))
                             (source-row-errors source-row)))))
      (when (import-process-scripts import-process)
        (setf (script-results instance)
              (apply-all-import-scripts slots-and-values)))
      (loop for (slotd . value) in slots-and-values
            for slot = (import-slot-name slotd)
            for default = (import-slot-default slotd)
            do
            (if (and (symbolp slot)
                     (slot-exists-p instance slot))
                (setf (slot-value instance slot)
                      (if (bogus-result-p value)
                          default
                          value))
                (push (cons (string slot) value) missing-slots)))
      (loop for slot in (import-process-slots-in-order import-process)
            for *slot* = (import-slot-name slot)
            for parser = (import-slot-parser slot)
            do
            (when (slot-exists-p instance *slot*)
              (let ((value (slot-value instance *slot*)))
                (setf (slot-value instance *slot*)
                      (cond ((not value) nil)
                            ((funcall parser value))
                            ((and (not (import-slot-important slot))
                                  (not (import-slot-key slot)))
                             value))))))
      (let ((keys-alright (check-imported-object instance import-process)))
        (when missing-slots
          (loop for *slot* in (import-process-indirect-slots import-process)
                do
                (funcall *slot* instance missing-slots)))
        (merge-with-existing instance import-process)
        (funcall (import-process-initform-initializer import-process)
                 instance)
        (when keys-alright
          (funcall (or
                    (import-process-normal-cache-insert import-process)
                    (import-process-cache-insert import-process))
                   instance *imported-cache*)))
      (validate-imported-object instance))
    (push source-row (import-rows instance))
    (setf
          (log-action instance) "imported"
          (user instance) (and (current-user)
                               (email (current-user)))
          (stamp-date instance) *import-stamp-date*
          (top-level instance) nil
          ;; Imported docs are no longer saved, and some preview
          ;; screens check for saved objects.
          (id instance) -1)
    instance))

#|
(defun remove-already-imported (already-imported errors)
  (let ((index -1))
    (alexandria:deletef (imported-docs *current-import*) already-imported
                        :count 1
                        :key (lambda (x) (incf index) x))
    (append errors (assoc-value index errors))))

|#

(defgeneric handle-duplicate-object (object))
(defmethod handle-duplicate-object (object))

(defun merge-with-existing (object import-process)
  (let* ((lookup (import-process-cache-lookup import-process))
         (normal-lookup (import-process-normal-cache-lookup import-process))
         (already-imported (funcall (or normal-lookup lookup)
                                    object *imported-cache*))
         (existing (or already-imported
                       (and (not (clear-old-versions-p (import-process-import import-process)))
                            (funcall lookup object *merge-cache*))))
         (class (class-of existing)))
    (cond ((and (not (import-process-merge-with-imported import-process))
                already-imported)
           (cond ((and (getf (options *current-import*) :handle-duplicates)
                       (handle-duplicate-object object)))
                 (t
                  (setf (import-duplicate-p object) t)
                  (setf (import-duplicate-p already-imported) t)
                  (pushnew (cons (import-process-main-key import-process)
                                 '(nil "Duplicate record."))
                           (source-row-errors (car (import-rows already-imported)))
                           :test #'equal)
                  (import-slot-error (import-process-main-key import-process)
                                     nil "Duplicate record."))))
          (existing
           (loop for indirect in (import-process-indirect-merges import-process)
                 do
                 (funcall indirect object existing))
           (loop for slot in (c2mop:class-slots class)
                 when
                 (and (not (eq (c2mop:slot-definition-name slot)
                               'errors))
                      (c2mop:slot-boundp-using-class class existing slot)
                      (xdb2::store-slot-p slot)
                      (not
                       (and (c2mop:slot-boundp-using-class class object slot)
                            (c2mop:slot-value-using-class class object slot))))
                 do
                 (setf (c2mop:slot-value-using-class class object slot)
                       (c2mop:slot-value-using-class class existing slot)))
           (when (and normal-lookup
                      (not already-imported)
                      (let ((normal (funcall normal-lookup object *normal-merge-cache*)))
                        (and normal
                             (not (eq existing normal)))))
             (import-slot-error (import-process-main-key import-process)
                                nil "Duplicate record when renaming key slots."))
           (cond (already-imported
                  (alexandria:deletef (imported-docs *current-import*) already-imported
                                      :count 1))
                 ((< *import-stamp-date* (stamp-date existing))
                  (import-slot-error (import-process-main-key import-process)
                                     nil "Existing document has an older stamp-date: ~a"
                                     (format-date (stamp-date existing)))))
           t))))

(defun find-substs (import position)
  (loop for subst in (substs import)
        when subst
        when (eql (field subst) position)
        collect (cons (what subst) (with subst))))

(defun make-function-from-script (script)
  (compile nil `(lambda (variables)
                  (declare (ignorable variables))
                  ,script)))

(defun find-scripts-for-slot (import slot)
  (loop for script in (scripts import)
        when (string-equal (field script) slot)
        collect (make-function-from-script (script script))))



(defun valid-values-parser (valid)
  (typecase valid
    ((cons (eql :runtime))
     (funcall (cadr valid)))
    ((cons (eql :allsort))
     (find-allsorts-for-validation
      (cadr valid)))
    (cons
     valid)
    ((not null)
     (fdefinition valid))))

(defun find-class-slot (string-designator class)
  (loop for slot in (class-slots class)
        for slot-name = (slot-definition-name slot)
        when (or (eq string-designator slot-name)
                 (string-equal slot-name string-designator))
        return slot-name))

(defun make-mapped-import-slots (class descriptions import)
  (labels ((make-slot (name position csv-field printer
                       &optional renames)
             (let ((actual-slot (find-class-slot name class)))
               (destructuring-bind (&key sum key show-key main-key
                                         important parser valid default
                                         type alternative-key)
                   (cdr (assoc name descriptions :test #'string-equal))
                 (declare (ignore type main-key show-key))
                 (let* ((name (or actual-slot name))
                        (replaces
                          (and alternative-key
                               (assoc name (alt-keys import))
                               alternative-key))
                        (renames (and renames
                                      (car (assoc renames descriptions
                                                  :test #'string-equal)))))
                   (make-import-slot :name name
                                     :position position
                                     :parser
                                     (typecase parser
                                       ((cons (eql :if))
                                        (let ((option (second parser))
                                              (consequent (third parser))
                                              (alternative (fourth parser)))
                                          (fdefinition
                                           (if (getf (options import) option)
                                               consequent
                                               alternative))))
                                       (null
                                        #'identity)
                                       (t
                                        (fdefinition parser)))
                                     :default default
                                     :script (find-scripts-for-slot import name)
                                     :key key
                                     :important important
                                     :valid-values (valid-values-parser valid)
                                     :csv-field csv-field
                                     :printer printer
                                     :renames renames
                                     :replaces replaces
                                     :sum-values sum))))))
    (loop for column in (columns import)
          for i from 0
          for slot = (slot column)
          collect (make-slot slot
                             i
                             (column-map column) (printer column)
                             (old-slot column)))))


(defun make-import-slots (class-name class import)
  (let* ((descriptions (cdr (assoc class-name *import-slot-descriptions*)))
         (mapped (make-mapped-import-slots class descriptions import))
         (in-order (loop for (slot . keys) in descriptions
                         when (and (getf keys :parser)
                                   (find slot mapped :key #'import-slot-name))
                         collect it)))
    (values mapped in-order)))

(defun make-import-process (import)
  (let* ((class-name (import-class import))
         (class (find-class (find-symbol 
                             (format nil "~@:(~a-~a~)" class-name 'import)
                             (symbol-package class-name))))
         (key-slots (key-slots class-name))
         
         renamed
         replaced)
    (unless (class-finalized-p class)
      (finalize-inheritance class))
    (multiple-value-bind (slots in-order) (make-import-slots class-name class import)
      (%make-import-process
       :import import
       :class class
       :slots slots
       :key-slots slots
       :slots-in-order in-order
       :indirect-slots (getf *import-indirect-slots* class-name)
       :indirect-merges (getf *import-indirect-merging* class-name)
       :cache-insert
       (make-cache-insert
        (loop for slot-name in key-slots
              for replaced-slot = (find slot-name slots :key #'import-slot-replaces)
              collect
              (cond (replaced-slot
                     (setf replaced t)
                     (find-class-slot (import-slot-name replaced-slot) class))
                    (t
                     slot-name))))
       :cache-lookup
       (make-cache-lookup
        (loop for slot-name in key-slots
              for replaced-slot = (loop for slot in slots
                                        when (or (eq slot-name (import-slot-renames slot))
                                                 (eq slot-name (import-slot-replaces slot)))
                                        return slot)
              collect
              (cond (replaced-slot
                     (setf renamed t)
                     (find-class-slot (import-slot-name replaced-slot) class))
                    (t
                     slot-name))))
       :normal-cache-lookup (and renamed (make-cache-lookup key-slots))
       :normal-cache-insert (and replaced (make-cache-insert key-slots))
       :merge-with-imported (getf (cdr (assoc class-name *import-options*))
                                  :merge-with-imported t)
       :main-key (main-key-slot class-name)
       :scripts (and (some #'import-slot-script slots)
                     t)
       :initform-initializer (initform-initializer class)))))

(defun ensure-import-process (import)
  (or (import-process import)
      (setf (import-process import) (make-import-process import))))

(defun import-class (import)
  (assoc-value (import-type import) *import-classes*))

(defun make-merge-cache (import-process)
  (make-cache (import-process-cache-insert import-process)
              (class-name (import-process-class import-process))
              (import-process-key-slots import-process)))

(defun make-normal-merge-cache (import-process)
  (or (and (import-process-normal-cache-insert import-process)
           (make-cache (import-process-normal-cache-insert import-process)
                       (class-name (import-process-class import-process))
                       (import-process-key-slots import-process)))
      *merge-cache*))

(defun make-imported-cache (import-process)
  (declare (ignore import-process))
  (make-hash-table :test #'equalp))

(defun ensure-lock (import)
  (or (lock import)
      (let ((slot (find 'lock (class-slots (class-of import)) :key #'slot-definition-name
                                                              :test #'eq)))
        (sb-ext:compare-and-swap  (standard-instance-access import
                                                            (slot-definition-location slot))
                                  nil
                                  (sb-thread:make-mutex))
        (lock import))))

(defun do-import (import)
  (sb-thread:with-mutex ((ensure-lock import))
    (let* ((*current-import* import)
           (*import-stamp-date* (or (import-stamp-date import)
                                    (get-universal-time)))
           (import-process (make-import-process import))
           (*merge-cache* (make-merge-cache import-process))
           (*normal-merge-cache* (make-normal-merge-cache import-process))
           (*imported-cache* (make-imported-cache import-process))
           (*deduplicate-cache* (and (getf (options import) :handle-duplicates)
                                     (make-hash-table :test #'equalp))))
      (setf (import-process import) import-process
            (errors import) nil
            (imported-docs import) nil)
      (loop for row across (csv-rows import)
            for i from 0
            do
            (push (make-object-from-csv row import-process i)
                  (imported-docs import)))
      (alexandria:nreversef (imported-docs import))
      (persist import))))

(defmethod apply-grid-filter (grid (filter (eql :new-import-review)))
  (let* ((widget-name (get-parameter "w"))
         (*sfx-context* (find-request-page-context (get-parameter "p")))
         (widget (get-widget widget-name))
         (import (and widget
                      (current-import widget)))
         (init (post-parameter "init")))
    (cond ((not import) (values nil nil))
          (init
           (values (setf (rows grid) (imported-docs import))
                   t))
          (t
           (values (rows grid)
                   t)))))

(defmethod apply-grid-filter (grid (filter (eql :import-review)))
  (let* ((widget-name (get-parameter "w"))
         (*sfx-context* (find-request-page-context (get-parameter "p")))
         (widget (get-widget widget-name))
         (id (integer-parameter "i"))
         (rows (rows widget))
         (import (and widget id
                      (and (if (listp rows)
                               (> (length rows) id)
                               (array-in-bounds-p rows id))
                           (elt (rows widget) id))))
         (init (post-parameter "init")))
    (setf (read-only grid) t)
    (cond ((not import) (values nil nil))
          (init
           (values (setf (rows grid) (imported-docs import))
                   t))
          (t
           (values (rows grid)
                   t)))))

(defun import-check-changes (import)
  (let* ((import-process (ensure-import-process import))
         (lookup (import-process-cache-lookup
                  import-process))
         (import-date (stamp-date import)))
    (loop for object in (imported-docs import)
          for existing = (funcall lookup object *merge-cache*)
          thereis (and existing
                       (>= (if (stringp (stamp-date existing))
                               0
                               (stamp-date existing))
                           import-date)))))

(defgeneric persist-imported (doc &key &allow-other-keys))

(defmethod persist-imported (doc &key (set-time t))
  (persist doc :set-time set-time))

(defun import-replace-old (wrapped-doc merge-finder &key clear-old-versions-p)
  (let* ((existing (funcall merge-finder wrapped-doc *merge-cache*))
         (*inhibit-change-marking* t)
         (doc (import-switch-class-recursively wrapped-doc)))
    (cond ((not existing)
           (setf (user doc) (and (current-user)
                                 (email (current-user)))
               
                 (log-action doc) "import-authorized"
                 (id doc) nil
                 (top-level doc) t)
           (persist-imported doc :set-time nil
                                 :wrapped wrapped-doc))
          ((and (not clear-old-versions-p)
                (< (stamp-date doc) (stamp-date existing)))) ;; ignored
          (t
           (let ((copy (copy-object existing)))
             (setf (old-versions doc) (cons copy (old-versions copy))
                   
                   (user doc) (and (current-user)
                                   (email (current-user)))
                  
                   (log-action doc) "import-authorized"
                   (id doc) (id existing)
                   (id doc) (id existing)
                   (top-level doc) t)
             (copy-into doc existing)
             (persist-imported existing :set-time nil
                                        :wrapped wrapped-doc)
             (when clear-old-versions-p
               (clear-old-versions existing)))))))

(defun subst-key (subst)
  (etypecase subst
    (list (car subst))
    (saved-subst (what subst))))

(defun subst-value (subst)
  (etypecase subst
    (list (cdr subst))
    (saved-subst (with subst))))

(defun save-substs (column import import-data root-entity)
  (let ((hash (make-hash-table :test #'equalp)))
    (loop for (id . replacement) in (replacements column)
          for value = (funcall (printer column) (svref (csv-rows import) id))
          unless (empty-p value)
          do (setf (gethash value hash) replacement))
    (loop for subst in (substs column)
          for what = (subst-key subst)
          for with = (subst-value subst)
          unless (empty-p what)
          do (setf (gethash what hash) with))
    (when (plusp (hash-table-count hash))
      (let ((column-data (alexandria:ensure-gethash (string (slot column))
                                                    (substs import-data)
                                                    (make-hash-table :test #'equalp)))
            (user (current-user)))
        (loop for what being the hash-key of hash
              using (hash-value with)
              do
              (setf (getf (gethash what column-data) user) with
                    (getf (gethash what column-data) root-entity) with))))))

;;TODO: check replacement hack with stas (assoc-value column-id replacements) changed to (assoc-value value replacements)
(defun save-combined-substs (columns import import-data root-entity)
  (let ((hash (make-hash-table)))
    (loop for column in columns
          for column-id from 0
          do
          (loop for replacement in (replacements column)
                do (push (cons column-id (subst-value replacement))
                         (gethash (subst-key replacement) hash))))
    (loop for id being the hash-key of hash
          using (hash-value replacements)
          do (setf (gethash id hash)
                   (loop for column in columns
                         for column-id from 0
                         for value = (funcall (printer column) 
					      (svref (csv-rows import) id))
                         for replacement = (assoc-value value replacements) ;;mark todo
                         collect (cons value replacement))))
    (when (plusp (hash-table-count hash))
      (let ((column-data (alexandria:ensure-gethash (string (slot (car columns)))
                                                    (substs import-data)
                                                    (make-hash-table :test #'equalp)))
            (user (current-user)))
        (loop for id being the hash-key of hash
              using (hash-value replacements)
              for what = (mapcar #'car replacements)
              for with = (mapcar #'cdr replacements)
              do
              (setf (getf (gethash what column-data) user) with
                    (getf (gethash what column-data) root-entity) with))))))

(defun find-saved-substs (import-data import column)
  (let ((column-data (gethash (string (slot column))
                              (substs import-data))))
    (when column-data
      (let ((hash (make-hash-table :test #'equalp))
            (root-entity (get-root-entity (id (entity import))))
            (user (current-user))
            (printer (printer column)))
        (loop for row across (csv-rows import)
              for value = (funcall printer row)
              unless (gethash value hash)
              do
              (let* ((replacements (gethash value column-data))
                     (replacement (or (getf replacements user)
                                      (getf replacements root-entity))))
                (when replacement
                  (setf (gethash value hash) t)
                  (unless (find-subst column value)
                    (push (make-instance 'saved-subst
                                         :what value
                                         :with replacement)
                          (substs column))))))))))

(defun find-saved-combined-substs (import-data import columns)
  (let ((column-data (gethash (string (slot (car columns)))
                              (substs import-data))))
    (when column-data
      (let ((root-entity (get-root-entity (id (entity import))))
            (user (current-user)))
        (loop for row across (csv-rows import)
              for id from 0
              for value = (loop for column in columns
                                collect (funcall (printer column) row))
              do
              (let* ((replacements (gethash value column-data))
                     (replacement (or (getf replacements user)
                                      (getf replacements root-entity))))
                (when replacement
                  (unless (loop for column in columns
                                thereis (find-replacement id column))
                    (loop for column in columns
                          for with in replacement
                          when with
                          do
                          (push (make-instance 'saved-replacement
                                               :row id
                                               :with with)
                                (replacements column)))))))))))

(defun find-all-saved-options (data import)
  (setf (scripts import)
        (saved-scripts data import))
  (find-all-saved-substs data import)
  (setf (options import)
        (getf (options data) (get-root-entity (id (entity import))))))

(defun find-all-saved-substs (data import)
  (multiple-value-bind (columns combined-columns) (split-combined-columns import)
    (loop for columns in combined-columns
          do (find-saved-combined-substs data import columns))
    (loop for column in columns
          when (slot column)
          do (find-saved-substs data import column))))

(defun forget-saved-subst (column subst import)
  (let ((data (find-import-data (import-type import))))
    (when data
      (let ((substs (substs data))
            (root-entity (get-root-entity (id (entity import))))
            (user (current-user)))
        (let* ((column-data (and substs (gethash column substs)))
               (entry (and column-data
                           (gethash (what subst) column-data))))
          (when entry
            (remf entry user)
            (remf entry root-entity)
            (setf (gethash (what subst) column-data) entry)
            (persist data)))))))

(defun split-combined-columns (import)
  (let ((combined (cdr (assoc (import-class import) *import-combined-substs*)))
        (columns (columns import)))
    (values (loop for column in columns
                  unless (loop for group in combined
                               thereis (member (slot column) group :test #'equalp))
                  collect column)
            (loop for set in combined
                  for match =  (loop for column in set
                                     for match = (find column columns :key #'slot :test #'string-equal)
                                     unless match return nil
                                     collect match)
                  when match
                  collect match
                  and do (setf columns (set-difference columns match))))))

(defun saved-scripts (data import)
  (let* ((root-entity (get-root-entity (id (entity import))))
         (scripts (cdar (getf (scripts data) root-entity))))
    (loop for script in scripts
          for copy = (make-instance 'saved-script 
                                    :field (field script)
                                    :script (script script)
                                    :raw-script (raw-script script))
          collect copy)))

(defun scripts-to-save (import)
  (let ((scripts (loop for script in (scripts import)
                       for copy = (make-instance 'saved-script 
                                                 :field (field script)
                                                 :script (script script)
                                                 :raw-script (raw-script script))
                       collect copy)))
    (and scripts
         (cons
          (format nil "~a - ~a"
                  (original-file-name import)
                  (format-universal-date-time (stamp-date import)))
          scripts))))

(defun save-import-data (import)
  (let* ((import-data (ensure-import-data (import-type import)))
         (map (slot-map import-data))
         (root-entity (get-root-entity (id (entity import))))
         (scripts (scripts-to-save import)))
    (when scripts
      (push scripts
            (getf (scripts import-data) root-entity)))
    (setf (getf (options import-data) root-entity)
          (options import))
    (multiple-value-bind (columns combined-columns) (split-combined-columns import)
      (loop for columns in combined-columns
            do (save-combined-substs columns import import-data root-entity))
      (loop for column in columns
            for slot = (slot column)
            when (typep column 'real-column)
            do
            (let* ((matches (gethash (column-map column) map))
                   (cons (assoc slot matches :test #'string-equal)))
              (save-substs column import import-data root-entity)
              (if cons
                  (incf (cdr cons))
                  (push (cons (and slot (string slot)) 0) matches))
              (setf (gethash (column-map column) map)
                    (sort matches #'> :key #'cdr))))
      (persist import-data))))


(defun unmapped-keys (import)
  (let ((key-slots (virtual-key-slots (import-class import)))
        (matches (car (cdaadr (assoc (import-type import)
                                     (matches import)
                                     :test #'equal)))))
    (loop for column in (columns import)
          do (alexandria:removef key-slots (slot column) :test #'string-equal))
    (setf key-slots
          (loop for slot in key-slots
                when (assoc slot matches)
                collect (pretty-field-name slot)))
    (when key-slots
      (frmt "~{~s~^, ~} ~[~;key is~:;keys are~] not mapped."
            key-slots (length key-slots)))))

(defun check-all-keys-mapped (import)
  (let ((unmapped (unmapped-keys import)))
    (when unmapped
      (grid-error "Cannot authorize, ~a" unmapped))))



(defun authorize-import (import &optional grid)
  (check-page-permission-or-error "Authorize")
  (let ((lock-grabbed))
    (handler-case
        (sb-thread:with-mutex (*import-lock* :wait-p nil)
          (setf *current-import-name*
                (frmt "Authorization of ~s " (import-type import))
                lock-grabbed t)
          (check-all-keys-mapped import)
          (setf (status import) :authorizing)
          (let* ((import-process (ensure-import-process import))
                 (*merge-cache* (make-merge-cache import-process))
                 (*normal-merge-cache* (make-normal-merge-cache import-process))
                 (merge-finder (import-process-cache-lookup
                                import-process))
                 (key-checker (getf *import-key-checkers* (import-class import)))
                 (normal-lookup (import-process-normal-cache-lookup import-process))
                 (merge-duplicates (import-process-merge-with-imported import-process)))
            (when (import-check-changes import)
              (setf (status import) :uploaded)
              (persist import :set-time nil)
              (when grid
                (setf (action grid) :authorize)
                (update-table grid)
                (grid-error "")))
            (save-import-data import)
            (loop with *inhibit-change-marking* = t
                  for doc in (imported-docs import)
                  when (and (funcall key-checker doc)
                            (or merge-duplicates
                                (not (import-duplicate-p doc)))
                            (or (not normal-lookup)
                                (not (funcall normal-lookup doc *normal-merge-cache*))))
                  do
                  (import-replace-old doc merge-finder
                                      :clear-old-versions-p (clear-old-versions-p import)))
            (setf (status import) :authorized)
            (persist import)
            (when grid
              (finish-editing grid))))
      (error (c)
        ;; Resignal the error outside of the lock
        (setf (status import) :error)
        (error c)))
    (unless lock-grabbed
      (grid-error "~a is in progress"
                  *current-import-name*))))

(defmethod handle-action ((grid importer-grid) (action (eql :do-authorize)))
  (authorize-import (editing-row grid) grid))

(defun make-errors-grid (grid)
  (make-widget 'importer-errors-grid
               :parent-grid grid
               :editable nil
               :searchable nil
               :sortable nil
               :box nil
               :bottom-buttons nil
               :columns (list
                         (make-instance 'grid-column
                                        :header "Column")
                         (make-instance 'grid-column
                                        :header "Value")
                         (make-instance 'grid-column
                                        :header "Error"))
               :js-callback
               "
function(oSettings) {
  $('.doc-header', oSettings.nTable).parent().each(function(){
console.log('hl');
  $(this).attr('colspan', 3);
  $(this).siblings().remove();
  $(this).css('padding', '0');
  $(this).css('text-align', 'center');
})
}"))

(defun render-errors-form (grid)
  (with-html
    (let ((box (make-widget 'box
                            :name "error-box"
                            :header "Errors"
                            :icon "exclamation"))
          (error-grid (make-errors-grid grid)))
      (setf (header-content box)
            (with-html-string (render-header-buttons error-grid)))
      (setf (content box)
            (with-html-string
              (:form :action ""
                     :id "permissions-form"
                     :method "post"
                     :onsubmit "return false;"
                     (render error-grid)
                     (let ((disable (disable-grid-buttons grid)))
                     (htm
                      (:div :class "btn-toolbar panel-footer"
                            (:button :class "btn-primary btn"
                                     :type "submit"
                                     :onclick
                                     (frmt
                                      "{~@[~a;~]~a;}"
                                      disable
                                      (js-render (editor grid)
                                                 (js-pair "grid-name" (name grid))
                                                 (js-pair "action" "cancel")))
                                     "Ok")))))))
      (render box))))

(defclass importer-errors-grid (grid)
  ())

(defun doc-errors (errors)
  (loop for (slot value message) in errors
        collect (list (format nil "~:(~@[~a~]~)" slot)
                      value message)))


(defmethod get-data-table-rows ((grid importer-errors-grid) &key start length)
  (let* ((import (editing-row (parent-grid grid)))
         (docs (imported-docs import))
         (printer (getf *import-descriptions*
                        (import-class import))))
    (values
     (loop for doc in docs
           for doc-id from 0
           append
           (loop for row in (import-rows doc)
                 for errors = (source-row-errors row)
                 with row-id = 0
                 until (= row-id (+ start length))
                 when (and errors (>= row-id start))
                 collect (list
                          (with-html-string
                            (:div :class "doc-header"
                                  :style "background-color: #d1cfd0;
border-bottom: 2px solid #A19B9E;
border-top: 2px solid #A19B9E;"
                                  (fmt "#~a ~a" doc-id
                                       (funcall printer doc))))
                          "" "")
                 and
                 append (doc-errors errors)
                 when errors
                 do (incf row-id)))
     (setf (total-row-count grid) (count-if #'doc-has-errors docs)))))

(defmethod export-csv ((grid importer-errors-grid))
  (let* ((import (editing-row (parent-grid grid)))
         (docs (imported-docs import))
         (printer (getf *import-plain-descriptions*
                        (import-class import))))
    (when docs
      (with-output-to-string (stream)
        (write-line "Doc number|Key|Column|Value|Error" stream)
        (loop for doc in docs
              for doc-id from 0
              do
              (loop for row in (import-rows doc) 
                    for errors = (source-row-errors row)
                    for key = (funcall printer doc)
                    do
                    (loop for (slot value message) in errors
                          do
                          (format stream "~a|~a|~@{~a~^|~}~%"
                                  doc-id
                                  key slot value message))))))))

(defmethod handle-action ((grid importer-grid) (action (eql :do-reject)))
  (check-page-permission-or-error "Reject")
  (let ((import (selected-row grid)))
    (setf (status import) :rejected)
    (setf (imported-docs import) nil
          (csv-rows import) nil)
    (persist import)
    (finish-editing grid)))

(defun remove-imported-docs (import)
  (let* ((import-process (ensure-import-process import))
         (normal-lookup (import-process-normal-cache-lookup import-process))
         (lookup (or normal-lookup
                     (import-process-cache-lookup import-process)))
         (cache (if normal-lookup
                    (make-normal-merge-cache import-process)
                    (make-merge-cache import-process))))
    (loop for doc in (imported-docs import)
          for imported = (funcall lookup doc cache)
          when imported
          do (remove-doc imported (email (current-user))))))
