(in-package :cl-sfx)

(defclass import-script-grid (grid)
  ((current-import :initarg :current-import
                  :initform nil
                  :accessor current-import))
  (:default-initargs :row-object-class 'import-script))

(defmethod get-rows ((grid import-script-grid))
  (remove nil (scripts (current-import grid))))

(defun add-import-script-grid (grid import &key (box t))
  (let* ((columns
           (list
            (make-instance 'grid-column
                           :name 'field
                           :header "Column"
                           :printer
                           (lambda (x)
                             (format nil "~:(~a~)" x)))
            (make-instance 'grid-column
                           :name 'raw-script
                           :header "Script")))
         (script-grid (make-widget 'import-script-grid
                                   :parent-grid grid
                                   :columns columns
                                   :box box
                                   :advanced-search nil))
         (import-data (find-import-data (import-type import)))
         (root-entity (get-root-entity (id (entity import))))
         (scripts (and import-data
                       (getf (scripts import-data) root-entity))))
    (setf (current-import script-grid) import)
    (when scripts
      (with-html
        (:div :class "form-group"
              (:div :class "row"
                    (:label :class "col-sm-2 control-label"
                            "Copy scripts from a previous import")
                    (:div :class "col-sm-4"
                          (render-edit-field "copy-scripts"
                                             nil
                                             :data (mapcar #'car scripts)
                                             :type :select))
                    (:div :class "col-sm-2"
                          (:button :class "btn-submit btn"
                                   :type "submit"
                                   :onclick
                                   (js-render (editor script-grid)
                                              (js-value "copy-scripts")
                                              (js-pair "grid-name" (name script-grid))
                                              (js-pair "action" "copy-scripts"))
                                   "Copy"))))))
    (render script-grid)))

(defmethod render-inline-buttons ((grid import-script-grid))
  (render-export-button grid))

(defmethod render-row-editor ((grid import-script-grid) row)
  (let* ((form (make-widget 'form :name "import-script-fields-form"
                                  :form-id "import-script-fields-edit-form"
                                  :grid-size 12
                                  :header "Scripts"))
         (form-section (make-widget 'form-section
                                    :name "form-section"))
         (import (current-import grid))
         (fields (find-allsorts-for-select
                  (format nil "Import Type - ~a"
                          (import-type import)))))
    (render form
            :grid grid
            :content
            (with-html-string
              (render
               form-section
               :label "Column"
               :input
               (with-html-string
                 (render-edit-field
                  "field"
                  (field row)
                  :type :select
                  :data fields)))
              (render
               form-section
               :label "Available columns"
               :input
               (format nil "~{~(~a~)~^, ~}"
                       (transform-variable-names (csv-fields import))))
              (render
               form-section
               :label "Available fucntions"
               :input
               "if, not, and, or, equal, equalp, =, /=, +, *, /, floor, ceiling, mod, rem, subseq, replace-all, split-string, format, parse-number")
              (render
               form-section
               :label "Script"
               :input
               (with-html-string
                 (render-edit-field
                  "script"
                  (cond ((post-parameter "script"))
                        ((or (raw-script row)
                             (script row))
                         (let ((*package* #.*package*))
                           (or (raw-script row) 
                               (write-to-string (script row)
                                                :case :downcase))))
                        (t
                         "")))))))))

(defmethod handle-action ((grid import-script-grid) (action (eql :delete)))
  (alexandria:removef (scripts (current-import grid)) (selected-row grid))
  (persist (current-import grid))
  (update-table grid))

(defmethod handle-action ((grid import-script-grid) (action (eql :save)))
  (with-parameters (field script)
    (let* ((row (editing-row grid))
           (import (current-import grid))
           (fields (transform-variable-names
                    (csv-fields import))))
      (handler-case (compile-script script fields)
        (error (c)
          (setf (error-message grid) (princ-to-string c)))
        (:no-error (compiled)
          (setf (field row) field
                (raw-script row) script
                (script row) compiled)
          (pushnew row (scripts import))
          (setf (status import) :uploaded)
          (persist import)
          (finish-editing grid))))))

(defmethod export-csv ((grid import-script-grid))
  (let* ((docs (grid-filtered-rows grid)))
    (when docs
      (with-output-to-string (stream)
        (write-line "Column|Script" stream)
        (loop for doc in docs
              do
              (format stream "~:(~a~)|~a~%"
                      (field doc)
                      (raw-script doc)))))))

(defmethod handle-action ((grid import-script-grid) (action (eql :copy-scripts)))
  (let* ((import (current-import grid))
         (import-data (find-import-data (import-type import)))
         (root-entity (get-root-entity (id (entity import))))
         (scripts (and import-data
                       (getf (scripts import-data) root-entity)))
         (scripts (cdr (assoc (post-parameter "copy-scripts") scripts :test #'equal))))
    (setf (scripts import)
          (append (loop for script in (scripts import)
                        when (or (not (typep script 'saved-script))
                                 (plusp (version script)))
                        collect script)
                  (loop for script in scripts
                        collect
                        (make-instance 'saved-script 
                                       :field (field script)
                                       :script (script script)
                                       :raw-script (raw-script script)))))
    (persist import)
    (update-table grid)))
