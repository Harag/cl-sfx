(in-package :cl-sfx)

(defclass import-page (widget)
  ())

(defmethod render ((widget import-page) &key)
  (let* ((columns
           (list
            (make-instance 'grid-column
                           :name 'entity
                           :printer (lambda (x)
                                      (and x
                                           (entity-name x))))
            (make-instance 'grid-column
                           :name 'import-type
                           :header "Type")
            (make-instance 'grid-column
                           :name 'file-name
                           :special-printer
                           (lambda (grid row value row-id)
                             (declare (ignore grid row-id))
                             (with-html-string
                               (:a :href (escape (format nil "~Aimports/~A"
                                                        (site-url *sfx-system*) value))
                                   (esc (original-file-name row))))))
            (make-instance 'grid-column
                           :name 'description
                           :header "Notes")
            (make-instance 'grid-column
                           :name 'user)
            (make-instance 'grid-column
                           :name 'import-stamp-date
                           :header "Effective Date"
                           :printer #'format-universal-date
                           :sort-key :verbatim)
            (make-instance 'grid-column
                           :name 'stamp-date
                           :header "Modified"
                           :printer #'format-universal-date-time
                           :sort-key :verbatim)
            (make-instance 'grid-column
                           :name 'status
                           :printer #'string-capitalize)))
         (grid (make-widget 'importer-grid :name "importer-grid"
                                           :columns columns
                                           :title "Importer"
                                           :initial-sort-column
                                           '(6 :descending))))
    (with-html-string
      (:p
       "By default the importer only shows active imports, use appropriate filters to see historical imports.")
      (render grid))))

(defclass import-templates-page (widget)
  ())

(defmethod render ((widget import-templates-page) &key)
  (let* ((columns
           (list
            (make-instance 'grid-column
                           :name 'name)
            (make-instance 'grid-column
                           :name 'import-type
                           :header "Import Type")))
         (grid (make-widget 'importer-templates-grid :name "templates-grid"
                                                     :columns columns
                                                     :title "Importer Templates"
                                                     :row-object-class 'import-template)))
    (render grid)))







