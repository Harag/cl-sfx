(in-package :cl-sfx)


(defclass import-log-grid (grid)
  ())

(defmethod list-grid-filters ((grid import-log-grid))
  '(with-audit-data))

(defun get-import-log-data (grid &key filter search)
  (declare (ignore grid search))

  (if (equal filter 'with-audit-data)
       (let ((docs))
         (dolist (doc (coerce (get-collection-docs 'sfx-import) 'list))
          (when (typep doc 'new-sfx-import)
            (when (old-versions doc)
                (setf docs (append docs (old-versions doc))))))
         (coerce docs 'vector))

       (find-docsx  
	(lambda (doc)
	  (typep doc 'new-sfx-import))
	'sfx-import)))


(defmethod get-rows ((grid import-log-grid))
  (setf (rows grid) (get-import-log-data grid 
                                          :filter (grid-filter grid)  
                                          :search (search-term grid))))

(defmethod render-row-editor ((grid import-log-grid) row)
  )



(defmethod handle-action ((grid import-log-grid) (action (eql :save)))
  )



(defmethod handle-action ((grid import-log-grid) (action (eql :cancel)))
  (finish-editing grid))



(defun print-entities (entities)
  (when entities
    (let ((e-list))
      ;;(break "~A" entities)
      (dolist (id entities)
        (setf e-list (append e-list (list (format nil "[~A]" (get-val (get-entity-by-id id) 'entity-name ))))))
      e-list)))


(defmethod export-csv ((grid import-log-grid))
  (let ((data (grid-filtered-rows grid)))
    (when data
      (with-output-to-string (stream)
        (format stream "Import Log Export Filter :~A    Search :~A    Date:~A ~%Context:~A~%~%"  
                (or (get-val grid 'grid-filter ) "") (or  (get-val grid 'search-term ) "")  (current-date-time) (print-context) )
        (format stream "~A~%" "Entity|Import Type|Status|Rec Count|User|Stamp Date|Stamp")

        (loop for doc being the element of data
             when (typep doc 'sfx-import)
              do (format stream "~A|~A|~A|~A|~A|~A|~A~%" 
                         (entity-name (entity doc)) 
                         (import-type doc) 
                         (status doc) 
                         (length (imported-docs doc)) 
                         (user doc) 
                         (format-universal-date (stamp-date doc)) 
                         (stamp-date doc) ))))))




(define-easy-handler (import-log :uri "/xpacks/import-log") ()
  (let* ((columns
           (list
            (make-instance 'grid-column
                           :name 'entity
                           :header "Entity Name"
                           :printer 'print-entity-name)
            (make-instance 'grid-column
                           :name 'import-type
                           :header "Import Type")
            (make-instance 'grid-column
                           :name 'status
                           :header "Status")
            (make-instance 'grid-column
                           :name 'imported-docs
                           :header "Rec Count"
                           :printer (lambda (docs)
                                      (length docs)))
            (make-instance 'grid-column
                           :name 'user
                           :header "User" )
            (make-instance 'grid-column
                           :name 'stamp-date
                           :header "Date"
                           :printer 'format-universal-date))
           )

         (grid (make-widget 'import-log-grid :name "import-log-grid"
                            :title "import-log"
                            :row-object-class 'xpacks-import)))
    (setf (columns grid) columns)
    (render (make-widget 'page :name "import-log-page"
                         :title "Import Log"
                         )
            :body (render-to-string grid))))



