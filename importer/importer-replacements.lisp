(in-package :cl-sfx)

(defclass data-fix-modal (ajax-widget)
  ((preview :initarg :preview
            :initform nil
            :accessor preview)))

(defgeneric render-replacement (row value import type))

(defun find-replacement (row-id column)
  (find row-id (replacements column) :key #'subst-key))

(defun find-replacements (object column)
  (loop for row in (import-rows object)
        thereis (find-replacement (source-row-id row) column)))

(defun save-replacement (replacement row-id column)
  (let ((existing (find-replacement row-id column)))
    (when (typep existing 'saved-replacement)
      (alexandria:deletef (replacements column) existing)
      (setf existing nil))
    (if existing
        (setf (cdr existing) replacement)
        (push (cons row-id replacement)
              (replacements column)))))

(defun find-subst (column value)
  (find value (substs column) :key #'subst-key :test #'equal))

(defun save-column-replacement (value replacement column)
  (let ((existing (find-subst column value)))
    (when (typep existing 'saved-subst)
      (alexandria:deletef (substs column) existing)
      (setf existing nil))
    (if existing
        (setf (cdr existing) replacement)
        (push (cons value replacement) (substs column)))))

(defun replacement-body (subst replacement
                         value column row-id column-id source
                         import modal)
  (let* ((slot-description (import-slot-description import (slot column)))
         (ok t))
    (with-html
      (:div :class "modal-content"
            (:div :class "modal-header"
                  (:h4 :class "modal-title"
                       :id "data-fix-label"
                       "Correct data"))
            (:div :class "modal-body"
                  (:div :id "replacement-form"
                        (:div :class "form-group"
                              (:label :for "replacement"
                                      (fmt "Replace \"~a\" with"
                                           (escape value)))
                              (setf ok
                                    (not
                                     (nth-value 1
                                                (render-replacement row-id
                                                                    (or (subst-value replacement)
                                                                        (subst-value subst)
                                                                        value)
                                                                    import
                                                                    (getf slot-description :type)))))
                              (:div :class "checkbox"
                                    (:label (:input :type "checkbox"
                                                    :name "whole-column"
                                                    :checked (when (and subst
                                                                        (not replacement))
                                                               "checked"))
                                            "Apply to the whole column.")))))
            (:div :class "modal-footer"
                  (when ok
                    (htm
                     (:button :type "button"
                              :class "btn btn-primary"
                              :onclick
                              (frmt
                               "{$(\"#data-fix-modal2\").modal(\"hide\");~a;}"
                               (multiple-value-call
                                   #'js-render-form-values modal
                                 "replacement-form"
                                 (js-pair "row" row-id)
                                 (js-pair "column" column-id)
                                 (if source
                                     (js-pair "source" source)
                                     (values))))
                              "Ok")))
                  (when (or replacement
                            subst)
                    (htm
                     (:button :type "button"
                              :class "btn btn-warning"
                              :onclick
                              (frmt
                               "{$(\"#data-fix-modal2\").modal(\"hide\");~a;}"
                               (multiple-value-call
                                   #'js-render modal
                                 (js-pair "remove" row-id)
                                 (js-pair "row" row-id)
                                 (js-pair "column" column-id)
                                 (if source
                                     (js-pair "source" source)
                                     (values))))
                              "Remove"))
                    (when (typep subst 'saved-subst)
                      (htm
                       (:button :type "button"
                                :class "btn btn-warning"
                                :onclick
                                (frmt
                                 "{$(\"#data-fix-modal2\").modal(\"hide\");~a;}"
                                 (multiple-value-call
                                     #'js-render modal
                                   (js-pair "forget-saved-subst" row-id)
                                   (js-pair "row" row-id)
                                   (js-pair "column" column-id)
                                   (if source
                                       (js-pair "source" source)
                                       (values))))
                                "Forget saved substitution"))))
                  (:button :type "button"
                           :class "btn btn-default"
                           :data-dismiss "modal"
                           "Cancel"))))))

(defmethod render ((modal data-fix-modal) &key)
  (let ((import (current-import (grid (preview modal)))))
    (with-parameters ((row-id "row") (column-id "column")
                      source
                      replacement remove forget-saved-subst
                      whole-column)
      (unless (and (setf row-id (ensure-parse-integer row-id))
                   (setf column-id (ensure-parse-integer column-id)))
        (return-from render))
      (setf source (ensure-parse-integer source))
      (when (equal whole-column "false")
        (setf whole-column nil))
      (let* ((column (nth column-id (columns import)))
             (object (elt (imported-docs import) row-id))
             (source-row (if source
                             (find source (import-rows object) :key #'source-row-id)
                             (car (import-rows object))))
             (value (funcall (printer column)
                             (source-row-values source-row)))
             (current-replacement (find-replacements object column))
             (current-subst (find-subst column value)))
        (flet ((remove-whole ()
                 (alexandria:deletef (substs column) current-subst))
               (remove-single ()
                 (setf (replacements column)
                       (delete-if (lambda (replacement)
                                    (if source
                                        (eql (subst-key replacement) source)
                                        (member (subst-key replacement)
                                                (import-rows object)
                                                :key #'source-row-id)))
                                  (replacements column)))))
          (cond (remove
                 (if current-replacement
                     (remove-single)
                     (remove-whole))
                 (update-import (preview modal)))
                (forget-saved-subst
                 (if current-replacement
                     (remove-single)
                     (remove-whole))
                 (assert (typep current-subst 'saved-subst))
                 (forget-saved-subst (slot column) current-subst import)
                 (update-import (preview modal)))
                (whole-column
                 (remove-single)
                 (save-column-replacement value replacement column)
                 (update-import (preview modal)))
                (replacement
                 (if source
                     (save-replacement replacement source column)
                     (loop for row in (import-rows object)
                           do
                           (save-replacement replacement (source-row-id row) column)))
                 (update-import (preview modal)))
                (t
                 (with-html
                   (:div :class "modal"
                         :id "data-fix-modal2"
                         :role "dialog"
                         :aria-labelledby "data-fix-label"
                         (:div :class "modal-dialog"
                               (replacement-body current-subst
                                                 current-replacement
                                                 value column
                                                 row-id column-id source
                                                 import modal))))
                 (defer-js "$('#data-fix-modal2').modal()")
                 (defer-js
                     "$('#data-fix-modal2').on('shown.bs.modal', function () {$('#replacement').focus(); })"))))))))

;;;


(defmethod render-replacement (row value import type)
  (with-html
    (:input :type "text"
            :name "replacement"
            :id "replacement"
            :class "form-control"
            :value (escape value))))

(defun render-select-choice (value choices &optional (name "replacement"))
  (let* ((value (and (member value choices :test #'equal)
                     value))
         (choices (if value
                      choices
                      (adjoin "" choices :test #'equal))))
    (render-select name choices value))
  :select)

(defmethod render-replacement (row value import (type (eql 'date)))
  (with-html
    (:input :type "text"
            :class "form-control date"
            :name "replacement"
            :id "replacement"
            :value (escape value))))

(defun render-radio-choice (value choices &key (inline t)
                                               (name "replacement"))
  (with-html
    (:br)
    (loop for choice in choices
          for selected = (equalp value choice)
          do
          (htm
           (:label :class (if inline
                              "radio-inline"
                              "radio")
                   (:input :type "radio"
                           :name name
                           :id (and selected name)
                           :value choice
                           :checked selected)
                   (esc choice)))))
  :radio)

(defmethod render-replacement (row value import (type (eql 'yes-no)))
  (render-radio-choice value '("Yes" "No")))
