(in-package :cl-sfx)

(defvar *public-key*
  (alexandria:read-file-into-string
   (merge-pathnames ".ssh/id_rsa.pub"
                    (user-homedir-pathname))))
(defvar *sftp-connection*)

(defclass ftp-connection (entity-doc)
  ((kind :initarg :kind
         :initform nil
         :accessor kind
         :type (member :ftp :sftp)
         :parser (lambda (x)
                   (find x '(:ftp :sftp) :test #'string-equal)))
   (name :initarg :name
         :initform nil
         :accessor name)
   (hostname :initarg :hostname
             :initform nil
             :accessor hostname)
   (port :initarg :port
         :initform 22
         :accessor port
         :db-type number
         :parser #'ensure-parse-integer)
   (username :initarg :username
              :initform nil
              :accessor username)
   (password :initarg :password
             :initform nil
             :accessor password)
   (directories :initarg :directories
                :initform nil
                :accessor directories)
   (refresh-frequency :initarg :refresh-frequency
                      :initform 60
                      :accessor refresh-frequency
                      :db-type number
                      :parser #'ensure-parse-integer)
   (task :initarg :task
         :initform nil
         :accessor task
         :storep nil)
   (logs :initarg :logs
         :initform nil
         :accessor logs))
  (:metaclass data-class)
  (:collection-name "ftp-connections")
  (:collection-type :license)
  (:default-initargs :top-level t))

(defun ftp-connection-collection ()
  (get-col-lic "ftp-connection"))

(defun ftp-connections ()
  (docs (ftp-connection-collection)))

(defmethod doc-collection ((doc ftp-connection))
  (ftp-connection-collection))

(defclass ftp-directory ()
  ((path :initarg :path
         :initform nil
         :accessor path)
   (import-type :initarg :import-type
                :initform nil
                :accessor import-type))
  (:metaclass data-class))

(defclass ftp-import-log ()
  ((date :initarg :date
         :initform nil
         :accessor date)
   (file-name :initarg :file-name
              :initform nil
              :accessor file-name) 
   (status :initarg :status
           :initform nil
           :accessor status))
  (:metaclass data-class))

;;;

(defclass ftp-import (new-sfx-import)
  ()
  (:metaclass storable-versioned-class))

(defclass bad-ftp-import (ftp-import)
  ((error-message :initarg :error-message
                  :initform nil
                  :accessor error-message))
  (:metaclass storable-versioned-class))

(defparameter *log-button*
  (make-instance 'grid-button
                 :action "logs"
                 :icon "exclamation"
                 :hint "View logs"))

(defclass ftp-grid (grid)
  ()
  (:default-initargs :row-object-class 'ftp-connection
                     :top-level t
                     :additional-buttons (list *log-button*))
  (:metaclass widget-class))

(defclass ftp-directory-grid (grid)
  ((path :initarg :path
         :initform nil
         :accessor path)
   (import-type :initarg :import-type
                :initform nil
                :accessor import-type))
  (:default-initargs :row-object-class 'ftp-directory)
  (:metaclass widget-class))

;;;

(defun render-ftp-import ()
  (render (make-widget 'ftp-grid
                       :columns
                       (list (make-instance 'grid-column
                                            :name 'entity
                                            :header "Entity Name"
                                            :printer 'print-entity-name)
                             (make-instance 'grid-column
                                            :name 'name)
                             (make-instance 'grid-column
                                            :name 'hostname)))))
(defmethod get-rows ((grid ftp-grid))
  (setf (rows grid)
        (find-docsx #'match-entity
                   'ftp-connection)))

(defmethod get-rows ((grid ftp-directory-grid))
  (let ((parent (parent-grid grid)))
    (setf (rows grid)
          (directories (editing-row parent)))))

;;;


(defun entity-list ()
  (loop for entity across (get-collection-docs 'entity)
        when (member (id entity) (context))
        collect (list (id entity)
                      (entity-name entity))))

(defun ftp-connection-editor (connection)
  (let ((kind (or (post-parameter "kind")
                  (and (kind connection)
                       (string (kind connection)))
                  "SFTP")))
   (with-html
     (:div
      :id "ftp-conn-form"
      :class "form-horizontal"
      (:div :class "form-group"
            (:label :class "col-sm-2 control-label"
                    "Kind")
            (:div :class "col-sm-4"
                  (render-edit-field "kind"
                                     kind
                                     :posted-value t
                                     :data '("SFTP" "FTP")
                                     :type :select)))
      (defer-js "$('#kind').change(function(){~
if($(this).val() == 'SFTP'){$('#pass-section').hide();$('#port-section').show();$('#key-section').show()}~
else{$('#pass-section').show();$('#key-section').hide();$('#port-section').hide()}})")
      (:div :class "form-group"
            (:label :class "col-sm-2 control-label"
                    "Entity")
            (:div :class "col-sm-4"
                  (render-edit-field "entity"
                                     (and (entity connection)
                                          (id (entity connection)))
                                     :posted-value t
                                     :data (entity-list)
                                     :type :select)))
      (:div :class "form-group"
            (:label :class "col-sm-2 control-label"
                    "Name")
            (:div :class "col-sm-4"
                  (render-edit-field "name"
                                     (name connection)
                                     :posted-value t)))
      (:div :class "form-group"
            (:label :class "col-sm-2 control-label"
                    "Hostname")
            (:div :class "col-sm-4"
                  (render-edit-field "hostname"
                                     (hostname connection)
                                     :posted-value t)))
      (:div :class "form-group"
            :id "port-section"
            :style (and (equal kind "FTP")
                        "display:none;")
            (:label :class "col-sm-2 control-label"
                    "Port")
            (:div :class "col-sm-4"
                  (render-edit-field "port"
                                     (port connection)
                                     :posted-value t)))
      (:div :class "form-group"
            (:label :class "col-sm-2 control-label"
                    "Username")
            (:div :class "col-sm-4"
                  (render-edit-field "username"
                                     (username connection)
                                     :posted-value t)))
      (:div :class "form-group"
            :id "pass-section"
            :style (and (equal kind "SFTP")
                        "display:none;")
            (:label :class "col-sm-2 control-label"
                    "Password")
            (:div :class "col-sm-4"
                  (render-edit-field "password"
                                     nil
                                     :posted-value t
                                     :type :password)))
      (:div :class "form-group"
            :id "key-section"
            :style (and (equal kind "FTP")
                        "display:none;")
            (:label :class "col-sm-2 control-label"
                    "Public Key")
            (:div :class "col-sm-4"
                  :style "word-wrap: break-word;"
                  "Add the public key to ~/.ssh/authorized_keys"
                  (:textarea :class "no-mce"
                             :style "width:100%"
                             :readonly t
                             :rows 10
                             (esc *public-key*))))
      (:div :class "form-group"
            (:label :class "col-sm-2 control-label"
                    "Refresh frequency")
            (:div :class "col-sm-4"
                  (render-edit-field "refresh-frequency"
                                     (refresh-frequency connection)
                                     :posted-value t
                                     :data '((1 "Every minute")
                                             (10 "Every 10 minutes")
                                             (60 "Every hour"))
                                     :type :select)))))))

(defun ftp-directory-editor (grid)
  (let ((grid (make-widget 'ftp-directory-grid
                           :parent-grid grid
                           :box nil
                           :columns
                           (list (make-instance 'grid-column
                                                :name 'path)
                                 (make-instance 'grid-column
                                                :name 'import-type)))))
    (finish-editing grid)
    (render grid)))

(defclass ftp-logs-grid (grid)
  ())

(defmethod get-rows ((grid ftp-logs-grid))
  (logs (editing-row (parent-grid grid))))

(defmethod render-row-editor ((grid ftp-grid) connection)
  (if (eq (action grid) :logs)
      (with-html
        (let ((box (make-widget 'box
                                :name "error-box"
                                :header "Log"
                                :icon "exclamation"))
              (logs-grid (make-widget 'ftp-logs-grid
                                      :parent-grid grid
                                      :editable nil
                                      :searchable nil
                                      :sortable nil
                                      :box nil
                                      :bottom-buttons nil
                                      :columns (list
                                                (make-instance 'grid-column
                                                               :name 'file-name)
                                                (make-instance 'grid-column
                                                               :name 'date
                                                               :printer #'format-universal-date-time)
                                                (make-instance 'grid-column
                                                               :name 'status)))))
          (setf (content box)
                (with-html-string
                  (:form :action ""
                         :id "permissions-form"
                         :method "post"
                         :onsubmit "return false;"
                         (render logs-grid)
                         (let ((disable (disable-grid-buttons grid)))
                           (htm
                            (:div :class "btn-toolbar panel-footer"
                                  (:button :class "btn-primary btn"
                                           :type "submit"
                                           :onclick
                                           (frmt
                                            "{~@[~a;~]~a;}"
                                            disable
                                            (js-render (editor grid)
                                                       (js-pair "grid-name" (name grid))
                                                       (js-pair "action" "cancel")))
                                           "Ok")))))))
          (render box)))
      (let ((new (not (id connection)))
            (box (make-widget 'tab-box
                              :header "FTP Connection")))
        (setf (header box) "FTP Connection"
              (body-content box)
              (let ((disable (disable-grid-buttons grid)))
                (with-html-string
                  (:div :class "btn-toolbar panel-footer"
                        (htm
                         (:button :class "btn-submit btn"
                                  :type "submit"
                                  :onclick
                                  (frmt
                                   "{~@[~a;~]~a;}"
                                   disable
                                   (js-render-form-values  (editor grid) "ftp-conn-form"
                                                           (js-pair "grid-name" (name grid))
                                                           (js-pair "action" "save")))
                                  "Save"))
                        (:button :class "btn-default btn"
                                 :onclick
                                 (frmt
                                  "{~@[~a;~]~a;}"
                                  disable
                                  (js-render-return-false (editor grid)
                                                          (js-pair "grid-name" (name grid))
                                                          (js-pair "action" "cancel")))
                                 "Cancel"))))
              (tabs box)
              (list*
               (list
                "Connection"
                (with-html-string (ftp-connection-editor connection)))
               (and (not new)
                    (list
                     (list
                      "Directories"
                      (with-html-string (ftp-directory-editor grid)))))))
        (render box))))

(defmethod render-row-editor ((grid ftp-directory-grid) directory)
  (let ((box (make-widget 'box
                          :header "FTP Directory")))
    (setf (content box)
          (with-html-string
            (:div
             :id "ftp-dir-form"
             :class "form-horizontal"
             (:div :class "form-group"
                   (:label :class "col-sm-2 control-label"
                           "Import type")
                   (:div :class "col-sm-4"
                         (render-edit-field "import-type"
                                            (import-type directory)
                                            :posted-value t
                                            :data (loop for (import-type) in *import-classes*
                                                        when (check-page-permission import-type)
                                                        collect import-type)
                                            :type :select)))
             (:div :class "form-group"
                   (:label :class "col-sm-2 control-label"
                           "Path")
                   (:div :class "col-sm-4"
                         (render-edit-field "path"
                                            (path directory)
                                            :posted-value t)))
             (let ((disable (disable-grid-buttons grid)))
               (with-html
                 (:div :class "row btn-toolbar panel-footer"
                       (htm
                        (:button :class "btn-submit btn"
                                 :type "submit"
                                 :onclick
                                 (frmt
                                  "{~@[~a;~]~a;}" ;
                                  disable
                                  (js-render-form-values  (editor grid) "ftp-dir-form"
                                                          (js-pair "grid-name" (name grid))
                                                          (js-pair "action" "save")))
                                 "Save"))
                       (:button :class "btn-default btn"
                                :onclick
                                (frmt
                                 "{~@[~a;~]~a;}"
                                 disable
                                 (js-render-return-false (editor grid)
                                                         (js-pair "grid-name" (name grid))
                                                         (js-pair "action" "cancel")))
                                "Cancel")))))))
    (render box)))

(defmethod handle-action ((grid ftp-grid) (action (eql :save)))
  (let* ((connection (editing-row grid))
         (old-username (username connection))
         (old-password (password connection))
         (old-hostname (hostname connection)))
    (synq-edit-data connection)
    (unless (setf (entity connection)
                  (get-entity-by-id (ensure-parse-integer (entity connection))))
      (grid-error "Entity is required"))
    (if (eq (kind connection) :sftp)
        (setf (password connection) nil)
        (setf (port connection) 22))
    (when (empty-p (name connection))
      (grid-error "Name is required"))
    (unless (and (equal old-username (username connection))
                 (equal old-hostname (hostname connection))
                 (or (empty-p (password connection))
                     (equal old-password (password connection))))
      (when (empty-p (username connection))
        (grid-error "Username is required"))
      (when (empty-p (hostname connection))
        (grid-error "Hostname is required"))
      (when (and
             (eq (kind connection) :ftp)
             (empty-p (password connection)))
        (grid-error "Password is required"))
      (test-connection connection))
    (persist connection)
    (schedule-connetion connection)
    (finish-editing grid)))

(defmethod handle-action ((grid ftp-directory-grid) (action (eql :save)))
  (let ((dir (editing-row grid))
        (connection (editing-row (parent-grid grid))))
    (synq-edit-data dir)
    (when (empty-p (import-type dir))
      (grid-error "Import type is required"))
    (when (empty-p (path dir))
      (grid-error "Path is required"))
    (test-directory dir connection)
    (pushnew dir (directories connection))
    (persist connection)
    (finish-editing grid)))

(defmacro with-ftp-connection ((var connection) &body body)
  (let ((con (gensym)))
    `(let ((,con ,connection))
       (ftp:with-ftp-connection (,var :hostname (hostname ,con)
                                      :username (username ,con)
                                      :password (password ,con))
         ,@body))))

(defun test-connection (connection)
  (case (kind connection)
    (:ftp
     (handler-case
         (with-ftp-connection (con connection))
       (ftp:ftp-error (c)
         (grid-error (ftp:error-message c)))))))

(defun test-directory (dir connection)
  (case (kind connection)
    (:ftp
     (handler-case
         (with-ftp-connection (con connection)
           (ftp::send-cwd-command con (path dir)))
       (ftp:ftp-error (c)
         (grid-error (ftp:error-message c)))))))

(defun make-sftp-process (&optional (connection *sftp-connection*))
  (sb-ext:run-program "sftp" (list "-qb" "-"
                                   "-o" "UserKnownHostsFile=/dev/null"
                                   "-o" "StrictHostKeyChecking=no"
                                   "-P"
                                   (princ-to-string
                                    (or (port connection) 22))
                                   (format nil "~a@~a"
                                           (username connection)
                                           (hostname connection)))
                      :search t :wait nil
                      :input :stream :output :stream))

(defun sftp-command (line)
  (let ((process (make-sftp-process)))
    (unwind-protect
         (let ((in (sb-ext:process-input process))
               (out (sb-ext:process-output process)))
           (write-line line in)
           (close in)
           (let* ((first-line)
                  (result (loop for line = (read-line out nil)
                                while line
                                do (setf line (string-trim #(#\Space #\Newline #\Return) line))
                                if first-line
                                collect line
                                else do (setf first-line line))))
             (if (zerop (sb-ext:process-exit-code (sb-ext:process-wait process)))
                 result
                 (error "~a~{~a~^~%~}" first-line result))))
      (sb-ext:process-close process))))

(defun ls (path)
  (sftp-command (format nil "ls -1 ~s" path)))

(defun download (remote local)
  (sftp-command (format nil "get ~s ~s" remote local)))

(defun move-file (from to)
  (sftp-command (format nil "rename ~s ~s" from to)))

(defun csv-file-p (filename)
  (let ((mismatch (mismatch filename ".csv" :from-end t)))
    (and mismatch
         (< mismatch (- (length filename) 3)))))

(defun sftp-create-import (file)
  (let* ((entity (entity *sftp-connection*))
         (import-file (make-import-filename entity))
         (local (merge-pathnames import-file (truename *tmp-directory*)))
         (local-namestring (namestring local))
         (time (get-universal-time)))
    (ensure-directories-exist local)
    (download file local-namestring)
    (flet ((maybe-fail (result)
             (when (stringp result)
               (push (make-instance 'ftp-import-log :date time
                                                    :file-name file
                                                    :status result)
                     (logs *sftp-connection*))
               (persist (make-instance 'bad-ftp-import
                                       :top-level t
                                       :entity entity
                                       :status :error
                                       :import-stamp-date time
                                       :original-file-name (file-namestring file)
                                       :file-name import-file
                                       :error-message result))
               (move-file file (format nil "~a-error-~a" file time))
               (error result))))
      (multiple-value-bind (header rows) (parse-csv local)
        (maybe-fail header)
        (let ((x (make-sfx-import (file-namestring file) import-file
                                    header rows
                                    :class 'ftp-import
                                    :entity entity
                                    :type :all)))
          (maybe-fail x)
          (push (make-instance 'ftp-import-log :date time
                                               :file-name file
                                               :status "Success")
                (logs *sftp-connection*))
          (persist x)
          (move-file file (format nil "~a-processed-~a" file time)))))))

(defun sftp-process-directory (directory)
  (loop for file in (ls (path directory))
        when (csv-file-p file)
        do (sftp-create-import file)))

(defun sftp-new-imports (connection)
  (let ((*sftp-connection* connection))
    (map nil #'sftp-process-directory (remove-duplicates (directories connection)
                                                         :key #'path
                                                         :test #'equalp))))

;;;

(defclass ftp-import-task (task)
  ((connection :initarg :connection
               :initform nil
               :accessor connection)))

(defun schedule-connetion (connection)
  (let* ((existing (task connection))
         (task (or existing
                   (make-instance 'ftp-import-task
                                  :connection connection))))
    (setf (repeat task)
          (* (refresh-frequency connection) 60))
    (cond (existing
           (setf (run-time task) nil)
           (reschedule-task task))
          (t
           (setf (task connection) task)
           (add-task task)))))

(defun schedule-ftp-imports ()
  (map nil #'schedule-connetion (ftp-connections)))

(defmethod run-task ((task ftp-import-task))
  (let ((connection (connection task)))
    (case (kind connection)
      (:sftp
       (sftp-new-imports connection)))))

;;;

(defmethod handle-action ((grid ftp-grid) (action (eql :logs)))
  (setf (selected-row grid) (editing-row grid)))
