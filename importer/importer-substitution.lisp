(in-package :cl-sfx)

(defclass import-subst-grid (grid)
  ((current-import :initarg :current-import
                  :initform nil
                  :accessor current-import))
  (:default-initargs :row-object-class 'import-subst))

(defun get-all-replacements (column column-name import
                             &key (colored t))
  (let ((hash (make-hash-table))
        (printer (printer column))
        (key-printer (getf (if colored
                               *import-descriptions*
                               *import-plain-descriptions*)
                           (import-class import))))
    (loop for replacement in (replacements column)
          do (setf (gethash (subst-key replacement) hash) (subst-value replacement)))
    (loop for doc in (imported-docs import)
          append
          (loop for row in (import-rows doc)
                for id = (source-row-id row)
                for rep = (gethash id hash)
                when rep
                collect
                (list column-name
                      (format nil "#~a ~a" id
                              (funcall key-printer doc))
                      (funcall printer (source-row-values row)) rep)))))

(defun format-substs (import &key (colored t))
  (loop for column in (columns import)
        for column-name = (if (typep column 'real-column)
                              (column-map column)
                              (frmt "Virtual for ~@[~a~]"
                                    (slot column)))
        append
        (loop for subst in (substs column)
              collect (list column-name
                            "All"
                            (subst-key subst) (subst-value subst)))
        append
        (get-all-replacements column column-name import :colored colored)))

(defmethod get-data-table-rows ((grid import-subst-grid) &key start length)
  (let* ((result (format-substs (current-import grid)))
         (total-length (length result)))
    (values
     (subseq result start (min total-length
                               (+ start length)))
     (setf (total-row-count grid)
           total-length))))

(defun add-import-subst-grid (grid import &key (box t))
  (let* ((columns
           (list
            (make-instance 'grid-column
                           :header "Column")
            (make-instance 'grid-column
                           :header "Row")
            (make-instance 'grid-column
                           :name 'what)
            (make-instance 'grid-column
                           :name 'with)))
         (subst-grid (make-widget 'import-subst-grid
                                   :parent-grid grid
                                   :columns columns
                                   :box box
                                   :editable nil
                                   :advanced-search nil)))
    (setf (current-import subst-grid) import)
    (render subst-grid)))

(defmethod render-inline-buttons ((grid import-subst-grid))
  (render-export-button grid))

(defmethod render-row-editor ((grid import-subst-grid) row)
  (let ((form (make-widget 'form :name "import-subst-fields-form"
                                 :form-id "import-subst-fields-edit-form"
                                 :grid-size 12
                                 :header "Substitutions"))
        (form-section (make-widget 'form-section
                                   :name "form-section"))
        (import (current-import grid)))
    (render form
            :grid grid
            :content
            (with-html-string
              (render
               form-section
               :label "Column"
               :input
               (with-html-string
                 (render-edit-field
                  "field"
                  (field row)
                  :type :select
                  :data
                  (find-allsorts-for-select
                   (format nil "Import Type - ~a"
                           (import-type import))))))
              (render
               form-section
               :label "What"
               :input
               (with-html-string
                 (render-edit-field
                  "what"
                  (what row))))
              (render
               form-section
               :label "With"
               :input
               (with-html-string
                 (render-edit-field
                  "with"
                  (with row))))))))

(defmethod handle-action ((grid import-subst-grid) (action (eql :delete)))
  (alexandria:removef (substs (current-import grid)) (selected-row grid))
  (persist (current-import grid))
  (update-table grid))

(defmethod handle-action ((grid import-subst-grid) (action (eql :save)))
  (let ((row (editing-row grid))
        (import (current-import grid)))
    (synq-edit-data row)
    (pushnew row (substs import))
    (setf (status import) :uploaded)
    (persist import)
    (finish-editing grid)))

(defmethod export-csv ((grid import-subst-grid))
  (let ((docs (format-substs (current-import grid) :colored nil)))
    (when docs
      (with-output-to-string (stream)
        (write-line "Column|Row|What|With" stream)
        (loop for doc in docs
              do
              (format stream "~{~a~^|~}~%" doc))))))
