(in-package :cl-sfx)

(defclass importer-preview-grid (grid)
  ((current-import :initarg :current-import
                   :initform nil
                   :accessor current-import)
   (first-error :initarg :first-error
                :initform nil
                :accessor first-error)
   (column-errors :initarg :column-errors
                  :initform nil
                  :accessor column-errors)
   (fix-modal :initarg :fix-modal
              :initform nil
              :accessor fix-modal)
   (expanded-rows :initarg :expanded-rows
                  :initform nil
                  :accessor expanded-rows)))

(defun column-equal (x y)
  (if (consp y)
      (member x y :test #'string-equal)
      (string-equal x y)))

(defun next-column-error (start import column)
  (loop named outer
        for doc in (nthcdr start (imported-docs import))
        for i from start
        do
        (loop for row in (import-rows doc)
              when (assoc (slot column) (source-row-errors row)
                          :test #'column-equal)
              do (return-from outer i))))

(defun format-row (source-row import
                   errors first-error
                   column-errors doc doc-id &optional individual)
  (let ((row (source-row-values source-row)))
    (loop for column in (columns import)
          for column-id from 0
          for scripted = (assoc column-id (script-results doc))
          for value = (if scripted
                          (cdr scripted)
                          (funcall (printer column) row))
          for error = (cdr (assoc (slot column) errors
                                  :test #'column-equal))
          for replacement = (or (find-replacement (source-row-id source-row) column)
                                (find-subst column value))
          do
          (setf value
                (if replacement
                    (with-html-string-no-indent
                      (esc value)
                      (:br)
                      (:span :class "glyphicon glyphicon-chevron-down")
                      (:br)
                      (if (typep replacement 'saved-subst)
                          (htm (:span :class "saved-subst"
                                     (esc (subst-value replacement))))
                          (esc (subst-value replacement))))
                    (escape value)))
          collect
          (with-html-string-no-indent
            (:div :class (frmt "replace-value~:[~; scripted-value~]~@[ ~a~]"
                               scripted
                               (and error
                                    (frmt "imp-error~:[~; first-error~]~
                                          ~@[ first-error-~a~]"
                                          (shiftf (car first-error) nil)
                                          (and (shiftf (aref column-errors column-id) nil)
                                               column-id))))
                  :title (and error
                              (escape (second error)))
                  :onclick (frmt "imp_fix(~a,~a~@[,~a~])" doc-id column-id
                                 (and individual (source-row-id source-row)))
                  (str value))))))

(defun combine-errors (doc)
  (let ((ht (make-hash-table :test #'equal)))
    (loop for row in (import-rows doc)
          do (loop for (slot . rest) in (source-row-errors row)
                   do (setf (gethash slot ht) rest)))
    (alexandria:hash-table-alist ht)))

(defun sum-column (slot docs)
  (loop for doc in docs
        for value = (slot-value doc slot)
        when (numberp value)
        sum value))

(defmethod get-data-table-rows ((grid importer-preview-grid) &key start length)
  (let* ((import (current-import grid))
         (docs (imported-docs import))
         (column-errors (make-array (length (columns import))
                                    :initial-element t))
         (end (+ start length))
         (expand (integer-parameter "expand"))
         (collapse (integer-parameter "collapse")))
    (when expand
      (push expand (expanded-rows grid)))
    (when collapse
      (alexandria:removef (expanded-rows grid) collapse))
    (values
     (append
      (loop with first-error = (list t)
            for doc-id from 0
            for doc in docs
            for rows = (import-rows doc)
            for merged-p = (cdr rows)
            for expanded = (and merged-p
                                (member doc-id (expanded-rows grid)))
            until (= doc-id end)
            when (>= doc-id start)
            collect (cons (cond ((not merged-p)
                                 "")
                                (expanded
                                 (with-html-string
                                   (:a :class "grid-button"
                                       :title "Collapse"
                                       :href (frmt "javascript:collapseTableRow(\"importer-preview-grid-table\",~a)"
                                                   doc-id)
                                       (:span :class "expand glyphicon glyphicon-chevron-down"))))
                                (t
                                 (with-html-string
                                   (:a :class "grid-button"
                                       :title "Expand"
                                       :href (frmt "javascript:expandTableRow(\"importer-preview-grid-table\",~a)"
                                                   doc-id)
                                       (:span :class "expand glyphicon glyphicon-chevron-right")))))
                          (format-row (car rows) import
                                      (if expanded
                                          (source-row-errors (car rows))
                                          (combine-errors doc))
                                      first-error column-errors doc doc-id))
            and
            when expanded
            append (loop for row in rows
                         collect
                         (cons
                          (with-html-string (:div :class "glyphicon glyphicon-random"))
                          (format-row row import
                                      (source-row-errors row)
                                      first-error column-errors doc doc-id t))))
      (list
       (cons ""
             (loop for column in (columns import)
                   for slot in (import-process-slots (ensure-import-process import))
                   for i from 0
                   for next-error = (next-column-error end import column)
                   collect
                   (with-html-string
                     (when next-error
                       (htm
                        (:a :title "Scroll to the next error"
                            :href (frmt "javascript:scrollToError(\"#~a\",~a,~a)"
                                        (name grid) next-error i)
                            (:span :class "error-arrow glyphicon glyphicon-arrow-down"))))
                     (when (import-slot-sum-values slot)
                       (htm
                        (:div (:h5 "Total")
                              (esc (fmt-money
                                    (sum-column (import-slot-name slot) docs)))))))))))
     (setf (total-row-count grid)
           (length docs)))))

(defclass import-preview (ajax-widget)
  ((grid :initarg :grid
         :initform nil
         :accessor grid)
   (map-modal :initarg :map-modal
              :initform nil
              :accessor map-modal)))

(defun all-errors (import)
  (let ((hash-table (make-hash-table :test #'eq)))
    (loop for doc in (imported-docs import)
          do
          (loop for row in (import-rows doc)
                do
                (loop for (slot) in (source-row-errors row)
                      do (loop for slot in (alexandria:ensure-list slot)
                               do (incf (gethash slot hash-table 0))))))
    (alexandria:hash-table-alist hash-table)))

(defun present-map (column)
  (when (column-map column)
    (with-output-to-string (str)
      (loop for clause in (column-map column)
            for value = (cdr clause)
            for plus = nil then (princ " + " str)
            do (prin1 value str)))))

(defun duplicate-columns (column import)
  (let ((slot (slot column)))
    (and slot
         (plusp (length (string slot)))
         (> (count slot (columns import)
                   :key #'slot
                   :test #'string-equal)
            1))))

(defgeneric render-column (column import preview parent column-id))

(defmethod render-column ((column real-column) import preview parent column-id)
  (with-html-no-indent
    (:div :id (frmt "column-~a" column-id)
     (esc (column-map column)))))

(defmethod render-column ((column virtual-column) import preview parent column-id)
  (with-html-no-indent
    (:button :type "button"
             :id (frmt "map-column-~a" column-id)
             :class "btn btn-default map-column"
             :data-toggle "tooltip"
             :data-placement "bottom"
             :title (present-map column)
             :onclick (js-render (map-modal preview)
                                 (js-pair "column" column-id))
             "Map")
    " "
    (:button :type "button"
             :class "btn btn-default remove-column"
             :title "Remove column"
             :onclick (frmt "~a;~a"
                            (disable-controls parent)
                            (js-render parent
                                       (js-pair "remove-column" column-id)))
             (:span :class "glyphicon glyphicon-minus"))))

(defun make-preview-grid-columns (preview import parent)
  (defer-js "$('.map-column').tooltip()")
  (loop with slots = (import-slot-names (import-type import))
        with errors = (all-errors import)
        for column in (columns import)
        for match = (if (typep column 'real-column)
                        (matches column))
        for value = (slot column)
        for i from 0
        for name = (frmt "map-~a" i)
        when (null (car match)) do (pop match)
        collect
        (make-instance
         'grid-column
         :header
         (with-html-string-no-indent
           (render-column column import preview parent i)
           (:span :id (frmt "error-~a" i)
                  (let ((errors (cdr (assoc (slot column)
                                            errors
                                            :test #'string-equal))))
                    (column-errors-html i (grid preview) errors)))
           (:div :id (frmt "duplicate-~a" i)
                 :style "white-space:nowrap"
                 :class 
                 (if (duplicate-columns column import)
                     "has-error virtual-column"
                     "virtual-column")
                 (when (zerop i)
                   (htm (:button :type "button"
                                 :id "add-column"
                                 :class "btn btn-default"
                                 :title "Add new column"
                                 :onclick (js-render parent
                                                     (js-pair "add-column" "t"))
                                 (:span :class "glyphicon glyphicon-plus"))))
                 (:select :class "form-control"
                          :id name
                          :onchange (js-render preview (js-value name))
                          (:option :value ""
                                   :selected (or (null value)
                                                 (null (column-map column))))
                          ;;   (break "~A" match)
                          (if (typep column 'real-column)
                              (loop for (field) in match
                                    do
                                    (htm (:option :value field
                                                  :selected (string-equal field value)
                                                  (esc (pretty-field-name field)))))
                              (loop for slot in slots
                                    do
                                    (htm (:option :value (escape slot)
                                                  :selected (string-equal slot value)
                                                  (esc (pretty-field-name slot))))))
                          (:optgroup
                           :label "Use as on old value for"
                           (loop for key in (key-slots (import-class import))
                                 for name = (frmt "IMPORT-OLD-~A" key)
                                 do
                                 (htm (:option :selected (string-equal value name)
                                               :value name
                                               (esc (pretty-field-name key))))))))))
        into columns
        finally
        (return
          (values (cons (make-instance  'grid-column
                                        :header "")
                        columns)
                  (when errors
                    (reduce #'+ errors :key #'cdr))))))

(defun make-import-mapping-from-matches (matches)
  (loop for (csv-header ((slot))) in matches
        when slot
        collect (make-instance 'sfx-import-mapping
                               :file-column csv-header
                               :import-slot slot)))

(defun apply-saved-substs (import column &optional old-map)
  (when (use-saved-substs import)
    (let ((data (find-import-data (import-type import))))
      (when data
        (let* ((combined (cdr (assoc (import-class import) *import-combined-substs*)))
               (combined-group (loop for group in combined
                                     when (or (member (slot column) group :test #'equalp)
                                              (member old-map group :test #'equalp))
                                     return group)))
          (flet ((delete-replacmenets (column)
                   (setf (replacements column)
                         (delete-if (lambda (x)
                                      (typep x 'saved-replacement))
                                    (replacements column)))))
            (cond (combined-group
                   (delete-replacmenets column)
                   (let ((combined-columns
                           (loop for combined in combined-group
                                 for column = (find combined (columns import) :key #'slot :test #'equalp)
                                 when column
                                 do (delete-replacmenets column)
                                 and
                                 collect column)))
                     (when (and (= (length combined-columns) (length combined-group))
                                (every #'slot combined-columns))
                       (find-saved-combined-substs data import combined-columns))))
                  (t
                   (setf (substs column) (delete-if (lambda (x)
                                                      (typep x 'saved-subst))
                                                    (substs column)))
                   (when (slot column)
                     (find-saved-substs data import column))))))))))

(defun disable-saved-options (import)
  (loop for column in (columns import)
        do
        (setf (substs column)
              (delete-if (lambda (x)
                           (typep x 'saved-subst))
                         (substs column))
              (replacements column)
              (delete-if (lambda (x)
                           (typep x 'saved-replacement))
                         (replacements column))))
  (setf (scripts import)
        (loop for script in (scripts import)
              when (or (not (typep script 'saved-script))
                       (plusp (version script)))
              collect script)))

(defun enable-saved-options (import)
  (let ((data (find-import-data (import-type import))))
    (when data
      (find-all-saved-substs data import)
      (setf (scripts import)
            (append (loop for script in (scripts import)
                          when (or (not (typep script 'saved-script))
                                   (plusp (version script)))
                          collect script)
                    (saved-scripts data import)))
      (setf (options import)
            (getf (options data) (get-root-entity (id (entity import))))))))

(defun set-changed-mapping (import)
  (loop for (key . value) in (post-parameters*)
        when (alexandria:starts-with-subseq "map-" key)
        do
        (let* ((column-id (ensure-parse-integer (subseq key (length "map-"))))
               (column (nth column-id (columns import)))
               (old-map (slot column)))
          (if (alexandria:starts-with-subseq "IMPORT-OLD-" value)
              (setf (old-slot column) (subseq value (length "IMPORT-OLD-"))
                    (slot column) value)
              (setf (slot column) (and (not (empty-p value))
                                       value)
                    (old-slot column) nil))
          (apply-saved-substs import column old-map))))

(defun total-errors-html (grid errors)
  (when (and errors
             (plusp errors))
    (with-html
      (:a :title "Scroll to the first error" :href
          (frmt "javascript:scrollToFirstErrColumn(\"#~a\", ~a)"
                (name grid)
                (position-if #'identity (column-errors grid)))
          (:div :class "badge badge-danger"
                (str errors))))))

(defun column-errors-html (column grid errors)
  (when (and errors
             (plusp errors))
    (let* ((column-errors (column-errors grid))
           (previous-error
             (and (plusp column)
                  (position-if #'identity column-errors :end column :from-end t)))
           (next-error
             (position-if #'identity column-errors :start (1+ column))))
     (with-html-no-indent
       (when previous-error
         (htm (:a :title "Previous column with errors"
                  :href (frmt "javascript:scrollToErrColumn(\"#~a\",~a)"
                              (name grid)
                              previous-error)
                  (:span :class "error-arrow glyphicon glyphicon-arrow-left"))
              " "))
       (:a :href
           (frmt "javascript:scrollToError(\"#~a\",~a,~a,true)"
                 (name grid)
                 (aref (column-errors grid) column)
                 column)
            :title "Scroll to the first error"
            (:div :class "badge badge-danger"
                  (str errors)))
       (when next-error
         (htm " "
              (:a :title "Next column with errors"
                  :href (frmt "javascript:scrollToErrColumn(\"#~a\",~a)"
                              (name grid)
                              next-error)
               (:span :class "error-arrow glyphicon glyphicon-arrow-right"))))))))



(defun key-column-errors (import)
  (let ((unmapped-keys (unmapped-keys import)))
    (when unmapped-keys
      (with-html-no-indent
        (:div :class "form-group"
              (:div :class "col-sm-offset-2 col-sm-6"
                    (:div :class "alert alert-danger"
                          (esc unmapped-keys))))))))

(defun show-errors (import grid)
  (let* ((all-errors (all-errors import))
         (error-count (reduce #'+ all-errors :key #'cdr)))
    (loop for column in (columns import)
          for errors = (cdr (assoc (slot column)
                                   all-errors
                                   :test #'string-equal))

          for duplicate = (duplicate-columns column import)
          for i from 0
          do
          (defer-js
              (frmt
               "$('#error-~a').html(~s)" i
               (substitute #\Space #\Newline
                           (with-html-string
                             (column-errors-html i grid errors)))))
          (defer-js
              (frmt
               "$('#duplicate-~a').attr('class', 'virtual-column ~:[~;has-error~]')"
               i duplicate)))
    (defer-js
        (frmt "$('#total-errors').html(~s)"
              (substitute #\Space #\Newline
                          (with-html-string
                            (total-errors-html grid error-count)))))
    (defer-js
        (frmt "$('#keys-not-mapped').html(~s)"
              (substitute #\Space #\Newline
                          (with-html-string
                            (key-column-errors import)))))))

(defun make-column-errors (import)
  (let* ((docs (imported-docs import))
         (column-errors (make-array (length (columns import))
                                    :initial-element nil)))
    (loop for i from 0
          for doc in docs
          do (loop for row in (import-rows doc)
                   for doc-errors = (source-row-errors row)
                   when doc-errors
                   do
                   (loop for column in (columns import)
                         for column-id from 0
                         for error = (cdr (assoc (slot column) doc-errors
                                                 :test #'column-equal))
                         when (and error
                                   (null (aref column-errors column-id)))
                         do (setf (aref column-errors column-id) i))))
    column-errors))

(defun put-matches-to-columns (matches import)
  (let ((data (find-import-data (import-type import))))
    (loop for column in (columns import)
          when (typep column 'real-column)
          do (let* ((field (column-map column))
                    (match (cadr (assoc field matches :test #'equal))))
               (setf (matches column) match
                     (slot column)
                     (remove-nil-string
                      (caar (or (and data
                                     (gethash field (slot-map data)))
                                match))))))))

(defun update-alt-keys (import)
  (let ((slots (cdr (assoc (import-class import)
                           *import-slot-descriptions*))))
    (loop for (key . value) in (post-parameters*)
          when (alexandria:starts-with-subseq "alt-key-" key)
          do (let* ((slot-name (subseq key (length "alt-key-")))
                    (slot (assoc slot-name slots :test #'string-equal)))
               (if (equal value "true")
                   (pushnew slot (alt-keys import) :test #'equal)
                   (alexandria:removef (alt-keys import) slot :test #'equal))))))

(defun update-options (import)
  (let ((options (getf (cdr (assoc (import-class import) *import-options*)) :options)))
    (when options
      (loop for (key . value) in (post-parameters*)
            when (alexandria:starts-with-subseq "opt-" key)
            do (let* ((option-name (subseq key (length "opt-")))
                      (option (loop for (key) on options by #'cddr
                                    when (string-equal key option-name)
                                    return key)))
                 (when option
                   (setf (getf (options import) option)
                         (equal value "true"))))))))

(defun doc-has-errors (doc)
  (loop for row in (import-rows doc)
        thereis (and (source-row-errors row) t)))

(defun first-error-position (import)
  (position-if #'doc-has-errors (imported-docs import)))



(defun update-import (import-preview)
  (let* ((grid (grid import-preview))
         (import (current-import grid))
         (posted-date (post-parameter "stamp-date"))
         (clear-old (post-parameter "clear-old"))
         (use-saved-substs (post-parameter "use-saved-substs")))
    (set-changed-mapping import)
    (when posted-date
      (setf (import-stamp-date import)
            (or (parse-date posted-date)
                (current-universal-date))))                                                  
    (update-alt-keys import)
    (update-options import)
    (when clear-old
      (setf (clear-old-versions-p import) (equal clear-old "true")))
    (when use-saved-substs
      (if (setf (use-saved-substs import) (equal use-saved-substs "true"))
          (enable-saved-options import)
          (disable-saved-options import)))
    (do-import import)
    (setf (column-errors grid) (make-column-errors import)
          (first-error grid) (first-error-position import))
    (update-one-table grid)
    (show-errors import grid)))

(defmethod render ((import-preview import-preview) &key)
  (with-parameters (description)
    (cond (description
           (let* ((grid (grid import-preview))
                  (import (current-import grid)))
             (setf (description import)
                   (and (not (equal description ""))
                        description))
             (when (parent-grid grid)
               (update-one-table (parent-grid grid)))
             (persist import)))
          (t
           (update-import import-preview)))))

(defun render-import-options (import preview)
  (let ((options (getf (cdr (assoc (import-class import) *import-options*)) :options)))
    (with-html
      (loop for (key description) on options by #'cddr
            for name = (frmt "opt-~a" key)
            for set = (getf (options import) key)
            do
            (htm (:div :class "form-group"
                       (:label :class "col-sm-3 control-label"
                               (esc description))
                       (:div :class "col-sm-6"
                             (render-edit-field name
                                                set
                                                :type :checkbox
                                                :on-change (js-render preview
                                                                      (js-checkbox-value name))))))))))

(defun render-import-edit-form (import parent &optional grid)
  (let* ((preview-grid (make-widget 'importer-preview-grid
                                    :parent-grid grid
                                    :box nil
                                    :editable nil
                                    :scroll-x t
                                    :sortable nil
                                    :searchable nil))
         (preview (make-widget 'import-preview :grid preview-grid))
         (fix-modal (make-widget 'data-fix-modal))
         (map-modal (make-widget 'column-map-modal :preview preview))
         (posted-type (post-parameter "import-type"))
         errors)
    (when posted-type
      (setf (import-type import) posted-type)
      (put-matches-to-columns (cadr (assoc (import-type import)
                                           (matches import)
                                           :test #'equal))
                              import)
      (when (use-saved-substs import)
        (disable-saved-options import)
        (enable-saved-options import)))
    (cond ((post-parameter "add-column")
           (push (make-instance 'virtual-column :sfx-import import)
                 (columns import)))
          ((integer-parameter "remove-column")
           (setf (columns import)
                 (remove-if (constantly t)
                            (columns import)
                            :start (integer-parameter "remove-column")
                            :count 1))))
    (setf (grid preview) preview-grid
          (fix-modal preview-grid) fix-modal
          (preview fix-modal) preview
          (current-import preview-grid) import
          (map-modal preview) map-modal)
    (render fix-modal)
    (render map-modal)
    (with-html
      (:div :id "grid-preview"
            (render preview)
            (setf (values (columns preview-grid) errors)
                  (make-preview-grid-columns preview
                                             import
                                             parent))
            (:div :class "row"
                  (:div :class "form-horizontal col-sm-offset-1 col-sm-6"
                        (:div :class "form-group"
                              (:label :class "col-sm-4 control-label"
                                      "Import Type"
                                      (:span :id "total-errors"
                                             (total-errors-html preview-grid errors)))
                              (:div :class "col-sm-5"
                                    (render-edit-field "import-type"
                                                       (import-type import)
                                                       :type :select
                                                       :data
                                                       (loop for (type) in (matches import)
                                                             when (check-page-permission type)
                                                             collect type)
                                                       :on-change
                                                       (js-render parent
                                                                  (js-value "import-type")))))
                        (:div :class "form-group"
                              (:label :class "col-sm-4 control-label"
                                      "Stamp Date")
                              (:div :class "col-sm-5"
                                    (render-edit-field "stamp-date"
                                                       (format-date (import-stamp-date import))
                                                       :type :date
                                                       :on-change (js-render preview
                                                                             (js-value "stamp-date")))))
                        (:div :class "form-group"
                              (:label :class "col-sm-4 control-label"
                                      "Clear Old Versions")
                              (:div :class "col-sm-5"
                                    (render-edit-field "clear-old"
                                                       (clear-old-versions-p import)
                                                       :type :checkbox
                                                       :on-change (js-render preview
                                                                             (js-checkbox-value "clear-old")))))
                        (render-import-options import preview)
                        (alt-key-checkbox import preview)
                        (:span :id "keys-not-mapped"
                               (key-column-errors import)))
                  (:div :class "form-group col-sm-3"
                        (:label :for "description"
                                "Note")
                        (render-edit-field
                         "description"
                         (description import)
                         :type :textarea
                         :mce nil
                         :on-change (js-render preview
                                               (js-value "description"))))
                  (:div :class "form-horizontal col-sm-3"
                   (:div :class "form-horizontal form-group"
                         (:label :class "control-label"
                                 "Apply saved options")
                         (render-edit-field "use-saved-substs"
                                            (use-saved-substs import)
                                            :type :checkbox
                                            :on-change (js-render preview
                                                                  (js-checkbox-value "use-saved-substs"))))))
            (render preview-grid)))
    preview))

(defun alt-key-checkbox (import preview)
  (let ((descriptions (assoc (import-class import)
                             *import-slot-descriptions*))
        alt-keys)
    (loop for (slot . options) in (cdr descriptions)
          when (getf options :alternative-key)
          do
          (push slot alt-keys))
    (when alt-keys
      (with-html
        (loop for slot in alt-keys
              for name = (frmt "alt-key-~a" slot)
              do
              (htm
               (:div :class "form-group"
                     (:label :class "col-sm-4 control-label"
                             (fmt "Use <em>~a</em> as a key"
                                  (pretty-field-name slot)))
                     (:div :class "col-sm-5"
                           (render-edit-field name
                                              (find slot (alt-keys import) :key #'car)
                                              :type :checkbox
                                              :on-change
                                              (js-render preview
                                                         (js-checkbox-value name)))))))))))
;;;

(defclass column-map-modal (ajax-widget)
  ((preview :initarg :preview
            :initform nil
            :accessor preview)))

(defun map-body (column-id import modal)
  (let ((column (nth column-id (columns import))))
    (with-html-no-indent
      (:div :class "modal-content"
        (:div :class "modal-header"
          (:h4 :class "modal-title"
               :id "data-fix-label"
               "Map Column"))
        (:div :class "modal-body"
          (:div :class "row"
            (:ul :id "sortable1"
                 :class "connectedSortable"
                 (:li :class "separator"
                      (:input :type "text" :placeholder "Enter text"
                              :disabled "t"))
                 (loop for field across (csv-fields import)
                       unless (loop for clause in (column-map column)
                                    for kind := (car clause)
                                    for value := (cdr clause)
                                    thereis (and (eq kind :field)
                                                 (equal value field)))
                       do
                       (htm (:li (esc field)))))
            (:span :class "lead"
                   "Drag fields from above to form new values.")
            (:ul :id "sortable2"
                 :class "connectedSortable"
                 (loop for clause in (column-map column)
                       for kind := (car clause)
                       for value := (cdr clause)
                       do
                       (if (eq kind :text)
                           (htm (:li :class "separator"
                                     (:input :type "text" :placeholder "Enter text"
                                             :value (escape value))))
                           (htm (:li :class "separator"
                                     (esc value))))))))
        (defer-js "$('#sortable1, #sortable2').sortable({connectWith: '.connectedSortable', 'helper': 'clone'}).disableSelection()")
        (defer-js "$('#sortable1').on('sortremove', function (event, ui)
                                                       {if (ui.item.attr('class') == 'separator') $('#sortable1').prepend($(ui.item).clone());})") ;
        (defer-js "$('#sortable1').on('sortreceive', function (event, ui)
{if (ui.item.attr('class') == 'separator') $(ui.item).remove(); })")
        (defer-js "$('#sortable2').on('sortreceive', function (event, ui)
{if (ui.item.attr('class') == 'separator') $('input', ui.item).prop('disabled', false); })")
        (:div :class "modal-footer"
          (htm
           (:button :type "button"
                    :class "btn btn-d"
                    :onclick
                    (frmt
                     "{$(\"#vmap2\").modal(\"hide\");~a;}"
                     (js-apply modal
                               (js-pair "column" column-id)
                               (js-pair "save" "t")
                               "sortableValues(\"sortable2\")"))
                    "Ok"))
          (:button :type "button"
                   :class "btn btn-default"
                   :data-dismiss "modal"
                   "Cancel"))))))

(defun make-column-map ()
  (loop for (key . value) in (post-parameters*)
        when
        (cond ((equal key "text")
               (cons :text value))
              ((equal key "field")
               (cons :field value)))
        collect it))

(defun set-column-map (column-id import)
  (let ((column (nth column-id (columns import))))
    (setf (column-map column) (make-column-map)
          (slot-value column 'printer) nil)
    (apply-saved-substs import column)
    (defer-js (frmt "$('#map-column-~a').attr('data-original-title', ~s)"
                    column-id (present-map column)))))

(defmethod render ((modal column-map-modal) &key)
  (let ((import (current-import (grid (preview modal)))))
    (with-parameters (save column)
      (setf column (ensure-parse-integer column))
      (cond (save
             (set-column-map column import)
             (update-import (preview modal)))
            (column
             (with-html
               (:div :class "modal"
                     :id "vmap2"
                     :role "dialog"
                     :aria-labelledby "vmap-label"
                     (:div :class "modal-dialog modal-lg"
                           (map-body column import modal))))
             (defer-js "$('#vmap2').modal()"))))))

(defun convert-old-import (old)
  (let ((new
          (make-instance 'new-sfx-import
                         :top-level t
                         :stamp-date (stamp-date old)
                         :effective-date (effective-date old)
                         :user (user old)
                         :entity (entity old)
                         :import-type (import-type old)
                         :import-stamp-date (import-stamp-date old)
                         :clear-old-versions-p (clear-old-versions-p old)
                         :original-file-name (original-file-name old)
                         :file-name (file-name old)
                         :description (description old)
                         :status (status old)
                         :scripts (scripts old)
                         :errors (and (import-errors old) t))))
    (unless (eql (status old) :rejected)
      (let ((header (parse-csv
                     (merge-pathnames (file-name new)
                                      *tmp-directory*)
                    )))
        (setf (csv-fields new) header)
        (when (import-type new)
          (setf (matches new)
                (determine-import-type header)))
        (setf (columns new)
              (loop for field across header
                    collect
                    (make-instance 'real-column
                                   :column-map field
                                   :sfx-import new
                                   :matches (assoc-value field
                                                         (cadar (matches new))))))))
    (loop for mapping in (mapping old)
          unless (empty-p (file-column mapping))
          do
          (let ((column (find (file-column mapping)
                              (columns new)
                              :key #'column-map :test #'string-equal)))
            (when column
             (etypecase mapping
               (sfx-import-mapping
                (setf (slot column)
                      (if (symbolp (import-slot mapping))
                          (import-slot mapping)
                          (find-symbol (import-slot mapping) :xpacks))))
               (sfx-import-mapping-rename
                (setf (slot column)
                      (find-symbol (frmt "IMPORT-OLD-~a" (import-slot mapping))
                                   :xpacks))
                (assert (slot column))
                (setf (old-slot column)
                      (if (symbolp (import-slot mapping))
                          (import-slot mapping)
                          (find-symbol (import-slot mapping) :xpacks))))))))
    (loop for subst in (substs old)
          unless (or (not subst)
                     (empty-p (field subst)))
          do
          (let ((column (find (field subst)
                              (columns new) :key #'slot :test #'string-equal)))
            (when column
              (push (cons (what subst) (with subst))
                    (substs column)))))
    new))

(defun convert-old-imports ()
  (let ((docs (get-collection-docs 'sfx-import)))
    (loop for i below (length docs)
          for import = (aref docs i)
          when (typep import 'sfx-import)
          do
          (let ((new (convert-old-import import)))
            (remove-doc import "Converted to the new import format.")
            (persist new)
            ;; Give it a chance to get GCed
            (setf (aref docs i) nil)))))
