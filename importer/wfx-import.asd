(defsystem wfx-import
  :depends-on (alexandria
               wfx
               wfx-base
               hunchentoot
               cl-who
               cl-ppcre
               csv-parser
               cl-json
               xdb2
               parse-number
               wfx-script
               cl-ftp)
  :serial t
  :components
  ((:file "packages")
   (:module "db"
    :serial t
    :components
    ((:file "importer")))
   (:file "csv")
   (:file "importer-meta")
   (:file "class-wrappers")
   (:file "importer-substitution")
   (:file "importer-script")
   (:file "importer-templates")
   (:file "importer")
   (:file "importer-replacements")
   (:file "new-importer")
   (:file "ftp")
   (:module "pages"
    :serial t
    :components
    ((:file "importer")))))
