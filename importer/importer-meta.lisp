(in-package :cl-sfx)

(defvar *current-import* nil)
(defvar *current-doc* nil)
(defvar *slot*)
(defvar *import-stamp-date*)

(define-condition import-condition (simple-condition)
  ((value :initarg :value
          :initform nil
          :accessor value)
   (slot :initarg :slot
         :initform nil
         :accessor slot)))

(define-condition import-error (import-condition)
  ())

(define-condition import-merged-errors (import-condition)
  ())

(define-condition import-warning (import-condition)
  ())

(defun import-error (value control-string &rest args)
  (signal 'import-error
          :value value
          :slot *slot*
          :format-control control-string
          :format-arguments args))

(defun import-slot-error (slot value control-string &rest args)
  (signal 'import-error
          :value value
          :slot slot
          :format-control control-string
          :format-arguments args))

(defun import-warn (value control-string &rest args)
  (signal 'import-warning
          :value value
          :slot *slot*
          :format-control control-string
          :format-arguments args))

(defvar *import-review-urls*)



(defun parse-yes-or-no (string)
  (or (cadr (assoc string '(("Yes" "Yes")
                            ("Y" "Yes")
                            ("No" "No")
                            ("N" "No"))
                   :test #'equalp))
      (import-error string "Invalid value, should be one of Yes, Y, N, or, No")))

(defun import-parse-number-no-error (string)
  (handler-case (parse-number:parse-number string)
    (error () (import-error string "Not a number"))))

(defun import-parse-date (string)
  (or (parse-date string)
      (import-error string
                    "Missing, bad format or invalid date, should be DD MMM YYYY")))

(defun merge-slots (new old slots)
  (loop for slot in slots
        when (or (not (slot-boundp new slot))
                 (not (slot-value new slot)))
        do
        (setf (slot-value new slot)
              (and (slot-boundp old slot)
                   (slot-value old slot)))))

(defun import-merge (new-doc old-doc
                     slot
                     key-accessor
                     slots-to-merge
                     &optional additional-merges)
  (cond ((not (slot-value old-doc slot)))
        ((not (slot-value new-doc slot))
         (setf (slot-value new-doc slot)
               (slot-value old-doc slot)))
        (t
         (loop with new-subdocs = (slot-value new-doc slot)
               for old in (slot-value old-doc slot)
               for new = (and old
                              (find (funcall key-accessor old)
                                    new-subdocs
                                    :test #'equalp
                                    :key key-accessor))
               if new
               do
               (alexandria:removef new-subdocs new)
               (loop for merge in additional-merges
                     do
                     (funcall merge new old))
               (merge-slots new old slots-to-merge)
               and
               collect new into merged
               else
               collect old into merged
               finally
               (setf (slot-value new-doc slot)
                     (append new-subdocs merged))))))


(defvar *import-indirect-slots* nil)

(defvar *import-indirect-merging* nil)

(defun check-percentage (x)
  (or (not (numberp x))
      (<= 0 x 135)
      (import-error x "Percents should be between 0 and 135")))

(defun date-not-in-the-future (date)
  (when (and (numberp date)
             (> date (get-universal-time)))
    (import-error (format-date date)
                  "Date cannot be in the future")))

(defun import-parse-money (string)
  (or (parse-money string)
      (import-error string "Not a number")))



(defun import-slot-names (import-type &optional import-class)
  (or (find-allsorts-for-validation
       (frmt "Import Type - ~a" import-type))
      (mapcar #'car
              (cdr (assoc (or import-class
                              (assoc-value import-type *import-classes*))
                          *import-slot-descriptions*)))))

(defun import-slot-description (import slot)
  (cdr (assoc slot (cdr (assoc (import-class import)
                               *import-slot-descriptions*))
              :test #'string-equal)))

(defun key-slots (class)
  (loop for (slot . args) in (cdr (assoc class
                                         *import-slot-descriptions*))
        when (getf args :key)
        collect slot))

(defun virtual-key-slots (class)
  (loop for (slot . args) in (cdr (assoc class
                                         *import-slot-descriptions*))
        when (or (and (getf args :key)
                      (getf args :show-key t))
                 (getf args :show-key))
        collect slot))

(defun main-key-slot (class)
  (loop for (slot . args) in (cdr (assoc class
                                         *import-slot-descriptions*))
        when (getf args :main-key)
        return slot))

(defun make-cache-insert (slots)
  (labels ((make-slot-lookups (slots)
             `(let* ((slot-value (or (slot-value object ',(car slots))
                                     (return)))
                     ,@(and (cdr slots)
                            `((hash (or (gethash slot-value hash)
                                        (setf (gethash slot-value hash)
                                              (make-hash-table :test 'equalp)))))))
                ,(if (cdr slots)
                     (make-slot-lookups (cdr slots))
                     `(setf (gethash slot-value hash) object)))))
    (compile nil `(lambda (object hash)
                    (block nil
                      ,(make-slot-lookups slots))))))

(defvar *import-class-collections*)


;;TODO: Should we just remove slots as a parameter?
(defun make-cache (insert class-name slots)
  (declare (ignore slots))
  (let ((objects (funcall (getf *import-class-collections* class-name)))
        (top-hash (make-hash-table :test 'equalp)))
    (loop for object across objects
          do (funcall insert object top-hash))
    top-hash))

(defun make-cache-lookup (slots)
  (labels ((make-slot-lookups (slots)
             `(let* ((slot-value (slot-value object ',(car slots)))
                     (hash (gethash slot-value hash)))
                (and hash ,@(and (cdr slots)
                                 `(,(make-slot-lookups (cdr slots))))))))
    (compile nil `(lambda (object hash)
                    ,(make-slot-lookups slots)))))

(defvar *import-descriptions*)

(defvar *import-plain-descriptions*)

(defun make-import-key-ckecker (class)
  (compile
   nil
   `(lambda (object)
      (and ,@(loop for slot in (key-slots class)
                   collect
                   `(slot-value object ',slot))
           t))))

(defvar *import-key-checkers*)
(defvar *import-options* nil)
(defvar *import-combined-substs* nil)
